/* global process */
var webpack = require('webpack');
var config = require('./webpack.common.config.js');
var Version = require('./version');
var publishAssets = process.env.PUBLISH_ASSETS;
var ExtractTextPlugin = require('extract-text-webpack-plugin');

// Linting failures should cause webpack to stop building and return an exit code
config.eslint.failOnError = true;

// When errors occur (e.g. when linting), bail out and don't continue.
// This is also the only way to get a failure exit code from the process.
config.bail = true;

config.output.filename = '[name].min.js';
config.output.libraryTarget = 'umd';
config.output.library = '[name]';

//configure the no dependencies build
var cloneDeep = require('lodash-compat/lang/cloneDeep');
var noDependencyConfig = cloneDeep(config);

noDependencyConfig.output.filename = '[name]-no-deps.min.js';
noDependencyConfig.externals = ['jquery', 'jquery-ui', 'backbone'];

if (publishAssets) {
    // This was pulled out into `versionMaker` but it would hang in jenkins
    // I have no idea why, but because of this problem we keep the git tagging in here for now
    var version = new Version();
    const execSync = require('child_process').execSync;

    try {
        execSync(`git tag ${version.nextVersion()}`);
        execSync(`git push origin ${version.nextVersion()}`);
    } catch (e) {
        console.log('Failed to tag version to commit', e);
        process.exit(1);
    }
}

config.module.loaders.push({ test: /\.scss$/i, loader: ExtractTextPlugin.extract(['css?sourceMap', 'postcss', 'sass?sourceMap']) });
config.plugins.push(new ExtractTextPlugin('[name].css'));
config.plugins.push(new webpack.DefinePlugin({
    ADD_TO_GLOBAL: false
}));

noDependencyConfig.module.loaders.push({ test: /\.scss$/i, loader: ExtractTextPlugin.extract(['css?sourceMap', 'postcss', 'sass?sourceMap']) });
noDependencyConfig.plugins.push(new ExtractTextPlugin('[name]-no-dep.css'));
config.plugins.push(new webpack.DefinePlugin({
    ADD_TO_GLOBAL: false
}));

module.exports = [config, noDependencyConfig];
