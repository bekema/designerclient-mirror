const $ = require('jquery');

const initTabs = function() {
    const $tabs = $('.js-tab');
    const $contentPanels = $('.js-content-panel');
    const ACTIVE_CLASS = 'is-active';

    function getCurrentTabName() {
        return $(`.js-tab.${ACTIVE_CLASS}`).data('tab');
    }

    function selectTab(tabName) {
        if (getCurrentTabName() !== tabName) {
            const $tab = $(`.js-tab[data-tab=${tabName}]`);

            $tabs.removeClass(ACTIVE_CLASS);
            $tab.addClass(ACTIVE_CLASS);

            $contentPanels.removeClass(ACTIVE_CLASS);
            $(`.js-content-panel-${tabName}`).addClass(ACTIVE_CLASS);
        }
    }

    $tabs.on('click', function(e) {
        e.preventDefault();
        selectTab($(this).data('tab'));
    });

    window.dcl.eventBus.on(window.dcl.eventBus.events.uploadStarted, () => {
        selectTab('images');
    });
};

module.exports = {
    initTabs: initTabs
};
