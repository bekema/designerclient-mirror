const dclUITabs = require('./dcl-ui-tabs');

const exports = {
    initTabs: dclUITabs.initTabs
};

/* global ADD_TO_GLOBAL */
/*
 * Using the webpack hot middleware doesn't work correctly with UMD targets,
 * so we have to add to the global scope manually
 */
if (ADD_TO_GLOBAL) {
    window.dclUI = exports;
}

module.exports = exports;
