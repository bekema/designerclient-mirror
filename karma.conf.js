var webpackConfig = require('./webpack.development.config');

// Add the rewire plugin to the babel loader
var babelLoader = webpackConfig.module.loaders.find(function(loader) { return loader.loader === 'babel'; });
babelLoader.query.plugins.push('babel-plugin-rewire');

webpackConfig.module.postLoaders = [{
    test: /\.js$/,
    exclude: /node_modules|test/,
    loader: 'istanbul-instrumenter'
}];

module.exports = function(config) {
    config.set({

        files: [
            // all files ending in 'test.js' are loaded for testing
            'test/**/*.test.js'
        ],

        frameworks: ['jasmine'],

        preprocessors: {
            'test/**/*.test.js': ['webpack'],
            'app/**/*.js': ['webpack']
        },

        reporters: ['spec', 'coverage'],

        coverageReporter: {
            dir: 'build/coverage/',
            reporters: [
              { type: 'html', subdir: 'report-html' },
              { type: 'lcov', subdir: 'report-lcov' }
              //{ type: 'cobertura', subdir: '.', file: 'cobertura.txt' }
            ]
        },

        webpack: webpackConfig,

        webpackMiddleware: {
            quiet: true
        },

        plugins: [
            require('karma-webpack'),
            require('karma-jasmine'),
            require('karma-coverage'),
            require('karma-phantomjs-launcher'),
            require('karma-spec-reporter')
        ],

        browsers: ['PhantomJS']
    });
};
