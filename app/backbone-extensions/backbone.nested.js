const Backbone = require('backbone');
const _ = require('underscore');

const NO_MATCH = -1;

const requireModule = (moduleName) => {
    if (moduleName.toLowerCase().indexOf('viewmodel') !== NO_MATCH) {
        return require('viewModels/' + moduleName);
    }

    return require('models/' + moduleName);
};

/*
* NESTED COLLECTION
*
* A nested collection should be created by a nested model. Once that happens, any model
* added to the collection (via add() or reset()) will be initialized to whatever instance
* is specified by the Model attribute in its JSON. It will also be given a reference to
* a parent.
*/
/* eslint backbone/collection-model: 0, backbone/no-constructor: 0, backbone/model-defaults: 0 */
Backbone.NestedCollection = Backbone.Collection.extend({

    constructor: function() {
        Backbone.Collection.apply(this, arguments);
        // Trigger a global event to notify that this collection has been created.
        Backbone.Events.triggerGlobal('register:collection', this);
    },

    // Override the collections _prepareModel method - this is what is internally called
    // by add(), and it should deal with both JSON and already created Models.
    _prepareModel: function(model, options) {
        options = options || { };
        options.parent = this.parent;

        if (!(model instanceof Backbone.Model)) {
            const attrs = model;
            const moduleName = attrs.module;
            const Constructor = moduleName ? requireModule(moduleName) : this.model;
            model = new Constructor(attrs, options);
        }

        if (this.parent) {
            model.parent = this.parent;
        }

        model.collection = this;

        return model;
    }
});

/*
* NESTED MODEL
*
* Models that extend this 'class' should call this initialize function. It will loop through attributes
* and will convert arrays into collections (if the collections have already been created), and convert
* objects into models (if the model JSON specifies a Model attribute).
*/
Backbone.NestedModel = Backbone.Model.extend({

    initialize: function(attrs, options) {
        if (options && options.parent) {
            this.parent = options.parent;
        }

        /* eslint backbone/no-model-attributes: 0 */  //see note below
        for (const attrKey in this.attributes) {
            const attrValue = this.attributes[attrKey];
            if (Array.isArray(attrValue) && this[attrKey]) {
                const collection = this[attrKey];
                collection.parent = this;
                collection.name = attrKey;
                collection.reset(attrValue);

                // Set the attribute for the collection equal to the collection itself - this
                // is necessary for serialization. Do this directly instead of through set, because
                // set will kick off a lot of events that we don't want to happen, and it'll also
                // add things to the previous attributes collection, which we don't want now.
                this.attributes[attrKey] = collection;
            } else if (_.isObject(attrValue) && attrValue.module) {
                this[attrKey] = new (requireModule(attrValue.module))(attrValue, { parent: this });
                this.attributes[attrKey] = this[attrKey];
                this[attrKey].parent = this;
            }
        }
    },

    constructor: function() {
        Backbone.Model.apply(this, arguments);
        Backbone.Events.triggerGlobal('register:model', this);
    },

    reset: function(updatedJson) {
        // Automatically reset any nested collections we've defined. The _prepareModel override in backbone-extensions.js
        // ensures that everything gets hooked up correctly.
        _(updatedJson).each(
            function(value, key) {
                if (this[key] instanceof Backbone.NestedCollection) {
                    this[key].reset(value, { silent: false });
                    delete updatedJson[key];
                }
            },
            this);

        // This is silent so that we don't kick off another update just because the server sent back new values.
        this.set(updatedJson, { silent: false });
        this.trigger('reset', this);
    }
});