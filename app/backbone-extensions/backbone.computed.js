// **ComputedAttributeMixin**

// This allows you to set an attribute to have a function value and then use
// get to retrieve the evaluated function result, rather than just the function. You
// cannot specify a computed setter.

// When one of the bindings changes, a change event will automatically be triggered
// for this property when the value changes. Generally, the cached value of the getter
// will be return, unless the `nocache` option is specified.

// Destroying this model will remove all of its bindings.

// Usage:

//     var MyModel = Backbone.Model.extend({
//
//         initialize : function() {
//             this.createComputedAttribute({
//                     attr: '...',
//                     get: function() { ... },
//                     bindings: [
//                         { attributes : [ ... ] },
//                         { model : ..., attribute : ... }
//                     ],
//                     nocache: true // optional
//                 });
//         }
//     });

const _ = require('underscore');
const Backbone = require('backbone');

let _computedChangeQueue = [];
let _atomic = false;

const _flushComputedChangeQueue = function() {
    if (_atomic) {
        return;
    }

    // This is a while loop because in theory we could trigger a change event that causes
    // another property to get set that will then push more models into the queue.
    while (_computedChangeQueue.length) {
        const model = _computedChangeQueue.pop();
        let changed = false;
        model.changed = {}; // TODO Circular test!
        for (const computedAttr in model._changedComputedAttributes) {
            const oldVal = model._changedComputedAttributes[computedAttr];
            delete model._changedComputedAttributes[computedAttr];
            const newVal = model.get(computedAttr);
            if (newVal !== oldVal) {

                //backbone resets the list of previous attributes after change to the static attributes, so let's keep our own list
                if (!model._previousComputedAttributes) {
                    model._previousComputedAttributes = {};
                }

                model._previousComputedAttributes[computedAttr] = oldVal;
                model.changed[computedAttr] = newVal;
                model.trigger('change:' + computedAttr, model);
                changed = true;
            }
        }
        if (changed) {
            model.trigger('change', model);
        }
    }

    _computedChangeQueue = [];
};

// This one time setup method will override Backbone's setter. This is only called
// once we're sure that computed properties are in use.
const setupComputedAttributes = _.once(function() {
    const _set = Backbone.Model.prototype.set;
    Backbone.Model.prototype.set = function() {
        let i, j, k, l;
        const recomputing = this._recomputing;
        this._recomputing = true;

        // Actually set this attribute, allowing all change events to fire.
        const retValue = _set.apply(this, arguments);

        // If any models are dependent on a changed attribute, notify them of the change. I'm not using
        // events here because I need to call this regardless of whether silent is true,
        // and I also don't want to bother with things getting piped through the event
        // infrastructure.
        if (this._dependsOnMe) {
            const keys = Object.keys(this.changedAttributes() || {});
            for (i = 0, l = keys.length; i < l; i++) {
                const dependsOnMe = this._dependsOnMe[keys[i]];
                if (dependsOnMe) {
                    for (j = 0, k = dependsOnMe.length; j < k; j++) {
                        const dep = dependsOnMe[j];
                        dep.model._changeDependency(dep.attr);
                    }
                }
            }
        }

        // Once per call stack, we want to clear out the queue of possibly changed computed attributes.
        // We will recompute + cache them, and trigger change events if they've changed.
        if (!recomputing) {
            _flushComputedChangeQueue();
            this._recomputing = false;
        }

        return retValue;
    };
});

Backbone.atomic =  function(fn, context) {
    if (_atomic) {
        throw new Error("Computed attributes are already in atomic mode (they won't be updated until the first function that called this is complete).");
    }
    _atomic = true;
    try {
        fn.apply(context || this);
    } catch (e) {
        throw e;
    // Don't leave the application in atomic mode, just because the function failed.
    } finally {
        _atomic = false;
    }
    _flushComputedChangeQueue();
};

Backbone.ComputedAttributeMixin = {

    _changeDependency: function(attr) {
        if (!_.contains(_computedChangeQueue, this)) {
            _computedChangeQueue.push(this);
        }

        // Set a flag so that the value is recomputed next time get() is called.
        this._dirty[attr] = true;

        // We need to keep track of the current value of each attribute so that we know
        // whether or not to trigger change events once we enter the recomputing state.
        if (!this._changedComputedAttributes) {
            this._changedComputedAttributes = {};
        }
        this._changedComputedAttributes[attr] = this._cachedAttributes[attr];

        // Recursively see if any other models are depending on my affected attributes.
        if (this._dependsOnMe && this._dependsOnMe[attr]) {
            const dependsOnMe = this._dependsOnMe[attr];
            for (let j = 0, k = dependsOnMe.length; j < k; j++) {
                const dep = dependsOnMe[j];
                dep.model._changeDependency(dep.attr);
            }
        }
    },

    // Override `get` so that we call the computed function for this attribute.
    get: function(attr) {
        const computed = this._computed && this._computed[attr];
        if (computed) {
            if (this._dirty[attr] || computed.nocache) {
                this._cachedAttributes[attr] = computed.get.call(this);
                delete this._dirty[attr];
            }
            return this._cachedAttributes[attr];
        }
        return this.attributes[attr];
    },

    previous: function(attr) {
        if (!attr) {
            return null;
        }

        const computed = this._computed && this._computed[attr];
        if (computed) {
            if (!this._previousComputedAttributes) {
                return null;
            }

            return this._previousComputedAttributes[attr];
        }

        if (!this._previousAttributes) {
            return null;
        }

        return this._previousAttributes[attr];
    },

    previousAttributes: function() {
        if (!this._previousAttributes && !this._previousComputedAttributes) {
            return null;
        }

        return _.extend(_.clone(this._previousAttributes) || {}, _.clone(this._previousComputedAttributes));
    },

    destroy: function() {
        Backbone.Model.prototype.destroy.apply(this, arguments);
        this.removeComputedAttributes();
    },

    // `createComputedAttributes` can be called when a model is initialized and it
    // will take all of the attributes in the model's `computed` hash, and turn
    // them into attributes.
    createComputedAttributes: function() {
        if (this.computed) {
            for (const attr in this.computed) {
                const val = this.computed[attr];
                this.createComputedAttribute({
                    attr,
                    get: val.get,
                    bindings: val.bindings.call(this),
                    nocache: val.nocache
                });
            }
        }

        if (!this._computed) {
            this._computed = {};
        }
    },

    createComputedAttribute: function(options) {
        setupComputedAttributes();

        if (!this._dependencies) {
            this._dependencies = {};
            this._dirty = {};
            this._cachedAttributes = {};
        }

        const computedAttr = options.attr;

        // Mark as dirty the first time so that the value is calculated no matter what.
        this._dirty[computedAttr] = true;

        // There are lots of different ways of specifying bindings.
        for (let i = 0, l = options.bindings.length; i < l; i++) {
            const binding = options.bindings[i];
            let attrs, model;

            if (_.isString(binding)) {
                attrs = [binding];
                model = this;
            } else if (Array.isArray(binding)) {
                attrs = binding;
                model = this;
            } else if (binding.attribute) {
                attrs = [binding.attribute];
                model = binding.model || this;
            } else if (binding.attributes) {
                attrs = binding.attributes;
                model = binding.model || this;
            } else {
                throw new Error('Attribute binding not defined correctly');
            }

            // This dependencies collection is just for storing references to the models that I depend on,
            // which we need in order to clean up correctly when this model is destroyed.
            let deps = this._dependencies[model.cid];
            if (!deps) {
                deps = this._dependencies[model.cid] = { model: model, attrs: {} };
            }

            // Add a two way link for each model - one to the dependent model so that if its attributes
            // are set, this will be notified. And one in the other direction so that these dependencies
            // can be removed when this is destroyed.
            for (let j = 0; j < attrs.length; j++) {
                const attr = attrs[j];
                if (attr === computedAttr && model === this) {
                    throw new Error('Cannot create a computed attribute that depends on itself');
                }

                if (!model._dependsOnMe) {
                    model._dependsOnMe = {};
                }
                if (!model._dependsOnMe[attr]) {
                    model._dependsOnMe[attr] = [];
                }
                model._dependsOnMe[attr].push({ model: this, attr: computedAttr });
                deps.attrs[attr] = true;
            }
        }

        // Store our getter and bindings privately so that we can access them later.
        if (!this._computed) {
            this._computed = {};
        }
        this._computed[computedAttr] = { get: options.get, bindings: options.bindings, nocache: options.nocache };
    },

    // When a model is destroyed, we can use its list of dependencies to find all of the other
    // models that it is bound to, and we can remove references from those models so that it
    // can be thoroughly cleaned up.
    removeComputedAttributes: function() {
        _.each(this._dependencies, function(dep) {
            _.each(dep.attrs, function(val, attr) {
                const dependsOnMe = dep.model._dependsOnMe[attr];
                if (dependsOnMe) {
                    const retain = dep.model._dependsOnMe[attr] = [];
                    for (let j = 0, k = dependsOnMe.length; j < k; j++) {
                        if (dependsOnMe[j].model !== this) {
                            retain.push(dependsOnMe[j]);
                        }
                    }
                    if (!retain.length) {
                        delete dep.model._dependsOnMe[attr];
                    }
                }
            }, this);
        }, this);
    }
};

module.exports = Backbone.ComputedAttributeMixin;
