const Backbone = require('backbone');
const _ = require('underscore');

/*
* GLOBAL EVENTS
*
* Add the ability to trigger/bind to global events to the Backbone.Events object, which in turn will give
* any model or view the ability to trigger/bind to global events.
*/
const globalEventFunctions = {
    triggerGlobal: function() {
        Backbone.trigger(...arguments);
    },

    // The Backbone object has the event functions applied natively, so it's intended to be used as the global event object.
    onGlobal: function() {
        Backbone.on(...arguments);
    },

    offGlobal: function() {
        Backbone.off(...arguments);
    },

    onceGlobal: function() {
        Backbone.once(...arguments);
    },

    // Since listenTo() is like the inverted event binding version of on(), we have to have `this` listen to the global `Backbone` object.
    listenToGlobal: function() {
        const args = [...arguments];
        args.unshift(Backbone);
        this.listenTo(...args);
    },

    stopListeningGlobal: function() {
        const args = [...arguments];
        args.unshift(Backbone);
        this.stopListening(...args);
    }
};

// Now all models, views, collections and anything else using Backbone.Events will have the global events.
_.extend(Backbone.Model.prototype, globalEventFunctions);
_.extend(Backbone.Collection.prototype, globalEventFunctions);
_.extend(Backbone.View.prototype, globalEventFunctions);
_.extend(Backbone.Events, globalEventFunctions);

/*
* toJSON()
*
* In addition to serializing all of the
* attributes of a model, also serialize any value that has a toJSON function of its own.
*
* For an explanation of why Backbone doesn't do this by default:
* - https://github.com/jashkenas/backbone/issues/483
* - http://backbonejs.org/#FAQ-nested
*/
Backbone.Model.prototype.toJSON = function() {
    const json = _.clone(this.attributes);
    _.each(json, (value, name) => {
        if (value && _.isFunction(value.toJSON)) {
            json[name] = value.toJSON();
        }
    });
    return json;
};
