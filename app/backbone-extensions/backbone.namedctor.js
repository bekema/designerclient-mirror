const _ = require('underscore');
const Backbone = require('backbone');

// http://stackoverflow.com/questions/14866014/debugging-javascript-backbone-and-marionette
const createNamedConstructor = function(name, constructor) {
    /* eslint no-new-func: 0 */
    const fn = new Function('constructor', 'return function ' + name + '() { /* dynamically created wrapper function */ constructor.apply(this, arguments); };');
    return fn(constructor);
};

const originalExtend = Backbone.View.extend;
const nameProp = '__name__';
const newExtend = function(protoProps, classProps) {
    if ((protoProps && protoProps[nameProp]) || this.name) {
        const name = protoProps[nameProp] || this.name;
        // wrap constructor from protoProps if supplied or 'this' (the function we are extending)
        const constructor = protoProps.hasOwnProperty('constructor') ? protoProps.constructor : this;
        protoProps = _.extend(protoProps, {
            constructor: createNamedConstructor(name, constructor)
        });
    }
    return originalExtend.call(this, protoProps, classProps);
};

/*
* This is a really useful method for creating class hierarchies. I'm promoting it to
* the Backbone object so that we don't get confused about what we're extending.
*/
Backbone.extend = Backbone.Model.extend = Backbone.Collection.extend = Backbone.Router.extend = Backbone.View.extend = newExtend;

module.exports = Backbone;
