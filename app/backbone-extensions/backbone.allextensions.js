const Backbone = require('backbone');

require('./backbone.computed');
require('./backbone.global');
require('./backbone.nested');

// TODO: Revisit named constructors
//require('./backbone.namedctor');

// Just trigger the event - don't try to do a lot of syncing that we don't care about.
Backbone.Model.prototype.destroy = function(options) {
    this.trigger('destroy', this, this.collection, options);
};

module.exports = Backbone;