const eventBus = require('EventBus');
const events = eventBus.events;

class ZoomStrategy {
    constructor({ initialZoom, minZoom, maxZoom, zoomIncrement }) {
        this.zoomFactor = initialZoom;
        this.minZoom = minZoom;
        this.maxZoom = maxZoom;
        this.zoomIncrement = zoomIncrement;
        eventBus.on(events.zoomIn, this.zoomIn, this);
        eventBus.on(events.zoomOut, this.zoomOut, this);
        eventBus.on(events.zoomSet, this.zoomSet, this);
    }

    zoomIn() {
        this.zoomFactor = this.zoomFactor + this.zoomIncrement;
        if (this.zoomFactor >= this.maxZoom) {
            this.zoomFactor = this.maxZoom;
        }
        eventBus.trigger(events.zoom, this.zoomFactor);
    }

    zoomOut() {
        this.zoomFactor = this.zoomFactor - this.zoomIncrement;
        if (this.zoomFactor <= this.minZoom) {
            this.zoomFactor = this.minZoom;
        }
        eventBus.trigger(events.zoom, this.zoomFactor);
    }

    zoomSet(zoomFactor) {
        this.zoomFactor = zoomFactor;
        if (this.zoomFactor <= this.minZoom) {
            this.zoomFactor = this.minZoom;
        }
        if (this.zoomFactor >= this.maxZoom) {
            this.zoomFactor = this.maxZoom;
        }
        eventBus.trigger(events.zoom, this.zoomFactor);
    }
}

module.exports = ZoomStrategy;
