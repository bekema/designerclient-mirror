// this merges objects recursively
const $ = require('jquery');
const config = require('publicApi/configuration/ui/widgets');
const contextualToolbarConfig = require('publicApi/configuration/ui/widgets/contextualToolbar');
const ZoomView = require('ui/widgets/ZoomView');
const CanvasHistoryActionsView = require('ui/widgets/CanvasHistoryActionsView');
const OrientationChangeView = require('ui/widgets/OrientationChangeView');
const UploadListView = require('ui/widgets/upload/UploadListView');
const UploadButtonView = require('ui/widgets/upload/UploadButtonView');
const AddTextFieldView = require('ui/widgets/text/AddTextFieldView');
const EditTextFieldsView = require('ui/widgets/text/EditTextFieldsView');
const ChangeScenesView = require('ui/widgets/changeScene/ChangeScenesView');
const AddShapeView = require('ui/widgets/shapes/AddShapeView');
const GeneratePdfView = require('ui/widgets/GeneratePdfView');
const SaveDocumentView = require('ui/widgets/saveDocumentView');
const PreviewDocumentView = require('ui/widgets/PreviewDocumentView');
const ContextualToolbarView = require('ui/widgets/ContextualToolbar');
const FeatureManager = require('FeatureManager');
const ValidationOverlayView = require('ui/widgets/validations/ValidationOverlayView');

const viewDictionary = {
    addShape: AddShapeView,
    addText: AddTextFieldView,
    changeScene: ChangeScenesView,
    contextualToolbar: ContextualToolbarView,
    validationOverlay: ValidationOverlayView,
    editText: EditTextFieldsView,
    generatePdf: GeneratePdfView,
    orientation: OrientationChangeView,
    uploadList: UploadListView,
    uploadButton: UploadButtonView,
    zoom: ZoomView,
    previewDocument: PreviewDocumentView,
    canvasHistory: CanvasHistoryActionsView,
    saveDocument: SaveDocumentView
};

const buildWidgets = function() {
    // add the separate contextual toolbar config back into the config object
    config.contextualToolbar = contextualToolbarConfig;

    // create all the views that are enabled
    Object.keys(config).forEach(key => {
        const uiOptions = config[key];
        if (uiOptions.enabled) {
            if (!FeatureManager[key] || (FeatureManager[key] && FeatureManager[key].enabled)) {
                const view = new viewDictionary[key](uiOptions);
                $(uiOptions.containerElement).append(view.render().el);
            }
        }
    });
};

module.exports = { buildWidgets };
