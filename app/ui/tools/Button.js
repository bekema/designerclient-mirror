const _ = require('underscore');
const Tool = require('ui/tools/Tool');
const toolbarButtonTemplate = require('ui/tools/templates/buttons/ToolbarButton.tmp');
const localization = require('Localization');
const eventBus = require('EventBus');

module.exports = Tool.extend({
    //TODO make the dcl-button class more general (messes with everything if I use it here)
    className: `${Tool.prototype.className} dcl-button--toolbar`,

    events: function() {
        return _.extend({
            'click button': 'onClick',
            'touchstart button': 'onClick'
        }, Tool.prototype.events);
    },

    initialize: function(options) {
        this.options = _.extend({}, this.options, options);

        this.options.template = this.options.template || toolbarButtonTemplate;
    },

    onClick: function() {
        eventBus.trigger(this.options.event);
    },

    render: function() {
        this.$el.html(this.options.template({
            buttonClasses: this.options.buttonClasses,
            iconName: this.options.iconName,
            title: this.options.title ? localization.getText(`tools.buttons.${this.options.title}.title`) : '',
            text: this.options.text ? localization.getText(`tools.buttons.${this.options.title}.text`) : ''
        }));
        return this;
    }
});
