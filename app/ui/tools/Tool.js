const Backbone = require('backbone');
const selectionManager = require('SelectionManager');

module.exports = Backbone.View.extend({
    className: 'dcl-tool',

    isActive: function() {
        return true;
    },

    isCompatible: function() {
        return true;
    },

    show: function() {
        if (this.isCompatible(selectionManager)) {
            this.$el.removeClass('hidden');
        } else {
            // Even though we tried to show the tool, its not compatible, make sure it stays hidden.
            this.hide();
        }
    },

    hide: function() {
        this.$el.addClass('hidden');
    },

    isPrimaryItem: function() {
        return true;
    }
});
