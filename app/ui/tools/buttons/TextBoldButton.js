const TextDisableableToggleButton = require('ui/tools/buttons/TextDisableableToggleButton');
const buttons = require('publicApi/buttons');

const TextBoldButton = TextDisableableToggleButton.extend({
    className: `${TextDisableableToggleButton.prototype.className} dcl-button--bold`,

    options: {
        attribute: 'fontBold',
        secondaryAttribute: 'fontItalic',
        fontOptionAttribute: 'Bold',
        buttonClasses: 'dcl-text-bold-button',
        iconName: 'bold',
        title: 'bold'
    }
});

TextBoldButton.tag = buttons.bold;

module.exports = TextBoldButton;
