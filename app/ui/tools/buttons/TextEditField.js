const _ = require('underscore');
const Tool = require('ui/tools/Tool');
const textEditFieldTemplate = require('ui/tools/templates/buttons/TextEditField.tmp');
const commandDispatcher = require('CommandDispatcher');
const selectionManager = require('SelectionManager');
const buttons = require('publicApi/buttons');

const TIMEOUT_DURATION = 100;

const TextEditField = Tool.extend({
    className: `${Tool.prototype.className} dcl-edit-text`,

    events: {
        'change input': 'onTextChange',
        'keyup input': 'onTextChange',
        'paste input': 'onTextChange',
        'mouseup input': 'onTextChange',
        'keydown input': 'stopPropagation'
    },

    initialize: function() {
        this.setText = _.throttle(this.setText.bind(this), TIMEOUT_DURATION, { leading: false });
    },

    render: function() {
        this.$el.html(textEditFieldTemplate());
        this.listenTo(selectionManager, 'add remove model:change:innerHTML', this.updateText);
        this.listenTo(selectionManager, 'add', this.onAdd);
        return this;
    },

    onAdd: function() {
        this.$('input')[0].focus();
    },

    isPrimaryItem: function() {
        return false;
    },

    isCompatible: function(itemViewModels) {
        return itemViewModels.length === 1 && itemViewModels.first().get('module') === 'TextFieldViewModel';
    },

    updateText: function() {
        if (!selectionManager.length) {
            return;
        }

        const newText = selectionManager.first().model.get('innerHTML');

        if (newText === this.$('input').val()) {
            return;
        }

        this.$('input').val(newText);
    },

    onTextChange: function(e) {
        this.stopPropagation(e);
        this.setText();
    },

    setText: function() {
        commandDispatcher.changeAttributes({ viewModels: selectionManager, attributes: { innerHTML: this.$('input').val() } });
    },

    stopPropagation: function(e) {
        e.stopPropagation();
    }
});

TextEditField.tag = buttons.textEdit;

module.exports = TextEditField;
