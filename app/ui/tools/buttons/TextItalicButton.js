const DisableableTextToggleButton = require('ui/tools/buttons/TextDisableableToggleButton');
const buttons = require('publicApi/buttons');

const TextItalicButton = DisableableTextToggleButton.extend({
    className: `${DisableableTextToggleButton.prototype.className} dcl-button--italic`,

    options: {
        attribute: 'fontItalic',
        secondaryAttribute: 'fontBold',
        fontOptionAttribute: 'Italic',
        buttonClasses: 'dcl-text-italic-button',
        iconName: 'italic',
        title: 'italic'
    }
});

TextItalicButton.tag = buttons.italic;

module.exports = TextItalicButton;
