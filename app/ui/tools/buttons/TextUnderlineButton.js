const TextToggleButton = require('ui/tools/buttons/TextToggleButton');
const buttons = require('publicApi/buttons');
const designerContext = require('DesignerContext');

const TextUnderlineButton = TextToggleButton.extend({
    className: `${TextToggleButton.prototype.className} dcl-button--underline`,

    options: {
        attribute: 'fontUnderline',
        buttonClasses: 'dcl-text-underline-button',
        iconName: 'underline',
        title: 'underline'
    },

    isCompatible: function(itemViewModels) {
        return !designerContext.embroidery && TextToggleButton.prototype.isCompatible.call(this, itemViewModels);
    }

});

TextUnderlineButton.tag = buttons.underline;

module.exports = TextUnderlineButton;
