const TextToggleButton = require('ui/tools/buttons/TextToggleButton');
const buttons = require('publicApi/buttons');
const designerContext = require('DesignerContext');

const TextStrikeoutButton = TextToggleButton.extend({
    className: `${TextToggleButton.prototype.className} dcl-button--strike-out`,

    options: {
        attribute: 'fontStrikeout',
        buttonClasses: 'dcl-text-strikeout-button',
        iconName: 'strikethrough',
        title: 'strikeout'
    },

    isCompatible: function(itemViewModels) {
        return !designerContext.embroidery && TextToggleButton.prototype.isCompatible.call(this, itemViewModels);
    }
});

TextStrikeoutButton.tag = buttons.strikeout;

module.exports = TextStrikeoutButton;
