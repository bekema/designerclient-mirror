const TextToggleButton = require('ui/tools/buttons/TextToggleButton');
const selectionManager = require('SelectionManager');
const buttons = require('publicApi/buttons');

const TextRightAlignButton = TextToggleButton.extend({
    className: `${TextToggleButton.prototype.className} dcl-button--right-align`,

    options: {
        attribute: 'horizontalAlignment',
        buttonClasses: 'dcl-text-right-align-button',
        iconName: 'alignright',
        title: 'rightAlign'
    },

    isActive: function() {
        if (!selectionManager.length) {
            return false;
        }
        return selectionManager.models.every(model => { return model.model.get(this.options.attribute) === 'right'; });
    },

    getAttributes: function() {
        const obj = {};
        obj[this.options.attribute] = 'right';
        return obj;
    }

});

TextRightAlignButton.tag = buttons.rightAlign;

module.exports = TextRightAlignButton;
