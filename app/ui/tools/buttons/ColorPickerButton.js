const HoverButton = require('ui/tools/HoverButton');
const ColorPicker = require('ui/tools/color/ColorPicker');
const selectionManager = require('SelectionManager');
const buttons = require('publicApi/buttons');

const changeColor = function() {
    if (selectionManager.models.length !== 0 && this.isCompatible(selectionManager)) {
        this.$('.dcl-icon__use').css({ fill: selectionManager.models[0].model.get(this.options.colorAttribute) });
    }
};

const colorPickerButton = HoverButton.extend({
    className: `${HoverButton.prototype.className} dcl-button--color-picker`,

    options: {
        colorAttribute: 'fontColor',
        buttonClasses: 'dcl-color-picker-button',
        iconName: 'fill'
    },

    initialize: function() {
        this.options.ChildView = this.options.ChildView || ColorPicker;
        this.options.childViewOptions = this.options.childViewOptions ||
        HoverButton.prototype.initialize.apply(this, arguments);
    },

    render: function() {
        HoverButton.prototype.render.apply(this, arguments);
        this.listenTo(selectionManager, `add remove model:change:${this.options.colorAttribute}`, changeColor.bind(this));
        return this;
    },

    isCompatible: function(itemViewModels) {
        return itemViewModels.every(itemViewModel => {
            return itemViewModel.get('module') === 'TextFieldViewModel';
        });
    }
});

colorPickerButton.tag = buttons.fontColor;

module.exports = colorPickerButton;
