const Button = require('ui/tools/Button');
const buttons = require('publicApi/buttons');
const events = require('EventBus').events;

const DeleteButton = Button.extend({
    className: `${Button.prototype.className} dcl-button--delete`,

    options: {
        buttonClasses: 'dcl-text-delete-button',
        iconName: 'trash',
        event: events.deleteItem,
        title: 'delete'
    }
});

DeleteButton.tag = buttons.delete;

module.exports = DeleteButton;
