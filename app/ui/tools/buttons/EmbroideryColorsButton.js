const buttons = require('publicApi/buttons');
const Button = require('ui/tools/Button');
const events = require('publicApi/events');

const embroideryColorPickerButton = Button.extend({
    options: {
        event: events.threadSelection,
        buttonClasses: 'dcl-color-picker-button',
        iconName: 'color',
        title: 'threadcolors'
    },

    isCompatible: function(itemViewModels) {
        return itemViewModels.length === 1 &&
            itemViewModels.models[0].get('module') === 'EmbroideryImageViewModel';
    }
});

embroideryColorPickerButton.tag = buttons.embroideryColors;

module.exports = embroideryColorPickerButton;
