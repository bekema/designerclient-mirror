const TextToggleButton = require('ui/tools/buttons/TextToggleButton');
const clients = require('services/clientProvider');
const selectionManager = require('SelectionManager');
const Button = require('ui/tools/Button');

module.exports = TextToggleButton.extend({
    className: `${TextToggleButton.prototype.className} dcl-button--disableable`,

    render: function() {
        Button.prototype.render.apply(this, arguments);
        this.listenTo(selectionManager, `add remove model:change:${this.options.attribute} model:change:fontFamily`, this.updateButton);
        return this;
    },

    updateButton: function() {
        TextToggleButton.prototype.updateButton.call(this);
        this.verifyEnabled();
    },

    currentFontOption: function(fontFamily) {
        return clients.font.fontMapping[fontFamily.toLowerCase()];
    },

    enabledForFont: function() {
        let enabledForFont = true;
        if (!selectionManager.length || !this.isCompatible(selectionManager)) {
            return false;
        }

        selectionManager.models.some(model => {
            const fontOption = this.currentFontOption(model.model.get('fontFamily'));

            if (!fontOption || !fontOption.Variants[this.options.fontOptionAttribute]) {
                enabledForFont = false;
                return true;
            }

            if (!fontOption.Variants.BoldItalic && model.model.get(this.options.secondaryAttribute)) {
                enabledForFont = false;
                return true;
            }
        });

        return enabledForFont;
    },

    verifyEnabled: function() {
        const $button = this.$('button');
        const enabled = this.enabledForFont();

        $button.toggleClass('dcl-toolbar-button--disabled', !enabled);
        $button.prop('disabled', !enabled);
    }
});
