const ColorPickerButton = require('ui/tools/buttons/ColorPickerButton');
const buttons = require('publicApi/buttons');

const strokeColorPickerButton = ColorPickerButton.extend({
    options: {
        colorAttribute: 'strokeColor',
        buttonClasses: 'dcl-color-picker-button',
        iconName: 'coloroutline'
    },

    isCompatible: function(itemViewModels) {
        return itemViewModels.every(itemViewModel => itemViewModel.get('module') === 'ShapeViewModel' || itemViewModel.get('module') === 'LineViewModel');
    }
});

strokeColorPickerButton.tag = buttons.strokeColor;

module.exports = strokeColorPickerButton;
