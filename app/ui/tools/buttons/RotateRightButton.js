const Button = require('ui/tools/Button');
const commandDispatcher = require('CommandDispatcher');
const selectionManager = require('SelectionManager');
const Util = require('util/Util');
const buttons = require('publicApi/buttons');

const ROTATION = 90;
const MAXROTATION = 360;

const RotateRightButton = Button.extend({
    className: `${Button.prototype.className} dcl-button--rotate-right`,

    options: {
        buttonClasses: 'dcl-text-rotate-right-button',
        iconName: 'undo',
        title: 'rotateRight'
    },

    onClick: function() {
        const currentRotation = selectionManager.first().model.get('rotation');
        // This will rotate and snap to 90 degree increments.
        const targetRotation = Util.mod(currentRotation - (Util.mod(currentRotation, ROTATION) !== 0 ? Util.mod(currentRotation, ROTATION) : ROTATION), MAXROTATION);
        commandDispatcher.changeAttributes({ viewModels: selectionManager, attributes: { rotation: targetRotation } });
    },

    isCompatible: function(itemViewModels) {
        return itemViewModels.length === 1;
    }
});

RotateRightButton.tag = buttons.rotateRight;

module.exports = RotateRightButton;
