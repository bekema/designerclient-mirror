const Button = require('ui/tools/Button');
const commandDispatcher = require('CommandDispatcher');
const selectionManager = require('SelectionManager');
const buttons = require('publicApi/buttons');

const SendBackwardButton = Button.extend({
    className: `${Button.prototype.className} dcl-button--send-backward`,

    options: {
        buttonClasses: 'dcl-text-sendbackward-button',
        iconName: 'sendback',
        title: 'sendBackward'
    },

    render: function() {
        Button.prototype.render.apply(this, arguments);
        this.listenTo(selectionManager, `add remove model:change:zIndex`, this.updateButton);
        return this;
    },

    isCompatible: function(itemViewModels) {
        return itemViewModels.length === 1 && !itemViewModels.first().model.get('zIndexLock');
    },

    isEnabled: function() {
        if (selectionManager.length === 0) {
            return false;
        }
        const currentZIndex = selectionManager.first().model.get('zIndex');
        const zIndexes = selectionManager.first().parent.itemViewModels
            .filter(ivm => !ivm.model.get('zIndexLock'))
            .map(ivm => ivm.model.get('zIndex'));

        return currentZIndex > Math.min(...zIndexes);
    },

    onClick: function() {
        commandDispatcher.changeZIndex({ selectedItem: selectionManager.first(), action: 'sendBackward' });
    },

    updateButton: function() {
        const $button = this.$('button');
        const enabled = this.isEnabled();
        $button.toggleClass('disabled', !enabled);
        $button.prop('disabled', !enabled);
    }

});

SendBackwardButton.tag = buttons.sendBackward;

module.exports = SendBackwardButton;
