const Button = require('ui/tools/Button');
const buttons = require('publicApi/buttons');
const events = require('EventBus').events;

const CropButton = Button.extend({
    className: `${Button.prototype.className} dcl-button--crop`,

    options: {
        event: events.crop,
        buttonClasses: 'dcl-text-crop-button',
        iconName: 'crop',
        title: 'crop'
    },

    isCompatible: function(itemViewModels) {
        return itemViewModels.length === 1 &&
            ['UploadedImageViewModel', 'EmbroideryImageViewModel'].indexOf(itemViewModels.first().get('module')) > -1; //eslint-disable-line no-magic-numbers
    }
});

CropButton.tag = buttons.crop;

module.exports = CropButton;
