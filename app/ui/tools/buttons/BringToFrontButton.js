const Button = require('ui/tools/Button');
const commandDispatcher = require('CommandDispatcher');
const selectionManager = require('SelectionManager');
const buttons = require('publicApi/buttons');

const BringForwardButton = Button.extend({
    className: `${Button.prototype.className} dcl-button--bring-to-front`,

    options: {
        buttonClasses: 'dcl-text-bringtofront-button',
        iconName: 'bringforward',
        title: 'bringToFront'
    },

    render: function() {
        Button.prototype.render.apply(this, arguments);
        this.listenTo(selectionManager, `add remove model:change:zIndex`, this.updateButton);
        return this;
    },

    isCompatible: function(itemViewModels) {
        return itemViewModels.length === 1 && !itemViewModels.first().model.get('zIndexLock');
    },

    isEnabled: function() {
        if (selectionManager.length === 0) {
            return false;
        }
        const currentZIndex = selectionManager.first().model.get('zIndex');
        const zIndexes = selectionManager.first().parent.itemViewModels
            .map(ivm => ivm.model.get('zIndex'));

        return currentZIndex < Math.max(...zIndexes);
    },

    onClick: function() {
        commandDispatcher.changeZIndex({ selectedItem: selectionManager.first(), action: 'bringToFront' });
    },

    updateButton: function() {
        const $button = this.$('button');
        const enabled = this.isEnabled();
        $button.toggleClass('disabled', !enabled);
        $button.prop('disabled', !enabled);
    }

});

BringForwardButton.tag = buttons.bringToFront;

module.exports = BringForwardButton;
