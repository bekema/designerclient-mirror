const Button = require('ui/tools/Button');
const commandDispatcher = require('CommandDispatcher');
const selectionManager = require('SelectionManager');

module.exports = Button.extend({
    className: `${Button.prototype.className} dcl-button--toggle`,

    render: function() {
        Button.prototype.render.apply(this, arguments);
        this.listenTo(selectionManager, `add remove model:change:${this.options.attribute}`, this.updateButton);
        return this;
    },

    isActive: function() {
        if (!selectionManager.length) {
            return false;
        }
        return selectionManager.models.every(model => { return model.model.get(this.options.attribute); });
    },

    isCompatible: function(itemViewModels) {
        return itemViewModels.every(itemViewModel => {
            return itemViewModel.get('module') === 'TextFieldViewModel';
        });
    },

    getAttributes: function() {
        const obj = {};
        obj[this.options.attribute] = !this.isActive();
        return obj;
    },

    onClick: function() {
        commandDispatcher.changeAttributes({ viewModels: selectionManager, attributes: this.getAttributes() });
    },

    updateButton:function() {
        if (this.isActive()) {
            this.$('button').addClass('active');
        } else {
            this.$('button').removeClass('active');
        }
    }
});
