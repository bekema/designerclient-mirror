const TextToggleButton = require('ui/tools/buttons/TextToggleButton');
const selectionManager = require('SelectionManager');
const buttons = require('publicApi/buttons');

const TextLeftAlignButton = TextToggleButton.extend({
    className: `${TextToggleButton.prototype.className} dcl-button--left-align`,

    options: {
        attribute: 'horizontalAlignment',
        buttonClasses: 'dcl-text-left-align-button',
        iconName: 'alignleft',
        title: 'leftAlign'
    },

    isActive: function() {
        if (!selectionManager.length) {
            return false;
        }
        return selectionManager.models.every(model => { return model.model.get(this.options.attribute) === 'left'; });
    },

    getAttributes: function() {
        const obj = {};
        obj[this.options.attribute] = 'left';
        return obj;
    }

});

TextLeftAlignButton.tag = buttons.leftAlign;

module.exports = TextLeftAlignButton;
