const TextToggleButton = require('ui/tools/buttons/TextToggleButton');
const selectionManager = require('SelectionManager');
const buttons = require('publicApi/buttons');

const TextCenterAlignButton = TextToggleButton.extend({
    className: `${TextToggleButton.prototype.className} dcl-button--align-center`,

    options: {
        attribute: 'horizontalAlignment',
        buttonClasses: 'dcl-text-center-align-button',
        iconName: 'aligncenter',
        title: 'centerAlign'
    },

    isActive: function() {
        if (!selectionManager.length) {
            return false;
        }
        return selectionManager.models.every(model => { return model.model.get(this.options.attribute) === 'center'; });
    },

    getAttributes: function() {
        const obj = {};
        obj[this.options.attribute] = 'center';
        return obj;
    }

});

TextCenterAlignButton.tag = buttons.centerAlign;

module.exports = TextCenterAlignButton;
