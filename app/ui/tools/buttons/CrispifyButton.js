const Button = require('ui/tools/Button');
const selectionManager = require('SelectionManager');
const commandDispatcher = require('CommandDispatcher');
const clients = require('services/clientProvider');
const buttons = require('publicApi/buttons');

const CrispifyButton = Button.extend({
    className: `${Button.prototype.className} dcl-button--crispify`,

    options: {
        buttonClasses: 'dcl-crispify-button',
        iconName: 'crispify',
        title: 'crispify'
    },

    render: function() {
        Button.prototype.render.apply(this, selectionManager);
        this.listenTo(selectionManager, `add remove model:change:crispify model:change:crispifyUrl`, this.updateButton);
        return this;
    },

    isActive: function() {
        return selectionManager.first().model.get('crispify');
    },

    originalPreviewUrl: function() {
        return selectionManager.first().model.get('oldPreviewUrl');
    },

    crispifyUrl: function() {
        return selectionManager.first().model.get('crispifyUrl');
    },

    isLoading: function() {
        return !this.crispifyUrl() && this.isActive();
    },

    isCompatible: function(itemViewModels) {
        const viewModel = itemViewModels.first();
        return clients.crispify &&
               itemViewModels.length === 1 &&
               viewModel.get('module') === 'UploadedImageViewModel' &&
               viewModel.model.get('fileType') &&
               (viewModel.model.get('fileType') === 'image/jpeg' ||
                viewModel.model.get('fileType') === 'image/png');
    },

    getAttributes: function() {
        const obj = {};
        if (this.isActive()) {
            obj.url = this.originalPreviewUrl();
            obj.previewUrl = this.originalPreviewUrl();
            obj.printUrl = selectionManager.first().model.get('oldPrintUrl');
        } else {
            obj.url = this.crispifyUrl();
            obj.previewUrl = this.crispifyUrl();
            obj.printUrl = this.crispifyUrl();
        }
        obj.crispify = !this.isActive();
        return obj;
    },

    onClick: function() {
        if (!selectionManager.length) {
            return;
        }
        if (!this.crispifyUrl()) {
            if (this.isLoading()) {
                return;
            }
            const crispifyingViewModel = selectionManager.first();
            crispifyingViewModel.model.set({ crispify: true });
            clients.crispify.superResolutionImage(crispifyingViewModel.model.get('printUrl'))
                .then(successResponse => {
                    //This is such that undo-ing works properly and so the crispifyUrl does not need to be requested again.
                    crispifyingViewModel.model.set(
                        { crispify: false,
                          crispifyUrl: successResponse.Output,
                          oldPreviewUrl: crispifyingViewModel.model.get('previewUrl'),
                          oldPrintUrl: crispifyingViewModel.model.get('printUrl')
                        });
                    commandDispatcher.changeAttributes({ viewModel: crispifyingViewModel, attributes:
                        { crispify: true,
                          url: successResponse.Output,
                          previewUrl: successResponse.Output,
                          printUrl: successResponse.Output } });
                }, () => {
                    crispifyingViewModel.model.set({ crispify: false });
                });
        } else {
            commandDispatcher.changeAttributes({ viewModels: selectionManager, attributes: this.getAttributes() });
        }
    },

    updateButton: function() {
        if (!selectionManager.length) {
            return;
        }
        if (this.isLoading()) {
            this.$('.dcl-toolbar-button__icon').addClass('dcl-spin');
        } else {
            this.$('.dcl-toolbar-button__icon').removeClass('dcl-spin');
        }
        if (this.isActive()) {
            this.$('button').addClass('active');
        } else {
            this.$('button').removeClass('active');
        }
    }

});

CrispifyButton.tag = buttons.crispify;

module.exports = CrispifyButton;
