const ColorPickerButton = require('ui/tools/buttons/ColorPickerButton');
const buttons = require('publicApi/buttons');

const fillColorPickerButton = ColorPickerButton.extend({
    options: {
        colorAttribute: 'fillColor',
        buttonClasses: 'dcl-color-picker-button',
        iconName: 'fill'
    },

    isCompatible: function(itemViewModels) {
        return itemViewModels.every(itemViewModel =>
            itemViewModel.get('module') === 'ShapeViewModel' && itemViewModel.model.get('module') !== 'Line');
    }
});

fillColorPickerButton.tag = buttons.fillColor;

module.exports = fillColorPickerButton;
