const _ = require('underscore');
const $ = require('jquery');
const Tool = require('ui/tools/Tool');
const hoverButtonTemplate = require('ui/tools/templates/buttons/HoverButton.tmp');
const hoverMenu = require('ui/tools/HoverMenu');
const localization = require('Localization');
const util = require('util/Util');

const hoverButton = Tool.extend({
    className: `${Tool.prototype.className} dcl-button--hoverable`,

    events: function() {
        return _.extend({
            'mouseover': 'onHover',
            'mouseout': 'offHover'
        }, Tool.prototype.events);
    },

    initialize: function(options) {
        this.options = _.extend({}, this.options, options);
        this.options.template = this.options.template || hoverButtonTemplate;

        this.$document = $(document);
        this.mouseMove = this.mouseMove.bind(this);
    },

    isCompatible: function(itemViewModels) {
        return (this.childView.isCompatible(itemViewModels));
    },

    render: function() {
        this.$el.html(this.options.template({
            buttonClasses: this.options.buttonClasses,
            iconName: this.options.iconName,
            text: this.options.title ? localization.getText(`tools.buttons.${this.options.title}.title`) : ''
        }));
        this.options.childViewOptions.parent = this;
        const ChildView = this.options.ChildView || hoverMenu;
        this.childView = new ChildView(this.options.childViewOptions);
        this.$el.append(this.childView.render().el);
        return this;
    },

    onHover: function() {
        if (!this.$el.hasClass('dcl-button--hover')) {
            this.$el.addClass('dcl-button--hover');
            this.$document.on(`mousemove.hoverable`, this.mouseMove);
            this.$document.on(`touchmove.hoverable`, this.mouseMove);
            this.$document.on(`touchstart.hoverable`, this.mouseMove);
            this.childView.show();
        }
    },

    mouseMove: function(e) {
        let hoveredOff = true;
        const buttonRect = this.el.getBoundingClientRect();
        const childRect = this.childView.el.getBoundingClientRect();
        const hoverPoint = { x: e.clientX, y: e.clientY };
        if (util.containsPoint(hoverPoint, buttonRect) || util.containsPoint(hoverPoint, childRect)) {
            hoveredOff = false;
        }
        if (hoveredOff) {
            this.$el.removeClass('dcl-button--hover');
            this.$document.off(`.hoverable`, this.mouseMove);
        }
    }
});

hoverButton.tag = 'hover';

module.exports = hoverButton;
