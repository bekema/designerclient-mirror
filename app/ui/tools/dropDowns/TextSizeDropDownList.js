const DropDownList = require('ui/tools/dropDowns/DropDownList');
const commandDispatcher = require('CommandDispatcher');
const selectionManager = require('SelectionManager');
const coreConfig = require('publicApi/configuration/core/items');

/* eslint-disable no-magic-numbers */

const TextSizeDropdownButton = DropDownList.extend({
    className: `${DropDownList.prototype.className} dcl-dropdown-list--text-size`,

    render: function() {
        DropDownList.prototype.render.apply(this, arguments);
        this.listenTo(selectionManager, 'add remove model:change:fontSize', this.updateDropDown);
        return this;
    },

    isCompatible: function(itemViewModels) {
        return itemViewModels.every(itemViewModel => {
            return itemViewModel.get('module') === 'TextFieldViewModel';
        });
    },

    configureSelectBox: function(value) {
        this.$('.dcl-select').select2({
            dropdownAutoWidth: true,
            width: 'auto',
            tags: true,
            selectOnBlur: true,
            placeholder: value,
            createTag: tag => {
                const int = parseInt(tag.term);
                if (!isNaN(int) && int <= coreConfig.text.maxFontSize && int >= coreConfig.text.minFontSize) {
                    return {
                        id: tag.term,
                        text: tag.term,
                        isNew: true
                    };
                }
            }
        });
    },

    // TODO: Make this product-aware. We don't want size 500 for pens or size 5 for building-sized banners to be an option
    listValues: function() {
        const sizes = [];
        let i;
        const max1 = 11 < coreConfig.text.maxFontSize ? 11 : coreConfig.text.maxFontSize;
        const max2 = 96 < coreConfig.text.maxFontSize ? 96 : coreConfig.text.maxFontSize;
        for (i = coreConfig.text.minFontSize; i <= max1; i++) {
            sizes.push(i);
        }
        for (; i <= max2; i += 4) {
            sizes.push(i);
        }
        for (; i <= coreConfig.text.maxFontSize; i += 40) {
            sizes.push(i);
        }
        return sizes;
    },

    valueSelected: function(value) {
        value = parseInt(value);
        commandDispatcher.changeAttributes({ viewModels: selectionManager, attributes: { fontSize: value } });
    },

    updateDropDown: function() {
        let setValue = '';
        if (selectionManager.length > 0) {
            const strokeWidths = selectionManager.map(viewModel => viewModel.model.get('fontSize'));
            setValue = strokeWidths.reduce((previous, current) => current < previous ? current : previous);

            if (!strokeWidths.every(sw => sw === setValue)) {
                setValue = `${setValue}+`;
            }
        }
        DropDownList.prototype.updateDropDown.call(this, setValue);
    }
});

TextSizeDropdownButton.tag = 'textsize';

module.exports = TextSizeDropdownButton;
