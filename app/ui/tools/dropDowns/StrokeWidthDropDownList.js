const DropDownList = require('ui/tools/dropDowns/DropDownList');
const commandDispatcher = require('CommandDispatcher');
const selectionManager = require('SelectionManager');

const StrokeSizeDropdownButton = DropDownList.extend({

    options: {
        labelText: ''
    },

    render: function() {
        DropDownList.prototype.render.apply(this, arguments);
        this.listenTo(selectionManager, 'add remove model:change:strokeWidth', this.updateDropDown);
        return this;
    },

    isCompatible: function(itemViewModels) {
        this.adjustZeroStrokeItem(itemViewModels);
        return itemViewModels.every(itemViewModel => itemViewModel.get('module') === 'ShapeViewModel' || itemViewModel.get('module') === 'LineViewModel');
    },

    listValues: function() {
        /* eslint-disable no-magic-numbers */
        return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
    },

    adjustZeroStrokeItem: function(itemViewModels) {
        if (itemViewModels.some(itemViewModel => itemViewModel.model.get('module') === 'Line')) {
            this.$('.dcl-select option[value="0"]').remove();
        } else if (this.$('.dcl-select option[value="0"]').length === 0) {
            this.$('.dcl-select').prepend('<option value="0">0</>');
        }
    },

    valueSelected: function(value) {
        value = parseInt(value);
        commandDispatcher.changeAttributes({ viewModels: selectionManager, attributes: { strokeWidth: value } });
    },

    updateDropDown: function() {
        let setValue = '';
        if (selectionManager.length > 0) {
            const strokeWidths = selectionManager.map(viewModel => viewModel.model.get('strokeWidth'));
            setValue = strokeWidths.reduce((previous, current) => current < previous ? current : previous);

            if (!strokeWidths.every(sw => sw === setValue)) {
                setValue = `${setValue}+`;
            }
        }
        DropDownList.prototype.updateDropDown.call(this, setValue);
    }
});

StrokeSizeDropdownButton.tag = 'strokewidth';

module.exports = StrokeSizeDropdownButton;
