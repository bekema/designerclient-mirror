const Tool = require('ui/tools/Tool');
const dropdownTemplate = require('ui/tools/templates/buttons/Dropdown.tmp');

module.exports = Tool.extend({
    className: `${Tool.prototype.className} dcl-dropdown-list`,

    events: {
        'change .dcl-select' : 'valueChanged'
    },

    initialize: function({ template = dropdownTemplate }) {
        this.template = template;
    },

    options: {
        labelText: ''
    },

    render: function() {
        this.$el.html(this.template({ values: this.listValues(), selectId: this.cid, labelText: this.options.labelText }));
        this.configureSelectBox('');
        return this;
    },

    configureSelectBox: function(value) {
        this.$('.dcl-select').select2({
            dropdownAutoWidth: true,
            width: 'auto',
            placeholder: value
        });
    },

    isCompatible: function() {
        return true;
    },

    listValues: function() {
        throw new Error('You must define listValues for your drop down');
    },

    valueChanged: function() {
        const value = this.$('.dcl-select').val();
        this.valueSelected(value);
    },

    valueSelected: function() {
        throw new Error('You must define valueSelected for your drop down');
    },

    updateDropDown: function(value) {
        this.$('.dcl-select').val(value);
        this.configureSelectBox(value);
    }

});
