const DropDownList = require('ui/tools/dropDowns/DropDownList');
const commandDispatcher = require('CommandDispatcher');
const clients = require('services/clientProvider');
const selectionManager = require('SelectionManager');

const TextFamilyDropDown = DropDownList.extend({
    className: `${DropDownList.prototype.className} dcl-dropdown-list--text-family`,

    render: function() {
        DropDownList.prototype.render.apply(this, arguments);
        this.listenTo(selectionManager, 'add remove model:change:fontSize', this.updateDropDown);
        return this;
    },

    isCompatible: function(itemViewModels) {
        return itemViewModels.every(itemViewModel => itemViewModel.get('module') === 'TextFieldViewModel');
    },

    listValues: function() {
        return clients.font.fontNames;
    },

    valueSelected: function(value) {
        const fontOption = clients.font.fontMapping[value.toLowerCase()];
        const attributes = this.scrubStyles(fontOption, { fontFamily: value });

        commandDispatcher.changeAttributes({ viewModels: selectionManager, attributes });
    },

    scrubStyles: function(fontOption, attributes) {
        if (!fontOption.Variants.Bold) {
            attributes.fontBold = false;
        }

        if (!fontOption.Variants.Italic) {
            attributes.fontItalic = false;
        }

        // Edge case - I have Bold and Italics selected on the current font - the font I am selecting does not allow both to be selected
        // this will disable both buttons, even though they should be enabled.
        if (!fontOption.Variants.BoldItalic) {
            const illegalStyle = selectionManager.models.some(viewModel => {
                return viewModel.model.get('fontBold') && viewModel.model.get('fontItalic');
            });

            if (illegalStyle) {
                attributes.fontBold = false;
                attributes.fontItalic = false;
            }
        }

        return attributes;
    },

    updateDropDown: function() {
        let setValue = '';
        let allSameValue = true;
        if (selectionManager.length > 0) {
            const firstValue = selectionManager.first().model.get('fontFamily');
            setValue = firstValue;
            selectionManager.some(itemViewModel => {
                const currentFont = itemViewModel.model.get('fontFamily');
                if (currentFont !== firstValue) {
                    allSameValue = false;
                    return true;
                }
            });
        }
        if (!allSameValue) {
            setValue = '';
        }

        DropDownList.prototype.updateDropDown.call(this, setValue);
    }
});

TextFamilyDropDown.tag = 'textfontfamily';

module.exports = TextFamilyDropDown;
