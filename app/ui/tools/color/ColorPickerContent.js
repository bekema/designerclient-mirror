const Backbone = require('backbone');
const $ = require('jquery');

module.exports = Backbone.View.extend({
    className: `dcl-color-picker__content`,

    initialize: function(options) {
        this.options = options;
        this.parent = options.parent;
    },

    isCompatible: function() {
        return true;
    },

    render: function() {
        this.options.panelTitle = this.optionTitle();
        this.$el.html(this.template(this.options));
        this.delegateEvents();
        return this;
    },

    closeWindow: function() {
        $('.dcl-button--hover').removeClass('dcl-button--hover');
    }
});
