const _ = require('underscore');
const HoverBox = require('ui/tools/HoverBox');
const commandDispatcher = require('CommandDispatcher');
const colorPickerTemplate = require('ui/tools/templates/color/ColorPicker.tmp');
const colorPickerOption = require('ui/tools/templates/color/ColorPickerOption.tmp');
const selectionManager = require('SelectionManager');
const eventBus = require('EventBus');
const events = eventBus.events;

module.exports = HoverBox.extend({
    className: `${HoverBox.prototype.className} dcl-hover-box--color-picker`,

    events: function() {
        return _.extend({
            'click .dcl-color-picker__option': 'onClickOption'
        }, HoverBox.prototype.events);
    },

    getColor: function() {
        return selectionManager.first().model.get(this.colorAttribute);
    },

    changeColor: function(color) {
        const attributes = {
            [this.colorAttribute]: color.hex
        };
        commandDispatcher.changeAttributes({ viewModels: selectionManager, attributes });
        eventBus.trigger(events.colorSelected, color);
    },

    initialize: function(options) {
        this.options = options;
        this.colorAttribute = this.options.colorAttribute;
        this.selected = 0;
        this.views = this.options.views;
        this.template = this.options.template || colorPickerTemplate;
    },

    // getViewList: function() {

    // }

    render: function() {
        this.$el.html(this.template());

        this.Views = this.views.map((view, i) => {
            view.parent = this;
            if (view.default) {
                this.selected = i;
            }
            return new view.constructor(view);
        }).filter(view => view.isCompatible());

        this.Views.forEach((view, index) =>
            this.$('.dcl-color-picker__options').append(colorPickerOption({
                title: view.optionTitle(),
                i: index,
                iconName: view.optionIcon()
            })));

        this.selectOption();

        return this;
    },

    isCompatible: function() {
        return true;
    },

    onClickOption: function(e) {
        const i = this.$(e.target).closest('.dcl-color-picker__option').data('index');
        if (this.selected !== i) {
            this.selected = i;
            this.selectOption();
        }
    },

    selectOption: function() {
        this.$('.dcl-color-picker__option').removeClass('selected');
        this.$(`[data-index="${this.selected}"]`).addClass('selected');
        this.$('.dcl-color-picker__container').html(this.Views[this.selected].render().el);
    },

    show: function() {
        this.Views[this.selected].render();
        return HoverBox.prototype.show.apply(this, arguments);
    }
});
