const ColorPickerChoices = require('ui/tools/color/ColorPickerChoices');
const localization = require('Localization');

module.exports = ColorPickerChoices.extend({
    className: `${ColorPickerChoices.prototype.className} dcl-color-picker__choices--provided`,

    optionTitle: function() {
        return localization.getText('tools.color.providedColors');
    },

    optionIcon: function() {
        return 'standardcolors';
    }

});
