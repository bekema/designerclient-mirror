const ColorPickerProvidedChoices = require('ui/tools/color/ColorPickerProvidedChoices');
const canvasToolsColorPickerChoicesTemplate = require('ui/tools/templates/color/CanvasToolsColorPickerChoices.tmp');

module.exports = ColorPickerProvidedChoices.extend({
    initialize: function(options) {
        options.template = canvasToolsColorPickerChoicesTemplate;
        return ColorPickerProvidedChoices.prototype.initialize.apply(this, arguments);
    },

    render: function() {
        return ColorPickerProvidedChoices.prototype.render.apply(this, arguments);
    }
});
