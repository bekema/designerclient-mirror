const ColorPicker = require('ui/tools/color/ColorPicker');
const ColorPickerCustom = require('ui/tools/color/ColorPickerCustom');
const CanvasToolsColorPickerProvidedChoices = require('ui/tools/color/CanvasToolsColorPickerProvidedChoices');
const commandDispatcher = require('CommandDispatcher');

module.exports = ColorPicker.extend({

    initialize: function(options) {
        this.viewModel = options.viewModel;
        options.views = [
            { constructor: CanvasToolsColorPickerProvidedChoices, default: true },
            { constructor: ColorPickerCustom }
        ];

        this.parent = options.parent;

        return ColorPicker.prototype.initialize.apply(this, arguments);
    },

    stopPropagation: function(e) {
        e.stopPropagation();
    },

    getColor: function() {
        return this.viewModel.model.getBackgroundColor();
    },

    changeColor: function(color, e) {
        this.parent.hide();

        this.stopPropagation(e);

        commandDispatcher.changeBackgroundColor({
            model: this.viewModel.model,
            color
        });
    }
});