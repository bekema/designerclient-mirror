const ColorPickerContent = require('ui/tools/color/ColorPickerContent');
const defaultTemplate = require('ui/tools/templates/color/ColorPickerChoices.tmp');
const selectionManager = require('SelectionManager');
const colorManager = require('ColorManager');
const $ = require('jquery');

/* eslint-disable no-magic-numbers */

const addSelectedClass = function(colors, hex) {
    if (!colors) { return; }
    return colors.map(color => {
        if (color.hex === hex) {
            color.selected = 'selected';
        } else {
            color.selected = '';
        }
        return color;
    });
};

const loadColors = function() {
    return new Promise(resolve => {
        if (!this.options.colors) {
            colorManager.getProvidedColors().then(colors => {
                this.options.colors = colors;
                this.updateSelected();
                resolve();
            });
        }
    });
};

module.exports = ColorPickerContent.extend({
    className: 'dcl-color-picker__choices',

    events: {
        'click .dcl-color-choice-button': 'onClickButton',
        'touchstart .dcl-color-choice-button': 'onClickButton'
    },

    initialize: function(options) {
        this.options = options;
        this.template = options.template || defaultTemplate;
        this.parent = options.parent;

        loadColors.call(this)
            .then(() => this.render());
    },

    render: function() {
        this.updateSelected();
        ColorPickerContent.prototype.render.apply(this, arguments);
        return this;
    },

    updateSelected: function() {
        let hex;
        const itemViewModels = selectionManager.models;
        if (itemViewModels.length > 0 && itemViewModels.every(itemViewModel => itemViewModel.get('module') === 'TextFieldViewModel')) {
            const firstColor = itemViewModels[0].model.get('fontColor');
            if (itemViewModels.every(itemViewModel => itemViewModel.model.get('fontColor') === firstColor)) {
                hex = firstColor;
            }
        }
        this.options.colors = addSelectedClass(this.options.colors, hex);
    },

    onClickButton: function(e) {
        this.$('.dcl-color-choice').removeClass('selected');
        const color = $(e.target).data('color');
        this.parent.changeColor(color, e);

        if (color) {
            this.$(`[data-hex="${color.hex}"]`).parent().addClass('selected');
        }
        this.closeWindow();
    }
});
