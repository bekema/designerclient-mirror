/* eslint-disable no-magic-numbers */

const $ = require('jquery');
const _ = require('underscore');
const colorConverter = require('util/ColorConverter');
const ColorPickerContent = require('ui/tools/color/ColorPickerContent');
const defaultTemplate = require('ui/tools/templates/color/ColorPickerCustom.tmp');
const globalConfig = require('configuration/Global');
const util = require('util/Util');
const localization = require('Localization');
const designerContext = require('DesignerContext');

module.exports = ColorPickerContent.extend({
    className: `dcl-color-picker__custom`,

    events: {
        'click .dcl-color-picker-preview': 'onClickApplyButton',
        'click .dcl-color-picker-apply-button': 'onClickApplyButton',
        'click': 'onClick',
        'mousemove': 'onMouseMove',
        'mouseup': 'onMouseUp',
        'mousedown .dcl-color-picker-wheel-container': 'onMouseDownColorWheel',
        'mousedown .dcl-color-picker-shader-container': 'onMouseDownShader',
        'touchstart': 'onTap',
        'touchmove .dcl-color-picker-wheel-container': 'onMouseMove',
        'touchmove .dcl-color-picker-shader-container': 'onMouseMove',
        'touchend .dcl-color-picker-wheel-container': 'onMouseUp',
        'touchend .dcl-color-picker-shader-container': 'onMouseUp',
        'touchstart .dcl-color-picker-wheel-container': 'onMouseDownColorWheel',
        'touchstart .dcl-color-picker-shader-container': 'onMouseDownShader',
        //'input .dcl-color-picker-cmyk': 'onInputCmykInput',
        'input .dcl-color-picker-rgb': 'onInputRgbInput'
        //'click .dcl-color-picker-eyedropper': 'onClickEyeDropper'
    },

    initialize: function({ parent, colorAttribute, template = defaultTemplate }) {
        this.parent = parent;
        this.template = template;
        this.colorAttribute = colorAttribute;
        this.tag = `colorpicker-${this.colorAttribute}`;
        this.options = this.parent.options;
    },

    render: function() {
        this.options.panelTitle = this.optionTitle;
        this.$el.html(this.template(this.options));
        const img = new Image();
        img.onload = function() {
            const canvas = this.$('.dcl-color-picker-wheel')[0];
            const context = canvas.getContext('2d');
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.drawImage(img, 0, 0, canvas.width, canvas.height);
            this.selectColorOnColorWheel();
        }.bind(this);
        img.src = globalConfig.colorWheelUrl; // triggers load

        // initialize drag handles
        this.$wheelHandle = this.$('.dcl-color-picker-wheel-handle');
        this.wheelHandleActive = false;
        this.$shaderHandle = this.$('.dcl-color-picker-shader-handle');
        this.shaderHandleActive = false;
        this.delegateEvents();
        return this;
    },

    optionTitle: function() {
        return localization.getText('tools.color.customColor');
    },

    optionIcon: function() {
        return 'colorwheel';
    },


    isCompatible: function() {
        return !designerContext.embroidery && ColorPickerContent.prototype.isCompatible.call(this);
    },

    //This is used to show a selected color on the color wheel with the slider in the correct position
    //To do so we need to convert to the HSV color model, get the V value for the shader and then set
    //the V to 100 so we can get the unshaded RGB value.
    selectColorOnColorWheel: function() {
        const hexColor = this.parent.getColor();
        if (hexColor) {
            //Convert to hsv and then remove all the 'V' so you can extract the shader value from the color wheel
            const color = colorConverter.hex2rgb(hexColor);
            color.hex = hexColor;
            const hsv = colorConverter.rgb2hsv(color);
            const colorWithFullV = colorConverter.hsv2rgb({ h: hsv.h, s: hsv.s, v: 100 });
            colorWithFullV.hex = colorConverter.rgb2hex(colorWithFullV);
            const position = this.findColorWheelPositionGivenColor(colorWithFullV);

            //Position the shader at the inverse of the V value
            this.positionShaderHandleAtPercentage((100 - hsv.v) / 100);
            //Update the color wheel to show the color with all the shading removed
            this.updateWheelHandlesToColor(colorWithFullV, position.x, position.y);
            //Update the final display with the current
            this.finalizeWheelColor(color);
        }
    },

    onInputRgbInput: function() {
        const rgb = { r: parseInt(this.$('.R').val()),
                    g: parseInt(this.$('.G').val()),
                    b: parseInt(this.$('.B').val()) };

        if (!_.values(rgb).every(function(value) {
            return !isNaN(value) && value !== null && value >= 0 && value <= 255;
        })) {
            return;
        }

        this.drawShader();
        rgb.hex = colorConverter.rgb2hex(rgb);
        this.updatePreview(rgb);
    },

    clearValues: function() {
        this.$('.dcl-color-picker-rgb').val('');
    //      this.$('.dcl-color-picker-cmyk').val('');
    },

    updateValues: function(color) {
        if (!color) {
            this.clearValues();
            this.updatePreview();
            return;
        }

        this.$('.R').val(color.r);
        this.$('.G').val(color.g);
        this.$('.B').val(color.b);
    //     this.$('.C').val(color.c);
    //     this.$('.Y').val(color.y);
    //     this.$('.M').val(color.m);
    //     this.$('.K').val(color.k);
        this.updatePreview(color);
    },

    onClick: function(e) {
        const $button = this.$('.dcl-color-picker-apply-button');
        const $preview = this.$('.dcl-color-picker-preview');

        if ($button[0] !== e.target && $preview[0] !== e.target) {
            e.stopPropagation();
        }
    },

    onClickApplyButton: function(e) {
        const $button = this.$('.dcl-color-picker-apply-button');

        if ($button.hasClass('textbutton-disabled')) {
            return;
        }

        if (!this.previewColor) {
            return;
        }

        this.setValue(this.previewColor, e);
        this.closeWindow();
    },

    setValue: function(color, e) {
        this.parent.changeColor(color, e);
    },

    updatePreviewTentative: function(color) {
        const $preview = this.$('.dcl-color-picker-preview');
        $preview.css('background-color', color.hex);
        this.clearValues();
        this.previewColor = '';
    },

    updatePreview: function(color) {
        const $preview = this.$('.dcl-color-picker-preview');
        const $button = this.$('.dcl-color-picker-apply-button');
        if (!color) {
            $preview.css('background-color', 'transparent');
            this.previewColor = '';
            $button.addClass('textbutton-disabled');
        } else {
            $preview.css('background-color', color.hex);
            this.previewColor = color;
            $button.removeClass('textbutton-disabled');
        }
    },

    /** Color Wheel and Shader Mouse Event Handlers **/
    onMouseDownColorWheel: function(e) {
        // Only if mouse actually down on a color pixel of the image (not the transparent corners)
        // or on the handle.
        if ($(e.target).is('.dcl-color-picker-handle')) {
            this.wheelHandleActive = true;
            return;
        }

        const { left: x, top: y } = util.getCoordinates(e);

        const $wheel = this.$('.dcl-color-picker-wheel');
        const wheelOffset = $wheel.offset();
        const context = $wheel[0].getContext('2d');
        const pixel = context.getImageData(x - wheelOffset.left, y - wheelOffset.top, 1, 1);
        if (pixel.data[3] === 255) {
            this.wheelHandleActive = true;
        }
    },

    onMouseDownShader: function() {
        //e.preventDefault();
        this.shaderHandleActive = true;
    },

    onTap: function(e) {
        if (this.wheelHandleActive) {
            this.moveWheelHandleOnMouseEvent(e);
        }
        if (this.shaderHandleActive) {
            this.moveShaderHandleOnMouseEvent(e);
        }
    },

    onMouseMove: function(e) {
        const { left: x, top: y } = util.getCoordinates(e);
        //e.preventDefault();
        // Chrome fires event repeatedly. Did we really move?
        if (x === this.lastX && y === this.lastY) {
            return;
        }

        this.lastX = x;
        this.lastY = y;

        let color;

        if (this.wheelHandleActive) {
            color = this.moveWheelHandleOnMouseEvent(e);
        }
        if (this.shaderHandleActive) {
            color = this.moveShaderHandleOnMouseEvent(e);
        }

        if (color) {
            this.finalizeWheelColor(color);
        }
    },

    onMouseUp: function(e) {
        let color;
        if (this.wheelHandleActive) {
            this.wheelHandleActive = false;
            color = this.moveWheelHandleOnMouseEvent(e);
            this.finalizeWheelColor(color);
        }

        if (this.shaderHandleActive) {
            this.shaderHandleActive = false;
            color = this.moveShaderHandleOnMouseEvent(e);
            this.finalizeWheelColor(color);
        }
    },

    moveWheelHandleOnMouseEvent: function(e) {
        e.preventDefault();
        const $wheel = this.$('.dcl-color-picker-wheel');
        const wheelOffset = $wheel.offset();

        /** get color under the click **/
        const context = $wheel[0].getContext('2d');

        // Normalize center to (0, 0) so the math works nicely.
        // Note: assumes wheel has same height and width (i.e., circle or square).
        const centerOffset = $wheel.width() / 2;
        const xOffset = wheelOffset.left + centerOffset;
        const yOffset = wheelOffset.top + centerOffset;

        let { left: x, top: y } = util.getCoordinates(e);
        x -= xOffset;
        y -= yOffset;

        let pixel = context.getImageData(x + centerOffset, y + centerOffset, 1, 1);

        if (pixel.data[3] < 255) {
            // clicked on a transparent area
            /** find edge **/
            // for each x between current x and center
            const slope = y / x;
            // x = x => 0 (0 is center)
            const xStep = x > 0 ? -1 : 1;
            for (; pixel.data[3] < 255 && (0 - x) * xStep >= 0; x += xStep) {
                // 'y=mx+b' line formula from high school math.
                // y-intercept (b) always 0 since center of circle at (0, 0).
                let yMax = 0; // Default to center. At x=0, slope = Infinity.
                if (isFinite(slope)) {
                    yMax = slope * x;
                }
                if (!isFinite(slope) || slope > 1 || slope < -1) {
                    // Helps handle drag smoothly along top/bottom.
                    // y = y => yMax
                    const yStep = y > 0 ? -1 : 1;
                    for (; pixel.data[3] < 255 && (yMax - y) * yStep >= 0; y += yStep) {
                        pixel = context.getImageData(x + centerOffset, y + centerOffset, 1, 1);
                    }
                } else {
                    y = yMax;
                    pixel = context.getImageData(x + centerOffset, y + centerOffset, 1, 1);
                }
            }
        }
        let color;
        if (pixel.data[3] < 255) {
            // if we still haven't found a color
            color = this.lastShaderColor;
            x = 0;
        } else {
            color = { r: pixel.data[0], g: pixel.data[1], b: pixel.data[2] };
            color.hex = colorConverter.rgb2hex(color);
        }

        //Get uncentered offset back
        x += centerOffset;
        y += centerOffset;
        color = this.updateWheelHandlesToColor(color, x, y);

        return color;
    },

    updateWheelHandlesToColor: function(color, x, y) {
        // TODO: can't get true width() until display'ed
        this.handleCenterOffset = this.handleCenterOffset || this.$wheelHandle.width() / 2;

        this.$wheelHandle.css('top', y);
        this.$wheelHandle.css('left', x);

        // use visibility not display to position before showing
        this.$wheelHandle.css('visibility', 'visible');

        // use existing shader value if already visible
        if (this.$shaderHandle.css('visibility') === 'hidden') {
            const $shader = this.$('.dcl-color-picker-shader');
            const shaderOffset = $shader.offset();
            this.currentShaderY = 0;
            this.$shaderHandle.offset({
                top: shaderOffset.top - this.handleCenterOffset,
                left: shaderOffset
            });
            this.$shaderHandle.css('visibility', 'visible');
        }

        // finally, use color under the click
        this.$wheelHandle.css('background-color', color.hex);
        this.drawShader(color);

        // if shader has a value, that's the color we really want
        if (this.currentShaderY > 0) {
            color = this.getCurrentShaderValueColor();
        }
        this.$shaderHandle.css('background-color', color.hex);
        this.updatePreviewTentative(color.hex);
        this.lastShaderColor = color;
        return color;
    },

    findColorWheelPositionGivenColor: function(color) {
        const $wheel = this.$('.dcl-color-picker-wheel');
        const wheelHeight = $wheel.height();
        const wheelWidth = $wheel.width();
        const totalPixelDataSize = wheelWidth * wheelHeight * 4;
        const context = $wheel[0].getContext('2d');
        const imageData = context.getImageData(0, 0, $wheel.width(), $wheel.height()).data;
        let location = null;
        let smallestColorDifference = null;
        //This could theoretically be more performant (right now it just loops through
        //each pixel to find the one with the smallest difference) but I haven't noticed
        //enough negative implications to make it worth the complexity / effort
        for (let i = 0; i < totalPixelDataSize; i = i + 4) {
            const r2 = imageData[i];
            const g2 = imageData[i + 1];
            const b2 = imageData[i + 2];
            const a2 = imageData[i + 3];
            const currentColorDifference = Math.abs(color.r - r2) + Math.abs(color.g - g2) + Math.abs(color.b - b2);
            if (a2 !== 0 && (!smallestColorDifference || currentColorDifference < smallestColorDifference)) {
                smallestColorDifference = currentColorDifference;
                location = i;
            }
        }
        location = location / 4;
        const x = location % wheelWidth;
        const y = Math.floor(location / wheelWidth);
        return { x, y };
    },

    moveShaderHandleOnMouseEvent: function(e) {
        e.preventDefault();
        const $shader = this.$('.dcl-color-picker-shader');
        const shaderOffset = $shader.offset();

        const { top: y } = util.getCoordinates(e);

        const newTop = Math.max(Math.min(y - shaderOffset.top, $shader.height()), 0);

        const rgb = this.positionShaderAndUpdate(newTop);

        return rgb;
    },

    positionShaderAndUpdate: function(top) {
        this.$shaderHandle.css('top', top);

        // Definitely want shown if click on shader.
        // (If the color wheel hasn't been clicked on, will let user choose pure grays.)
        this.$shaderHandle.css('visibility', 'visible');

        // get color under the click
        this.currentShaderY = top;
        const color = this.getCurrentShaderValueColor();
        this.$shaderHandle.css('background-color', color.hex);
        this.updatePreviewTentative(color.hex);
        this.lastShaderColor = color;

        return color;
    },

    positionShaderHandleAtPercentage: function(percentValue) {
        const $shader = this.$('.dcl-color-picker-shader');
        const height = $shader.height();
        const positionToPlace = percentValue * height;
        this.positionShaderAndUpdate(positionToPlace);
    },

    getCurrentShaderValueColor: function() {
        const $shader = this.$('.dcl-color-picker-shader');
        const context = $shader[0].getContext('2d');
        const pixel = context.getImageData(0, this.currentShaderY, 1, 1);
        const color = { r: pixel.data[0], g: pixel.data[1], b: pixel.data[2] };
        color.hex = colorConverter.rgb2hex(color);
        return color;
    },

    drawShader: function(color) {
        const $shader = this.$('.dcl-color-picker-shader');
        if (!color) {
            this.$('.dcl-color-picker-handle').css('visibility', 'hidden');
            // it just looks jarring if shader suddenly redraws when CMYK edited
            if ($shader.is(':visible')) {
                return;
            }
            color = { r: 255, g: 255, b: 255, hex: '#FFFFFF' };
        }

        const canvas = $shader[0];
        const context = canvas.getContext('2d');
        context.rect(0, 0, canvas.width, canvas.height);
        const gradient = context.createLinearGradient(0, 0, 0, canvas.height);
        gradient.addColorStop(0, color.hex);
        gradient.addColorStop(1, '#000000');
        context.fillStyle = gradient;
        context.fill();
    },

    finalizeWheelColor: function(color) {
        if (!color) {
            return;
        }

        this.updateValues(color);
    }
    //TODO add eyedropper
    // /** Eyedropper Events **/
    // onClickEyeDropper: function() {
    //     this.triggerGlobal('startEyedropper');
    //     this.listenToGlobal('eyedropperComplete', _.bind(this.getColorFromEyedropperValue, this));
    //     //We currently need to store the selection for the eyedropper because
    //     //the item can get deselected by the time the eyedropper is complete
    //     this.storedSelection = documentRepository.getActiveCanvasViewModel().itemViewModels.filter(function(itemViewModel) {
    //         return itemViewModel.get('selected') === true;
    //     });
    //     this.triggerGlobal('contextualToolbar:hide');
    // },

    //The eyedropper returns the rgb string for the color it sampled. If this color already
    //exists on an item use that, otherwise hit the server to find the CMYK values
    //TODO - Possible optimizations - also check recent/all colors in addition to colors currently
    //being used
    // getColorFromEyedropperValue: function(colorString) {
    //     this.onEyedropperComplete(colorString);
    // },

    // onEyedropperComplete: function(color) {
    //     this.stopListeningGlobal('eyedropperComplete');
    //     if (color !== null) {
    //         selectionManager.select(this.storedSelection);
    //         this.storedSelection = null;
    //         this.addAllColor(color); //Eyedropper colors are added to all colors
    //         this.setValue(color);
    //         this.manager.get('viewModels').trigger('item:focus');
    //         this.expandTool();
    //     }
    // },

        // addAllColor: function(color) {
    //     const isColorInAllColors = _.some(this.$('.all-colors').find('.dcl-color-picker-swatch'), function(element) {
    //         return $(element).data('color') === color.toString();
    //     });
    //     if (!isColorInAllColors) {
    //         this.$('.all-colors').find('.dcl-color-picker-swatch').first()
    //             .before($('<div>', { 'class': 'dcl-color-picker-swatch', 'data-color': color.toString() })
    //             .append($('<div>', { 'class': 'dcl-color-picker-swatch-inner' })
    //             .css('background-color', color.toHex())));
    //     }
    // },
});
