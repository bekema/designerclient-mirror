const ColorPickerChoices = require('ui/tools/color/ColorPickerChoices');
const colorManager = require('ColorManager');
const localization = require('Localization');

module.exports = ColorPickerChoices.extend({
    className: `${ColorPickerChoices.prototype.className} dcl-color-picker__choices--recent`,

    optionTitle: function() {
        return localization.getText('tools.color.recentColors');
    },

    optionIcon: function() {
        return 'standardcolors';
    },

    render: function() {
        this.options.colors = colorManager.getHistory();
        ColorPickerChoices.prototype.render.apply(this, arguments);
        return this;
    }

});
