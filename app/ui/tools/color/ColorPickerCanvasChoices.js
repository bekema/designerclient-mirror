const ColorPickerChoices = require('ui/tools/color/ColorPickerChoices');
const colorManager = require('ColorManager');
const localization = require('Localization');

module.exports = ColorPickerChoices.extend({
    className: `${ColorPickerChoices.prototype.className} dcl-color-picker__choices--canvas`,

    optionTitle: function() {
        return localization.getText('tools.color.canvasColors');
    },

    optionIcon: function() {
        return 'canvaschoices';
    },

    render: function() {
        this.options.colors = colorManager.getAllColorsOnCanvas();
        ColorPickerChoices.prototype.render.apply(this, arguments);
        return this;
    }

});
