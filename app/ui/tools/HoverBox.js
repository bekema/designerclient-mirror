const $ = require('jquery');
const Backbone = require('backbone');

//The hoverbox is a generic rectangle that has no concept of what it contains,
//just its own size and where it should be positioned
module.exports = Backbone.View.extend({
    className: `dcl-hover-box`,

    events: {
        'mousedown' : 'preventDefault',
        'touchstart': 'preventDefault'
    },

    preventDefault: function(e) {
        e.stopPropagation();
    },

    isCompatible: function() {
        return true;
    },

    show: function() {
        let top = 0;
        let left = 0;
        const parentRect = this.options.parent.el.getBoundingClientRect();
        const boxRect = this.el.getBoundingClientRect();
        const windowWidth = $(window).width();
        const windowHeight = $(window).height();
        if (parentRect.top + parentRect.height + boxRect.height > windowHeight) {
            top = -boxRect.height;
        } else {
            top = parentRect.height;
        }
        if (parentRect.left + boxRect.width > windowWidth) {
            left = windowWidth - (parentRect.left + boxRect.width) - 1;
        }

        this.$el.css({ top, left });
    }
});
