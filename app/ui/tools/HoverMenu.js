const _ = require('underscore');
const HoverBox = require('ui/tools/HoverBox');
const hoverMenuTemplate = require('ui/tools/templates/HoverMenu.tmp');
const selectionManager = require('SelectionManager');

module.exports = HoverBox.extend({
    className: `${HoverBox.prototype.className} dcl-hover-box--menu`,

    initialize: function(options) {
        this.options = _.extend({}, this.options, options);
        this.options.template = this.options.template || hoverMenuTemplate;
    },

    isCompatible: function(itemViewModels) {
        return this.options.buttons.some(button => button.isCompatible(itemViewModels));
    },

    render: function() {
        this.$el.html(this.options.template());
        this.options.buttons.forEach(button => {
            const el = button.render().el;
            el.className += ' dcl-button--menu';
            this.$el.append(el);
        });
        return this;
    },

    show: function() {
        if (this.isCompatible(selectionManager)) {
            this.options.buttons.forEach(button => {
                button.show();
            });
            HoverBox.prototype.show.apply(this, arguments);
        }
    }
});
