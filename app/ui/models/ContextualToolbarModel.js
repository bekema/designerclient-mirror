const Backbone = require('backbone');

module.exports = Backbone.Model.extend({
    defaults: {
        top: 0,
        left: 0,
        padding: 0,
        buttons: [],
        viewableButtons: [],
        positionBelow: false
    }
});
