const Backbone = require('backbone');

module.exports = Backbone.Model.extend({
    defaults: {
        maintainProportions: false,
        handles: 'ne, se, sw, nw',
        crop: {
            left: 0,
            top: 0,
            right: 0,
            bottom: 0
        }
    }
});
