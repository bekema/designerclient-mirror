const lng = require('Localization');

module.exports = function(key) {
    const translation = lng.getText(key);
    return translation;
};