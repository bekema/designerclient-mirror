const Button = require('ui/tools/Button');
const defaultTemplate = require('ui/widgets/templates/PdfButton.tmp');
const generatePdfProof = require('publicApi/pdfProof');

module.exports = Button.extend({
    className: 'dcl-pdf-button',

    initialize: function({ template = defaultTemplate }) {
        Button.prototype.initialize.call(this, { template });
    },

    onClick: function() {
        this.getPdf();
    },

    getPdf: function() {
        const $button = this.$('button');
        $button.find('.dcl-button__spin-loader').addClass('dcl-button__spin-loader--active');
        $button.prop('disabled', true);
        generatePdfProof().then(
            pdfUrl => window.open(pdfUrl, '_blank'),
            error => console.log(error) //eslint-disable-line
        ).then(() => {
            $button.find('.dcl-button__spin-loader').removeClass('dcl-button__spin-loader--active');
            $button.prop('disabled', false);
        });
    }
});
