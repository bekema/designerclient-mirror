const Backbone = require('backbone');
const dragAndDrop = require('DragAndDrop');
const documentRepo = require('DocumentRepository');
const itemConfig = require('publicApi/configuration/core/items').shape;
const { Modes } = require('ui/widgets/shapes/Constants');

const ns = 'http://www.w3.org/2000/svg';

const shapeBuilders = {
    Line: $el => {
        const shape = document.createElementNS(ns, 'line');
        shape.setAttribute('x1', '0');
        shape.setAttribute('x2', '100%');
        shape.setAttribute('y1', '50%');
        shape.setAttribute('y2', '50%');
        $el.empty().append(shape);
    },

    Ellipse: $el => {
        const shape = document.createElementNS(ns, 'circle');
        shape.setAttribute('cx', '50%');
        shape.setAttribute('cy', '50%');
        shape.setAttribute('r', '48%');

        $el.empty().append(shape);
    },

    Rectangle: $el => {
        const shape = document.createElementNS(ns, 'rect');
        shape.setAttribute('width', '98%');
        shape.setAttribute('height', '98%');
        shape.setAttribute('x', '1%');
        shape.setAttribute('y', '1%');

        $el.empty().append(shape);
    }
};

const createShape = ($el, type) => {
    shapeBuilders[type]($el);
};

const createDraggableHelper = $el => {
    const $clone = $el.clone();

    const canvasViewModel = documentRepo.getActiveCanvasViewModel();
    const safeArea = canvasViewModel.get('zoomedSafeArea');

    const maxSize = Math.min(safeArea.width, safeArea.height);

    $clone.css({
        width: `${maxSize * itemConfig.initialSize}px`,
        height: `${maxSize * itemConfig.initialSize}px`
    });

    return $clone;
};

module.exports = Backbone.View.extend({
    //tagName: 'canvas',

    className: 'dcl-add-shapes__icon',

    events: {
        'click': 'onClick',
        'touchend': 'onClick'
    },

    initialize: function({ mode = Modes.Draw, type }) {
        this.mode = mode;
        this.shapeType = type;

        const svg = document.createElementNS(ns, 'svg');
        this.setElement(svg);
        this.$el.addClass('dcl-add-shapes__icon');
    },

    render() {
        createShape(this.$el, this.shapeType); // eslint-disable-line
        if (this.mode === Modes.dragAndDrop || this.mode === Modes.all) {
            this.addDraggable();
        }
        return this.$el;
    },

    onClick: function() {
        if (this.mode === Modes.draw || this.mode === Modes.all) {
            this.triggerGlobal('enableDrawable', this.shapeType);
        }
    },

    addDraggable() {
        this.draggable = new dragAndDrop.Draggable(this.$el, {
            data: { shapeType: this.shapeType },
            helper: () => {
                return createDraggableHelper(this.$el).addClass('dragging-repository-image');
            },
            updateHelper: function(options) {
                const height = options.helper.height();
                const width = options.helper.width();
                const top = options.inputPosition.top - height / 2;
                const left = options.inputPosition.left - width / 2;
                options.helper.css('transform', `translate(${left}px, ${top}px)`);
            }
        });
    }
});
