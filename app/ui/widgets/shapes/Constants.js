module.exports = {
    Modes: {
        draw: 'draw',
        dragAndDrop: 'dragAndDrop',
        all: 'all'
    },

    Shapes: {
        rectangle: 'Rectangle',
        ellipse: 'Ellipse',
        line: 'Line'
    }
};