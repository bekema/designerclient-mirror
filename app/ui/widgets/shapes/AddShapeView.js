const Backbone = require('backbone');
const defaultTemplate = require('ui/widgets/templates/AddShape.tmp');
const ShapeIconView = require('ui/widgets/shapes/ShapeIconView');
const { Modes, Shapes } = require('ui/widgets/shapes/Constants');

module.exports = Backbone.View.extend({
    className: 'dcl-add-shapes',

    initialize: function({ template = defaultTemplate, mode = Modes.dragAndDrop }) {
        this.template = template;
        this.mode = mode;
    },

    render: function() {
        this.$el.html(this.template());
        this.$('.add-shapes__icons-wrapper').append(new ShapeIconView({ type: Shapes.rectangle, mode: this.mode }).render())
                     .append(new ShapeIconView({ type: Shapes.ellipse, mode: this.mode }).render())
                     .append(new ShapeIconView({ type: Shapes.line, mode: this.mode }).render());
        return this;
    }
});
