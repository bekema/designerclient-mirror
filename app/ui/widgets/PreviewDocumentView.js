const Button = require('ui/tools/Button');
const defaultTemplate = require('ui/widgets/templates/previewDocument.tmp');
const previewDocument = require('publicApi/previewDocument');

module.exports = Button.extend({

    initialize: function({ template = defaultTemplate }) {
        Button.prototype.initialize.call(this, { template });
    },

    onClick: function() {
        this.previewDoc();
    },

    previewDoc: function() {
        const $button = this.$('button');
        $button.find('.dcl-button__spin-loader').addClass('dcl-button__spin-loader--active');
        $button.prop('disabled', true);
        previewDocument().then(
            renderedUrl => window.open(renderedUrl, '_blank'),
            error => console.log(error) //eslint-disable-line
        ).then(() => {
            $button.find('.dcl-button__spin-loader').removeClass('dcl-button__spin-loader--active');
            $button.prop('disabled', false);
        });
    }
});
