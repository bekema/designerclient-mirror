const Backbone = require('backbone');
const $ = require('jquery');
const embroideryThreadButtonTemplate = require('ui/widgets/templates/embroidery/ThreadButton.tmp');

module.exports = Backbone.View.extend({
    className: 'dcl-modal__embroidery-menu-button-container',
    tagName: 'div',

    events: {
        'click' : 'showColorPicker',
        'touchstart': 'showColorPicker'
    },

    preventDefault: function(e) {
        e.stopPropagation();
    },

    initialize: function() {
        this.listenTo(this.model, `change:color`, this.updateColor);
    },

    render: function() {
        this.$el.html(embroideryThreadButtonTemplate({ hex: this.model.get('color').hex }));

        const ColorPickerProvidedChoices = require('ui/tools/color/ColorPickerProvidedChoices');
        this.$('.dcl-hover-box--color-picker').append(new ColorPickerProvidedChoices({ parent: this }).render().el);

        return this;
    },

    showColorPicker: function() {
        this.hideColorPickers();
        this.$('.dcl-hover-box--color-picker').addClass('dcl-hover-box--color-picker-visible');
    },

    hideColorPickers: function() {
        $('.dcl-hover-box--color-picker-visible').removeClass('dcl-hover-box--color-picker-visible');
    },

    updateColor: function(model) {
        this.hideColorPickers();
        this.$('.dcl-modal__embroidery-colors-box').css({ backgroundColor: model.get('color').hex });
    },


    changeColor: function(color, e) {
        this.model.set({ color });
        this.preventDefault(e);
    }
});
