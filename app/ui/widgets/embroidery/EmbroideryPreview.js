const Backbone = require('backbone');

module.exports = Backbone.View.extend({
    className: 'dcl-embroidery-preview__item',
    tagName: 'img',

    initialize() {
        this.listenTo(this.model, 'change:url', this.render);
    },

    render() {
        this.$el.attr('src', this.model.get('url'));
        return this;
    }
});
