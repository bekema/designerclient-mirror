const Backbone = require('backbone');
const $ = require('jquery');
const _ = require('underscore');
const embroideryModificationView = require('ui/widgets/templates/embroidery/EmbroideryModificationView.tmp');
const clients = require('services/clientProvider');
const ThreadButtonView = require('ui/widgets/embroidery/ThreadButtonView');
const EmbroideryPreview = require('ui/widgets/embroidery/EmbroideryPreview');
const globalConfig = require('configuration/Global');

const translateOverrideToColor = function(colorOverride) {
    return {
        Ordinal:  colorOverride.Ordinal,
        thread: colorOverride.color,
        hex: colorOverride.hex
    };
};

const buildModelCollection = function(colors) {
    const colorOverrides = this.itemViewModel.model.get('colorOverrides');

    colors.forEach(color => {
        const model = new Backbone.Model({
            color,
            originalColor: _.clone(color)
        });

        if (colorOverrides) {
            const colorOveride = colorOverrides.find(override => color.Ordinal === override.Ordinal);
            if (colorOveride) {
                model.set({ color: translateOverrideToColor(colorOveride) });
            }
        }
        this.collection.add(model);
    });
};

const translateXtiThreads = function(colors) {
    return colors.map(color => ({
        thread: color.Id,
        Ordinal: color.Ordinal,
        hex: clients.embroidery.hexForThread(color.Id)
    }));
};

const getXtiThreadColors = function(xtiUrl) {
    //TODO more hard coded mapping stuff..
    return clients.embroidery.interrogate(
        xtiUrl,
        'http://embroidery.documents.cimpress.io/columbus-mapping'
    );
};

const getColorOverrides = function() {
    return _.filter(this.collection.models, model => model.get('color').hex !== model.get('originalColor').hex)
        .map(model => ({
            Ordinal: model.get('originalColor').Ordinal,
            color: model.get('color').thread,
            hex: model.get('color').hex
        }));
};


module.exports = Backbone.View.extend({
    className: 'dcl-modal dcl-modal--style-embroidery',

    events: {
        'click .dcl-modal__close-button' : 'cancelChanges',
        'click .dcl-button--cancel' : 'cancelChanges',
        'click .dcl-button--submit' : 'updateColors',
        'click .dcl-button--reset-colors' : 'resetColors',
        'mousedown' : 'stopPropagation'
    },

    initialize: function({ containerElement, itemViewModel }) {
        this.containerElement = containerElement;
        this.itemViewModel = itemViewModel;
        this.collection = new Backbone.Collection();
        this.listenTo(this.collection, 'change:color', this.updatePreview);

        const sceneUri = globalConfig.scenes[itemViewModel.parent.model.id - 1].designAreaSceneUri;
        this.encodedUri = window.encodeURIComponent(sceneUri);

        this.promise = new Promise((resolve, reject) => {
            this.resolve = resolve;
            this.reject = reject;
        });
    },

    stopPropagation: function(e) {
        e.stopPropagation();
    },

    open: function() {
        getXtiThreadColors(this.xtiUrl())
            .then(translateXtiThreads)
            .then(buildModelCollection.bind(this))
            .then(() => {
                this.$el.html(embroideryModificationView({ source: this.xtiUrl() }));

                this.collection.models.forEach(model => this.$('.dcl-modal__embroidery-thread-list').append(
                        new ThreadButtonView({ model }).render().el
                ));

                const view = new EmbroideryPreview({ model: this.itemViewModel });
                this.$('.dcl-modal__embroidery-preview-image').prepend(view.render().el);
                const width = this.$('.dcl-modal__embroidery-preview-image').width();
                const sceneUrl = `https://rendering.documents.cimpress.io/v1/vp/preview?width=${width}&scene=${this.encodedUri}`;

                this.$('.dcl-modal__embroidery-preview-image').css({
                    backgroundImage: `url(${sceneUrl})`,
                    backgroundRepeat: 'no-repeat'
                });
            });

        $(this.containerElement).append(this.el);

        return this.promise;
    },

    xtiUrl: function() {
        return this.itemViewModel.model.get('xtiUrl');
    },

    resetColors: function() {
        this.collection.models.forEach(model => model.set({ color: model.get('originalColor') }));
    },

    cancelChanges: function() {
        this.reject();
        this.remove();
    },

    updateColors: function() {
        this.resolve(getColorOverrides.call(this));
        this.remove();
    },

    updatePreview: function() {
        this.itemViewModel.model.set({ colorOverrides: getColorOverrides.call(this) });
    }
});
