const Backbone = require('backbone');
const documentRepository = require('DocumentRepository');
const commandDispatcher = require('CommandDispatcher');
const defaultTemplate = require('ui/widgets/templates/text/AddTextField.tmp');

module.exports = Backbone.View.extend({
    className: 'dcl-add-text',

    events: {
        'click button': 'onAdd'
    },

    initialize: function({ template = defaultTemplate }) {
        this.template = template;
    },

    onAdd: function() {
        const viewModel = documentRepository.getActiveCanvasViewModel();
        commandDispatcher.createTextField({
            viewModel,
            attributes: {
                writingMode: 'horizontal-tb',
                innerHTML: ''
            }
        });
    },

    render: function() {
        this.$el.html(this.template());
        return this;
    }
});
