const _ = require('underscore');
const Backbone = require('backbone');
const defaultTemplate = require('ui/widgets/templates/text/EditTextField.tmp');
const commandDispatcher = require('CommandDispatcher');
const selectionManager = require('SelectionManager');
const localization = require('Localization');

const TIMEOUT_DURATION = 100;

module.exports = Backbone.View.extend({
    className: 'dcl-edit-text__item',

    events: {
        'change input': 'onTextChange',
        'keyup input': 'onTextChange',
        'paste input': 'onTextChange',
        'mouseup input': 'onTextChange',
        'keydown input': 'stopPropagation',
        'focus input': 'onFocus',
        'click .dcl-edit-text__button-delete': 'onDelete',
        'touchstart .dcl-edit-text__button-delete': 'onDelete'
    },

    initialize: function({ template, viewModel }) {
        this.template = template || defaultTemplate;
        this.viewModel = viewModel;

        this.listenTo(this.model, 'change:innerHTML', this.updateText);
        this.listenTo(this.viewModel, 'destroy', this.destroySelf);
        this.listenTo(this.viewModel.parent, 'change:enabled', this.toggleEnabled);
        this.listenTo(this.viewModel.parent, 'change:hidden', this.toggleVisible);

        this.setText = _.throttle(this.setText.bind(this), TIMEOUT_DURATION, { leading: false });

        this.toggleEnabled();
        this.toggleVisible();
    },

    render: function() {
        const modelJson = this.model.toJSON();
        modelJson.placeholder = localization.getText(`text.${this.model.get('placeholder')}`);
        this.$el.html(this.template(modelJson));
        this.delegateEvents();
        return this;
    },

    onDelete: function() {
        if (this.viewModel.parent.get('enabled')) {
            commandDispatcher.delete({ viewModel: this.viewModel });
        }
    },

    onFocus: function() {
        selectionManager.select([this.viewModel]);
    },

    onTextChange: function(e) {
        this.stopPropagation(e);
        this.setText();
    },

    setText: function() {
        commandDispatcher.changeAttributes({ viewModel: this.viewModel, attributes: { innerHTML: this.$('input').val() } });
    },

    stopPropagation: function(e) {
        e.stopPropagation();
    },

    updateText: function() {
        const text = this.model.get('innerHTML');
        const $input = this.$('input');
        if (text !== $input.val()) {
            $input.val(text);
        }
    },

    toggleEnabled: function() {
        const enabled = this.viewModel.parent.get('enabled');
        this.$el.toggleClass('dcl-edit-text__item--canvas-enabled', enabled);
        this.$('input').prop('readonly', !enabled);
    },

    toggleVisible: function() {
        this.$el.toggleClass('dcl-edit-text__item--canvas-visible', !this.viewModel.parent.get('hidden'));
    },

    destroySelf: function() {
        this.remove();
    }
});
