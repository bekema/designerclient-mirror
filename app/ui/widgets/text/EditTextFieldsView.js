const _ = require('underscore');
const Backbone = require('backbone');
const documentRepository = require('DocumentRepository');
const EditTextFieldView = require('ui/widgets/text/EditTextFieldView');

// Private method for sorting, canvas id takes priority
const getViewSortRank = function(view) {
    return -view.model.get('zIndex') + (view.viewModel.parent.model.get('id') * 10000); // eslint-disable-line no-magic-numbers
};

module.exports = Backbone.View.extend({
    className: 'dcl-edit-text',

    initialize: function({ childTemplate }) {
        this.childTemplate = childTemplate;
        this.collection = new Backbone.Collection();
        this.listenTo(this.collection, 'remove', this.onRemove.bind(this));
        this.listenTo(this.collection, 'model:change:zIndex', this.sort.bind(this));
        const itemViewModels = documentRepository.getAllItemViewModelsCollection();
        itemViewModels.forEach(itemViewModel => {
            this.newTextField(itemViewModel);
        });
        this.listenTo(itemViewModels, 'add', this.newTextField.bind(this));
        this.textViews = [];
    },

    render: function() {
        this.textViews.forEach(view => {
            this.$el.append(view.render().el);
        });
        return this;
    },

    onRemove: function(itemViewModel) {
        const index = _.findIndex(this.textViews, textView => textView.viewModel.id === itemViewModel.id);
        if (index >= 0) {
            this.textViews.splice(index, 1);
            this.render();
        }
    },

    sort: function() {
        this.textViews = _.sortBy(this.textViews, textView => getViewSortRank(textView));
        this.render();
    },

    newTextField: function(itemViewModel) {
        if (itemViewModel.get('module') === 'TextFieldViewModel') {
            this.collection.add(itemViewModel);
            const view = new EditTextFieldView({ model: itemViewModel.model, viewModel: itemViewModel, templates: this.templates });
            const insertIndex = _.sortedIndex(this.textViews, view, textView => getViewSortRank(textView)); //
            this.textViews.splice(insertIndex, 0, view);
            this.render();
        }
    }
});
