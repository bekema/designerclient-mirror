const Backbone = require('backbone');
const $ = require('jquery');
const cropModalTemplate = require('ui/widgets/templates/CropModal.tmp');
const resize = require('Resize');
const dragAndDrop = require('DragAndDrop');
const CropModel = require('ui/models/CropModel');

/* eslint-disable no-magic-numbers */

const MAX_WIDTH_OR_HEIGHT = 300;

module.exports = Backbone.View.extend({
    className: 'dcl-modal dcl-modal--crop',

    events: {
        'click .js-modal-close' : 'cancelCrop',
        'click .dcl-cancel-crop' : 'cancelCrop',
        'click .dcl-submit-crop' : 'cropImage',
        'mousedown' : 'preventDefault'
    },

    initialize: function({ containerElement, viewModel }) {
        this.containerElement = containerElement;
        this.itemViewModel = viewModel;
        this.source = viewModel.model.get('previewUrl');
        this.deferred = $.Deferred();
        this.model = new CropModel();
        this.listenTo(this.model, 'change:crop', this.drawRect);
    },

    preventDefault: function(e) {
        e.preventDefault();
        e.stopPropagation();
    },

    cropImage: function() {
        this.deferred.resolve(this.cropPixelsToPercentage(this.model.get('crop')));
        this.remove();
    },

    cancelCrop: function() {
        this.deferred.reject();
        this.remove();
    },

    drawRect: function() {
        this.ctx.fillRect(0, 0, this.maxWidth, this.maxHeight);
        const crop = this.model.get('crop');
        this.ctx.clearRect(crop.left, crop.top, this.maxWidth - crop.left - crop.right, this.maxHeight - crop.top - crop.bottom);
    },

    open: function() {
        this.$el.html(cropModalTemplate({ source: this.source }));
        $(this.containerElement).append(this.el);
        const $image = this.$('.dcl-full-crop-image');

        $image.load(() => {
            const naturalWidth = $image[0].naturalWidth;
            const naturalHeight = $image[0].naturalHeight;
            if (naturalWidth > naturalHeight) {
                this.maxWidth = MAX_WIDTH_OR_HEIGHT;
                this.maxHeight = MAX_WIDTH_OR_HEIGHT * (naturalHeight / naturalWidth);
            } else {
                this.maxHeight = MAX_WIDTH_OR_HEIGHT;
                this.maxWidth = MAX_WIDTH_OR_HEIGHT * (naturalWidth / naturalHeight);
            }
            $image.css({ height: this.maxHeight, display: 'block' });
            this.$('.dcl-faded-image-canvas').attr({ width: this.maxWidth, height: this.maxHeight });
            this.ctx = this.$('.dcl-faded-image-canvas')[0].getContext('2d');
            this.ctx.fillStyle = '#ffffff';
            const pixelCrop = this.cropPercentageToPixels(this.itemViewModel.model.get('crop'));

            this.model.set({
                crop: pixelCrop
            });

            this.$('.dcl-resize-crop-area').css({
                top: pixelCrop.top,
                left: pixelCrop.left,
                width: this.maxWidth - pixelCrop.left - pixelCrop.right,
                height: this.maxHeight - pixelCrop.top - pixelCrop.bottom
            });

            this.addResizable();
            this.addDragAndDroppable();
        });

        return this.deferred.promise();
    },

    addResizable: function() {
        this.resizable = new resize.Resizable(
            this.$('.dcl-resize-crop-area'), {
                handles: this.model.get('handles'),
                maintainProportions: this.model.get('maintainProportions'),
                containResize: true,
                maxWidth: this.maxWidth,
                maxHeight: this.maxHeight,
                resize: this.onResize.bind(this)
            }
        );
    },

    addDragAndDroppable: function() {
        this.draggable = new dragAndDrop.Draggable(
            this.$('.dcl-resize-crop-area'), {
                selectorToIgnore: '.dcl-ui-resizable-handle, .dcl-ui-resizable-resizing',
                appendTo: function() { return '.dcl-resize-crop-area'; },
                start: this.startDrag.bind(this),
                drag: this.onDrag.bind(this),
                updateHelper: function(options) {
                    options.helper.css({
                        'top': options.position.top,
                        'left': options.position.left
                    });
                },
                containDrag: true,
                deleteHelper: false,
                maxWidth: this.maxWidth,
                maxHeight: this.maxHeight
            }
        );
    },

    cropPercentageToPixels: function(cropPercentage) {
        return {
            top: cropPercentage.top * this.maxHeight,
            left: cropPercentage.left * this.maxWidth,
            right: cropPercentage.right * this.maxWidth,
            bottom: cropPercentage.bottom * this.maxHeight
        };
    },

    cropPixelsToPercentage: function(cropPixels) {
        return {
            top: cropPixels.top / this.maxHeight,
            left: cropPixels.left / this.maxWidth,
            right: cropPixels.right / this.maxWidth,
            bottom: cropPixels.bottom / this.maxHeight
        };
    },

    startDrag: function() {
        const crop = this.model.get('crop');
        this.draggable.width = this.maxWidth - crop.left - crop.right;
        this.draggable.height = this.maxHeight - crop.top - crop.bottom;
    },

    onDrag: function(e, ui) {
        const previousCrop = this.model.get('crop');
        const top = ui.position.top;
        const left = ui.position.left;
        const right = previousCrop.left + previousCrop.right - ui.position.left;
        const bottom = previousCrop.top + previousCrop.bottom - ui.position.top;

        this.model.set({
            crop: {
                top,
                left,
                right,
                bottom
            }
        });
    },

    onResize: function(e, ui) {
        const top = ui.position.top;
        const left = ui.position.left;
        const right = this.maxWidth - ui.position.left - ui.size.width;
        const bottom = this.maxHeight - ui.position.top - ui.size.height;

        this.model.set({
            crop: {
                top,
                left,
                right,
                bottom
            }
        });
        return  {
            handlePosition: {
                left: ui.position.left,
                top: ui.position.top
            },
            handleSize: {
                width: ui.size.width,
                height: ui.size.height
            }
        };
    }
});
