const Backbone = require('backbone');

module.exports = Backbone.View.extend({

    initialize: function() {
        this.listenTo(this.model, 'destroy', this.onDestroy);
    },

    onDestroy: function() {
        this.remove();
    }

});
