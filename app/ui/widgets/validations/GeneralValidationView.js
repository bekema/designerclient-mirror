const ValidationView = require('ui/widgets/validations/ValidationView');
const generalValidationTemplate = require('ui/widgets/templates/validations/GeneralValidation.tmp');

const VISIBLE_TIME = 6;

module.exports = ValidationView.extend({
    className: 'dcl-general-validation',

    initialize: function({ template }) {
        ValidationView.prototype.initialize.apply(this, arguments);
        this.template = template || generalValidationTemplate;
    },

    render: function() {
        this.$el.append(this.template(this.model.toJSON()));
        this.$el.addClass(`dcl-general-validation--${this.model.get('severity')}`);
        setTimeout(this.fadeOut.bind(this), VISIBLE_TIME * 1000); //eslint-disable-line no-magic-numbers
        return this;
    },

    fadeOut: function() {
        this.remove();
    }

});
