const $ = require('jquery');
const ValidationView = require('ui/widgets/validations/ValidationView');
const contextualValidationTemplate = require('ui/widgets/templates/validations/ContextualValidation.tmp');
const eventBus = require('EventBus');
const events = eventBus.events;
const selectionManager = require('SelectionManager');

module.exports = ValidationView.extend({
    className: 'dcl-contextual-validation',

    initialize: function({ template }) {
        ValidationView.prototype.initialize.apply(this, arguments);
        this.template = template || contextualValidationTemplate;
        eventBus.on(events.windowResize, this.position.bind(this));
        this.listenTo(this.model.get('data').itemViewModel, 'change:handleBoundingBox', this.position);
        this.listenTo(selectionManager, 'select', this.onSelection);
        this.position();
    },

    // All of this assumes it is attached to an itemViewModel
    position: function() {
        const location = this.model.get('location');
        const itemViewModel = this.model.get('data').itemViewModel;
        const containerPosition = $(`#${itemViewModel.parent.id}`).offset();
        const itemPosition = itemViewModel.get('handleBoundingBox');
        const itemSize = itemViewModel.get('handleSize');
        let top = containerPosition.top + itemPosition.top;
        let left = containerPosition.left + itemPosition.left;
        if (location.x || location.x === 0) {
            left += location.x;
        } else {
            left += (itemSize.width / 2);
        }
        if (location.y || location.y === 0) {
            top += location.y;
        } else {
            top += (itemSize.height / 2);
        }
        this.$el.css({ display: 'block', top: `${top}px`, left: `${left}px` });
    },

    //TODO this assumes itemViewModel for contextual validations. Once there are other types, alter this.
    onSelection: function(itemViewModels) {
        const viewModel = this.model.get('data').itemViewModel;
        if (itemViewModels.some(vm => vm.id === viewModel.id)) {
            this.$el.show();
        } else {
            this.$el.hide();
        }
    },

    render: function() {
        this.$el.append(this.template(this.model.toJSON()));
        this.$el.addClass(`dcl-contextual-validation--${this.model.get('severity')}`);
        return this;
    }

});
