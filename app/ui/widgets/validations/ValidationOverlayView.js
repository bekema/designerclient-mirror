const $ = require('jquery');
const Backbone = require('backbone');
const validationManager = require('validations/ValidationManager');
const ContextualValidationView = require('ui/widgets/validations/ContextualValidationView');
const GeneralValidationView = require('ui/widgets/validations/GeneralValidationView');
const validationOverlayTemplate = require('ui/widgets/templates/validations/ValidationOverlay.tmp');
const eventBus = require('EventBus');
const events = eventBus.events;
const uiConfig = require('publicApi/configuration/ui');
const validationConfig = require('publicApi/configuration/validations');

module.exports = Backbone.View.extend({
    className: 'dcl-validation-overlay',

    events: {
        'mousedown' : 'preventDefault',
        'touchstart': 'preventDefault'
    },

    initialize: function({ templates }) {
        this.templates = templates || {};
        this.collection = new Backbone.Collection();
        this.listenTo(validationManager, 'add', this.newValidation);
        this.listenTo(this.collection, 'add', this.onAdd);
        eventBus.on(events.windowResize, this.positionGeneralValidations.bind(this));
    },

    //Assumed itemViewModels are contextual and everything else is general.
    onAdd: function(validation) {
        if (validation.get('data').itemViewModel) {
            const view = new ContextualValidationView({ model: validation, template: this.templates.contextual });
            this.$('.dcl-validation-overlay__contextual').append(view.render().el);
        } else {
            const view = new GeneralValidationView({ model: validation, template: this.templates.general });
            this.$('.dcl-validation-overlay__general').append(view.render().el);
        }
    },

    positionGeneralValidations: function() {
        const containerRect = $(uiConfig.canvas.container)[0].getBoundingClientRect();
        const css = { left: containerRect.left, top: containerRect.bottom, width: containerRect.width };
        this.$('.dcl-validation-overlay__general').css(css);
    },

    newValidation: function(validation) {
        const name = validation.get('name');
        if (validationConfig[name] && validationConfig[name].overlay) {
            this.collection.add(validation);
        }
    },

    render: function() {
        this.$el.append(validationOverlayTemplate());
        this.positionGeneralValidations();
        return this;
    },

    preventDefault: function(e) {
        e.stopPropagation();
    }

});
