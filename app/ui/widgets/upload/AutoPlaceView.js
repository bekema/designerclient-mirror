const Backbone = require('backbone');
const eventBus = require('EventBus');
const events = eventBus.events;

module.exports = Backbone.View.extend({

    className: 'dcl-autoplace',

    events: {
        'click .auto-place' : 'autoPlace'
    },

    initialize: function({ model, template, strategy }) {
        this.template = template;
        this.model = model;
        this.strategy = strategy;
    },

    render: function() {
        return this.$el.html(this.template);
    },

    autoPlace: function() {
        eventBus.trigger(events.autoPlace, { model: this.model, strategy: this.strategy });
    }
});
