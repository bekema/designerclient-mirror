const Backbone = require('backbone');
const UploadView = require('ui/widgets/upload/UploadView');
const clientProvider = require('services/clientProvider');
const UploadCollection = require('ui/models/UploadCollection');
const eventBus = require('EventBus');
const events = eventBus.events;

module.exports = Backbone.View.extend({
    className: 'dcl-upload-list',
    initialize: function({ templates }) {
        this.templates = templates;
        this.collection = new UploadCollection();
        this.uploadViews = [];
        this.collection.each((uploadModel) => {
            this.uploadViews.push(new UploadView({ model: uploadModel, templates }));
        });
        this.listenTo(this.collection, 'add', this.onAdd);
        this.listenTo(this.collection, 'remove', this.render);
        eventBus.on(events.uploadStarted, this.newUpload, this);
        this.loadPreviousUploads();
    },

    render: function() {
        this.$el.empty();
        this.uploadViews.forEach(view => {
            this.$el.prepend(view.render().el);
        });

        eventBus.trigger(events.uploadRendered);
        return this;
    },

    loadPreviousUploads: function() {
        clientProvider.account.getUploads()
            .then(storedUploads => clientProvider.upload.retrieveUploads(storedUploads))
            .then(retrievedUploads => this.collection.add(retrievedUploads));
    },

    onAdd: function(model) {
        const view = new UploadView({ model, templates: this.templates });
        this.uploadViews.push(view);
        this.$el.prepend(view.render().el);
    },

    newUpload: function(upload) {
        this.collection.add(upload);
    }
});
