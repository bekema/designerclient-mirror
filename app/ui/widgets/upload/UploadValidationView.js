const $ = require('jquery');
const Backbone = require('backbone');
const defaultTemplate = require('ui/widgets/templates/upload/ValidationsPopover');

const hasUnacknowledgedValidations = function(validations) {
    return validations !== undefined && !validations.every(validation => validation.get('acknowledged'));
};

const acknowledgeValidations = function(validations) {
    validations.each(validation => validation.set('acknowledged', true));
};

module.exports = Backbone.View.extend({
    className: '',

    events: {
        'click' : 'showValidations'
    },

    initialize({ template = defaultTemplate }) {
        this.template = template;
        this.listenTo(this.collection, 'change add remove', this.render);
    },

    render() {
        if (hasUnacknowledgedValidations(this.collection)) {
            this.updateValidations();
        } else {
            this.hideValidations();
        }
    },

    showValidations(e) {
        e.preventDefault();
        e.stopPropagation();

        if (!hasUnacknowledgedValidations(this.collection)) {
            return;
        }

        this.$el.closest('.uploaded').popover('toggle');
    },

    updateValidations() {
        this.$el.fadeIn();
        const validations = this.collection.map(validation => validation.toJSON());
        const $popoverContent = $(this.template({ validations }));
        $popoverContent.find('.close').on('click', () => this.$el.closest('.uploaded').popover('hide'));
        $popoverContent.find('.dismiss').on('click', () => acknowledgeValidations(this.collection));
        this.$el.closest('.uploaded').popover({
            title: 'Upload failed validations',
            placement: 'right',
            content: $popoverContent,
            html: true,
            container: 'body',
            trigger: 'manual'
        });
    },

    hideValidations() {
        this.$el.fadeOut();
        //this.$el.closest('.uploaded').popover('destroy');
    }
});
