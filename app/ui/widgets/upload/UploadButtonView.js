const $ = require('jquery');
const Button = require('ui/tools/Button');
const defaultTemplate = require('ui/widgets/templates/upload/UploadButton');

module.exports = Button.extend({
    className: 'dcl-upload-button',

    initialize: function({ input, template = defaultTemplate }) {
        Button.prototype.initialize.call(this, { input, template });
    },

    onClick: function() {
        $(this.options.input).trigger('click');
    }
});
