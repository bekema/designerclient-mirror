const Backbone = require('backbone');
const dragAndDrop = require('DragAndDrop');
const documentRepo = require('DocumentRepository');
const UploadValidationView = require('ui/widgets/upload/UploadValidationView');
const AutoPlaceView = require('ui/widgets/upload/AutoPlaceView');
const featureConfig = require('publicApi/configuration/features');
const AutoPlaceStrategies = require('strategies/AutoPlaceStrategies');
const defaultAutoPlaceTemplate = require('ui/widgets/templates/upload/AutoPlace');
const defaultCompleteTemplate = require('ui/widgets/templates/upload/UploadedImage.tmp');
const defaultInProgressTemplate = require('ui/widgets/templates/upload/UploadingImage.tmp');
const itemConfig = require('publicApi/configuration/core/items').image;
const eventBus = require('EventBus');
const events = eventBus.events;

module.exports = Backbone.View.extend({
    className: 'dcl-upload-list__item',

    events: {
        'click .delete' : 'deleteUpload',
        'dblclick .image-wrapper' : 'autoPlace'
    },

    initialize({ templates }) {
        this.inProgressTemplate = templates.uploadInProgress || defaultInProgressTemplate;
        this.completeTemplate = templates.uploadComplete || defaultCompleteTemplate;
        this.validationTemplate = templates.validation;

        if (featureConfig.autoPlace) {
            this.autoPlaceTemplate = templates.autoPlace || defaultAutoPlaceTemplate;
            let strategy = featureConfig.autoPlace.strategy;

            if (typeof strategy === 'string') {
                strategy = AutoPlaceStrategies[strategy];
            }

            if (!strategy) {
                throw Error(`Provided auto place strategy '${featureConfig.autoPlace.strategy}' valid options are: ${Object.getOwnPropertyNames(AutoPlaceStrategies)}`);
            }

            this.autoPlaceStrategy = strategy;
        }

        this.updatePathLoading(0);

        this.listenTo(this.model, 'change:uploadComplete', this.render);
        this.listenTo(this.model, 'change:percentComplete', this.showProgress);
        this.listenTo(this.model, 'change:state', this.updateState);
        eventBus.on(events.uploadDelete, this.destroySelfById, this);
        eventBus.on(events.uploadEmbroideryProcessComplete, this.updateModel, this);
    },

    updateModel: function(model) {
        if (model.get('requestId') === this.model.get('requestId')) {
            this.model.get('pages')[0].digitizedId = model.get('pages')[0].digitizedId;
            this.model.get('pages')[0].xtiUrl = model.get('pages')[0].xtiUrl;
            this.render();
        }
    },

    render() {
        if (this.model.get('uploadComplete')) {
            this.$el.html(this.completeTemplate(this.model.toJSON()));
            const $images = this.$('.image-wrapper img');

            this.model.get('pages').map((upload, index) => {
                const $img = $images.eq(index);
                this.addDraggable($img, upload, this.model.get('fileType'));
            });
            this.validationView = new UploadValidationView({ collection: this.model.get('validations'), el: this.$('.actions .validation'), template: this.validationTemplate });
            this.validationView.render();

            if (this.autoPlaceStrategy) {
                this.autoPlaceView = new AutoPlaceView({ model: this.model, template: this.autoPlaceTemplate, strategy: this.autoPlaceStrategy });
                this.$('.panel-collapse').append(this.autoPlaceView.render());
            }
        } else {
            this.$el.html(this.inProgressTemplate({
                fileName: this.model.get('file').name,
                requestId: this.model.get('requestId'),
                percentComplete: this.model.get('percentComplete'),
                message: this.model.get('state')
            }));
            this.showThumbnail(this.model.get('file'));
        }
        this.delegateEvents();
        return this;
    },

    autoPlace: function() {
        eventBus.trigger(events.autoPlace, { model: this.model, strategy: this.autoPlaceStrategy });
    },

    showProgress() {
        const percentComplete = this.model.get('percentComplete');

        this.$('.dcl-uploading-percentage').text(Math.round(percentComplete) + '%');
        this.updatePathLoading(percentComplete);
    },

    updatePathLoading: function(percentComplete) {
        this.$('.dcl-path-loading').attr('stroke-dashoffset', this.calcDashOffset(percentComplete));
    },

    calcDashOffset: function(val) {
        const completePercentage = 100;
        const r = this.$('.dcl-path-loading').attr('r');
        const c = Math.PI * (r * 2);

        if (val < 0) { val = 0; }
        if (val > completePercentage) { val = completePercentage; }

        const dashOffsetValue = ((completePercentage - val) / completePercentage) * c;

        return dashOffsetValue;
    },

    updateState() {
        this.$('.dcl-image-process-message').text(this.model.get('state'));
    },

    deleteUpload() {
        eventBus.trigger(events.uploadDelete, this.model.get('requestId'));
        this.destroySelf();
    },

    destroySelf() {
        this.remove();
    },

    destroySelfById(requestId) {
        if (this.model.get('requestId') === requestId) {
            this.remove();
        }
    },

    showThumbnail(original) {
        this.$('.js-uploading-thumb img').attr('src', URL.createObjectURL(original));
    },

    addDraggable($img, upload, fileType) {
        $img.on('load', () => {
            this.draggable = new dragAndDrop.Draggable($img, {
                data: { image: upload, fileType },
                helper: function() {
                    const $clone = $img.clone().addClass('dragging-repository-image');
                    const canvasViewModel = documentRepo.getActiveCanvasViewModel();
                    const safeArea = canvasViewModel.get('zoomedSafeArea');

                    const canvasAspectRatio = safeArea.width / safeArea.height;
                    const aspectRatio = $img.width() / $img.height();

                    let width, height;

                    if (canvasAspectRatio > 1 && aspectRatio > 1) {
                        // image and canvas are wide - make it as tall as possible
                        height = safeArea.height * itemConfig.initialSize;
                        width = height * aspectRatio;
                        if (width > safeArea.width) {
                            width = safeArea.width;
                            height = width / aspectRatio;
                        }
                    } else if (canvasAspectRatio <= 1 && aspectRatio <= 1) {
                        // image and canvas are tall - make it as wide as possible
                        width = safeArea.width * itemConfig.initialSize;
                        height = width / aspectRatio;
                        if (height > safeArea.height) {
                            height = safeArea.height;
                            width = height * aspectRatio;
                        }
                    } else {
                        const maxSize = Math.min(safeArea.width, safeArea.height);
                        width = maxSize * itemConfig.initialSize * (aspectRatio > 1 ? 1 : aspectRatio);
                        height = maxSize * itemConfig.initialSize * (aspectRatio > 1 ? 1 / aspectRatio : 1);
                    }

                    /* eslint-disable no-magic-numbers */
                    $clone.css({
                        width: `${width}px`,
                        height: `${height}px`
                    });

                    return $clone;
                },
                updateHelper: function(options) {
                    const height = options.helper.height();
                    const width = options.helper.width();
                    const top = options.inputPosition.top - height / 2;
                    const left = options.inputPosition.left - width / 2;
                    options.helper.css('transform', `translate(${left}px, ${top}px)`);
                }
            });
        });
    }
});
