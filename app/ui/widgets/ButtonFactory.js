const _ = require('underscore');
const cropButton = require('ui/tools/buttons/CropButton');
const deleteButton = require('ui/tools/buttons/DeleteButton');
const textBoldButton = require('ui/tools/buttons/TextBoldButton');
const textItalicButton = require('ui/tools/buttons/TextItalicButton');
const textUnderlineButton = require('ui/tools/buttons/TextUnderlineButton');
const textStrikeoutButton = require('ui/tools/buttons/TextStrikeoutButton');
const textLeftAlignButton = require('ui/tools/buttons/TextLeftAlignButton');
const textCenterAlignButton = require('ui/tools/buttons/TextCenterAlignButton');
const textRightAlignButton = require('ui/tools/buttons/TextRightAlignButton');
const textSizeDropDownList = require('ui/tools/dropDowns/TextSizeDropDownList');
const textFamilyDropDownList = require('ui/tools/dropDowns/TextFamilyDropdownList');
const strokeWidthDropDownList = require('ui/tools/dropDowns/StrokeWidthDropDownList');
const rotateLeftButton = require('ui/tools/buttons/RotateLeftButton');
const rotateRightButton = require('ui/tools/buttons/RotateRightButton');
const textEditField = require('ui/tools/buttons/TextEditField');
const colorPicker = require('ui/tools/buttons/ColorPickerButton');
const crispifyButton = require('ui/tools/buttons/crispifyButton');
const fillColorPicker = require('ui/tools/buttons/FillColorPickerButton');
const embroideryColorsButton = require('ui/tools/buttons/EmbroideryColorsButton');
const strokeColorPicker = require('ui/tools/buttons/StrokeColorPickerButton');
const bringForwardButton = require('ui/tools/buttons/BringForwardButton');
const sendBackwardButton = require('ui/tools/buttons/SendBackwardButton');
const bringToFrontButton = require('ui/tools/buttons/BringToFrontButton');
const sendToBackButton = require('ui/tools/buttons/SendToBackButton');
const hoverButton = require('ui/tools/HoverButton');

const availableButtons = [cropButton, deleteButton, textBoldButton, textItalicButton, textUnderlineButton,
                        textStrikeoutButton, textLeftAlignButton, textCenterAlignButton, textRightAlignButton,
                        rotateLeftButton, rotateRightButton, textSizeDropDownList, textEditField, textFamilyDropDownList,
                        colorPicker, crispifyButton, strokeWidthDropDownList, fillColorPicker, strokeColorPicker,
                        embroideryColorsButton, bringForwardButton, sendBackwardButton, bringToFrontButton, sendToBackButton, hoverButton];

// each button is an object whose name references a default button, or a passed in constructor
const buildButton = function(Button, parent) {
    let FoundButton;
    let options = { parent };

    if (_.isFunction(Button)) {
        // Assumed to be a constructor function
        FoundButton = Button;
    } else if (_.isObject(Button)) {
        // An object which contains the name of a default button and options for the button
        FoundButton = _.findWhere(availableButtons, { tag: Button.name });
        options = _.extend(options, Button);
        if (options.childViewOptions && options.childViewOptions.Buttons) {
            // This is a recursive build, build the childs buttons as well
            options.childViewOptions.buttons = buildButtons(options.childViewOptions.Buttons); // eslint-disable-line
        }
    } else if (_.isString(Button)) {
        // The name of a default button
        FoundButton = _.findWhere(availableButtons, { tag: Button });
    }

    if (FoundButton) {
        return new FoundButton(options);
    }

    throw new Error(`Button ${Button} was unable to be found`);
};

const buildButtons = function(Buttons, parent) {
    return Buttons.map(Button => buildButton(Button, parent));
};

module.exports = { buildButtons, buildButton };