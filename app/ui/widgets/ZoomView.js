const Backbone = require('backbone');
const defaultTemplate = require('ui/widgets/templates/Zoom.tmp');
const eventBus = require('EventBus');
const events = eventBus.events;

/* eslint-disable no-magic-numbers */

module.exports = Backbone.View.extend({
    className: 'dcl-zoom',

    events: {
        'click .js-zoom-in' : 'zoomIn',
        'click .js-zoom-out' : 'zoomOut'
    },

    initialize: function({ template = defaultTemplate }) {
        this.template = template;
    },

    zoomIn: function() {
        eventBus.trigger(events.zoomIn);
    },

    zoomOut: function() {
        eventBus.trigger(events.zoomOut);
    },

    // TODO perhaps use this for a slider
    setZoom: function(zoomAmount) {
        eventBus.trigger(events.zoomSet, zoomAmount);
    },

    render: function() {
        this.$el.html(this.template());
        return this;
    }
});
