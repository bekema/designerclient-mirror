const Backbone = require('backbone');
const defaultTemplate = require('ui/widgets/templates/changeScene/ChangeScene.tmp');
const eventBus = require('EventBus');
const events = eventBus.events;

module.exports = Backbone.View.extend({
    className: 'dcl-change-scene__item',

    events: {
        'click .dcl-change-scene__item': 'onClick'
    },

    initialize: function({ template, viewModel }) {
        this.template = template || defaultTemplate;
        this.viewModel = viewModel;

        //TODO finish this widget (ticket for this)
        this.listenTo(this.viewModel, 'destroy', this.destroySelf);
        this.listenTo(this.viewModel, 'change:enabled', this.toggleEnabled);
        this.listenTo(this.viewModel, 'change:hidden', this.toggleVisible);

        this.toggleEnabled();
        this.toggleVisible();
    },

    render: function() {
        const modelJson = this.model.toJSON();
        this.$el.html(this.template(modelJson));
        this.delegateEvents();
        return this;
    },

    onClick: function() {
        if (!this.viewModel.get('enabled')) {
            const id = this.model.get('id');
            eventBus.trigger(events.manageCanvases, { enabledCanvases: [id], visibleCanvases: [id], activeCanvas: id });
        }
    },

    toggleEnabled: function() {
        this.$el.toggleClass('dcl-change-scene__item--enabled', this.viewModel.get('enabled'));
    },

    toggleVisible: function() {
        this.$el.toggleClass('dcl-change-scene__item--visible', !this.viewModel.get('hidden'));
    },

    destroySelf: function() {
        this.remove();
    }
});
