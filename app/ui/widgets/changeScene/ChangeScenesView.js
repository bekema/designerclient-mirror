const Backbone = require('backbone');
const documentRepository = require('DocumentRepository');
const ChangeSceneView = require('ui/widgets/changeScene/ChangeSceneView');

module.exports = Backbone.View.extend({
    className: 'dcl-change-scene',

    initialize: function({ childTemplate }) {
        this.childTemplate = childTemplate;
        this.collection = new Backbone.Collection();
        this.listenTo(this.collection, 'remove', this.render.bind(this));
        const canvasViewModels = documentRepository.getViewModelRepository();
        this.listenTo(canvasViewModels, 'add', this.newScene.bind(this));
        this.sceneViews = [];
    },

    render: function() {
        this.sceneViews.forEach(view => {
            this.$el.append(view.render().el);
        });
        return this;
    },

    newScene: function(canvasViewModel) {
        this.collection.add(canvasViewModel);
        const view = new ChangeSceneView({ model: canvasViewModel.model, viewModel: canvasViewModel, templates: this.templates });
        this.sceneViews.push(view);
        this.render();
    }
});
