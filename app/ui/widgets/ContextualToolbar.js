const Backbone = require('backbone');
const $ = require('jquery');
const selectionManager = require('SelectionManager');
const contextualToolbarTemplate = require('ui/widgets/templates/ContextualToolbar.tmp');
const ContextualToolbarModel = require('ui/models/ContextualToolbarModel');
const buttonFactory = require('ui/widgets/ButtonFactory');

/* eslint-disable no-magic-numbers */

module.exports = Backbone.View.extend({
    className: 'dcl-contextual-toolbar dcl-invisible',

    events: {
        'mousedown' : 'preventDefault',
        'touchstart': 'preventDefault'
    },

    initialize: function(options) {
        this.model = new ContextualToolbarModel({
            padding: options.padding,
            buttons: buttonFactory.buildButtons(options.Buttons, this),
            positionBelow: options.positionBelow
        });

        this.listenTo(selectionManager, 'item:focus dragstop resizestop rotatestop add', this.show);
        this.listenTo(selectionManager, 'remove', this.onSelectionRemove);
        this.listenTo(selectionManager, 'dragstart resizestart rotatestart', this.hide);
        this.listenTo(selectionManager, 'change:handleBoundingBox', this.positionToolbar);
    },

    render: function() {
        this.$el.html(contextualToolbarTemplate());
        this.model.get('buttons').forEach(button => {
            if (button.isPrimaryItem()) {
                this.$('.dcl-contextual-toolbar-primary-row').append(button.render().el);
            } else {
                this.$('.dcl-contextual-toolbar-secondary-row').append(button.render().el);
            }
            button.hide();
        });
        return this;
    },

    preventDefault: function(e) {
        e.stopPropagation();
    },

    onSelectionRemove: function() {
        if (selectionManager.length === 0) {
            this.hide();
        } else {
            this.show();
        }
    },

    emptyToolbar: function() {
        this.model.get('buttons').forEach(button => button.hide());
    },

    positionToolbar: function() {
        //Reset its position before positioning
        this.$el.css({
            left: 0,
            top: 0
        });

        const itemViewModel = selectionManager.first();
        if (!itemViewModel) {
            return null;
        }
        const boundingBox = itemViewModel.get('handleBoundingBox');
        if (!boundingBox) {
            return null;
        }
        const $canvasElement = $(`#${itemViewModel.parent.id}`);
        const canvasOffset = $canvasElement.offset();
        const itemWidth = boundingBox.width;
        const itemHeight = boundingBox.height;
        let itemTop = boundingBox.top;
        const itemLeft = boundingBox.left;
        const width = this.$el.outerWidth(true);
        const height = this.$el.outerHeight(true);
        const windowWidth = $(window).width();
        const windowHeight = $(window).height();
        const rotation = itemViewModel.model.get('rotation');

        if (rotation < 220 && rotation > 140) {
            itemTop -= 10;
        }

        if (itemTop < 0) {
            itemTop = 0;
        }

        let left = itemLeft + canvasOffset.left + (itemWidth / 2) - (width / 2);
        let top;
        if (this.model.get('positionBelow')) {
            top = itemTop + canvasOffset.top + itemHeight + this.model.get('padding');
        } else {
            top = itemTop + canvasOffset.top - height - this.model.get('padding');
        }

        if (left < 0) {
            left = 0;
        }
        if (top < 0) {
            top = 0;
        }
        if (windowWidth < (left + width)) {
            left = windowWidth - width - 1;
        }
        if (windowHeight < (top + height)) {
            top = windowHeight - height;
        }
        this.model.set({ top, left });
        this.$el.css({ top, left });
    },

    populateToolbar: function() {
        this.model.get('buttons').forEach(button => button.show());
    },

    show: function() {
        this.emptyToolbar();
        this.populateToolbar();
        this.positionToolbar();
        this.$el.removeClass('dcl-invisible');
    },

    hide: function() {
        this.$el.addClass('dcl-invisible');
    }
});
