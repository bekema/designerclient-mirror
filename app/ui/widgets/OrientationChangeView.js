const Backbone = require('backbone');
const defaultTemplate = require('ui/widgets/templates/OrientationChange.tmp');
const commandDispatcher = require('CommandDispatcher');

module.exports = Backbone.View.extend({
    className: 'dcl-document-rotate',

    events: {
        'click .js-btn-doc-rotate' : 'rotateDocument'
    },

    initialize: function({ template = defaultTemplate }) {
        this.template = template;
    },

    rotateDocument: function() {
        commandDispatcher.changeDocumentOrientation();
    },

    render: function() {
        this.$el.html(this.template());
        return this;
    }
});
