const Button = require('ui/tools/Button');
const defaultTemplate = require('ui/widgets/templates/saveDocument.tmp');
const saveDocumentToUds = require('publicApi/saveDocumentToUds');
const saveDocument = require('publicApi/saveDocument');

module.exports = Button.extend({

    initialize: function({ template = defaultTemplate }) {
        Button.prototype.initialize.call(this, { template });
    },

    onClick: function() {
        this.saveDocument();
    },

    saveDocument: function() {
        const $button = this.$('button');
        $button.prop('disabled', true);
        
        saveDocumentToUds()
        .then(() => {
            saveDocument();
            $button.prop('disabled', false);
        });
    }
});
