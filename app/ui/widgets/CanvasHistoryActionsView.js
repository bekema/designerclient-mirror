const $ = require('jquery');
const Backbone = require('backbone');
const defaultTemplate = require('ui/widgets/templates/CanvasHistoryActions.tmp');
const eventBus = require('EventBus');
const events = eventBus.events;
const documentRepository = require('DocumentRepository');
const History = require('History');

module.exports = Backbone.View.extend({
    className: 'dcl-canvas-history',

    events: {
        'click .dcl-action-btn--undo' : 'undo',
        'click .dcl-action-btn--redo' : 'redo'
    },

    initialize: function({ template = defaultTemplate }) {
        this.template = template;
        this.canvasCollection = documentRepository.getViewModelRepository();

        eventBus.on(events.historyChanged, this.configureButtons);
        this.listenTo(this.canvasCollection, 'change:enabled', this.configureButtons);

        this.configureButtons();
    },

    configureButtons: function() {
        const historyState = History.currentState();
        if (historyState.index > 0) {
            $('.dcl-action-btn--undo').removeClass('dcl-action-btn__icon--disabled');
        } else {
            $('.dcl-action-btn--undo').addClass('dcl-action-btn__icon--disabled');
        }

        if (historyState.total > historyState.index) {
            $('.dcl-action-btn--redo').removeClass('dcl-action-btn__icon--disabled');
        } else {
            $('.dcl-action-btn--redo').addClass('dcl-action-btn__icon--disabled');
        }
    },

    undo: function() {
        eventBus.trigger(events.undo, this);
    },

    redo: function() {
        eventBus.trigger(events.redo, this);
    },

    render: function() {
        this.$el.html(this.template());
        return this;
    }
});
