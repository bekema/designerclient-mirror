// DCL needs to have the ability to know what designer context we are currently using (print/embroidery).
// This object will be the canolical source of that information.

require('es6-promise');

const defaultTechnology = () => 'print';

const normalizeTechnology = technology => {
    if (technology.toLowerCase() === 'embroidery') {
        return 'embroidery';
    }

    return defaultTechnology();
};

module.exports = {
    init: function({ clients, technology }) {
        this.clients = clients;
        this.designTechnology = normalizeTechnology(technology);
        this.fontFlavor = this.designTechnology;
        this.uploadContext = this.designTechnology;
        this[this.designTechnology] = true;

        return this.setup();
    },

    setup: function() {
        if (!this.embroidery) { return Promise.resolve(); }

        // Temporary update the font adapater repository url, until MCP supports embroidery fonts...
        this.clients.font.fontRepositoryUrl = 'http://fonts.documents.vistaprint.io';

        // TODO -  properly catch rejections and throw an Error in the catch handler with a description of the problem
        return this.clients.embroidery.loadThreadData();
    },

    // Function which returns the proper color property value for the context we are in
    // Example: for embroidery we want thread(MCP-THREAD-ID) but for print we want rgb(HEX)
    // parameter: hex color value - this could be expanded to be a color object with variety of values but currently we store everything on models in hex
    itemColor: function(hexColor) {
        return this.colorMap[this.designTechnology].call(this, hexColor);
    },

    filterColors: function(colors) {
        if (this.embroidery) {
            colors.forEach(color => color.thread = color.thread || this.clients.embroidery.threadForHex(color.hex));
        }

        return this.embroidery ? colors.filter(color => color.thread) : colors;
    },

    colorMap: {
        print: function(color) { return `rgb(${color})`; },
        embroidery: function(color) { return `thread(${this.clients.embroidery.threadForHex(color)})`; }
    },

    defaultTechnology
};
