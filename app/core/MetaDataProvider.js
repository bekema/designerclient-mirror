// The `MetaDataProvider` is used for constructing the appropriate view and view models for a given model. This can be
// populated from server output, or must be set up on the client to create new view-model linkages.

const _ = require('underscore');
const Backbone = require('backbone');

let allMetaData = {}; // { moduleName : { model = "modelModule", view = "viewModule", ... }
let currentProfile = 'default';

module.exports = {

    setProfile: function(profile) {
        currentProfile = profile;
    },

    getMetaData: function(model) {
        if (model instanceof Backbone.Model) {
            model = model.get('module');
        }
        return allMetaData[model];
    },

    getView: function(viewModel) {
        // First see if there is any meta data for the view model, if not, fall back to its model.
        const metaData = this.getMetaData(viewModel) || this.getMetaData(viewModel.model);
        if (!metaData) {
            throw new Error('Metadata could not be determined from view model');
        }
        const Constructor = require(`views/${metaData.view}`);
        const options = {
            viewModel: viewModel,
            profile: currentProfile
        };
        return new Constructor(options);
    },

    getViewModel: function(model, parent, viewModelAttributes) {
        const metaData = this.getMetaData(model);
        if (!metaData) {
            throw new Error('Metadata could not be determined from model');
        }
        const Constructor = require(`viewModels/${metaData.viewModel}`);
        const viewModel = new Constructor({ model: model }, { parent: parent });
        if (viewModelAttributes) {
            Object.keys(viewModel.defaults)
                  .filter(key => viewModelAttributes[key])
                  .forEach(key => viewModel.set(key, viewModelAttributes[key]));
        }
        return viewModel;
    },

    // Returns a promise that the view model, view, and model scripts will all be loaded and will be
    // ready to use once the promise has been resolved.
    registerMetaData: function(metaDataList) {
        metaDataList = Array.isArray(metaDataList) ? metaDataList : [metaDataList];
        const deps = [];

        _.each(metaDataList, function(metaData) {
            if (!metaData.model) {
                throw new Error('No model module provided in meta data');
            }

            allMetaData[metaData.model] = metaData;
            deps.push(metaData.model);
            if (metaData.view) {
                deps.push(metaData.view);
            }
            if (metaData.viewModel) {
                deps.push(metaData.viewModel);
            }
        });
    },

    // For unit testing purposes.
    clear: function() {
        allMetaData = {};
    }
};
