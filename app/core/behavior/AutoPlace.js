const Backbone = require('backbone');
const _ = require('underscore');
const commandDispatcher = require('CommandDispatcher');
const documentRepository = require('DocumentRepository');
const eventBus = require('EventBus');
const events = eventBus.events;

/* eslint-disable no-magic-numbers */

const onAutoPlace = function({ model, strategy }) {
    const upload = model.get('pages');
    const maxUpload = Math.min(upload.length, documentRepository.getAutoPlaceCanvasCount());

    for (let index = 0; index < maxUpload; index++) {
        const target = documentRepository.getNextAutoPlaceTarget(index);
        if (target === null) {
            continue;
        }

        const options = {
            image: upload[index],
            fileType: model.get('fileType'),
            viewModel: target,
            attributes: {
                zIndex: -1000, // this should be the bottom-most item
                zIndexLock: true
            },
            strategy: strategy
        };

        commandDispatcher.autoPlaceImage(options);
    }
};

module.exports = _.extend({
    disable: function() {
        eventBus.off(events.autoPlace, onAutoPlace);
    },
    enable: function() {
        eventBus.on(events.autoPlace, onAutoPlace);
    }
}, Backbone.Events);
