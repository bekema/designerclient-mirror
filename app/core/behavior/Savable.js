//This class handles saving functionality via the global save/saveas events
//OPTIONS
//loginRequired- set to false to allow saving without login
//silent - set silent to true to prevent the success or failed dialog from showing up
//name - specify a name to set on the document that will be seen in my portfolio
//successCallback - function to call after save is completed successfully
//saveas - added by the saveas global event to determine correct tracking event to call

const $ = require('jquery');
const _ = require('underscore');
const Backbone = require('backbone');
const documentRepository = require('DocumentRepository');
const urlUtil = require('util/UrlUtil');
const config = require('configuration/behavior/Savable');
const globalConfig = require('configuration/Global');

/* eslint-disable no-magic-numbers */

const setName = function(name) {
    const documentName = documentRepository.getActiveDocument().get('name');
    if (!documentName) { //If the name is empty or null then set it to the given one or default name
        documentRepository.getActiveDocument().set('name', name || globalConfig.defaultDocumentName);
    } else if (name) {
        documentRepository.getActiveDocument().set('name', name);
    }
};

const forceDocumentColorization = function(doc) {
    return !doc.canvases.each(function(canvas) { //change this to .some for doing checks
        // if blank we cannot have any items
        if (canvas.get('colorization') === 3 && canvas.items.length > 0) {
            canvas.set('colorization', 1); // set it to color
        }
        //return canvas.get("colorization") === 3 && canvas.items.length > 0;
    });
};

const showLoadingBox = function() {
    Backbone.Events.triggerGlobal('showloadingdialog');
};

const hideLoadingBox = function() {
    Backbone.Events.triggerGlobal('hideloadingdialog');
};

const showSaveSuccessDialog = function() {
    if (!this.saveSuccessDialog) {
        this.saveSuccessDialog = $.modalDialog.create({
            content: '#saveSuccessDialog',
            id: 'saveSuccess'
        });
    }
    this.saveSuccessDialog.open();
};

const showSaveFailedDialog = function() {
    if (!this.saveFailedDialog) {
        this.saveFailedDialog = $.modalDialog.create({
            content: '#saveFailedDialog',
            id: 'saveFailed'
        });
    }
    this.saveFailedDialog.open();
};

const save = function(successCallback, options) {
    Backbone.Events.triggerGlobal('savestart');
    if (options.saveas) {
        Backbone.Events.triggerGlobal('analytics:track', 'SaveAs');
    } else {
        Backbone.Events.triggerGlobal('analytics:track', 'Save');
    }

    const doc = documentRepository.getActiveDocument();

    forceDocumentColorization(doc);

    const docJson = doc.toJSON();

    const payload = {
        docJson,
        saveAsNew: options.saveas
    };

    showLoadingBox();

    const saveUrl = urlUtil.createUrl(config.path);

    $.ajax({
        type: 'post',
        contentType: 'application/json; charset=UTF-8',
        url: saveUrl.toString(),
        data: JSON.stringify(payload)
    })
    .done(function(response) {
        hideLoadingBox();
        doc.set('persistenceData', response.Id);
        if (!options.silent) {
            showSaveSuccessDialog.call(this);
        }
        Backbone.Events.triggerGlobal('savecomplete', response); // Global event.
        if (successCallback) {
            successCallback();
        }
    }.bind(this))
    .fail(function() {
        hideLoadingBox();
        if (!options.silent && config.showAlertOnError) {
            showSaveFailedDialog.call(this);
        }
        Backbone.Events.triggerGlobal('savefailed');
    }.bind(this));
};

const onSave = function(options) {
    options = options || {};
    setName(options.name);
    save.call(this, options.successCallback, options);
};

const onSaveAs = function() {
    this.triggerGlobal('opensaveasdialog', { saveas: true });
};

module.exports = _.extend({
    disable: function() {
        this.stopListening();
    },
    enable: function() {
        this.listenToGlobal('save', onSave);
        this.listenToGlobal('saveas', onSaveAs);
    }
},
    Backbone.Events);
