const $ = require('jquery');
const _ = require('underscore');
const Backbone = require('backbone');
const _remove = require('lodash-compat/array/remove');

/* eslint-disable no-magic-numbers */

let registrations = [];
let activeElement = null;

const focusManager = _.extend({}, Backbone.Events);

const findNextElement = function() {
    let i, element, tabIndex;
    let currentTabIndex = $(activeElement).attr('tabindex');
    const currentIndex = _.findIndex(registrations, function(registration) {
        return registration.triggerElement === activeElement;
    });

    // Look for the next element (in order of registration) with the same tabindex
    // This is similar to what the DOM does except its order of element in the DOM, which I don't really want to evaluate...
    for (i = currentIndex + 1; i < registrations.length; i++) {
        element = registrations[i].triggerElement;
        if (element === activeElement) {
            continue;
        }
        tabIndex = $(element).attr('tabindex');
        if (tabIndex === currentTabIndex) {
            return element;
        }
    }

    let bestElement = null;
    let bestTabIndex = Infinity;
    if (_.isUndefined(currentTabIndex)) {
        currentTabIndex = -1;
    }

    for (i = 0; i < registrations.length; i++) {
        element = registrations[i].triggerElement;
        if (element === activeElement) {
            continue;
        }
        tabIndex = $(element).attr('tabindex');
        if (!_.isUndefined(tabIndex)) {
            if (tabIndex === -1) {
                continue;
            } else if (tabIndex === currentTabIndex + 1) {
                return element;
            } else if (tabIndex > currentTabIndex && tabIndex < bestTabIndex) {
                bestTabIndex = tabIndex;
                bestElement = element;
            }
        }
    }

    return bestElement;
};

const onTab = function(e) {
    // We think that we have an element focused, but it is not the active element.
    if (activeElement !== null && document.activeElement !== activeElement) {
        const nextElement = findNextElement();
        if (nextElement) {
            e.preventDefault();
            // Do a real focus.
            nextElement.focus();
        }
    }
};

focusManager.enable = function() {
    focusManager.onGlobal('tab', onTab);
};

focusManager.disable = function() {
    focusManager.offGlobal();
    registrations.forEach(function(registration) {
        $(registration.element).off('.focusmanager');
    });
    registrations = [];
    activeElement = null;
};

focusManager.register = function(element, triggerElement) {
    const options = {
        element: element,
        triggerElement: triggerElement || element
    };
    registrations.push(options);

    $(element).on('vpfocus.focusmanager focus.focusmanager', function() {
        activeElement = options.triggerElement;
    });
};

focusManager.remove = function(element) {
    $(element).off('.focusmanager');
    _remove(registrations, function(registration) {
        return registration.element === element;
    });
};

module.exports = focusManager;
