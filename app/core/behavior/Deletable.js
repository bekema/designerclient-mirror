// The `Deletable` widget makes it possible to delete items.
const _ = require('underscore');
const Backbone = require('backbone');
const selectionManager = require('SelectionManager');
const commandDispatcher = require('CommandDispatcher');
const ViewModel = require('viewModels/ViewModel');
const eventBus = require('EventBus');
const events = eventBus.events;

const onDelete = function(e) {
    const args = {};

    if (e && e instanceof ViewModel) {
        args.viewModels = [e];
    } else if (selectionManager.length > 0) {
        args.viewModels = selectionManager.toArray();
    } else {
        return;
    }

    args.viewModels = _.filter(args.viewModels, function(viewModel) {
        return viewModel.get('deletable');
    });

    if (args.viewModels.length) {
        commandDispatcher.delete(args);
    }
};

module.exports = _.extend({
    disable: function() {
        eventBus.off(events.deleteItem, onDelete);
    },
    enable: function() {
        eventBus.on(events.deleteItem, onDelete);
    }
},
    Backbone.Events);
