const _ = require('underscore');
const Backbone = require('backbone');
const commandDispatcher = require('CommandDispatcher');
const eventBus = require('core/EventBus');
const selectionManager = require('SelectionManager');
const EmbroideryModificationView = require('publicApi/configuration/ui').embroidery.EmbroideryModificationView;
const events = eventBus.events;
const EmbroideryImageViewModel = require('viewModels/EmbroideryImageViewModel');

const cloneViewModel = function(viewModel) {
    const modelClone = viewModel.model.clone();

    const viewModelClone = new EmbroideryImageViewModel({ model: modelClone });
    viewModelClone.parent = viewModel.parent;

    return viewModelClone;
};

const openEmbroideryModel = function() {
    const itemViewModel = selectionManager.first();

    if (itemViewModel.get('digitizationNeeded')) { return; }

    const embroideryModifyView = new EmbroideryModificationView({ containerElement: 'body', itemViewModel: cloneViewModel(itemViewModel) });
    const promise = embroideryModifyView.open();

    promise.then(
        (data) => {
            commandDispatcher.overrideColors({
                viewModel: itemViewModel,
                colorOverrides: data
            });
        },
        () => {}
    );
};

module.exports = _.extend({
    disable: function() {
        eventBus.off(events.threadSelection, openEmbroideryModel);
    },
    enable: function() {
        eventBus.on(events.threadSelection, openEmbroideryModel);
    }
}, Backbone.Events);
