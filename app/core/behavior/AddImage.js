const Backbone = require('backbone');
const _ = require('underscore');
const commandDispatcher = require('CommandDispatcher');
const documentRepository = require('DocumentRepository');
const uploadedImageConfig = require('publicApi/configuration/core/items').image.defaults;
const eventBus = require('EventBus');
const events = eventBus.events;
const designerContext = require('DesignerContext');

/* eslint-disable no-magic-numbers */

// Given a vp.image.EditedImage from the Add Image pop or similar object,
// return appropriate image model for Studio.
const getDesignerImageAttributes = function(selectedImage) {
    let attributes = {
        imageId: selectedImage.id,
        isVector: selectedImage.isVector,
        isLogo: selectedImage.isLogo,
        isPhoto: selectedImage.isPhoto,
        url: selectedImage.url
    };

    attributes = _.defaults(attributes, uploadedImageConfig);
    attributes.rotation = selectedImage.rotation || 0;
    // Don't include initialHeight and initialWidth: those should get set later based on the model's height and width.

    if (designerContext.embroidery) {
        attributes.module = 'EmbroideryImage';
    }

    return attributes;
};

const getCrop = function(cropAttributes, viewModelToReplace, shouldCropHorizontally) {
    const imageAR = cropAttributes.width / cropAttributes.height;
    const model = viewModelToReplace.model;
    const itemAR = model.get('width') / model.get('height');

    let trim;

    if (shouldCropHorizontally(imageAR, itemAR)) {
        // image too wide
        trim = (1 - itemAR / imageAR) / 2; // divide by 2 to center
        return { top: 0, left: trim, bottom: 0, right: trim };
    } else {
        // image too tall
        trim = (1 - imageAR / itemAR) / 2;
        return { top: trim, left: 0, bottom: trim, right: 0 };
    }
};

const getScaleToFitCrop = function(cropAttributes, viewModelToReplace) {
    return getCrop(cropAttributes, viewModelToReplace, function(imageAspectRatio, itemAspectRatio) {
        return itemAspectRatio > imageAspectRatio;
    });
};

const getDefaultCrop = function(cropAttributes, viewModelToReplace) {
    return getCrop(cropAttributes, viewModelToReplace, function(imageAspectRatio, itemAspectRatio) {
        return imageAspectRatio > itemAspectRatio;
    });
};

const replaceImage = function(viewModelToReplace, newImage, maintainOldCrop) {
    const modelToReplace = viewModelToReplace.model;
    let newImageAttributes = {};

    //If we have a module then use the new image, otherwise we need to get the attributes
    if (newImage.module) {
        newImageAttributes = _.clone(newImage);
    } else if (newImage.type >= 0) {
        newImageAttributes = getDesignerImageAttributes(newImage);
    }

    //If we're replacing a placeholder, store its details so we know which placeholder the image replaced
    if (_.contains(modelToReplace.get('module'), 'placeholder')) {
        newImageAttributes.placeHolderReplaced = {
            height: modelToReplace.get('height'),
            width: modelToReplace.get('width'),
            top: modelToReplace.get('top'),
            left: modelToReplace.get('left')
        };
    }

    // Copy over old model attributes except:
    const keepAttributes = _.omit(modelToReplace.attributes, [
        'imageId',
        'uploadToken',
        'module', // Could have a different module.
        'crop',
        'rotation',
        'id', // New image will of course have a different Id.
        'plantPrefix',
        'variantType',
        'isVector',
        'isLogo',
        'isPhoto'
    ]);
    _.extend(newImageAttributes, keepAttributes);

    // in the full bleed path we need to turn on the finish
    if (documentRepository.getActiveCanvasViewModel().get('isFullUpload')) {
        if (!(newImageAttributes.finishes && newImageAttributes.finishes.length)) {
            const finish = _.first(documentRepository.getActiveCanvas().get('finishes'));
            if (finish) {
                newImageAttributes.finishes = [finish];
            }
        }
    }

    // Make sure to preserve initial height and width
    // TODO: ROTATION
    newImageAttributes.initialWidth = modelToReplace.get('width');
    newImageAttributes.initialHeight = modelToReplace.get('height');

    //Calculate a new crop if there isn't one set
    if (!maintainOldCrop) {
        const cropAttributes = {
            height: newImage.height,
            width: newImage.width,
            rotation: newImageAttributes.rotation
        };

        if (newImage.isLogo) {
            newImageAttributes.crop = getScaleToFitCrop(cropAttributes, viewModelToReplace);
        } else {
            newImageAttributes.crop = getDefaultCrop(cropAttributes, viewModelToReplace);
        }
    }

    //Unlock placeholders if you add a logo or add an image that should be unlocked
    if (newImage.isLogo || newImage.locked === false) {
        newImageAttributes.locked = false;
    }

    //If we are replacing an image then it is assumed that the user has added this image
    newImageAttributes.userCreated = true;

    commandDispatcher.replace({
        viewModel: viewModelToReplace,
        attributes: newImageAttributes,
        selectNewModels: true
    });

    Backbone.Events.triggerGlobal('analytics:track', viewModelToReplace, 'Replace');
};

// This is called when replacing an existing image as well as a placeholder
const onReplaceImage = function(viewModelToReplace, newImage, maintainOldCrop) {
    replaceImage(viewModelToReplace, newImage, maintainOldCrop);
};

const onAddImage = function(options) {
    commandDispatcher.createImage(options);
};

module.exports = {
    getDesignerImageAttributes,
    getDefaultCrop,
    getScaleToFitCrop,
    disable: function() {
        eventBus.off(events.replaceImage, onReplaceImage);
        eventBus.off(events.addImage, onAddImage);
    },
    enable: function() {
        eventBus.on(events.replaceImage, onReplaceImage);
        eventBus.on(events.addImage, onAddImage);
    }
};
