//Warn users if they try to navigate away with unsaved changes
const _ = require('underscore');
const eventBus = require('EventBus');
const events = eventBus.events;

const onSaveComplete = function() {
    window.onbeforeunload = null;
};

const onBeforeUnload = function() {
    // TODO: localize
    return 'You have unsaved changes. Are you sure you want to lose them?';
};

const onCommand = function() {
    window.onbeforeunload = onBeforeUnload;
};

module.exports = _.extend({
    disable: function() {
        eventBus.off(`${events.executeCommand} ${events.undoCommand}`, onCommand);
        eventBus.off(events.saveComplete, onSaveComplete);
        window.onbeforeunload = null;
    },
    enable: function() {
        // Both execute and undo change the state of the document in a way that will require saving.
        eventBus.on(`${events.executeCommand} ${events.undoCommand}`, onCommand);
        eventBus.on(events.saveComplete, onSaveComplete);
    }
});
