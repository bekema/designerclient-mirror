const $ = require('jquery');
const _ = require('underscore');
const Backbone = require('backbone');
const documentRepository = require('DocumentRepository');

const previewWidth = 600;

//Add the doc id to the query string
const onPreview = function() {
    const doc = documentRepository.getActiveDocument();

    const docId = doc.get('persistenceData');

    // TODO: Turn this into a template or a view
    $.modalDialog.create({
        content: '<div id="preview">'
            + `<img style="margin: auto; display: block;" src="/lp.aspx?page=1&width=${previewWidth}&doc_id=${docId}" />`
            + `<img style="margin: auto; display: block;" src="/lp.aspx?page=2&width=${previewWidth}&doc_id=${docId}" />`
            + '</div>',
        skin: 'lightbox',
        destroyOnClose: true,
        title: 'Preview',
        maxWidth: previewWidth,
        onclose: function() {
            $('#preview').remove();
        }
    })
    .open();

    documentRepository.triggerGlobal('analytics:track', 'Preview');
};

module.exports = _.extend({
    disable: function() {
        this.stopListening();
    },
    enable: function() {
        this.listenToGlobal('preview', onPreview);
    }
},
    Backbone.Events);
