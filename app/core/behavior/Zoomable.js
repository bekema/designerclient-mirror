const $ = require('jquery');
const _ = require('underscore');
const Backbone = require('backbone');
const documentRepository = require('DocumentRepository');
const ZoomStrategy = require('ui/strategies/ZoomStrategy');
const uiConfig = require('publicApi/configuration/ui');
const eventBus = require('EventBus');
const events = eventBus.events;

/* eslint-disable no-magic-numbers */

const zoomFactorDictionary = {};

const getAvailableWidthForCanvas = function() {
    return $(uiConfig.canvas.container).width();
};

const getAvailableHeightForCanvas = function() {
    return $(uiConfig.canvas.container).height();
};

const setZoomFactor = function(canvasViewModel, zoomFactor) {
    canvasViewModel.set('zoomFactor', zoomFactor);
};

const scaleZoomFactor = function(canvasViewModel, zoomFactor) {
    const zoomRequest = zoomFactorDictionary[canvasViewModel.id];
    return zoomFactor * zoomRequest.zoomScale;
};

// This should only be called for resizable canvas configuration
const getResizedZoomScale = function(canvasViewModel) {
    const productDimensions = canvasViewModel.get('canvasSize');
    const availableWidth = getAvailableWidthForCanvas();
    const availableHeight = getAvailableHeightForCanvas();

    let calculatedZoom = availableWidth / productDimensions.width * (uiConfig.zoomStrategy.initialWidth || 1);
    const zoomHeight = productDimensions.height * calculatedZoom;
    const maxHeight = availableHeight * (uiConfig.zoomStrategy.initialHeight || 1);

    if (zoomHeight > maxHeight) {
        const ratio = maxHeight / zoomHeight;
        calculatedZoom *= ratio;
    }

    return calculatedZoom;
};

const updateZoomFactor = function(zoomFactor) {

    let canvasViewModels = [];
    // Do we want the act of zooming to update all of this document's canvases?
    const activeDocumentViewModel = documentRepository.getActiveDocumentViewModel();
    if (activeDocumentViewModel) {
        canvasViewModels = activeDocumentViewModel.canvasViewModels.toArray();
    }

    canvasViewModels.forEach(canvasViewModel => {
        const scaledZoomFactor = scaleZoomFactor(canvasViewModel, zoomFactor);
        setZoomFactor(canvasViewModel, scaledZoomFactor);
        zoomFactorDictionary[canvasViewModel.id].zoomFactor = zoomFactor;
        // Now that we are storing a value for this canvas, we want to make sure that we remove it if the canvas is destroyed.
        if (_.isUndefined(zoomFactorDictionary[canvasViewModel.id])) {
            this.listenToOnce(canvasViewModel, 'destroy', () => delete zoomFactorDictionary[canvasViewModel.id]);
        }
    });
};

const onZoom = function(zoomFactor) {
    if (zoomFactor <= 0) {
        return;
    }
    // Need to use `call` because 'updateZoom' is scoped to the Window object, while 'this' is the Zoomable object, which
    // was bound to the `zoomin` event handler.
    updateZoomFactor.call(this, zoomFactor);
};

const initializeZoomFactor = function(canvasViewModel) {

    const initialZoom = uiConfig.zoomStrategy.initialZoom || 1;
    const zoomScale = getResizedZoomScale(canvasViewModel);
    const scaledZoomFactor = zoomScale * initialZoom;

    setZoomFactor(canvasViewModel, scaledZoomFactor);
    zoomFactorDictionary[canvasViewModel.id] = {
        zoomFactor: initialZoom,
        // The scale should be considered static, and everything should be based off this initialized value
        zoomScale
    };
};

const onWindowResize = function() {

    const activeDocumentViewModel = documentRepository.getActiveDocumentViewModel();
    if (activeDocumentViewModel && activeDocumentViewModel.canvasViewModels) {
        activeDocumentViewModel.canvasViewModels.each(canvasViewModel => {
            const zoomScale = getResizedZoomScale(canvasViewModel);
            zoomFactorDictionary[canvasViewModel.id].zoomScale = zoomScale;
            const scaledZoomFactor = scaleZoomFactor(canvasViewModel, zoomFactorDictionary[canvasViewModel.id].zoomFactor);
            setZoomFactor(canvasViewModel, scaledZoomFactor);
        });
    }
};

module.exports = _.extend({
    initializeZoomFactor,

    disable: function() {
        this.stopListening();
        eventBus.off(events.zoom, onZoom);
        if (uiConfig.zoomStrategy.resizeEnabled) {
            eventBus.off(events.windowResize, onWindowResize);
        }
    },

    enable: function() {
        eventBus.on(events.zoom, onZoom);

        this.zoomStrategy = new ZoomStrategy(uiConfig.zoomStrategy);

        if (uiConfig.zoomStrategy.resizeEnabled) {
            eventBus.on(events.windowResize, onWindowResize);
        }

        _.each(zoomFactorDictionary, (value, id) => {
            const canvasViewModel = documentRepository.getCanvasViewModel(id);
            if (canvasViewModel) {
                this.listenToOnce(canvasViewModel, 'destroy', () => delete zoomFactorDictionary[id]);
            }
        });
    }
},
Backbone.Events);
