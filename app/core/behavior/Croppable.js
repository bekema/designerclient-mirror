const _ = require('underscore');
const Backbone = require('backbone');
const commandDispatcher = require('CommandDispatcher');
const eventBus = require('core/EventBus');
const selectionManager = require('SelectionManager');
const CropModalView = require('publicApi/configuration/ui').crop.CropModalView;
const events = eventBus.events;
const uploadsUrlBuilder = require('services/clientProvider').upload.uploadsUrlBuilder;
const processingStepsManager = require('uploads/processingStepsManager');
const dataUtil = require('uploads/processingSteps/embroidery/dataUtil');

const buildUploadModel = function(itemViewModel, uploadId) {
    itemViewModel.model.set({ digitizedId: null });
    this.upload = new Backbone.Model({
        requestId: itemViewModel.model.get('requestId'),
        pages: [
            { uploadId }
        ]
    });
};

const getUpdatedAttributes = function(viewModel, crop) {
    const height = viewModel.model.get('initialHeight') - (viewModel.model.get('initialHeight') * (crop.bottom + crop.top));
    const width = viewModel.model.get('initialWidth') - (viewModel.model.get('initialWidth') * (crop.left + crop.right));
    const digitizedId = this.upload.get('pages')[0].digitizedId;
    const xtiUrl = uploadsUrlBuilder.getOriginalUrl(this.upload.get('pages')[0].digitizedId);

    return { height, width, digitizedId, xtiUrl };
};

const openCropModal = function() {
    //TODO perhaps have a different way of getting the viewModel
    const viewModel = selectionManager.first();
    const cropModalView = new CropModalView({ containerElement: 'body', viewModel });
    const promise = cropModalView.open();

    promise.then(
        (crop) => {
            if (viewModel.get('module') === 'EmbroideryImageViewModel') {
                const printUrl = viewModel.model.get('printUrl');
                const img = new Image();

                img.crossOrigin = 'Anonymous';
                img.onload = function() {
                    return dataUtil.cropOriginalImage(crop, img)
                        .then(response => buildUploadModel.call(this, viewModel, response[0].uploadId))
                        .then(() => processingStepsManager.executeUploadSteps(this.upload, viewModel))
                        .then(() => {
                            const attributes = getUpdatedAttributes.call(this, viewModel, crop);
                            viewModel.model.set(attributes);
                        });
                };
                img.src = printUrl;
            } else {
                commandDispatcher.cropImage({
                    viewModel,
                    crop,
                    //TODO do something with the locking/unlocking
                    unlocked: true
                });
            }
        }
    );
};

module.exports = _.extend({
    disable: function() {
        eventBus.off(events.crop, openCropModal);
    },
    enable: function() {
        eventBus.on(events.crop, openCropModal);
    }
}, Backbone.Events);
