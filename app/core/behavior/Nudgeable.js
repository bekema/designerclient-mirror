// The `Nudgeable` widget lets us move items via keystrokes.
const _ = require('underscore');
const Backbone = require('backbone');
const keyboardManager = require('KeyboardManager');
const selectionManager = require('SelectionManager');
const commandDispatcher = require('CommandDispatcher');
const documentRepository = require('DocumentRepository');
const config = require('configuration/behavior/Nudgeable');

const ARROW_KEYS = {
    left: {
        keyCode: 37,
        direction: 'left',
        multiply: -1
    },
    top: {
        keyCode: 38,
        direction: 'top',
        multiply: -1
    },
    right: {
        keyCode: 39,
        direction: 'left',
        multiply: 1
    },
    bottom: {
        keyCode: 40,
        direction: 'top',
        multiply: 1
    }
};

const onKeyDown = function(e) {
    if (selectionManager.length === 0) {
        return;
    }

    const zoomFactor = documentRepository.getActiveCanvasViewModel().get('zoomFactor');

    for (const key in ARROW_KEYS) {
        if (ARROW_KEYS[key].keyCode === e.which) {
            const delta = {
                top: 0,
                left: 0
            };

            delta[ARROW_KEYS[key].direction] += (e.ctrlKey ? config.nudgeLong : config.nudgeShort) * ARROW_KEYS[key].multiply / zoomFactor;
            commandDispatcher.move({
                viewModels: selectionManager.filter(function(vm) {
                    return !vm.model.get('locked');
                }),
                delta
            });
            e.preventDefault();
            break;
        }
    }
};

module.exports = _.extend({
    disable: function() {
        this.stopListening();
    },
    enable: function() {
        this.listenTo(keyboardManager, 'keydown', onKeyDown);
    }
},
    Backbone.Events);
