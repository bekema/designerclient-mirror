// The `history` keeps track of commands.
const documentRepository = require('DocumentRepository');
const eventBus = require('EventBus');
const events = eventBus.events;

let allCommands = {};
let redoing = false;

const hashCanvases = (canvases) => {
    return canvases.map(canvas => canvas.id).join(',');
};

const currentState = function() {
    const id = hashCanvases(documentRepository.getEnabledCanvasViewModels());
    const commandList = allCommands[id];
    const index = commandList ? commandList.index : 0;
    const total = commandList ? commandList.commands.length : 0;

    return {
        index,
        total
    };
};

const triggerHistoryChange = function() {
    eventBus.trigger(events.historyChanged, currentState);
};

const onExecuteCommand = function(cmd) {
    if (!redoing) {
        const id = hashCanvases(documentRepository.getEnabledCanvasViewModels());
        if (!allCommands[id]) {
            allCommands[id] = { commands: [], index: 0 };
        }

        const index = allCommands[id].index;
        let commands = allCommands[id].commands;

        if (index < commands.length) {
            commands = allCommands[id].commands = commands.slice(0, index);
        }
        commands.push(cmd);
        allCommands[id].index++;
        triggerHistoryChange();
    }
};

const undo = function() {
    const id = hashCanvases(documentRepository.getEnabledCanvasViewModels());
    if (!allCommands[id]) {
        return false;
    }

    const index = allCommands[id].index;
    const commands = allCommands[id].commands;

    if (index > 0) {
        commands[index - 1].undo();
        allCommands[id].index--;
        eventBus.trigger(events.track, 'Undo');
        triggerHistoryChange();
        return true;
    }
    return false;
};

const redo = function() {
    const id = hashCanvases(documentRepository.getEnabledCanvasViewModels());
    if (!allCommands[id]) {
        return false;
    }

    const index = allCommands[id].index;
    const commands = allCommands[id].commands;

    if (index < commands.length) {
        redoing = true;
        commands[index].execute();
        allCommands[id].index++;
        redoing = false;
        eventBus.trigger(events.track, 'Redo');
        triggerHistoryChange();
        return true;
    }
    return false;
};


const clear = function() {
    allCommands = {};
};

const disable = function() {
    eventBus.off(events.executeCommand, onExecuteCommand);
    eventBus.off(events.undo, undo);
    eventBus.off(events.redo, redo);
};

const enable = function() {
    eventBus.on(events.executeCommand, onExecuteCommand);
    eventBus.on(events.undo, undo);
    eventBus.on(events.redo, redo);
};

// ### Public API ###
module.exports = {

    disable,
    enable,

    // `undo` the last command.
    undo,

    // `redo` the command that was most recently undone.
    redo,

    // `clear` doesn't really have a purpose outside of unit testing.
    clear,

    currentState
};
