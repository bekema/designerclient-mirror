const _ = require('underscore');
const _cloneDeep = require('lodash-compat/lang/cloneDeep');
const uiConfig = require('publicApi/configuration/ui');
const FeatureManager = require('FeatureManager');

const newSurfaceMask = function(surface) {
    const mask =  {
        pathType: 'EVENODD',
        paths: [{
            anchorX: 0,
            anchorY: 0,
            pathPoints: [
                {
                    ordinal: 0,
                    endPointX: 0,
                    endPointY: 0,
                    firstControlPointX: null,
                    firstControlPointY: null,
                    secondControlPointX: null,
                    secondControlPointY: null
                },
                {
                    ordinal: 0,
                    endPointX: surface.widthInMm,
                    endPointY: 0,
                    firstControlPointX: null,
                    firstControlPointY: null,
                    secondControlPointX: null,
                    secondControlPointY: null
                },
                {
                    ordinal: 0,
                    endPointX: surface.widthInMm,
                    endPointY: surface.heightInMm,
                    firstControlPointX: null,
                    firstControlPointY: null,
                    secondControlPointX: null,
                    secondControlPointY: null
                },
                {
                    ordinal: 0,
                    endPointX: 0,
                    endPointY: surface.heightInMm,
                    firstControlPointX: null,
                    firstControlPointY: null,
                    secondControlPointX: null,
                    secondControlPointY: null
                }
            ]
        }]
    };

    surface.masks.push(mask);
};

const surfaceIsRectangle = function(surface) {
    let containsNoControlPoints = false;
    // Check if surface is bleed or trim
    surface.masks.forEach(function(mask) {
        mask.paths.forEach(function(path) {
            path.pathPoints.forEach(function(pathPoint) {
                // if there are no control porint, the surface is a rectangle
                const numberOfPoints = Object.keys(path.pathPoints).length;
                if (numberOfPoints === 3 || numberOfPoints === 4) { // eslint-disable-line no-magic-numbers
                    if (pathPoint.firstControlPointX === null && pathPoint.firstControlPointY === null
                    && pathPoint.secondControlPointX === null && pathPoint.secondControlPointY === null) {
                        containsNoControlPoints = true;
                    }
                }
            });
        });
    });

    return containsNoControlPoints || !surface.masks.length;
};

const convertSurfaceSpecToMarginViewModel = function(surface) {
    let safetyMargin = { top: 0, bottom: 0, left: 0, right: 0 };
    let bleedMargin = { top: 0, bottom: 0, left: 0, right: 0 };

    // Loop through all the masks in this surface
    surface.masks.forEach(function(mask) {
        const pathType = mask.pathType;
        // Set safety and bleed margins
        mask.paths.forEach(function(path) {
            switch (pathType) {
                case 'BLEED':
                    bleedMargin = {
                        top: path.anchorY,
                        right: surface.widthInMm - path.pathPoints[1].endPointX,
                        bottom: surface.heightInMm - path.pathPoints[2].endPointY,
                        left: path.anchorX
                    };
                    break;
                case 'TRIM':
                    safetyMargin = {
                        top: path.anchorY,
                        right: surface.widthInMm - path.pathPoints[1].endPointX,
                        bottom: surface.heightInMm - path.pathPoints[2].endPointY,
                        left: path.anchorX
                    };
                    break;
            }
        });
    });

    // bleeds should be the full size of the document, but we actually want to
    // specify the size of the bleed margin to be to the trim line, and the size
    // of the trim margin to be inside of the bleed margin
    if (bleedMargin.top === 0 && bleedMargin.left === 0 && bleedMargin.right === 0 && bleedMargin.bottom === 0) {
        bleedMargin = {
            top: safetyMargin.top,
            left: safetyMargin.left,
            right: safetyMargin.right,
            bottom: safetyMargin.bottom
        };
    }

    return  {
        safetyMargin,
        bleedMargin
    };
};

const buildSceneChrome = function(scene, sceneUri, type) {
    return _.extend(_cloneDeep(scene), {
        type,
        sceneUri,
        module: 'SceneViewModel'
    });
};

const buildDesignAreaSceneChrome = function(scene) {
    return {
        sceneUri: scene.designAreaSceneUri,
        type: 'underlay',
        module: 'DesignAreaSceneViewModel'
    };
};

const buildOverlayChrome = function(module, type = 'overlay') {
    return {
        module,
        type
    };
};

const buildCanvasBorderChrome = function() {
    return buildOverlayChrome('CanvasBorderViewModel');
};

const buildCanvasRulerChrome = function() {
    return buildOverlayChrome('CanvasRulerViewModel');
};

const buildCanvasDimensionsChrome = function() {
    return buildOverlayChrome('CanvasDimensionsViewModel');
};

const buildCanvasToolsChrome = function() {
    return buildOverlayChrome('CanvasToolsViewModel', 'tools');
};

const buildChromes = function(chromeData) {
    if (!chromeData) {
        return;
    }

    const chromes = [];
    // Check if old surface specification
    if (chromeData.surface && uiConfig.canvas.showMargins) {
        let marginChrome = chromeData.surface;
        if (surfaceIsRectangle(marginChrome)) {
            // Simple rectangle shapes
            marginChrome = convertSurfaceSpecToMarginViewModel(marginChrome);
            marginChrome.module = 'MarginsViewModel';
        } else {
            // Add new mask around the whole surface for even-odd algorithm
            newSurfaceMask(marginChrome);
            // Complicated margin shapes
            marginChrome.module = 'SurfaceSpecificationViewModel';
        }

        marginChrome.type = 'overlay';
        chromes.push(marginChrome);

    }

    // draw canvas chromeData
    if (chromeData.canvasObjects) {
        const canvasObjectChrome = chromeData.canvasObjects;
        canvasObjectChrome.module = 'CanvasObjectsViewModel';
        canvasObjectChrome.type = 'overlay';
        chromes.push(canvasObjectChrome);
    }

    if (chromeData.scene) {
        chromeData.scene.underlaySceneUri
            && chromes.push(buildSceneChrome(chromeData.scene, chromeData.scene.underlaySceneUri, 'underlay'));

        chromeData.scene.overlaySceneUri
            && chromes.push(buildSceneChrome(chromeData.scene, chromeData.scene.overlaySceneUri, 'overlay'));

        chromeData.scene.designAreaSceneUri &&
            chromes.push(buildDesignAreaSceneChrome(chromeData.scene));

        if (uiConfig.canvas.chromes.border.enabled) {
            chromes.push(buildCanvasBorderChrome());
        }
    }

    if (uiConfig.canvas.chromes.ruler.enabled) {
        chromes.push(buildCanvasRulerChrome());
    }
    if (uiConfig.canvas.chromes.dimensions.enabled) {
        chromes.push(buildCanvasDimensionsChrome());
    }

    if (uiConfig.canvas.chromes.canvasTools.enabled && FeatureManager.canvasBackgroundColor.enabled) {
        chromes.push(buildCanvasToolsChrome());
    }

    return chromes;
};

module.exports = {
    buildChromes
};
