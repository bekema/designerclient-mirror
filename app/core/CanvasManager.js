const _ = require('underscore');
const documentRepository = require('DocumentRepository');
const util = require('util/Util');
const eventBus = require('EventBus');
const events = eventBus.events;

// Accepts a canvas or a list of canvases to be enabled
const enableCanvases = (options) => {
    if (!util.isNumberDefined(options.enabledCanvas) && options.enabledCanvases !== 0 && !(_.isArray(options.enabledCanvases) && options.enabledCanvases.length >= 1)) {
        return false;
    }
    const canvases = documentRepository.getAllCanvasViewModels();
    const enabledCanvases = options.enabledCanvases || [options.enabledCanvas];
    const allCanvasesEnabled = options.enabledCanvas === 0 || options.enabledCanvases === 0;

    canvases.forEach(canvas => {
        canvas.set({
            enabled: allCanvasesEnabled || enabledCanvases.some(id => id === canvas.model.id)
        });
    });
    return true;
};

// Accepts a canvas or a list of canvases to be visible
// Enabled canvases are automatically added to be viewed
const viewCanvases = (options) => {
    if (!util.isNumberDefined(options.visibleCanvas) && options.visibleCanvases !== 0 && !(_.isArray(options.visibleCanvases) && options.visibleCanvases.length >= 1)) {
        return false;
    }
    const canvases = documentRepository.getAllCanvasViewModels();
    const visibleCanvases = options.visibleCanvases || [options.visibleCanvas];
    const allCanvasesVisible = options.visibleCanvas === 0 || options.visibleCanvases === 0;

    canvases.forEach(canvas => {
        const visible = allCanvasesVisible || canvas.get('enabled') || visibleCanvases.some(id => id === canvas.model.id);
        canvas.set({ hidden: !visible });
    });
    return true;
};

// Accepts a canvas to be activated
// Will ensure an enabled canvas is activated
const activateCanvas = (options) => {
    const canvases = documentRepository.getAllCanvasViewModels();
    let activeCanvas = options.activeCanvas;

    if (!activeCanvas && documentRepository.getEnabledCanvasViewModels().some(canvas => canvas.model.id === documentRepository.getActiveCanvasViewModel().model.id)) {
        return false;
    }
    if (!activeCanvas || (activeCanvas && !documentRepository.getEnabledCanvasViewModels().some(canvas => canvas.model.id === activeCanvas))) {
        activeCanvas = documentRepository.getEnabledCanvasViewModels()[0].model.id;
    }

    // You cannot have two active canvases at once, so you must set all to false first.
    canvases.forEach(canvas => canvas.set({ active: false }));
    _.find(canvases, canvas => canvas.model.id === activeCanvas).set({ active: true });
    return true;
};

// A complex action that combines the enabling, viewing, and activating actions.
const manageCanvases = (options) => {
    enableCanvases(options);
    viewCanvases(options);
    activateCanvas(options);
};

let enabled = false;

module.exports = {
    manageCanvases,

    enable: function() {
        if (!enabled) {
            eventBus.on(events.manageCanvases, manageCanvases);
        }
        enabled = true;
    },

    disable: function() {
        if (enabled) {
            eventBus.off(events.manageCanvases, manageCanvases);
        }
        enabled = false;
    }
};
