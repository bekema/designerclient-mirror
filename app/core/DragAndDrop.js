// ## Drag and Drop

// This library manages all the necessary communication between draggables and droppables. There are several sections of code here.
// The first is responsible for actually dealing with the interaction between the active
// **draggables** and **droppables**. The remaining are all for representing single instances of draggables and droppables.

// All internal DOM event binding use the `dcl` namespace. All triggered drop-related events are suffixed with "dcl" in order to distinguish them
// from the HTML5 drag and drop API events, which we are intentionally trying to abstract away.

// List of events:

// * `drag`
// * `dragstart`
// * `dragstop`
// * `dcldragactivate`
// * `dcldragdeactivate`
// * `dcldrop`
// * `dcldragenter`
// * `dcldragleave`
// * `dcldragover`

const $ = require('jquery');
const _ = require('underscore');
const Backbone = require('backbone');
const util = require('util/Util');

const current = {
    draggable: null,
    droppable: null
};

// `offsets` is a cache for the top, left, height, and width of all active droppables that will accept the current draggable.
let offsets = {};
const droppables = {};

// `cid` is a counter for the internal ids that we use for droppables.
let cid = 0;

// `lastX` and `lastY` store the last known input coordinates.
let lastX, lastY;

// `html5Enabled` holds the one time check of whether or not HTML5 drag and drop is supported by the user's browser.
let html5Enabled = false;

// `getActiveDroppableAt` determine what droppable has the highest priority at the given coordinates. It assumes that all
// droppables that are present in `offsets` are valid candidates.
const getActiveDroppableAt = function(x, y) {
    let activeDroppable = null;

    for (const id in offsets) {
        if (droppables.hasOwnProperty(id)) {
            const droppable = droppables[id];
            const offset = offsets[id];

            if (x >= offset.left &&
                x <= offset.left + offset.width &&
                y >= offset.top &&
                y <= offset.top + offset.height &&
                (!activeDroppable || droppable.priority > activeDroppable.priority)) {
                activeDroppable = droppable;
            }
        }
    }

    return activeDroppable;
};

// `onDrag` is the handler that is called on every drag event.
const onDrag = function(e) {
    if (e) {
        // We have to use the originalEvent for touch interactions and HTML5 events.
        const coords = util.getCoordinates(e, current.draggable.touchIdentifier);
        lastX = coords.left;
        lastY = coords.top;
    }

    const droppable = getActiveDroppableAt(lastX, lastY);

    // If we've left the current droppable, trigger the appropriate `dcldragleave` events.
    if (current.droppable && (!droppable || current.droppable.id !== droppable.id)) {
        current.droppable.$el.removeClass('dcl-over');
        current.droppable.$el.trigger('dcldragleave', { draggable: current.draggable });
    }

    // Trigger the appropriate `dcldragover` and `dcldragenter` events for the new droppable.
    if (droppable) {
        if (!current.droppable || current.droppable.id !== droppable.id) {
            droppable.$el.addClass('dcl-over'); // TODO Make this configurable
            droppable.$el.trigger('dcldragenter', { draggable: current.draggable });
        }

        droppable.$el.trigger('dcldragover', { draggable: current.draggable });
    }

    current.droppable = droppable;
};

// Called at the start of each drag, this is responsible for caching the current offset and size of each
// droppable. The offset is only computed and cached if the droppable is enabled, visible, and will
// accept the current draggable.
const prepareOffset = function(droppable) {
    if (!droppable.disabled &&
        current.draggable &&
        droppable.accepts(current.draggable) &&
        droppable.$el.is(':visible')) {
        offsets[droppable.id] = $.extend(droppable.$el.offset(), {
            width: droppable.$el.width(),
            height: droppable.$el.height()
        });
    }
};

const prepareOffsets = function() {
    offsets = {};

    for (const id in droppables) {
        if (droppables.hasOwnProperty(id)) {
            prepareOffset(droppables[id]);
        }
    }

    // Hm. This code seems suspicious. Why is it here?
    if (current.draggable) {
        onDrag();
    }
};

// Register a new droppable for consideration when dragging.
const register = function(droppable) {
    droppable.id = cid;
    droppables[cid] = droppable;
    cid++;

    // If a drag is in progress when a new droppable is registered, we immediately want
    // to see if it is a valid drop target.
    if (current.draggable) {
        prepareOffset(droppable);
    }
};

// `start` is called when a drag begins. It is responsible for determining what drop targets
// are currently active and for caching their current positions.
const start = function() {
    prepareOffsets();

    // `bindDragHandler` is an abstraction to support either regular or HTML5 drags. It will delegate to either
    // `dragover`, `mousemove`, or `touchmove` events.
    current.draggable.bindDragHandler(onDrag);

    // We only want to activate the valid drop targets, so we only need to consider
    // the droppables that we've created offsets for.
    for (const id in offsets) {
        if (droppables.hasOwnProperty(id)) {
            droppables[id].$el.trigger('dcldragactivate', { draggable: current.draggable });
        }
    }
};

// `stop` is called when a drag ends. It is responsible for determining what target, if any, we should
// trigger the `drop` event on. `ui` is an optional argument which has positional information.
const stop = function(e, ui) {
    if (!current.draggable) {
        return;
    }
    current.draggable.unbindDragHandler(onDrag);

    const coords = util.getCoordinates(e, current.draggable.touchIdentifier);

    // We have to have an event with actual coordinates if we're going to be able to fire a drop event.
    if (_.isNumber(coords.left)) {
        const droppable = getActiveDroppableAt(coords.left, coords.top);
        if (droppable) {
            droppable.$el.removeClass('dcl-over');
            // Reuse the original event, but modify the type. This preserves pageX/pageY, but makes sure that
            // any `dcldrop` handlers get triggered.
            const event = new $.Event('dcldrop', { pageX: coords.left, pageY: coords.top });
            if (!current.draggable.isHTML5) {
                droppable.$el.trigger(event, _.extend({ draggable: current.draggable }, ui));
            // For HTML5 we want to pass along the dataTransfer object, and we're using originalEvent because it has pageX/pageY.
            } else {
                droppable.$el.trigger(event, _.extend({ dataTransfer: e.originalEvent.dataTransfer }, ui));
            }
            // If the pseudo-event has preventDefault() called, call that on the real event.
            if (event.isDefaultPrevented()) {
                e.preventDefault();
            }
        }
    }

    for (const id in offsets) {
        if (droppables.hasOwnProperty(id)) {
            droppables[id].$el.trigger('dcldragdeactivate', { draggable: current.draggable });
        }
    }

    if (current.droppable) {
        current.droppable.$el.removeClass('dcl-over');
        current.droppable = null;
    }
};

const removeOffset = function(droppable) {
    delete offsets[droppable.id];
};

const remove = function(droppable) {
    delete droppables[droppable.id];
    delete offsets[droppable.id];
};

// ### HTML5Draggable ###

// `HTML5Draggable` is a wrapper class to make sure that we can handle HTML5 drags in order to capture things dragged from the desktop.
const HTML5Draggable = function() {
    // Check for this in your droppable's `accepts()` method if you want to exclude all HTML5 draggables.
    this.isHTML5 = true;

    this.bindDragHandler = function(fn) {
        $(document).on('dcldragover', fn);
    };

    this.unbindDragHandler = function(fn) {
        $(document).off('dcldragover', fn);
    };

    // Compatibility with regular draggable, which uses jQuery's data.
    const data = {};
    this.data = function(key, val) {
        if (!key) {
            return data;
        } else if (!val) {
            return data[key];
        } else {
            data[key] = val;
        }
    };
};

const prepareHTML5Draggable = function(e) {
    current.draggable = new HTML5Draggable(e);
    start(e);
};

const initializeHTML5 = _.once(function() {
    // If HTML5 uploads are supported, create a document listener for them. We only do this
    // once and the listener will be attached for the entire life of the application.
    if ('draggable' in document.createElement('div')) { // TODO Supposedly this may not work reliably for iOS.
        html5Enabled = true;
        $(document).on('dcldragenterdrag', prepareHTML5Draggable);
    }
});

// ### Draggable ###

const Draggable = function($el, options) {
    const me = this;
    this.$el = $el;
    this.touchIdentifier = null;

    const initialize = function() {
        options =
            _.extend({
                helper: function() {
                    return $el;
                },
                // Distance in pixels after mousedown the mouse must move before dragging should start.
                distance: 5,
                // Width and height must be set at each start function call for this to work
                containDrag: false,
                width: 0,
                height: 0,
                maxWidth: 0,
                maxHeight: 0,
                deleteHelper: true
            },
                options);

        if (options.data) {
            me.$el.data(options.data);
        }

        me.setEnabled(true);
    };

    // Called when a drag starts in order to store the starting position of the element.
    const cachePositions = function(e) {
        me.position = me.$el.position();
        me.originalPosition = _.clone(me.position);
        me.inputPosition = me.originalInputPosition = util.getCoordinates(e, me.touchIdentifier);
        me.clickOffset = {
            top: me.inputPosition.top - me.$el.offset().top,
            left: me.inputPosition.left - me.$el.offset().left
        };
    };

    const removeDocumentHandlers = function() {
        $(document).off('mousedown.dcl mouseup.dcl mousemove.dcl touchmove.dcl touchend.dcl');
    };

    // Quick little helper method to return the object with positional coordinates that we'll expose to the consumer.
    const getEventData = function() {
        return {
            originalPosition: me.originalPosition,
            position: me.position,
            helper: me.$helper
        };
    };

    const onMove = function(e) {
        // Short circuit touch events that aren't for the touch that started the drag.
        const eventShouldNotCauseDrag = me.touchIdentifier && !e.originalEvent.changedTouches.some(touch => {
            return touch.identifier === me.touchIdentifier;
        });

        if (eventShouldNotCauseDrag) {
            return;
        }

        // Update the position based on the change in mouse position relative to where it started.
        me.inputPosition = util.getCoordinates(e, me.touchIdentifier);
        me.position.left = (me.inputPosition.left - me.originalInputPosition.left) + me.originalPosition.left;
        me.position.top = (me.inputPosition.top - me.originalInputPosition.top) + me.originalPosition.top;

        if (options.containDrag) {
            if (me.position.left < 0) {
                me.position.left = 0;
            }
            if (me.position.top < 0) {
                me.position.top = 0;
            }
            if (me.position.left + me.width > options.maxWidth) {
                me.position.left = options.maxWidth - me.width;
            }
            if (me.position.top + me.height > options.maxHeight) {
                me.position.top = options.maxHeight - me.height;
            }
        }

        if (!me.dragStarted) {
            // Don't start the drag until the mouse has moved a minimum distance.
            const dx = me.inputPosition.left - me.originalInputPosition.left;
            const dy = me.inputPosition.top - me.originalInputPosition.top;
            if (Math.sqrt(dx * dx + dy * dy) > options.distance) {
                me.$el.trigger('dragstart', getEventData());
                if (options.start && options.start(e, getEventData()) === false) {
                    removeDocumentHandlers();
                    return;
                }

                me.dragStarted = true;
                me.$helper = options.helper(e);
                me.$helper
                    .css({ position: 'absolute' })
                    .appendTo(!options.appendTo ? 'body' : options.appendTo());

                me.originalHelperPosition = me.$helper.position();

                // Do the work that is scoped to the entire library so that this draggable
                // can begin interacting with droppables.
                current.draggable = me;
                start(e);
            }
        }

        if (me.dragStarted) {
            e.preventDefault();

            // This order is important. We trigger all of the events first, which give consumers the ability
            // to modify the "position" of the draggable, which we will then use to actually set the css of the helper. This
            // is useful in the case of snapping.
            me.$el.trigger('drag', getEventData());
            if (options.drag) {
                options.drag(e, getEventData());
            }

            const top = me.originalHelperPosition.top + me.position.top - me.originalPosition.top;
            const left = me.originalHelperPosition.left + me.position.left - me.originalPosition.left;
            if (options.updateHelper) {
                options.updateHelper({ helper: me.$helper, position: { top, left }, inputPosition: me.inputPosition });
            } else {
                me.$helper.css({ top, left });
            }
        }
    };

    const onInputStop = function(e) {
        if (me.touchIdentifier) {
            // A different touch event has ended than the one we are tracking, don't stop!
            if (!e.originalEvent.changedTouches.some(t => {
                return t.identifier === me.touchIdentifier;
            })) {
                return false;
            }
        }

        // We have to unbind everything even if the drag hasn't officially started, i.e. moved more than the minimum distance.
        removeDocumentHandlers();

        // Stop any dragging / dropping interactions that we know about.
        if (current.draggable === me) {
            stop(e, getEventData());
            me.$el.trigger('dragstop', getEventData());
            if (options.stop) {
                options.stop(e);
            }

            current.draggable = null;
            if (options.deleteHelper) {
                me.$helper.remove();
                delete me.$helper;
            }
            me.dragStarted = false;
            me.touchIdentifier = null;
        }
    };

    const onTouchStart = function(e) {
        e.preventDefault();
        e.stopPropagation();

        //only start a drag if there are currently no other touches
        if (e.originalEvent.touches.length > 1) {
            return;
        }

        if (current.draggable) {
            return;
        }

        if (options.selectorToIgnore && $(e.target).closest(options.selectorToIgnore).length > 0) {
            removeDocumentHandlers();
            return;
        }

        const eventedElement = this;
        const touch = _.find(e.originalEvent.changedTouches, function(t) {
            return $(t.target).closest(eventedElement).length > 0;
        });

        // This should never happen - invalid touch?
        if (!touch) {
            return;
        }

        me.touchIdentifier = touch.identifier;

        cachePositions(e);
        $(document).on('touchmove.dcl', onMove);
        $(document).on('touchend.dcl', onInputStop);
    };

    const onMouseDown = function(e) {
        if (current.draggable) {
            return;
        }

        if (e.which !== 1) {
            return;
        }

        if (options.selectorToIgnore && $(e.target).closest(options.selectorToIgnore).length > 0) {
            return;
        }

        e.preventDefault();
        e.stopPropagation();

        cachePositions(e);

        $(document).on('mousemove.dcl', onMove);
        $(document).on('mouseup.dcl', onInputStop);
        // Add mousedown so that other mouse buttons can stop the drag, and also to help prevent "sticky" drags if the mouse button
        // is released without the mouseup event being triggered (this is especially common when debugging).
        $(document).on('mousedown.dcl', onInputStop);
    };

    this.data = function() {
        return me.$el.data(...arguments);
    };

    this.bindDragHandler = function(fn) {
        $(document).on('mousemove.dcl touchmove.dcl', fn);
    };

    this.unbindDragHandler = function(fn) {
        $(document).off('mousemove.dcl touchmove.dcl', fn);
    };

    this.setEnabled = function(enabled) {
        if (enabled) {
            me.$el.on('mousedown.dcl', onMouseDown);
            me.$el.on('touchstart.dcl', onTouchStart);
        } else {
            me.$el.off('mousedown.dcl touchstart.dcl');
        }
    };

    initialize();
};

// ### Droppable ###

// Create `Droppables` in order to decorate an element such that it can accept `Draggables`.
const Droppable = function(el, options) {
    const me = this;
    this.$el = $(el);

    const onHTML5Drop = function(e) {
        stop(e);
        current.draggable = null;
    };

    const initialize = function() {
        options = options || {};
        me.accepts = options.accepts ||
            function() {
                return true;
            };
        me.priority = options.priority || 0;
        register(me);

        initializeHTML5();
        if (html5Enabled) {
            me.$el.on('dcldropdrag', onHTML5Drop);
        }
        me.enable();
    };

    this.disable = function() {
        me.disabled = true;
        me.$el.off('dcldragover');
        removeOffset(me);
    };

    this.enable = function() {
        me.disabled = false;
        // A requirement for HTML5 drags is that if we're going to accept a drop, we have to call preventDefault() on the dragover.
        me.$el.on('dcldragover', function(e) {
            e.preventDefault();
        });
        prepareOffset(me);
    };

    // TODO I haven't tested this.
    this.destroy = function() {
        me.disable();
        me.$el.off('dcldropdrag');
        remove(me);
    };

    initialize();
};

// Make it possible to trigger events on droppables. TODO Draggable will work if you pass in event handlers as properties,
// but droppable doesn't support that. You have to bind to events on its element. This pattern should be normalized.
_.extend(Droppable.prototype, Backbone.Events);

// ### Public exports ###
module.exports = {
    current: current,
    Droppable: Droppable,
    Draggable: Draggable
};
