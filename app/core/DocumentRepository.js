// This is a default implementation of a collection of documents and canvases. It should be used as the highest level
// storage for documents, and is consumed by `Commands` when they need to figure out what document to
// apply a diff to. It can also be used to provide global access to the "active" canvas.
const $ = require('jquery');
const _ = require('underscore');
const Backbone = require('backbone');
let activeCanvasViewModel = null;

// This is a pass-through object so that consumers only have to bind events once and will
// still have them triggered no matter when the active canvas view model changes.
const itemViewModelEventBus = _.extend({}, Backbone.Events);

// The collection of all canvas view models that the repository knows about.
const viewModelRepository = new Backbone.Collection();

// The collection of all item view models that the repository knows about.
const allItemViewModelsCollection = new Backbone.Collection();

const DocumentRepository = _.extend({
    getViewModelRepository: function() {
        return viewModelRepository;
    },

    getDocument: function(id) {
        const canvasViewModel = viewModelRepository.find(function(vm) {
            return vm.parent.model.id === id;
        });
        if (canvasViewModel) {
            return canvasViewModel.parent.model;
        }
        return null;
    },

    getDocumentViewModel: function(id) {
        const canvasViewModel = viewModelRepository.find(function(vm) {
            return vm.parent.id === id;
        });
        if (canvasViewModel) {
            return canvasViewModel.parent;
        }
        return null;
    },

    getAllItemViewModelsCollection: function() {
        return allItemViewModelsCollection;
    },

    addCanvasViewModel: function(canvasViewModel) {
        if (canvasViewModel.parent.model.get('alternativeOrientation')) {
            canvasViewModel.model.set({ height: canvasViewModel.model.get('width'), width: canvasViewModel.model.get('height') });
            canvasViewModel.chromes.models.forEach(model => {
                model.set({ rotation: 90 });
            });
        }

        return viewModelRepository.add(canvasViewModel);
    },

    removeCanvasViewModel: function(canvasViewModel) {
        viewModelRepository.remove(canvasViewModel);
    },

    getCanvas: function(id) {
        const canvasViewModel = viewModelRepository.find(function(vm) {
            return vm.model.id === id;
        });
        if (canvasViewModel) {
            return canvasViewModel.model;
        }
        return null;
    },

    getCanvasViewModel: function(id) {
        return viewModelRepository.get(id);
    },

    getActiveDocument: function() {
        const canvas = this.getActiveCanvas();
        if (canvas) {
            return canvas.parent;
        }
        return null;
    },

    getActiveDocumentViewModel: function() {
        if (activeCanvasViewModel) {
            return activeCanvasViewModel.parent;
        }
        return null;
    },

    getActiveCanvas: function() {
        if (activeCanvasViewModel) {
            return activeCanvasViewModel.model;
        }
        return null;
    },

    getActiveCanvasViewModel: function() {
        return activeCanvasViewModel;
    },

    getAllCanvasViewModels: function() {
        return viewModelRepository.models;
    },

    getEnabledCanvasViewModels: function() {
        return viewModelRepository.models.filter(canvas => canvas.get('enabled'));
    },

    getVisibleCanvasViewModels: function() {
        return viewModelRepository.models.filter(canvas => !canvas.get('hidden'));
    },

    getAutoPlaceCanvasCount: function() {
        let counter = 0;

        viewModelRepository.models.forEach(model => {
            let isValid = true;

            model.attributes.itemViewModels.every(viewModel => {
                isValid = !viewModel.model.attributes.autoPlaced;
                return isValid;
            });

            if (isValid === true) {
                counter++;
            }
        });
        return counter;
    },

    getNextAutoPlaceTarget: function() {
        let autoPlaceTarget = null;
        viewModelRepository.models.some(model => {

            if (!model.get('enabled')) {
                return false;
            }

            let properTarget = true;

            model.attributes.itemViewModels.every(viewModel => {
                properTarget = !viewModel.model.attributes.autoPlaced;
                return properTarget;
            });

            if (properTarget === true) {
                autoPlaceTarget = model;
            }

            return properTarget;
        });

        return autoPlaceTarget;
    },

    getActiveCanvasViewElement: function() {
        if (activeCanvasViewModel) {
            return $(`#${activeCanvasViewModel.id}`);
        }
        return null;
    },

    getActiveItemViewModelEventBus: function() {
        return itemViewModelEventBus;
    },

    canvasSize: function(canvas) {
        return {
            height: canvas.get('height'),
            width: canvas.get('width')
        };
    },


    clear: function() {
        activeCanvasViewModel = null;
        viewModelRepository.reset();
    }

// Friendly reminder: `Backbone.Events` should never be the first argument in a call to `_.extend()`, or you will extend
// the `Backbone.Events` object itself.
}, Backbone.Events);

viewModelRepository.on('add remove change:active', function(canvasViewModel) {
    if (canvasViewModel.get('active')) {
        if (activeCanvasViewModel) {
            throw new Error('There is already an active canvas view model');
        }
        if (canvasViewModel.itemViewModels) {
            itemViewModelEventBus.listenTo(canvasViewModel.itemViewModels, 'all', function() {
                itemViewModelEventBus.trigger(...arguments);
            });
        }
        activeCanvasViewModel = canvasViewModel;

        if (!canvasViewModel.model.get('viewed')) {
            canvasViewModel.set('isFirstView', true);
            canvasViewModel.model.set('viewed', true);
        } else {
            canvasViewModel.set('isFirstView', false);
        }

        DocumentRepository.trigger('change:activeCanvasViewModel', canvasViewModel);
    } else if (canvasViewModel.previous('active')) {
        if (activeCanvasViewModel) {
            itemViewModelEventBus.stopListening();
        }
        activeCanvasViewModel = null;
    }
});

DocumentRepository.listenTo(viewModelRepository, 'add remove', () => {
    viewModelRepository.stopListening();
    viewModelRepository.models.forEach((canvasViewModel) => {
        canvasViewModel.itemViewModels.forEach(itemViewModel => {
            allItemViewModelsCollection.add(itemViewModel);
        });

        viewModelRepository.listenTo(canvasViewModel.itemViewModels, 'add', (itemViewModel) => {
            allItemViewModelsCollection.add(itemViewModel);
        });
    });
});

// Called after a nested collection is initialized, and used by commands to track changes.
Backbone.on('register:collection', function(collection) {
    collection.on('add', function(model) {
        DocumentRepository.trigger('add', model, collection);
    });
    collection.on('remove', function(model) {
        DocumentRepository.trigger('remove', model, collection);
    });
});

// Called after a nested model is initialized, and used by commands to track changes.
Backbone.on('register:model', function(model) {
    model.on('change', function() {
        DocumentRepository.trigger('change', model);
    });
    model.on('model:change', function() {
        DocumentRepository.trigger('model:change', model);
    });
});

module.exports = DocumentRepository;
