const _ = require('underscore');
const ViewModel = require('viewModels/ViewModel');
const metaDataProvider = require('MetaDataProvider');
const chromeModuleFactory = require('ChromeModuleFactory');

// The canvas view model that we get from the server doesn't have references to the canvas, so we need a way
// to prepare the attributes that we'll actually pass to the canvas view model constructor so that they do
// include the appropriate references to the canvas and to the items.
const createPopulatedCanvasViewModelAttributes = function(rawCanvasViewModel, canvas) {
    const canvasViewModelSpec = _.clone(rawCanvasViewModel);

    canvasViewModelSpec.model = canvas;
    canvasViewModelSpec.itemViewModels = [];
    canvasViewModelSpec.chromes = [];

    const createItemViewModel = function(item, parentViewModel, sparseItemViewModels) {
        let itemViewModelSpec;
        const itemViewModelId = ViewModel.getViewModelId(item);
        const rawItemViewModel = _.findWhere(sparseItemViewModels, { id: itemViewModelId });
        if (rawItemViewModel) {
            itemViewModelSpec = _.clone(rawItemViewModel);
        } else {
            itemViewModelSpec = {
                id: itemViewModelId,
                module: metaDataProvider.getMetaData(item).viewModel
            };
        }

        itemViewModelSpec.model = item;
        parentViewModel.itemViewModels.push(itemViewModelSpec);

        // If this is a nested item, create the nested view models.
        if (item.items) {
            itemViewModelSpec.itemViewModels = [];
            item.items.forEach(function(childItem) {
                createItemViewModel(childItem, itemViewModelSpec, rawItemViewModel ? rawItemViewModel.itemViewModels : []);
            });
        }
    };

    canvas.items.forEach(function(item) {
        createItemViewModel(item, canvasViewModelSpec, rawCanvasViewModel ? rawCanvasViewModel.itemViewModels : []);
    });

    if (rawCanvasViewModel && rawCanvasViewModel.chromes) {
        _.each(rawCanvasViewModel.chromes, function(chrome) {
            canvasViewModelSpec.chromes.push(_.extend({ model: canvas }, chrome));
        });
    }

    return canvasViewModelSpec;
};

// The document view model JSON that comes from the server is sparsely populated, so we
// have to fill it with the relevant document references. `rawDocViewModel` can
// be null or undefined if you want to create it from scratch.
const createDocumentViewModel = function(doc, surfaceSpecifications, scenes, canvasObjects) {

    // The viewModelSpecs are what we'll actually pass to the DocumentViewModel constructor. The rawViewModels are
    // the (optional) input. The separation is necessary in order to make sure that we don't cause any leaks by attaching
    // models to input JSON that is attached to the window.
    const docViewModelSpec = { model: doc };

    if (!docViewModelSpec.id) {
        docViewModelSpec.id = ViewModel.getViewModelId(doc);
    }

    docViewModelSpec.canvasViewModels = doc.canvases.map((canvas, index) => {
        const canvasObjectsChromes = canvasObjects ? canvasObjects.pages[index] : undefined;
        const scene = scenes ? scenes[index] : undefined;
        const chromes = {
            surface: surfaceSpecifications.surfaces[index],
            canvasObjects: canvasObjectsChromes,
            scene
        };

        const canvasViewModelId = ViewModel.getViewModelId(canvas);
        const rawCanvasViewModel = {
            id: canvasViewModelId,
            module: metaDataProvider.getMetaData(canvas).viewModel,
            chromes: chromeModuleFactory.buildChromes(chromes)
        };

        return createPopulatedCanvasViewModelAttributes(rawCanvasViewModel, canvas);
    });

    return new (require('viewModels/' + metaDataProvider.getMetaData(doc).viewModel))(docViewModelSpec);
};

module.exports = {
    createDocumentViewModel
};
