const metadata = require('configuration/MetaData');
const commands = require('configuration/Commands');
const behavior = require('configuration/Behavior');

module.exports = {
    metadata,
    commands,
    behavior
};
