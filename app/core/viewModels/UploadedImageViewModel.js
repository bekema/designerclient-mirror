const _ = require('underscore');
const ImageViewModel = require('viewModels/ImageViewModel');

module.exports = ImageViewModel.extend({
    defaults: _.extend({}, ImageViewModel.prototype.defaults, {
        'module': 'UploadedImageViewModel'
    })
});
