const Backbone = require('backbone');
const ViewModel = require('viewModels/ViewModel');

module.exports = ViewModel.extend({

    __name__: 'DocumentViewModel',

    constructor: function() {
        this.canvasViewModels = new Backbone.NestedCollection();
        ViewModel.apply(this, arguments);
    }

});
