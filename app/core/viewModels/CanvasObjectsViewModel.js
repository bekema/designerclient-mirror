const ChromeViewModel = require('viewModels/ChromeViewModel');

module.exports = ChromeViewModel.extend({

    __name__: 'CanvasObjectsViewModel',

    defaults: {
        objects: [
            {
                className: null,
                content: [
                    {
                        className: null,
                        value: null,
                        top: 0,
                        left: 0,
                        width: 0,
                        height: 0
                    }
                ]
            }
        ],
        module: 'CanvasObjectsViewModel'
    }
});
