const _ = require('underscore');
const ChromeViewModel = require('viewModels/ChromeViewModel');
const rulerConfig = require('publicApi/configuration/ui').canvas.chromes.ruler;
const colorManager = require('ColorManager');
const colorConverter = require('util/ColorConverter');
const globalConfig = require('configuration/Global');

module.exports = ChromeViewModel.extend({

    __name__: 'CanvasRulerViewModel',

    defaults: _.extend({
        color: rulerConfig.color || 'rgba(0, 0, 0, 0.5)',
        height: 0,
        width: 0,
        lines: rulerConfig.lines,
        length: rulerConfig.length,
        lineWidth: rulerConfig.lineWidth,
        margin: rulerConfig.margin,
        module: 'CanvasRulerViewModel'
    }, ChromeViewModel.prototype.defaults),

    initialize: function(rulerViewModel, canvasViewModel) {
        ChromeViewModel.prototype.initialize.apply(this, arguments);

        // TODO: This might break someday in time or not.....
        if (globalConfig.scenes[canvasViewModel.parent.model.id - 1] && !rulerConfig.color) {
            colorManager.getMostVisibleDefaultColor(canvasViewModel.parent)
                .then(hex => {
                    const rgb = colorConverter.hex2rgb(hex);
                    this.set('color', `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, 0.5)`);
                });
        }
    },

    computed: _.extend({}, ChromeViewModel.prototype.computed, {
        top: {
            get: function() {
                return -(this.get('margin') + this.get('length'));
            },
            bindings: function() {
                return ['margin', 'length'];
            }
        },
        left: {
            get: function() {
                return -(this.get('margin') + this.get('length'));
            },
            bindings: function() {
                return ['margin', 'length'];
            }
        },
        width: {
            get: function() {
                return this.parent.get('size').width + (-this.get('left') * 2);
            },
            bindings: function() {
                return [
                    ['left'],
                    { model: this.parent, attribute: 'size' }
                ];
            }
        },
        height: {
            get: function() {
                return this.parent.get('size').height + (-this.get('top') * 2);
            },
            bindings: function() {
                return [
                    ['top'],
                    { model: this.parent, attribute: 'size' }
                ];
            }
        }
    })

});
