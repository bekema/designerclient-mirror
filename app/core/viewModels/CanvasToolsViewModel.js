const canvasToolsConfig = require('publicApi/configuration/ui').canvas.chromes.canvasTools;
const ChromeViewModel = require('viewModels/ChromeViewModel');

module.exports = ChromeViewModel.extend({

    defaults: {
        top: canvasToolsConfig.top,
        left: canvasToolsConfig.left
    },

    __name__: 'CanvasToolsViewModel'
});
