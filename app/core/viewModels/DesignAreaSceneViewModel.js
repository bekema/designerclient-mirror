const _ = require('underscore');
const ChromeViewModel = require('viewModels/ChromeViewModel');

const DesignAreaSceneViewModel = ChromeViewModel.extend({

    __name__: 'DesignAreaSceneViewModel',

    defaults: _.extend({
        module: 'DesignAreaSceneViewModel'
    }, ChromeViewModel.prototype.defaults),

    computed: _.extend({}, ChromeViewModel.prototype.computed, {
        url: {
            get: function() {
                const width = this.get('dimensions').width;
                const encodedUri = window.encodeURIComponent(this.get('sceneUri'));
                return `https://rendering.documents.cimpress.io/v1/dcl/preview?width=${width}&scene=${encodedUri}`;
            },

            bindings: function() {
                return ['sceneUri', 'dimensions'];
            }
        }
    })
});

module.exports = DesignAreaSceneViewModel;
