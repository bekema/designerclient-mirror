const Backbone = require('backbone');

module.exports = Backbone.NestedCollection.extend({

    __name__: 'ItemViewModelCollection',

    comparator: function(itemViewModel) {
        return itemViewModel.model.get('zIndex');
    },

    byModule: function(moduleName) {
        return this.where({
            module: moduleName
        });
    }
});
