const _ = require('underscore');
const ChromeViewModel = require('viewModels/ChromeViewModel');
const colorManager = require('ColorManager');
const colorConverter = require('util/ColorConverter');
const borderConfig = require('publicApi/configuration/ui').canvas.chromes.border;
const globalConfig = require('configuration/Global');

module.exports = ChromeViewModel.extend({

    __name__: 'CanvasBorderViewModel',

    defaults: _.extend({
        color: borderConfig.color || 'rgba(0, 0, 0, 0.5)',
        borderWidth: borderConfig.lineWidth,
        module: 'CanvasBorderViewModel'
    }, ChromeViewModel.prototype.defaults),

    initialize: function(borderViewModel, canvasViewModel) {
        ChromeViewModel.prototype.initialize.apply(this, arguments);

        // TODO: This might break someday in time or not.....
        if (globalConfig.scenes[canvasViewModel.parent.model.id - 1] && !borderConfig.color) {
            colorManager.getMostVisibleDefaultColor(canvasViewModel.parent)
                .then(hex => {
                    const rgb = colorConverter.hex2rgb(hex);
                    this.set('color', `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, 0.5)`);
                });
        }
    },

    computed: _.extend({}, ChromeViewModel.prototype.computed, {
        top: {
            get: function() {
                return -this.get('borderWidth');
            },
            bindings: function() {
                return ['borderWidth'];
            }
        },
        left: {
            get: function() {
                return -this.get('borderWidth');
            },
            bindings: function() {
                return ['borderWidth'];
            }
        },
        width: {
            get: function() {
                return this.parent.get('size').width - this.get('left') - this.get('right');
            },
            bindings: function() {
                return [
                    'left', 'right',
                    { model: this.parent, attribute: 'size' }
                ];
            }
        },
        height: {
            get: function() {
                return this.parent.get('size').height - this.get('top') - this.get('bottom');
            },
            bindings: function() {
                return [
                    'top', 'bottom',
                    { model: this.parent, attribute: 'size' }
                ];
            }
        }
    })

});
