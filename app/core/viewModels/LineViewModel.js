const _ = require('underscore');
const ShapeViewModel = require('viewModels/ShapeViewModel');

module.exports = ShapeViewModel.extend({
    initialize: function() {
        ShapeViewModel.prototype.initialize.apply(this, arguments);
    },

    computed: _.extend({}, ShapeViewModel.prototype.computed, {
        handles: {
            get: function() {
                if (this.model.get('locked')) {
                    return '';
                }

                return 'e,w';
            },
            bindings: function() {
                return [
                    { model: this.model, attribute: 'locked' }
                ];
            }
        }
    })
});
