const _ = require('underscore');
const ChromeViewModel = require('viewModels/ChromeViewModel');

const SceneViewModel = ChromeViewModel.extend({

    __name__: 'CanvasViewModel',

    defaults: _.extend({
        module: 'SceneViewModel'
    }, ChromeViewModel.prototype.defaults),

    computed: _.extend({}, ChromeViewModel.prototype.computed, {
        url: {
            get: function() {
                const width = Math.min(this.get('realDimensions').width, this.get('widthInPixels'));
                const encodedUri = window.encodeURIComponent(this.get('sceneUri'));
                return `https://rendering.documents.cimpress.io/v1/dcl/preview?width=${width}&scene=${encodedUri}`;
            },
            bindings: function() {
                return ['sceneUri', 'realDimensions', 'widthInPixels'];
            }
        },

        realDimensions: {
            get: function() {
                return {
                    top: this.get('top'),
                    left: this.get('left'),
                    width: this.get('width'),
                    height: this.get('height')
                };
            },
            bindings: function() {
                return [
                    ['top', 'left', 'width', 'height']
                ];
            }
        },

        //This is important that we override this, as we don't want this to be taken into account when designating
        //the margins, so just return the canvas size instead.
        dimensions: {
            get: function() {
                const size = this.parent.get('size');
                return {
                    height: size.height,
                    width: size.width,
                    top: 0,
                    left: 0
                };
            },
            bindings: function() {
                return [{ model: this.parent, attribute: 'size' }];
            }
        },

        top: {
            get: function() {
                const top = this.get('decorationAreaDimensions').top;
                const zoom = this.get('relativeZoom');
                return Math.round(-top * zoom);
            },
            bindings: function() {
                return ['decorationAreaDimensions', 'relativeZoom'];
            }
        },

        left: {
            get: function() {
                const left = this.get('decorationAreaDimensions').left;
                const zoom = this.get('relativeZoom');
                return Math.round(-left * zoom);
            },
            bindings: function() {
                return ['decorationAreaDimensions', 'relativeZoom'];
            }
        },

        width: {
            get: function() {
                const zoom = this.get('relativeZoom');
                return Math.round(this.get('widthInPixels') * zoom);
            },
            bindings: function() {
                return ['widthInPixels', 'relativeZoom'];
            }
        },

        height: {
            get: function() {
                const zoom = this.get('relativeZoom');
                return Math.round(this.get('heightInPixels') * zoom);
            },
            bindings: function() {
                return ['heightInPixels', 'relativeZoom'];
            }
        },

        decorationAreaDimensions: {
            get: function() {
                const pos = this.get('positionWithinImage');
                const top = pos.topLeft.y;
                const right = pos.topRight.x;
                const bottom = pos.bottomLeft.y;
                const left = pos.topLeft.x;
                const width = right - left;
                const height = bottom - top;

                return {
                    top,
                    right,
                    bottom,
                    left,
                    width,
                    height
                };

            },
            bindings: function() {
                return ['positionWithinImage'];
            }
        },

        relativeZoom: {
            get: function() {
                const { width: imageWidth } = this.get('decorationAreaDimensions');
                const { width: canvasWidth } = this.parent.get('size');

                return canvasWidth / imageWidth;
            },
            bindings: function() {
                return [
                    ['decorationAreaDimensions'],
                    { model: this.parent, attribute: 'size' }
                ];
            }
        }
    })
});

module.exports = SceneViewModel;
