const _ = require('underscore');
const ImageViewModel = require('viewModels/ImageViewModel');
const metaDataProvider = require('MetaDataProvider');
const clients = require('services/clientProvider');

module.exports = ImageViewModel.extend({
    defaults: _.extend({}, ImageViewModel.prototype.defaults, {
        'module': 'EmbroideryImageViewModel'
    }),

    computed: _.extend({}, ImageViewModel.prototype.computed, {
        digitizationNeeded: {
            get: function() {
                return this.model.get('module') === 'EmbroideryImage' && !this.model.get('digitizedId');
            },
            bindings: function() {
                return [{ model: this.model, attributes: ['module', 'digitizedId'] }];
            }
        },

        url: {
            get: function() {
                const attributes = this.model.pick(metaDataProvider.getMetaData('EmbroideryImage').previewAttributes);
                attributes.zoom = this.parent.get('zoomFactor');

                if (this.model.get('digitizedId')) {
                    attributes.url = attributes.xtiUrl;
                } else {
                    attributes.url = attributes.previewUrl;
                }
                return clients.preview.renderImage(attributes);
            },
            bindings: function() {
                const attributes = metaDataProvider.getMetaData('EmbroideryImage').previewAttributes;
                return [
                    { model: this.model, attributes },
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        }
    })
});
