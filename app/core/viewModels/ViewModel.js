// ## View Model
// This is the base class for document, canvas, and item related View Models.
// Usage: `const viewModel = new ViewModel({ model : model }, { parent : parentViewModel });`

const _ = require('underscore');
const Backbone = require('backbone');
const ComputedAttributeMixin = require('backbone-extensions/backbone.computed');

const ViewModel = Backbone.NestedModel.extend({

    __name__: 'ViewModel',

    constructor: function(attrs, options) {
        if (!attrs.model) {
            throw new Error('ViewModel requires a model attribute');
        }

        this.model = attrs.model;
        // We don't want the model to be accessible as an attribute later on.
        Backbone.NestedModel.call(this, _.omit(attrs, 'model'), options);
    },

    initialize: function(attrs, options) {
        // If this is created as part of a nested model, `parent` will be set automatically after initialization is complete.
        // However, we need it set now since id generation and computed attributes depend on it.
        if (options && options.parent) {
            this.parent = options.parent;
        }
        if (!this.has('id')) {
            this.set('id', ViewModel.getViewModelId(this.model));
        }
        if (this.has('idSuffix')) {
            this.set('id', `${this.get('id')}${this.get('idSuffix')}`);
        }
        this.createComputedAttributes();
        this.addListeners();
        Backbone.NestedModel.prototype.initialize.apply(this, arguments);
    },

    onModelEvent: function(eventName) {
        //Pass along all arguments but prefix event with 'model:' and change the model to be the viewmodel
        const args = Array.prototype.slice.call(arguments); //arguments is an arraylike object so we need to slice to turn it into an array
        args.splice(0, 2, `model:${eventName}`, this);
        this.trigger(...args);
    },

    addListeners: function() {
        this.listenTo(this.model, 'destroy', this.destroy);
        this.listenTo(this.model, 'all', this.onModelEvent);
    }
});

_.extend(ViewModel.prototype, ComputedAttributeMixin);

let viewModelIdPrefix = 'd';

// Static functions for getting properly constructed view model ids.
ViewModel.setIdPrefix = function(prefix) {
    viewModelIdPrefix = prefix;
};

ViewModel.getViewModelId = function(model) {
    let id = !_.isUndefined(model.id) ? model.id : _.uniqueId('vm');
    let parent = model.parent;
    while (parent) {
        id = `${parent.id}-${id}`;
        parent = parent.parent;
    }

    return `${viewModelIdPrefix}${id}`;
};

module.exports = ViewModel;
