//Base class for any underlays/overlays
const ViewModel = require('viewModels/ViewModel');

module.exports = ViewModel.extend({

    defaults: {
        top: 0,
        left: 0
    },

    computed: {
        //The width and height of the chromes are zoomed, since there are aspects to chromes that
        //do not zoom in when the canvas is zoomed in. Thus applying a zoomfactor to an unzoomed width
        //will grant you the wrong value.
        width: {
            get: function() {
                return this.parent.get('size').width;
            },
            bindings: function() {
                return [{ model: this.parent, attribute: 'size' }];
            }
        },
        height: {
            get: function() {
                return this.parent.get('size').height;
            },
            bindings: function() {
                return [{ model: this.parent, attribute: 'size' }];
            }
        },
        dimensions: {
            get: function() {
                return {
                    top: this.get('top'),
                    left: this.get('left'),
                    width: this.get('width'),
                    height: this.get('height')
                };
            },
            bindings: function() {
                return [
                    ['top', 'left', 'width', 'height']
                ];
            }
        }
    }
});
