const _ = require('underscore');
const ItemViewModel = require('viewModels/ItemViewModel');
const clients = require('services/clientProvider');
const metaDataProvider = require('MetaDataProvider');
const localization = require('Localization');

/* eslint-disable no-magic-numbers */

module.exports = ItemViewModel.extend({

    __name__: 'TextFieldViewModel',

    defaults: _.extend({}, ItemViewModel.prototype.defaults, {
        'snapBox': { top: 0, left: 0, width: 0, height: 0 },
        'originalBaselines': [],
        'module': 'TextFieldViewModel',
        'analyticsPrefix': 'TextField'
    }),

    computed: _.extend({}, ItemViewModel.prototype.computed, {
        'url': {
            get: function() {
                const attributes = this.model.pick(metaDataProvider.getMetaData('TextField').previewAttributes);
                attributes.zoom = this.parent.get('zoomFactor');
                if (!attributes.innerHTML) {
                    attributes.innerHTML = localization.getText(`text.${this.model.get('placeholder')}`);
                }
                return clients.preview.renderTextField(attributes);
            },
            bindings: function() {
                const attributes = metaDataProvider.getMetaData('TextField').previewAttributes;
                return [
                    { model: this.model, attributes },
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },


        'analyticsPrefixSubCategory': {
            get: function() {
                return this.model.get('writingMode') === 'horizontal-tb' ?
                    'Horizontal' : 'Vertical';
            },
            bindings: function() {
                return [
                    { model: this.model, attribute: 'writingMode' }
                ];
            }
        },
        'snapPosition': {
            get: function() {
                const zoom = this.parent.get('zoomFactor');
                const previewPosition = this.get('previewPosition');
                const snapBox = this.get('snapBox');

                return {
                    top: previewPosition.top - Math.round(snapBox.top * zoom),
                    left: previewPosition.left - Math.round(snapBox.left * zoom)
                };
            },
            bindings: function() {
                return [
                    ['previewPosition', 'snapBox'],
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },
        'snapSize': {
            get: function() {
                const zoom = this.parent.get('zoomFactor');
                const snapBox = this.get('snapBox');

                return {
                    width: Math.round(snapBox.width * zoom),
                    height: Math.round(snapBox.height * zoom)
                };
            },
            bindings: function() {
                return [
                    'snapBox',
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },
        'baselines': {
            get: function() {
                const position = this.get('handlePosition');
                const zoomFactor = this.parent.get('zoomFactor');

                return this.get('originalBaselines').map(function(baseline) {
                    return position.top + Math.round(baseline * zoomFactor);
                });
            },
            bindings: function() {
                return [
                    ['originalBaselines', 'handlePosition'],
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },
        'previewPosition': {
            get: function() {
                const zoom = this.parent.get('zoomFactor');
                const position = ItemViewModel.prototype.computed.previewPosition.get.apply(this);
                const tempDelta = this.get('temporarySizeDelta');
                const rotation = this.model.get('rotation');
                if (tempDelta) {
                    const alignment = this.model.get('horizontalAlignment');
                    if (alignment === 'right' && rotation === 0) {
                        position.top += Math.round(tempDelta.height * zoom);
                        position.left += Math.round(tempDelta.width * zoom);
                    } else if (alignment === 'center') {
                        position.top += Math.round((tempDelta.height * zoom) / 2);
                        position.left += Math.round((tempDelta.width * zoom) / 2);
                    }
                }
                return position;
            },
            bindings: function() {
                const bindings = ItemViewModel.prototype.computed.previewPosition.bindings.apply(this);
                bindings.push(['temporarySizeDelta', 'horizontalAlignment']);
                bindings.push({ model: this.parent, attribute: 'zoomFactor' });
                bindings.push({ model: this.model, attribute: 'rotation' });
                return bindings;
            }
        },

        'transformOrigin': {
            get: function() {
                const handleSize = this.get('handleSize');
                const handlePosition = this.get('handlePosition');
                const previewPosition = this.get('previewPosition');

                const handleCenterX = handlePosition.left + handleSize.width / 2;
                const centeredX = handleCenterX - previewPosition.left;

                const handleCenterY = handlePosition.top + handleSize.height / 2;
                const centeredY = handleCenterY - previewPosition.top;

                return `${centeredX}px ${centeredY}px 0`;
            },

            bindings: function() {
                return ['handleSize', 'handlePosition', 'previewPosition'];
            }
        },

        'relativePreviewPosition': {
            // provides the top and left of the preview based on the handle
            get: function() {
                const handlePos = this.get('handlePosition');
                const previewPos = this.get('previewPosition');
                const left = previewPos.left - handlePos.left;
                const top = previewPos.top - handlePos.top;

                return { left, top };
            },
            bindings: function() {
                return ['handlePosition', 'previewPosition'];
            }
        },
        'previewSize': {
            get: function() {
                if (this.model.get('innerHTML').length === 0 && this.model.get('clickText').length === 0) {
                    return {
                        width: 0,
                        height: 0
                    };
                }
                const zoom = this.parent.get('zoomFactor');
                const previewBox = this.get('previewBox');
                const tempDelta = this.get('temporarySizeDelta');
                if (tempDelta) {
                    const handleSize = this.get('handleSize');
                    return {
                        width: Math.round(Math.min(previewBox.width * zoom, handleSize.width)),
                        height: Math.round(Math.min(previewBox.height * zoom, handleSize.height))
                    };
                }

                return ItemViewModel.prototype.computed.previewSize.get.apply(this);
            },
            bindings: function() {
                const bindings = ItemViewModel.prototype.computed.previewSize.bindings.apply(this);
                bindings.push({ model: this.model, attributes: ['innerHTML', 'clickText'] });
                bindings.push({ model: this.parent, attribute: 'zoomFactor' });
                bindings.push(['previewBox', 'temporarySizeDelta', 'handleSize']);
                return bindings;
            }
        },
        'handles': {
            get: function() {
                if (this.model.get('locked')) {
                    return '';
                }
                return 'e,w';
            },
            bindings: function() {
                return [{ model: this.model, attribute: 'locked' }];
            }
        },
        'colors': {
            get: function() {
                return [this.model.get('fontColor')];
            },
            bindings: function() {
                return [
                    { model: this.model, attribute: 'fontColor' }
                ];
            }
        }
    }),

    initialize: function() {
        ItemViewModel.prototype.initialize.apply(this, arguments);
        this.on('loadimage', this.onLoadImageWithMetaData, this);
        // Sometimes the models have heights and widths that aren't correct, but we don't determine that until
        // the text field is rendered. We do render the correct size in the ItemViewModelBuilder
        const actualSize = this.get('actualSize');
        if (actualSize && actualSize.height && actualSize.width) {
            this.model.set('width', actualSize.width);
            this.model.set('height', actualSize.height);
        }
    },

    onLoadImageWithMetaData: function(e) {
        this.set({
            'previewBox': e.metaData.previewBox,
            'snapBox': e.metaData.snapBox,
            'originalBaselines': e.metaData.baselines
        });

        const imgHeight = e.metaData.actual.height;

        const attrs = {
            top: this.model.get('top'),
            left: this.model.get('left'),
            width: this.model.get('width'),
            height: this.model.get('height')
        };

        //Adjust the height if it had to be changed by the server and grow the field
        if (attrs.height !== imgHeight) {
            attrs.height = imgHeight;
        }

        this.model.set(attrs);
    }
});
