const _ = require('underscore');
const ItemViewModel = require('viewModels/ItemViewModel');
const util = require('util/Util');
const RectangleSnapMixin = require('views/tools/mixins/RectangleSnapMixin');
const clients = require('services/clientProvider');
const metaDataProvider = require('MetaDataProvider');

const ShapeViewModel = ItemViewModel.extend({

    defaults: _.extend({}, ItemViewModel.prototype.defaults, {
        'snapBox': { top: 0, left: 0, width: 0, height: 0 },
        'analyticsPrefix': 'Shape',
        'module': 'ShapeViewModel'
    }),

    initialize: function() {
        ItemViewModel.prototype.initialize.apply(this, arguments);

        // this ensures the handles for shapes have a size before
        // actually being rendered by the preview service
        this.set('temporarySizeDelta', {
            width: this.model.get('width'),
            height: this.model.get('height')
        });

        this.on('loadimage', this.onLoadImage, this);
    },

    computed: _.extend({}, ItemViewModel.prototype.computed, {
        url: {
            get: function() {
                const attributes = this.model.pick(metaDataProvider.getMetaData(this.model).previewAttributes);
                attributes.zoom = this.parent.get('zoomFactor');
                return clients.preview.renderShape(attributes);
            },
            bindings: function() {
                const attributes = metaDataProvider.getMetaData(this.model).previewAttributes;
                return [
                    { model: this.model, attributes },
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },

        handlePosition: {
            get: function() {
                return this.get('previewPosition');
            },
            bindings: function() {
                return ['previewPosition'];
            }
        },

        handleSize: {
            get: function() {
                return this.get('previewSize');
            },
            bindings: function() {
                return ['previewSize'];
            }
        },

        colors: {
            get: function() {
                return _.uniq([this.model.get('fillColor'), this.model.get('strokeColor')]);
            },
            bindings: function() {
                return [
                    { model: this.model, attributes: ['fillColor', 'strokeColor'] }
                ];
            }
        },

        selectable: {
            get: function() {
                return !this.model.get('isBackground') && this.parent.get('enabled');
            },
            bindings: function() {
                return [
                    'isBackground',
                    { model: this.parent, attribute: 'enabled' }
                ];
            }
        }
    }),

    onLoadImage: function({ metaData }) {
        this.set({
            previewBox: metaData.previewBox,

            snapBox: {
                top: 0,
                left: 0,
                width: metaData.previewBox.width,
                height: metaData.previewBox.height
            }
        });
    }
});

util.addMixin(ShapeViewModel.prototype, RectangleSnapMixin);

module.exports = ShapeViewModel;
