const _ = require('underscore');
const ChromeViewModel = require('viewModels/ChromeViewModel');

module.exports = ChromeViewModel.extend({

    __name__: 'SurfaceSpecificationViewModel',

    // Surface Specifiactions by: https://corewiki.cimpress.net/wiki/Design_Client_Library_surface_specifications
    // Surface Specification is loaded by the index.html
    defaults: _.extend({
        rotation: 0,
        previewPosition: { left: 0, top: 0 },
        position: { left: 0, top: 0 },
        name: null,
        widthInMm: 0,
        heightInMm: 0,
        decorationTechnologyMetadata: null,
        fulfillerMetadata: [{
            key: 'IsCustomizable',
            value: 'True'
        }],
        masks: [{
            pathType: null,
            paths: [{
                anchorX: 0,
                anchorY: 0,
                pathPoints: [{
                    ordinal: 0,
                    firstControlPointX: null,
                    firstControlPointY: null,
                    secondControlPointX: null,
                    secondControlPointY: null,
                    endPointX: 0,
                    endPointY: 0
                }]
            }]
        }],
        module: 'SurfaceSpecificationViewModel'
    }, ChromeViewModel.prototype.defaults),

    computed: _.extend({}, ChromeViewModel.prototype.computed, {
        'width': {
            get: function() {
                return Math.round(this.get('widthInMm') * this.parent.get('zoomFactor'));
            },
            bindings: function() {
                return [
                    'widthInMm',
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },
        'height': {
            get: function() {
                return Math.round(this.get('heightInMm') * this.parent.get('zoomFactor'));
            },
            bindings: function() {
                return [
                    'heightInMm',
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },
        'safeArea': {
            get: function() {
                return {
                    width: this.get('width'),
                    height: this.get('height')
                };
            },
            bindings: function() {
                return ['width', 'height'];
            }
        },
        'size': {
            get: function() {
                return {
                    width: this.get('width'),
                    height: this.get('height')
                };
            },
            bindings: function() {
                return ['width', 'height'];
            }
        },
        'previewSize': {
            get: function() {
                return {
                    width: this.get('width'),
                    height: this.get('height')
                };
            },
            bindings: function() {
                return ['width', 'height'];
            }
        }
    }),

    getMargins: function() {
        const masks = this.get('masks');
        const margins = [];

        // Loop through the masks
        masks.forEach(function(mask) {

            // Loop through the paths
            mask.paths.forEach(function(path) {
                const pathType = mask.pathType;
                const pathString = this.createSVGPath(path);

                const margin = {
                    pathType,
                    pathString,
                    options: {
                        strokeWidth: 1,
                        color: 'black'
                    }
                };

                margins.push(margin);

            }, this);

        }, this);

        return margins;
    },

    createSVGPath: function(path) {
        let pathString = 'M ' + path.anchorX + ' ' + path.anchorY;

        // Check if pathpoint contains curves
        if (path.pathPoints[0]) {
            if (path.pathPoints[0].firstControlPointX > 0 || path.pathPoints[0].secondControlPointX > 0) {
                // Detect curves and straight lines
                pathString += ' C ';
            } else {
                pathString += ' L ';
            }
        }

        // Loop through the pathPoints
        path.pathPoints.forEach(function(pathPoint) {
            if (pathPoint.firstControlPointX !== null) {
                pathString += ' ' + pathPoint.firstControlPointX;
            }

            if (pathPoint.firstControlPointY !== null) {
                pathString += ' ' + pathPoint.firstControlPointY;
            }

            if (pathPoint.secondControlPointX !== null) {
                pathString += ' ' + pathPoint.secondControlPointX;
            }

            if (pathPoint.secondControlPointY !== null) {
                pathString += ' ' + pathPoint.secondControlPointY;
            }

            pathString += ' ' + pathPoint.endPointX;
            pathString += ' ' + pathPoint.endPointY;
        }, this);

        // Close SVG path
        pathString += ' Z';

        return pathString;
    }
});
