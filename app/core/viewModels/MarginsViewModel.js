const _ = require('underscore');
const ChromeViewModel = require('viewModels/ChromeViewModel');

module.exports = ChromeViewModel.extend({

    __name__: 'MarginsViewModel',

    defaults: _.extend({
        safetyMargin: {
            top: 0,
            right: 0,
            bottom: 0,
            left: 0
        },
        bleedMargin: {
            top: 0,
            right: 0,
            bottom: 0,
            left: 0
        },
        module: 'MarginsViewModel'
    }, ChromeViewModel.prototype.defaults),

    computed: _.extend({}, ChromeViewModel.prototype.computed, {
        safeArea: {
            get: function() {
                return {
                    top: this.get('safetyMargin').top + this.get('bleedMargin').top,
                    left: this.get('safetyMargin').left + this.get('bleedMargin').left,
                    bottom: this.get('safetyMargin').bottom + this.get('bleedMargin').bottom,
                    right: this.get('safetyMargin').right + this.get('bleedMargin').right,
                    width: this.parent.get('canvasSize').width - this.get('safetyMargin').left - this.get('safetyMargin').right - this.get('bleedMargin').left - this.get('bleedMargin').right,
                    height: this.parent.get('canvasSize').height - this.get('safetyMargin').top - this.get('safetyMargin').bottom - this.get('bleedMargin').top - this.get('bleedMargin').bottom
                };
            },
            bindings: function() {
                return [
                    'bleedMargin', 'safetyMargin',
                    { model: this.parent, attribute: 'canvasSize' }
                ];
            }
        },
        bleedMarginTop: {
            get: function() {
                return Math.round(this.get('bleedMargin').top * this.parent.get('zoomFactor'));
            },
            bindings: function() {
                return [
                    'bleedMargin',
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },
        bleedMarginLeft: {
            get: function() {
                return Math.round(this.get('bleedMargin').left * this.parent.get('zoomFactor'));
            },
            bindings: function() {
                return [
                    'bleedMargin',
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },
        bleedMarginBottom: {
            get: function() {
                return Math.round(this.get('bleedMargin').bottom * this.parent.get('zoomFactor'));
            },
            bindings: function() {
                return [
                    'bleedMargin',
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },
        bleedMarginRight: {
            get: function() {
                return Math.round(this.get('bleedMargin').right * this.parent.get('zoomFactor'));
            },
            bindings: function() {
                return [
                    'bleedMargin',
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },
        safetyMarginTop: {
            get: function() {
                return Math.round(this.get('safetyMargin').top * this.parent.get('zoomFactor'));
            },
            bindings: function() {
                return [
                    'safetyMargin',
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },
        safetyMarginLeft: {
            get: function() {
                return Math.round(this.get('safetyMargin').left * this.parent.get('zoomFactor'));
            },
            bindings: function() {
                return [
                    'safetyMargin',
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },
        safetyMarginBottom: {
            get: function() {
                return Math.round(this.get('safetyMargin').bottom * this.parent.get('zoomFactor'));
            },
            bindings: function() {
                return [
                    'safetyMargin',
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },
        safetyMarginRight: {
            get: function() {
                return Math.round(this.get('safetyMargin').right * this.parent.get('zoomFactor'));
            },
            bindings: function() {
                return [
                    'safetyMargin',
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },
        totalMarginTop: {
            get: function() {
                return this.get('safetyMarginTop') + this.get('bleedMarginTop');
            },
            bindings: function() {
                return ['safetyMarginTop', 'bleedMarginTop'];
            }
        },
        totalMarginRight: {
            get: function() {
                return this.get('safetyMarginRight') + this.get('bleedMarginRight');
            },
            bindings: function() {
                return ['safetyMarginRight', 'bleedMarginRight'];
            }
        },
        totalMarginBottom: {
            get: function() {
                return this.get('safetyMarginBottom') + this.get('bleedMarginBottom');
            },
            bindings: function() {
                return ['safetyMarginBottom', 'bleedMarginBottom'];
            }
        },
        totalMarginLeft: {
            get: function() {
                return this.get('safetyMarginLeft') + this.get('bleedMarginLeft');
            },
            bindings: function() {
                return ['safetyMarginLeft', 'bleedMarginLeft'];
            }
        }
    })
});
