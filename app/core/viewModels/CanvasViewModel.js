const _ = require('underscore');
const ChromeViewModelCollection = require('viewModels/ChromeViewModelCollection');
const globalConfig = require('configuration/Global');
const ItemViewModelCollection = require('viewModels/ItemViewModelCollection');
const metaDataProvider = require('MetaDataProvider');
const urlUtil = require('util/UrlUtil');
const ViewModel = require('viewModels/ViewModel');
const eventBus = require('EventBus');
const events = eventBus.events;

module.exports = ViewModel.extend({

    __name__: 'CanvasViewModel',

    defaults: {
        zoomFactor: 1.0,
        active: false,
        enabled: true,
        hidden: false,
        itemsHidden: false,
        disableImageUpdates: false
    },

    computed: {
        canvasSize: {
            get: function() {
                return {
                    width: Math.round(this.model.get('width')),
                    height: Math.round(this.model.get('height'))
                };
            },
            bindings: function() {
                return [{ model: this.model, attributes: ['width', 'height'] }];
            }
        },
        size: {
            get: function() {
                const zoom = this.get('zoomFactor');
                return {
                    width: Math.round(this.model.get('width') * zoom),
                    height: Math.round(this.model.get('height') * zoom)
                };
            },
            bindings: function() {
                return [
                    'zoomFactor',
                    { model: this.model, attributes: ['width', 'height'] }
                ];
            }
        },
        safeArea: {
            get: function() {
                const marginsViewModel = this.get('chromes').findWhere({ module: 'MarginsViewModel' });
                return marginsViewModel ? marginsViewModel.get('safeArea') : { top: 0, left: 0, bottom: 0, right: 0, width: this.model.get('width'), height: this.model.get('height') };
            },
            bindings: function() {
                return [
                    'chromes',
                    { model: this.model, attributes: ['width', 'height'] }
                ];
            }
        },
        zoomedSafeArea: {
            get: function() {
                return _.mapObject(this.get('safeArea'), function(val) {
                    return val * this.get('zoomFactor');
                }, this);
            },
            bindings: function() {
                return ['safeArea', 'zoomFactor'];
            }
        },
        maxChromeDimensions: {
            get: function() {
                if (this.chromes && this.chromes.length) {
                    const left = this.chromes.min(chromeViewModel => chromeViewModel.get('dimensions').left).get('dimensions');
                    const top = this.chromes.min(chromeViewModel => chromeViewModel.get('dimensions').top).get('dimensions');
                    const right = this.chromes.max(chromeViewModel => chromeViewModel.get('dimensions').width + chromeViewModel.get('dimensions').left).get('dimensions');
                    const bottom = this.chromes.max(chromeViewModel => chromeViewModel.get('dimensions').height + chromeViewModel.get('dimensions').top).get('dimensions');
                    const canvasWidth = this.get('size').width;
                    const canvasHeight = this.get('size').height;

                    return {
                        top: top.top,
                        left: left.left,
                        right: canvasWidth - (right.width + right.left),
                        bottom: canvasHeight - (bottom.height + bottom.top),
                        width: right.width + right.left - left.left,
                        height: bottom.height + bottom.top - top.top
                    };
                } else {
                    //if no chromes return canvas size
                    return _.extend({ top: 0, left: 0, right: 0, bottom: 0 }, this.get('size'));
                }
            },
            bindings: function() {
                return [
                    'size',
                    { model: this.chromes, attribute: 'dimensions' }
                ];
            }
        }
    },

    constructor: function() {
        this.itemViewModels = new ItemViewModelCollection();
        this.chromes = new ChromeViewModelCollection();
        ViewModel.apply(this, arguments);
    },

    getPreviewUrl: function(options) {
        options = options || {};
        const path = this.getPreviewPath();
        const url = urlUtil.createUrl(path);
        url.setItem('zoom', options.zoomFactor || this.get('zoomFactor'));
        // Width is special - shouldn't be in the url keys, because we don't want items to use
        // it when referring to their parents..
        url.setItem('width', this.model.get('width'));
        url.setItem('height', this.model.get('height'));
        //TODO: Use the meta data url keys for the canvas and auto combine that with the options.
        const colorization = options.colorization !== null ? options.colorization : this.model.get('colorization');
        url.setItem('color', colorization);
        const previewObjects = this.itemViewModels.map(function(itemViewModel) {
            return itemViewModel.getPreviewObject(itemViewModel.model.toJSON());
        });
        url.setItem('items', JSON.stringify(previewObjects));
        return url.toString();
    },

    getPreviewPath: function() {
        if (this.containsBermudaRenderingOnlyItem()) {
            // use web server because image server does not have access to BDA (Bermuda) database
            return globalConfig.webServerPreviewPath;
        }
        return globalConfig.imageServerPreviewPath;
    },

    onAddItem: function(item, itemCollection, options) {
        this.itemViewModels.add(metaDataProvider.getViewModel(item, this, options.viewModelAttributes));

        eventBus.trigger(events.manageCanvases, { activeCanvas: this.model.id });
    },

    addListeners: function() {
        if (this.model.items) {
            // Automatically handle creating view models when the underlying models are added. The item view models
            // listen to destroy events on their models, and take care of removing themselves from this collection.
            this.listenTo(this.model.items, 'add', this.onAddItem);
        }
        ViewModel.prototype.addListeners.apply(this, arguments);
    },

    getImageLoadPromises: function() {
        return this.itemViewModels.map(function(itemViewModel) {
            return itemViewModel.get('imageLoadPromise');
        });
    }
});
