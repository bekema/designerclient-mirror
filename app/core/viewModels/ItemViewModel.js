const _ = require('underscore');
const ViewModel = require('viewModels/ViewModel');
const metaDataProvider = require('MetaDataProvider');

module.exports = ViewModel.extend({

    __name__: 'ItemViewModel',

    defaults: {
        'deletable': true,
        'previewBox': {
            top: 0,
            left: 0,
            width: 0,
            height: 0
        },
        'temporarySizeDelta': null,
        'handleShape': 'rect'
    },

    computed: {
        'selectable': {
            get: function() {
                return this.parent.get('enabled');
            },
            bindings: function() {
                return [
                    { model: this.parent, attribute: 'enabled' }
                ];
            }
        },
        'draggable': {
            get: function() {
                return !this.model.get('locked') && this.parent.get('enabled');
            },
            bindings: function() {
                return [
                    { model: this.model, attribute: 'locked' },
                    { model: this.parent, attribute: 'enabled' }
                ];
            }
        },
        'handlePosition': {
            get: function() {
                const zoom = this.parent.get('zoomFactor');
                return {
                    top: Math.round(this.model.get('top') * zoom || 0),
                    left: Math.round(this.model.get('left') * zoom || 0)
                };
            },
            bindings: function() {
                return [
                    { model: this.model, attributes: ['top', 'left'] },
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },
        'handleSize': {
            get: function() {
                const zoom = this.parent.get('zoomFactor');
                return {
                    width: Math.round(this.model.get('width') * zoom),
                    height: Math.round(this.model.get('height') * zoom)
                };
            },
            bindings: function() {
                return [
                    { model: this.model, attributes: ['width', 'height'] },
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },
        'previewPosition': {
            get: function() {
                const zoom = this.parent.get('zoomFactor');
                const previewBox = this.get('previewBox');
                const tempDelta = this.get('temporaryPositionDelta') || {
                    left: 0,
                    top: 0
                };
                return {
                    left: Math.round((this.model.get('left') + previewBox.left + tempDelta.left) * zoom),
                    top: Math.round((this.model.get('top') + previewBox.top + tempDelta.top) * zoom)
                };
            },
            bindings: function() {
                return [
                    ['previewBox', 'temporaryPositionDelta'],
                    { model: this.model, attributes: ['top', 'left'] },
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },
        'previewSize': {
            get: function() {
                const zoom = this.parent.get('zoomFactor');
                const previewBox = this.get('previewBox');
                const tempDelta = this.get('temporarySizeDelta') || {
                    width: 0,
                    height: 0
                };
                return {
                    width: Math.round((previewBox.width + tempDelta.width) * zoom),
                    height: Math.round((previewBox.height + tempDelta.height) * zoom)
                };
            },
            bindings: function() {
                return [
                    ['previewBox', 'temporarySizeDelta'],
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },
        'handles': {
            get: function() {
                if (this.model.get('locked')) {
                    return '';
                }

                return 'all';
            },
            bindings: function() {
                return [
                    { model: this.model, attribute: 'locked' }
                ];
            }
        },
        'resizable': {
            get: function() {
                return !_.isEmpty(this.get('handles')) && this.parent.get('enabled');
            },
            bindings: function() {
                return [
                    'handles',
                    { model: this.parent, attribute: 'enabled' }
                ];
            }
        }
    },

    getPreviewObject: function(json) {
        const metaData = metaDataProvider.getMetaData(json.module);
        let o = {};

        if (metaData.urlKeys) {
            _.each(metaData.urlKeys, function(key) {
                if (json[key]) {
                    o[key] = json[key];
                }
            }, this);
        } else {
            o = _.omit(json, 'items');
        }

        if (json.items) {
            o.items = [];
            _.each(json.items, function(itemJson) {
                o.items.push(this.getPreviewObject(itemJson));
            }, this);
        }

        return o;
    },

    getPreviewObjects: function() {
        // Convert to JSON so we can modify top/left without triggering events.
        const previewObjects = [];
        const item = this.model.toJSON();
        item.top = item.left = 0;
        previewObjects.push(this.getPreviewObject(item));
        return previewObjects;
    }

});
