const _ = require('underscore');
const ItemViewModel = require('viewModels/ItemViewModel');
const util = require('util/Util');
const RectangleSnapMixin = require('views/tools/mixins/RectangleSnapMixin');
const clients = require('services/clientProvider');
const metaDataProvider = require('MetaDataProvider');

/* eslint-disable no-magic-numbers */

const CROP_THRESHOLD = 0.01;

const ImageViewModel = ItemViewModel.extend({

    initialize: function() {
        ItemViewModel.prototype.initialize.apply(this, arguments);
        this.on('loadimage', this.onLoadImage, this);
    },

    defaults: _.extend({}, ItemViewModel.prototype.defaults, {
        'snapBox': { top: 0, left: 0, width: 0, height: 0 },
        'analyticsPrefix': 'Image',
        'colors': []
    }),

    computed: _.extend({}, ItemViewModel.prototype.computed, {
        url: {
            get: function() {
                const attributes = this.model.pick(metaDataProvider.getMetaData('UploadedImage').previewAttributes);
                attributes.zoom = this.parent.get('zoomFactor');
                attributes.url = attributes.previewUrl;

                return clients.preview.renderImage(attributes);
            },
            bindings: function() {
                const attributes = metaDataProvider.getMetaData('UploadedImage').previewAttributes;
                return [
                    { model: this.model, attributes },
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },

        selectable: {
            get: function() {
                return this.parent.get('enabled');
            },
            bindings: function() {
                return [
                    { model: this.parent, attribute: 'enabled' }
                ];
            }
        },

        handles: {
            get: function() {
                if (this.model.get('locked')) {
                    return '';
                }

                return 'corners';
            },
            bindings: function() {
                return [{ model: this.model, attribute: 'locked' }];
            }
        },

        // Returns the position of the model with the scale to fit white space removed. If there is
        // no scale to fit crop, just returns the position of the model.
        //TODO: I'm not sure what this is actually used for/doing?
        unzoomedScaleToFitPosition: {
            get: function() {
                let left = this.model.get('left');
                let top = this.model.get('top');

                const crop = this.model.get('crop');
                if (crop) {
                    const horizontalCrop = crop.left + crop.right;
                    const verticalCrop = crop.top + crop.bottom;

                    const croppedSize = this.get('unzoomedScaleToFitSize');

                    // This is all to come up with smarter handles for scale-to-fit images.
                    if (horizontalCrop < -CROP_THRESHOLD) {
                        left -= crop.left * croppedSize.width;
                    } else if (verticalCrop < -CROP_THRESHOLD) {
                        top -= crop.top * croppedSize.height;
                    }
                }

                return {
                    left,
                    top
                };
            },
            bindings: function() {
                return [
                    'unzoomedScaleToFitSize',
                    { model: this.model, attributes: ['crop', 'top', 'left', 'rotation'] }
                ];
            }
        },

        // Returns the size of the model with the scale to fit white space removed. If there is
        // no scale to fit crop, just returns the size of the model.
        //TODO also not sure what this one is doing either. Both are used for resize
        unzoomedScaleToFitSize: {
            get: function() {
                let width = this.model.get('width');
                let height = this.model.get('height');

                const crop = this.model.get('crop');
                if (crop) {
                    const horizontalCrop = crop.left + crop.right;
                    const verticalCrop = crop.top + crop.bottom;

                    if (horizontalCrop < -CROP_THRESHOLD) {
                        width = width / (1 - horizontalCrop);
                    } else if (verticalCrop < -CROP_THRESHOLD) {
                        height = height / (1 - verticalCrop);
                    }
                }
                return {
                    width,
                    height
                };
            },
            bindings: function() {
                return [{ model: this.model, attributes: ['crop', 'width', 'height', 'rotation'] }];
            }
        },

        handlePosition: {
            get: function() {
                const zoom = this.parent.get('zoomFactor');
                const position = this.get('unzoomedScaleToFitPosition');

                return {
                    top: Math.round(position.top * zoom || 0),
                    left: Math.round(position.left * zoom || 0)
                };
            },
            bindings: function() {
                return [
                    'unzoomedScaleToFitPosition',
                    { model: this.model, attribute: 'rotation' },
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },

        handleSize: {
            get: function() {
                const zoom = this.parent.get('zoomFactor');

                let width = this.model.get('width');
                let height = this.model.get('height');

                const crop = this.model.get('crop');
                if (crop) {
                    const horizontalCrop = crop.left + crop.right;
                    const verticalCrop = crop.top + crop.bottom;

                    // For scale to fit crops, shrink the handles to only show the image.
                    if (horizontalCrop < -CROP_THRESHOLD) {
                        width = width / (1 - horizontalCrop);
                    } else if (verticalCrop < -CROP_THRESHOLD) {
                        height = height / (1 - verticalCrop);
                    }
                }
                return {
                    width: Math.round(width * zoom),
                    height: Math.round(height * zoom)
                };
            },
            bindings: function() {
                return [
                    { model: this.model, attributes: ['width', 'height', 'crop'] },
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        }
    }),

    onLoadImage: function(e) {
        const box = {
            top: 0,
            left: 0,
            width: e.width,
            height: e.height
        };

        this.set({
            'previewBox': box,
            'snapBox': box
        });
    }
});

util.addMixin(ImageViewModel.prototype, RectangleSnapMixin);

module.exports = ImageViewModel;
