const Backbone = require('backbone');

const ChromeViewModelCollection = Backbone.NestedCollection.extend({

    __name__: 'ChromeViewModelCollection'

});

module.exports = ChromeViewModelCollection;
