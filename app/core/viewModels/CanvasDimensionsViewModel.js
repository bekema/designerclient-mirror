const _ = require('underscore');
const ChromeViewModel = require('viewModels/ChromeViewModel');
const colorManager = require('ColorManager');
const colorConverter = require('util/ColorConverter');
const dimensionsConfig = require('publicApi/configuration/ui').canvas.chromes.dimensions;
const globalConfig = require('configuration/Global');
const measurementConverter = require('util/MeasurementConverter');
const util = require('util/Util');

module.exports = ChromeViewModel.extend({

    __name__: 'CanvasDimensionsViewModel',

    defaults: _.extend({
        color: dimensionsConfig.color || 'rgba(0, 0, 0, 0.5)',
        margin: dimensionsConfig.margin,
        measurement: dimensionsConfig.measurement,
        precision: dimensionsConfig.precision,
        lineWidth: dimensionsConfig.lineWidth,
        showTop: dimensionsConfig.showTop,
        showRight: dimensionsConfig.showRight,
        showBottom: dimensionsConfig.showBottom,
        showLeft: dimensionsConfig.showLeft,
        module: 'CanvasDimensionsViewModel'
    }, ChromeViewModel.prototype.defaults),

    initialize: function(dimensionsViewModel, canvasViewModel) {
        ChromeViewModel.prototype.initialize.apply(this, arguments);

        // TODO: This might break someday in time or not.....
        if (globalConfig.scenes[canvasViewModel.parent.model.id - 1] && !dimensionsConfig.color) {
            colorManager.getMostVisibleDefaultColor(canvasViewModel.parent)
                .then(hex => {
                    const rgb = colorConverter.hex2rgb(hex);
                    this.set('color', `rgba(${rgb.r}, ${rgb.g}, ${rgb.b}, 0.5)`);
                });
        }
    },

    computed: _.extend({}, ChromeViewModel.prototype.computed, {
        displayableCanvasDimensions: {
            get: function() {
                const measurement = this.get('measurement');
                const precision = this.get('precision');
                const width = measurementConverter.convert(this.parent.model.get('width'), 'mm', measurement);
                const height = measurementConverter.convert(this.parent.model.get('height'), 'mm', measurement);
                return {
                    width: `${util.round(width, precision)}${measurement}`,
                    height: `${util.round(height, precision)}${measurement}`
                };
            },
            bindings: function() {
                return [
                    ['precision', 'measurement'],
                    { model: this.parent.model, attributes: ['width', 'height'] }
                ];
            }
        },
        //This is for the width text
        fontHeight: {
            get: function() {
                //This is currently a static font size, so this is a static value
                //TODO make font size configurable
                return 10; //eslint-disable-line
            },
            bindings: function() {
                return ['displayableCanvasDimensions'];
            }
        },
        //This is for the height Text
        fontWidth: {
            get: function() {
                //This is currently an estimate.
                //TODO perhaps estimate better
                const WIDTH_ESTIMATE = 7;
                const heightText = this.get('displayableCanvasDimensions').height;
                let total = heightText.length * WIDTH_ESTIMATE;
                //dot is smaller, so it only takes 3 pixels
                if (heightText.indexOf('.') >= 0) {
                    total -= 3;
                }
                return total;
            },
            bindings: function() {
                return ['displayableCanvasDimensions'];
            }
        },
        top: {
            get: function() {
                return this.get('showTop') ? -((this.get('fontHeight') / 2) + this.get('margin')) : 0;
            },
            bindings: function() {
                return ['margin', 'fontHeight', 'showTop'];
            }
        },
        left: {
            get: function() {
                return this.get('showLeft') ? -((this.get('fontWidth') / 2) + this.get('margin')) : 0;
            },
            bindings: function() {
                return ['margin', 'fontWidth', 'showLeft'];
            }
        },
        right: {
            get: function() {
                return this.get('showRight') ? -((this.get('fontWidth') / 2) + this.get('margin')) : 0;
            },
            bindings: function() {
                return ['margin', 'fontWidth', 'showRight'];
            }
        },
        bottom: {
            get: function() {
                return this.get('showBottom') ? -((this.get('fontHeight') / 2) + this.get('margin')) : 0;
            },
            bindings: function() {
                return ['margin', 'fontHeight', 'showBottom'];
            }
        },
        width: {
            get: function() {
                return this.parent.get('size').width - this.get('left') - this.get('right');
            },
            bindings: function() {
                return [
                    'left', 'right',
                    { model: this.parent, attribute: 'size' }
                ];
            }
        },
        height: {
            get: function() {
                return this.parent.get('size').height - this.get('top') - this.get('bottom');
            },
            bindings: function() {
                return [
                    'top', 'bottom',
                    { model: this.parent, attribute: 'size' }
                ];
            }
        }
    })

});
