const localization = require('Localization');

const name = 'uploadSize';
const type = 'upload';

module.exports = {

    validate: (upload) => {
        const validation = {
            name,
            type,
            severity: 'success',
            message: '',
            data: { size: upload.files[0].size, name: upload.files[0].name }
        };
        const validationConfig = require('publicApi/configuration/validations');
        if (upload.files.some(file => file.size > validationConfig.uploadSize.maxSize)) {
            validation.severity = 'error';
            validation.message = localization.getText(`validations.${name}.error`);
        }
        return validation;
    }

};
