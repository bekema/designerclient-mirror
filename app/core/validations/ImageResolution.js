const localization = require('Localization');

const MM_TO_INCH = 0.0393701;
const name = 'imageResolution';
const type = 'itemViewModel';

//TODO when this is needed a second time, refactor this to a utility file
const pixelsToInch = (pixels, zoomFactor) => {
    const sizeInMm = pixels / zoomFactor;
    const sizeInInches = sizeInMm * MM_TO_INCH;
    return sizeInInches;
};

const calculateDpi = (naturalSize, currentSize, zoomFactor) => {
    const sizeInInches = pixelsToInch(currentSize, zoomFactor);
    return naturalSize / sizeInInches;
};

const isUnderThreshold = (imageDpi, dpiRatio, threshold) => {
    return threshold < 1
        ? dpiRatio < threshold
        : imageDpi < threshold;
};

module.exports = {

    validate: (viewModel) => {
        const config = require('publicApi/configuration/validations').imageResolution;
        const validation = {
            name,
            type,
            validationId: viewModel.id,
            severity: 'success',
            message: '',
            data: { itemViewModel: viewModel }
        };

        const dimensions = viewModel.get('previewSize');
        const naturalDimensions = viewModel.model.get('naturalDimensions');
        const crop = viewModel.model.get('crop');
        const zoom = viewModel.parent.get('zoomFactor');

        if (!zoom) {
            return {};
        }

        //if naturalDimensions isn't specified or height/width is 0, we can't do the validation properly (this will usually happen for vectors, placeholders, etc)
        if (!naturalDimensions || !naturalDimensions.height || !naturalDimensions.width) {
            return {};
        }

        if (!dimensions.height || !dimensions.width) {
            return {};
        }

        const widthFactor = 1.0 - (crop.right + crop.left);
        const heightFactor = 1.0 - (crop.top + crop.bottom);

        const containerSize = {
            width: dimensions.width / widthFactor,
            height: dimensions.height / heightFactor
        };

        const dpiWidth = calculateDpi(naturalDimensions.width, containerSize.width, zoom);
        const dpiHeight = calculateDpi(naturalDimensions.height, containerSize.height, zoom);
        const dpi = Math.min(dpiWidth, dpiHeight);
        const dpiRatio = dpi / config.productDpi;

        validation.data.dpiRatio = dpiRatio;

        if (isUnderThreshold(dpi, dpiRatio, config.errorThreshold)) {
            validation.severity = 'error';
            validation.message = localization.getText(`validations.${name}.error`);
        } else if (isUnderThreshold(dpi, dpiRatio, config.warningThreshold)) {
            validation.severity = 'warning';
            validation.message = localization.getText(`validations.${name}.warning`);
        }
        return validation;
    }

};
