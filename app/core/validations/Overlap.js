// `Overlap` is a valdation that can be used perform pixel by pixel comparisons
const _ = require('underscore');
const designerContext = require('DesignerContext');
const util = require('util/Util');
const localization = require('Localization');

/* eslint-disable no-magic-numbers */

const name = 'overlap';
const type = 'itemViewModel';

const haveSameParent = function(itemView1, itemView2) {
    const parent1 = itemView1.viewModel.parent;
    const parent2 = itemView2.viewModel.parent;

    return parent1 && parent2 && parent1 === parent2;
};

const pixelsOverlap = function(imgData1, dimensions1, imgData2, dimensions2, alphaTolerance, pixelTolerance) {
    let top1, left1, height1, width1,
        top2, left2, height2, width2,
        overlapXMin, overlapXMax, overlapYMin, overlapYMax;

    top1 = Math.round(dimensions1.top);
    left1 = Math.round(dimensions1.left);
    height1 = Math.round(dimensions1.height);
    width1 = Math.round(dimensions1.width);

    top2 = Math.round(dimensions2.top);
    left2 = Math.round(dimensions2.left);
    height2 = Math.round(dimensions2.height);
    width2 = Math.round(dimensions2.width);

    overlapYMin = Math.max(top1, top2);
    overlapYMax = Math.min(top1 + height1, top2 + height2);
    overlapXMin = Math.max(left1, left2);
    overlapXMax = Math.min(left1 + width1, left2 + width2);

    const pixels1 = imgData1.data;
    const pixels2 = imgData2.data;

    const pixels1length = pixels1.length;
    const pixels2length = pixels2.length;

    let pixelCount = 0;

    for (let pixelX = overlapXMin; pixelX < overlapXMax; pixelX++) {
        for (let pixelY = overlapYMin; pixelY < overlapYMax; pixelY++) {
            // NOTE: this logic is all inline (instead of in smaller functions) for performance.
            // Since this loop is over every pixel and gets run for every pair of images,
            // we want to avoid the overhead of function calls
            // Ref: https://fisheye101.vistaprint.net/fisheye/cru/DEV-43368
            // Ref: https://fisheye101.vistaprint.net/fisheye/cru/DEV-43386

            // Imagine all the pixels of the image are in a height-by-width matrix
            // (i.e.: height = # of rows, width = # of columns):
            // E.g. a 15 pixel image (height = 3, width = 5) might look like this:
            // _______________________
            //  0 |  1 |  2 |  3 |  4
            //  5 |  6 |  7 |  8 |  9
            // 10 | 11 | 12 | 13 | 14
            // _______________________
            // Example: Finding pixel 11:
            // To figure out that the pixel at position (1, 2) (i.e.: x-coord/column = 1, y-coord/row = 2)
            // is 11, we would do the following calculation:
            // 1) Get to the correct row by doing y-coord * width, e.g.: 2 * 5 = 10
            // 2) Get to the the correct column by offsetting by (adding) the x-coord, e.g.: 10 + 1 = 11
            // I.e.: const pixelNum = yCoord * width + xCoord;

            // The " * 4 + 3 " is done because:
            // pixels in ImageData objects (Uint8ClampedArray)
            // take up 4 spaces in the array for their 4 parts: Red, Green, Blue, Alpha
            // Ref: http://www.w3schools.com/tags/canvas_getimagedata.asp

            const alphaPosition1 = ((pixelY - top1) * width1 + (pixelX - left1)) * 4 + 3;
            const alphaPosition2 = ((pixelY - top2) * width2 + (pixelX - left2)) * 4 + 3;

            // Count the times that each image has a "visible" pixel at the same coordinate.
            if (alphaPosition1 <= pixels1length && pixels1[alphaPosition1] > alphaTolerance
                    && alphaPosition2 <= pixels2length && pixels2[alphaPosition2] > alphaTolerance) {
                pixelCount++;
                if (pixelCount > pixelTolerance) {
                    return true;
                }
            }
        }
    }

    return false;
};

const getOverlaps = function(candidateItemView, itemViewsToCompare, proxyPosition, draggedViewModels, alphaTolerance, pixelTolerance) {
    const dimensions1 = _.extend({}, candidateItemView.viewModel.get('previewBoundingBox'), proxyPosition);

    const data1 = candidateItemView.getImageData();
    if (!data1) {
        return [];
    }

    return _.filter(itemViewsToCompare, function(itemView2) {
        if (itemView2 === candidateItemView || (draggedViewModels && _.contains(draggedViewModels, itemView2.viewModel))) {
            return false;
        }

        if (!designerContext.embroidery && candidateItemView.viewModel.get('module') !== 'TextFieldViewModel' && itemView2.viewModel.get('module') !== 'TextFieldViewModel') {
            return false;
        }

        const data2 = itemView2.getImageData();
        if (!data2) {
            return false;
        }

        const dimensions2 = itemView2.viewModel.get('previewBoundingBox');

        if (!haveSameParent(candidateItemView, itemView2)) {
            return false;
        }

        if (util.intersects(dimensions1, dimensions2)) {
            return pixelsOverlap(data1, dimensions1, data2, dimensions2, alphaTolerance, pixelTolerance);
        }

        return false;
    });
};

module.exports = {

    validate: (candidateItemView, itemViewsToCompare, proxyPosition, draggedViewModels) => {
        const validation = {
            name,
            type,
            validationId: candidateItemView.viewModel.id,
            message: '',
            severity: 'success',
            data: { itemViewModel: candidateItemView.viewModel }
        };

        const overlapConfig = require('publicApi/configuration/validations').overlap;
        const overlapViews = getOverlaps(candidateItemView, itemViewsToCompare, proxyPosition, draggedViewModels, overlapConfig.alphaTolerance, overlapConfig.pixelTolerance);
        const overlapIds = overlapViews.map(overlapView => overlapView.viewModel.id);

        if (overlapIds.length > 0) {
            validation.data.overlapIds = overlapIds;
            if (designerContext.embroidery) {
                validation.severity = 'error';
                validation.message = localization.getText(`validations.${name}.error`);
            } else {
                validation.severity = 'warning';
                validation.message = localization.getText(`validations.${name}.warning`);
            }
        }

        return validation;
    }
};
