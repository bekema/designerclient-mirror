const options = require('publicApi/configuration/validations');
const ValidationStrategy = require('validations/ValidationStrategy');

const publicApi = {};

const initialize = function() {
    delete publicApi.initialize;

    Object.keys(options).forEach(key => {
        //Strategy just has to be at minimum an object that has a validate() function that returns a validation result
        const strategy = options[key].strategy;
        const enabled = options[key].enabled;

        if (!strategy || !enabled) { return; }

        publicApi[key] = new ValidationStrategy(strategy);
    });
};

publicApi.initialize = initialize;

module.exports = publicApi;
