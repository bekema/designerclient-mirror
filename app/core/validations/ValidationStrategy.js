const ValidationManager = require('validations/ValidationManager');
const Validation = require('models/Validation');

// This is the base class for a validation strategy
// This exists to be passed in a logic strategy, and perform the leg work for
// Dealing with the validation manager
class ValidationStrategy {
    constructor(strategy) {
        this.strategy = strategy;
    }

    // Returns a validation result object, designating 'success', 'warning' or 'error'
    // See Validation.js for what should be returned by a strategy validation
    // Validations will only be added to validation manager if they failed validation
    validate() {
        const result = this.strategy.validate(...arguments);
        result.successful = result.severity === 'success';
        if (result.successful) {
            if (ValidationManager.has(result)) {
                ValidationManager.removeValidation(result);
            }
        } else {
            let validation = ValidationManager.getValidation(result);
            if (validation) {
                delete result.validationId;
                delete result.type;
                delete result.name;
                validation.set(result);
            } else {
                validation = new Validation(result);
                ValidationManager.addValidation(validation);
            }
        }
        return result;
    }
}

module.exports = ValidationStrategy;
