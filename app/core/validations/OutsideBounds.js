const _ = require('underscore');
const localization = require('Localization');

const name = 'outsideBounds';
const type = 'itemViewModel';

//Add a little give to the validation (so things right on the edge don't throw a validation error)
const GIVE = 1;

const getLeftOverlap = (dimensions) => (dimensions.safetyMargin.left - GIVE) - dimensions.item.left;

const getRightOverlap = (dimensions) => dimensions.item.right - (dimensions.safetyMargin.right + GIVE);

const getTopOverlap = (dimensions) => (dimensions.safetyMargin.top - GIVE) - dimensions.item.top;

const getBottomOverlap = (dimensions) => dimensions.item.bottom - (dimensions.safetyMargin.bottom + GIVE);

module.exports = {

    validate: (viewModel, itemDimensions, margin) => {
        const validation = {
            name,
            type,
            validationId: viewModel.id,
            severity: 'success',
            message: '',
            location: {},
            data: { itemViewModel: viewModel }
        };

        if (!itemDimensions) {
            return validation;
        }

        const canvasViewModel = viewModel.parent;
        const canvasSize = canvasViewModel.get('size');
        let safeArea;
        if (margin) {
            safeArea = canvasViewModel.get('zoomedSafeArea');
        } else {
            safeArea = {
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                width: canvasSize.width,
                height: canvasSize.height
            };
        }

        const dimensions = {
            item: {
                left: itemDimensions.left,
                top: itemDimensions.top,
                width: itemDimensions.width,
                height: itemDimensions.height,
                right: itemDimensions.left + itemDimensions.width,
                bottom: itemDimensions.top + itemDimensions.height
            },
            safetyMargin: _.extend({}, safeArea, {
                right: safeArea.left + safeArea.width,
                bottom: safeArea.top + safeArea.height
            })
        };

        if (dimensions.item.width === 0 || dimensions.item.height === 0) {
            return validation;
        }

        const leftOverlap = getLeftOverlap(dimensions);
        const rightOverlap = getRightOverlap(dimensions);
        const topOverlap = getTopOverlap(dimensions);
        const bottomOverlap = getBottomOverlap(dimensions);
        if (leftOverlap > 0) {
            validation.location.x = leftOverlap;
        } else if (rightOverlap > 0) {
            validation.location.x = itemDimensions.width - rightOverlap;
        } else if (topOverlap > 0) {
            validation.location.y = topOverlap;
        } else if (bottomOverlap > 0) {
            validation.location.y = itemDimensions.height - bottomOverlap;
        }

        if (validation.location.x || validation.location.y) {
            validation.severity = 'warning';
            validation.message = localization.getText(`validations.${name}.warning`);
        }

        return validation;
    }

};
