const Backbone = require('backbone');
const eventBus = require('EventBus');
const events = eventBus.events;

const buildValidationId = (type, id, name) => `${type}-${id}-${name}`;

const ValidationManager = Backbone.Collection.extend({
    addValidation(validations) {
        validations = Array.isArray(validations) ? validations : [validations];

        validations.forEach(validation => {
            validation.set('id', buildValidationId(validation.get('type'), validation.get('validationId'), validation.get('name')));
        });

        this.add(validations);
    },

    removeValidation({ type, validationId, name }) {
        const validation = this.get(buildValidationId(type, validationId, name));
        if (validation) {
            validation.destroy();
        }
    },

    removeValidationsForItem({ type, validationId }) {
        const validations = this.where({ type, validationId });
        validations.forEach(validation => validation.destroy());
    },

    has({ type, validationId, name }) {
        return !!this.getValidation({ type, validationId, name });
    },

    getValidation({ type, validationId, name }) {
        return this.get(buildValidationId(type, validationId, name));
    },

    getValidationsForItem({ type, validationId }) {
        return this.where({ type, validationId });
    },

    enable() {
        eventBus.on(events.addValidation, this.addValidation, this);
        eventBus.on(events.removeValidation, this.removeValidation, this);
    },

    disable() {
        eventBus.off(events.addValidation, this.addValidation);
        eventBus.off(events.removeValidation, this.removeValidation);
    }
});

module.exports = new ValidationManager();
