// The `SelectionManager` is a singleton responsible for updating the selected and deselected mode on the
// view models that are passed around via the `select` global event. It also handles global events from
// the `KeyboardManager` which are used for multi-select.
const _ = require('underscore');
const Backbone = require('backbone');
const documentRepository = require('DocumentRepository');
const eventBus = require('EventBus');
const events = eventBus.events;

let handlingSelect = false;
let updatingSelection = false;
let viewModelsToUpdate = [];
let managedViewModels = [];
let enabled = false;

// This is used in the case that a view model's selection state is updated without select being
// called on it, which happens when undoing and redoing commands. This will look at all the given view models and
// update their states accordingly. View models with selected will be added to the current selection and view models
// without selected will be removed from the current selection.
const updateSelection = function(viewModels) {
    if (!handlingSelect) {
        const shouldCallSelect = !updatingSelection;
        updatingSelection = true;

        if (Array.isArray(viewModels)) {
            viewModelsToUpdate = viewModelsToUpdate.concat(viewModels);
        } else {
            viewModelsToUpdate.push(viewModels);
        }

        if (shouldCallSelect) {
            // Use _.defer to wait for the current call stack to finish setting all selected attributes and then call the proper select.
            _.defer(function() {
                const viewModelsToSelect = _.filter(viewModelsToUpdate, function(viewModel) {
                    return viewModel.get('selected');
                }, this);
                // select() may not trigger the `select` event because the view model attributes have already been set at this point.
                if (!this.select(viewModelsToSelect)) {
                    this.trigger('select', this);
                }
                viewModelsToUpdate = [];
                updatingSelection = false;
            }.bind(this));
        }
    }
};

// Select all of the item view models that we are managing.
const onSelectAll = function(e) {
    // I'm not sure if it's fair for the selection manager to prevent bubbling of the original `ctrl+A` event,
    // which is what this does, but my hypothesis is that the `SelectionManager` is the only thing that should
    // respond to this event.
    if (e) {
        e.preventDefault();
    }

    // Select the children of every enabled view model that we know about.
    const enabledViewModels = _.filter(managedViewModels, function(viewModel) {
        return !viewModel.get('disabled');
    });

    const itemViewModels = _.flatten(_.pluck(_.pluck(enabledViewModels, 'itemViewModels'), 'models'));
    this.select(itemViewModels);
};

const select = function(viewModels, options) {
    handlingSelect = true;
    let changed = false;
    viewModels = Array.isArray(viewModels) ? viewModels : [viewModels];
    options = options || {};

    // If we're not adding to the selection, then we need to deselect
    // all of the models and remove them from our collection. We do it one by one to ensure
    // that add and remove events are always fired.
    if (!options.multi) {
        while (this.length > 0) {
            changed = true;
            this.last().unset('selected');
            this.pop();
        }
    }

    _.each(viewModels, function(viewModel) {
        if (!viewModel.has('selectable') || viewModel.get('selectable')) {
            //lets try to make the changes to the viewmodel and the selection manager more atomic
            //(avoid the scenario where the selectionManager is at 0 when stuff is selected, or vice versa)
            const originalValue = viewModel.get('selected');
            viewModel.set('selected', true, { silent: true });
            this.add(viewModel);
            if (originalValue !== true) {
                changed = true;
                viewModel.trigger('change:selected');
            }
        }
    }, this);

    if (changed) {
        this.trigger('select', this);
    }
    handlingSelect = false;
    return changed;
};

const deselect = function(viewModels) {
    handlingSelect = true;
    let changed = false;
    viewModels = Array.isArray(viewModels) ? viewModels : [viewModels];

    _.each(viewModels, function(viewModel) {
        if (!viewModel.has('selectable') || viewModel.get('selectable')) {
            //lets try to make the changes to the viewmodel and the selection manager more atomic
            //(avoid the scenario where the selectionManager is at 0 when stuff is selected, or vice versa)
            const originalValue = viewModel.get('selected');
            viewModel.unset('selected', { silent: true });
            this.remove(viewModel);
            if (originalValue !== false) {
                changed = true;
                viewModel.trigger('change:selected');
            }
        }
    }, this);

    if (changed) {
        this.trigger('select', this); // This is needed to update the handles
    }

    handlingSelect = false;
    return changed;
};

// Call `manage` with a view model that contains item view models in order to have those view
// models more actively managed; i.e. selected with `ctrl+A`. We do this rather than passing a view
// model in during initialize so that there is more flexibility in terms of which canvases can
// be selected.
const manage = function(viewModels) {
    if (!Array.isArray(viewModels)) {
        viewModels = [viewModels];
    }

    _.each(viewModels, vm => {
        if (!(vm.itemViewModels instanceof Backbone.Collection)) {
            throw new Error('Cannot manage a view model that does not have an item view model collection');
        }

        this.listenTo(vm.itemViewModels, 'change:selected', updateSelection);
    });

    managedViewModels = viewModels;
};

const SelectionManager = Backbone.Collection.extend({ // eslint-disable-line backbone/collection-model

    enable: function() {
        if (!enabled) {
            // `selectall` is triggered by the `KeyboardManager`.
            eventBus.on(events.selectAll, onSelectAll, this);
            eventBus.on(events.select, select, this);
        }
        enabled = true;
        this.listenTo(documentRepository, 'change:activeCanvasViewModel', function(canvasViewModel) {
            this.manage(canvasViewModel);
        });
    },

    disable: function() {
        this.offGlobal();
        this.off();
        enabled = false;
    },

    // Because the selected view models must be updated atomically on select, this is the only valid method
    // to call in order to update the selction manager.
    select: select,

    deselect: deselect,

    // The `SelectionManager` operates on **view models** but we often want to act on **models**. Use this method
    // to get access to the models. Otherwise, you can just iterate over the manager itself, since it's a collection.
    getModels: function() {
        return _.pluck(this.models, 'model'); // eslint-disable-line backbone/no-collection-models
    },

    // Call `manage` with a view model that contains item view models in order to have those view
    // models more actively managed; i.e. selected with `ctrl+A`. We do this rather than passing a view
    // model in during initialize so that there is more flexibility in terms of which canvases can
    // be selected.
    manage: manage,

    // There is one selectionManager for the whole document,
    // this is in case we want to know which viewModels are currently being managed
    getManagedViewModels: function() {
        return managedViewModels;
    }
});

module.exports = new SelectionManager();
