const _ = require('underscore');
const Backbone = require('backbone');
const eventBus = require('EventBus');
const events = eventBus.events;
const processingStepsManager = require('uploads/processingStepsManager');
const localization = require('Localization');
var uploadedFiles = {};

const manageNewUpload = function({ requestId, promise, file }) {
    const upload = new Backbone.Model({
        uploadComplete: false,
        percentComplete: 0,
        requestId,
        file,
        validations: new Backbone.Collection(),
        state: localization.getText('widgets.uploadStateTextUploading')
    });

    uploadedFiles[requestId] = upload;

    promise.then(uploadResponse => {
        upload.set(uploadResponse);
        eventBus.trigger(events.uploadComplete, upload);
        processingStepsManager.executeUploadSteps(upload).then(() => {
            eventBus.trigger(events.uploadPostStepsComplete, upload);
            upload.set({
                state: 'Ready to be placed.',
                uploadComplete: true
            });
        });
    }).catch(() => eventBus.trigger(events.uploadDelete, upload.get('requestId')));

    eventBus.trigger(events.uploadStarted, upload);
};

const progressUpload = function({ loaded, total, requestId }) {
    const percentComplete = loaded / total * 100; // eslint-disable-line no-magic-numbers
    const upload = _.find(uploadedFiles, uploadData => uploadData.get('requestId') === requestId);
    if (upload) {
        upload.set({ percentComplete: percentComplete });
    }
};

module.exports = {
    enable() {
        eventBus.on(events.uploadNew, manageNewUpload);
        eventBus.on(events.uploadProgress, progressUpload);
    },

    disable() {
        eventBus.off(events.uploadNew, manageNewUpload);
    }
};
