const _ = require('underscore');
const cleanImage = require('uploads/processingSteps/embroidery/cleanImage');
const designerContext = require('DesignerContext');
const digitizeImage = require('uploads/processingSteps/embroidery/digitizeImage');
const preflight = require('uploads/processingSteps/preflight');
const prepareForCleanImage = require('uploads/processingSteps/embroidery/prepareForCleanImage');

const embroideryUploadSteps = [
    prepareForCleanImage,
    cleanImage,
    digitizeImage
];

const printUploadSteps = [
    preflight
];

const uploadSteps = {
    print: printUploadSteps,
    embroidery: embroideryUploadSteps
};

const executeStep = function(upload, viewModel, step) {
    upload.set('state', step.message());
    // For on canvas processing we need to push some stuff to the view model.
    if (viewModel) {
        viewModel.set({
            percentComplete: upload.get('percentComplete'),
            state: upload.get('state')
        });
    }
    return step.action(upload);
};

module.exports = {
    preflight,
    prepareForCleanImage,
    cleanImage,
    digitizeImage,

    getProcessingSteps: function() {
        return uploadSteps[designerContext.uploadContext];
    },

    executeUploadSteps: function(upload, viewModel) {
        // TODO - this can probably be refactored to just take in a view model, and forgot about this 'upload' thing
        if (!this.getProcessingSteps().length) {
            return Promise.resolve(upload);
        }

        let promise = Promise.resolve(upload);

        // TODO: Enable parallel execution?

        const maxProgress = this.getProcessingSteps().length;
        let currentProgress = 0;
        _.each(this.getProcessingSteps(), step => {
            promise = promise.then(() => {
                currentProgress += 1;
                const percentComplete = currentProgress / maxProgress * 100; // eslint-disable-line no-magic-numbers
                upload.set('percentComplete', percentComplete);
                return executeStep(upload, viewModel, step);
            });
        });
        return promise;
    }
};