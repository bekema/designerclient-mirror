const clients = require('services/clientProvider');
const dataUtil = require('uploads/processingSteps/embroidery/dataUtil');
const localization = require('Localization');

module.exports = {
    name: () => localization.getText('imageProcessingSteps.digitization.name'),
    message: () => localization.getText('imageProcessingSteps.digitization.message'),

    action: (upload) => {
        const uploadsUrlBuilder = require('services/clientProvider').upload.uploadsUrlBuilder;
        const imgUrl = uploadsUrlBuilder.getOriginalUrl(upload.get('pages')[0].cleanId);

        return clients.embroidery.digitizeImage(imgUrl)
            .then(data => new Blob([data], { type: 'application/xml' }))
            .then(blob => dataUtil.uploadBlob(blob, 'digitized.xti', 'digitizedId', upload));
    }
};
