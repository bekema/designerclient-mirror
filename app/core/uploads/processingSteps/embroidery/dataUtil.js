const clients = require('services/clientProvider');

const dataToBlob = function(data) {
    const byteString = atob(data);

    const ia = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], { type: 'image/png' });
};

module.exports = {
    uploadImage: function(data, fileName, attributeId, uploadModel) {
        const blob = dataToBlob(data);
        return this.uploadBlob(blob, fileName, attributeId, uploadModel);
    },

    uploadBlob: function(blob, fileName, attributeId, uploadModel) {
        return clients.upload.uploadFileBlob(blob, fileName)
            .then(responseData => uploadModel.get('pages')[0][attributeId] = responseData[0].uploadId);
    },

    resizeCanvas: function({ canvas, width, height }) {
        // We are sizing our image to 600x600 keeping the aspect ratio for clean image processing.
        // This size was previously defined by the clean image process that we are replacing with this processingStep.
        const setWidth = 600;
        const setHeight = 600;
        let ratio = 1;

        canvas.width = setWidth;
        canvas.height = setHeight;

        if (width > height) {
            ratio = setWidth / width;
            canvas.height = ratio * height;

        } else if (height > width) {
            ratio = setHeight / height;
            canvas.width = ratio * width;
        }
    },

    cropOriginalImage: function(crop, img) {
        const canvas = document.createElement('canvas');
        const ctx = canvas.getContext('2d');

        const canvasAttributes = {
            left: crop.left * img.width,
            top: crop.top * img.height,
            right: crop.right * img.width,
            bottom: crop.bottom * img.height,
            width: (1 - crop.left - crop.right) * img.width,
            height: (1 - crop.top - crop.bottom) * img.height,
            canvas: canvas
        };

        this.resizeCanvas(canvasAttributes);

        ctx.drawImage(
            img,
            canvasAttributes.left,
            canvasAttributes.top,
            canvasAttributes.width,
            canvasAttributes.height,
            0,
            0,
            canvas.width,
            canvas.height
        );

        const data = canvas.toDataURL('image/png').split(',')[1];
        const blob = dataToBlob(data);

        return clients.upload.uploadFileBlob(blob, 'cropped.png');
    }
};