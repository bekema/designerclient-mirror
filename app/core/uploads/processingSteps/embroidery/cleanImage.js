const clients = require('services/clientProvider');
const dataUtil = require('uploads/processingSteps/embroidery/dataUtil');
const localization = require('Localization');
const _ = require('underscore');

module.exports = {
    name: () => localization.getText('imageProcessingSteps.cleanImage.name'),
    message: () => localization.getText('imageProcessingSteps.cleanImage.message'),

    action: (upload) => {
        const uploadsUrlBuilder = require('services/clientProvider').upload.uploadsUrlBuilder;
        const imgUrl = uploadsUrlBuilder.getOriginalUrl(upload.get('pages')[0].preparedId);

        return clients.embroidery.cleanImage(imgUrl)
            .then(cleanResponse => {
                // Return max thread colors item
                return cleanResponse.cleaningResults
                    .sort((a, b) => _.uniq(a.colorSequence) - _.uniq(b.colorSequence))[0].image;

            })
            .then(imageData => dataUtil.uploadImage(imageData, 'cleanedImage.png', 'cleanId', upload));
    }
};
