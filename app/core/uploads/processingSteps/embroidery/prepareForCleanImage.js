const dataUtil = require('uploads/processingSteps/embroidery/dataUtil');
const localization = require('Localization');

const getImageDataForCleanImage = function(imageUrl) {
    return new Promise(resolve => {
        const img = new Image();
        const canvas = document.createElement('canvas');
        const ctx = canvas.getContext('2d');

        img.crossOrigin = 'Anonymous';
        img.onload = function() {
            dataUtil.resizeCanvas({ canvas, width: img.width, height: img.height });
            ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
            const data = canvas.toDataURL('image/png').split(',')[1];
            resolve(data);
        };
        img.src = imageUrl;
    });
};

module.exports = {
    name: () => localization.getText('imageProcessingSteps.imagePreparation.name'),
    message: () => localization.getText('imageProcessingSteps.imagePreparation.message'),

    action: (upload) => {
        const uploadsUrlBuilder = require('services/clientProvider').upload.uploadsUrlBuilder;
        const imgUrl = uploadsUrlBuilder.getOriginalUrl(upload.get('pages')[0].uploadId);

        // TODO - probably some sort of error handling would be wise?
        return getImageDataForCleanImage(imgUrl)
            .then(imageData => dataUtil.uploadImage(imageData, 'preparedImage.png', 'preparedId', upload));
    }
};

