const _ = require('underscore');
const clients = require('services/clientProvider');
const config = require('configuration/Upload').uploadSteps.preflight;
const validationManager = require('validations/ValidationManager');

// Creates an object where the keys are the name of the validation and
// the value is the severity
const getUniqueValidations = function(results) {
    const unique = {};

    _.each(results, result => {
        _.each(result.validation.Output.RuleHits, ruleHit => {
            unique[ruleHit.Name] = ruleHit;
        });
    });

    return unique;
};

const createValidations = function(upload, results) {
    const unique = getUniqueValidations(results);

    const validations =
        unique.filter(ruleHit => ruleHit.Severity >= config.validationSeverity)
              .map((ruleHit) => {
                  return {
                      type: 'preflight',
                      itemType: 'upload',
                      itemId: upload.get('requestId'),
                      severity: ruleHit.Severity === 1 ? 'warning' : 'error',
                      name: ruleHit.Name,
                      data: ruleHit
                  };
              });

    upload.get('validations').add(validations);
    validationManager.addValidation(validations);
};

const validatePage = function(page) {
    return new Promise((resolve, reject) => {
        clients.preflight.validatePdf(page.originalUrl)
          .then((result) => resolve({ page: page.pageNumber, validation: result }))
          .catch(reject);
    });
};

module.exports = {
    name: () => 'preflight',
    message: () => 'Validating image...',
    action: (upload) => {
        if (upload.get('fileType') !== 'application/pdf') {
            return Promise.resolve(upload);
        }
        return Promise
            .all(upload.get('pages').map(validatePage))
            .then(results => {
                createValidations(upload, results);
                return upload;
            })
            .catch(error => upload); // eslint-disable-line
    }
};