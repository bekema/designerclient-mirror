const selectionManager = require('SelectionManager');
const colorManager = require('ColorManager');
const canvasManager = require('CanvasManager');
const history = require('History');
const keyboardManager = require('KeyboardManager');
const uploadManager = require('uploads/UploadManager');
const validationManager = require('validations/ValidationManager');

module.exports = {
    Savable: true,
    Nudgeable: true,
    Deletable: true,
    AddImage: true,
    AutoPlace: true,
    Croppable: true,
    NavigationWarning: true,
    FocusManager: true,
    Zoomable: true,
    // Probably move this to the view, not a behavior?
    ThreadConfigurable: true,
    SelectionManager: {
        module: selectionManager,
        enabled: true
    },
    ColorManager: {
        module: colorManager,
        enabled: true
    },
    CanvasManager: {
        module: canvasManager,
        enabled: true
    },
    History: {
        module: history,
        enabled: true
    },
    KeyboardManager: {
        module: keyboardManager,
        enabled: true
    },
    UploadManager: {
        module: uploadManager,
        enabled: true
    },
    ValidationManager: {
        module: validationManager,
        enabled: true
    }
};
