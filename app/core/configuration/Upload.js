module.exports = {
    uploadSteps: {
        preflight: {
            // only show warnings for serverity level 2 or higher
            validationSeverity: 2
        }
    }
};