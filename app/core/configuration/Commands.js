module.exports = {
    'add': 'Add',
    'changeAttributes': 'ChangeAttributes',
    'changeZIndex': 'ChangeZIndex',
    'changeCanvas': 'ChangeCanvas',
    'createImage': 'CreateImage',
    'createTextField': 'CreateTextField',
    'createShape': 'CreateShape',
    'changeBackgroundColor': 'ChangeBackgroundColor',
    'cropImage': 'CropImage',
    'delete': 'Delete',
    'move': 'Move',
    'overrideColors': 'OverrideColors',
    'resize': 'Resize',
    'resizeImage': 'ResizeImage',
    'autoPlaceImage': 'AutoPlaceImage',
    'changeDocumentOrientation': 'ChangeDocumentOrientation'
};
