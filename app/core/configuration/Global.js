module.exports = {
    maxTextLength: 2000,
    enableCanvasInteraction: true,
    clearPixelUrl: require('images/clearpixel.gif'),
    colorWheelUrl: require('images/colorwheel.png')
};
