module.exports = [
    {
        'model': 'TextField',
        'viewModel': 'TextFieldViewModel',
        'view': 'items/TextFieldView',
        'previewAttributes': ['width', 'fontSize', 'fontColor', 'fontItalic', 'fontBold', 'fontUnderline', 'fontFamily', 'horizontalAlignment', 'fontStrikeout', 'innerHTML']
    },
    {
        'model': 'BackgroundImage',
        'viewModel': 'BackgroundImageViewModel',
        'view': 'items/BackgroundImageView'
    },
    {
        'model': 'UploadedImage',
        'viewModel': 'UploadedImageViewModel',
        'view': 'items/ImageView',
        'previewAttributes': ['width', 'height', 'crop', 'previewUrl']
    },
    {
        'model': 'Rectangle',
        'viewModel': 'ShapeViewModel',
        'view': 'items/ShapeView',
        'previewAttributes': ['module', 'width', 'height', 'fillColor', 'strokeWidth', 'strokeColor', 'strokeLineJoin', 'strokeLineCap']
    },
    {
        'model': 'Ellipse',
        'viewModel': 'ShapeViewModel',
        'view': 'items/ShapeView',
        'previewAttributes': ['module', 'width', 'height', 'fillColor', 'strokeWidth', 'strokeColor']
    },
    {
        'model': 'Line',
        'viewModel': 'LineViewModel',
        'view': 'items/ShapeView',
        'previewAttributes': ['module', 'width', 'height', 'strokeWidth', 'strokeColor', 'strokeLineJoin', 'strokeLineCap']
    },
    {
        'model': 'EmbroideryImage',
        'viewModel': 'EmbroideryImageViewModel',
        'view': 'items/ImageView',
        'previewAttributes': ['width', 'height', 'crop', 'xtiUrl', 'colorOverrides', 'previewUrl', 'digitizedId']
    },
    {
        'model': 'Canvas',
        'viewModel': 'CanvasViewModel',
        'view': 'CanvasView'
    },
    {
        'model': 'Document',
        'viewModel': 'DocumentViewModel'
    },
    {
        //TODO why are viewmodels models?
        //TODO Perhaps have chromeviews and chromeviewmodels in their own different folder structure
        'model': 'CanvasBorderViewModel',
        'view': 'CanvasBorderView'
    },
    {
        'model': 'CanvasBorderOverlayViewModel',
        'view': 'CanvasBorderOverlayView'
    },
    {
        'model': 'CanvasDimensionsViewModel',
        'view': 'CanvasDimensionsView'
    },
    {
        'model': 'CanvasObjectsViewModel',
        'view': 'CanvasObjectsView'
    },
    {
        'model': 'CanvasRulerViewModel',
        'view': 'CanvasRulerView'
    },
    {
        'model': 'CanvasToolsViewModel',
        'view': 'CanvasToolsView'
    },
    {
        'model': 'DesignAreaSceneViewModel',
        'view': 'DesignAreaSceneView'
    },
    {
        'model': 'MarginsViewModel',
        'view': 'MarginsView'
    },
    {
        'model': 'SceneViewModel',
        'view': 'SceneView'
    },
    {
        'model': 'SurfaceSpecificationViewModel',
        'view': 'SurfaceSpecificationView'
    }
];
