const allViewModels = [
    'TextFieldViewModel',
    'UploadedImageViewModel',
    'ShapeViewModel',
    'LineViewModel'
];

module.exports = {
    snapThreshold: 6,

    //Define what viewmodels can snap to what view models
    snapOptions: {
        'TextFieldViewModel': allViewModels,
        'UploadedImageViewModel': allViewModels
    },

    isSnappingDisabled: function(viewModel) {
        return (viewModel.model.get('curveRadius') && viewModel.model.get('curveRadius') !== 0)
                || (viewModel.model.get('writingMode') === 'vertical-rl')
                || (viewModel.model.get('writingMode') === 'vertical-lr');
    },

    isSnappingToCenterDisabled: function(viewModel) {
        return (viewModel.model.get('writingMode') === 'vertical-rl')
            || (viewModel.model.get('writingMode') === 'vertical-lr');
    },

    ignoreSnappingTo: function(siblingViewModel) {
        const previewSize = siblingViewModel.get('previewSize');

        return (previewSize.width === 0 || previewSize.height === 0) // Ignore anything empty
                //Ignore all text fields that are vertical
                || (siblingViewModel.model.get('writingMode') && siblingViewModel.model.get('writingMode') === 'vertical-rl')
                || (siblingViewModel.model.get('writingMode') && siblingViewModel.model.get('writingMode') === 'vertical-lr');
    }
};
