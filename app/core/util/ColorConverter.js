const _ = require('underscore');
const util = require('util/Util');
const designerContext = require('DesignerContext');

/* eslint-disable no-magic-numbers */

// const valueRanges = {
//     rgb:   { r: [0, 255], g: [0, 255], b: [0, 255] },
//     cmy:   { c: [0, 100], m: [0, 100], y: [0, 100] },
//     cmyk:  { c: [0, 100], m: [0, 100], y: [0, 100], k: [0, 100] },
//     alpha: { alpha: [0, 1] },
//     HEX:   { HEX: [0, 16777215] }
// };

const HEX_REGEX = /^#([0-9a-fA-F]{3}|[0-9a-fA-F]{6})$/;

const isRgbDefined = function(color) {
    return _.isObject(color) && !_.isArray(color) && !_.isFunction() && util.isNumberDefined(color.r) && util.isNumberDefined(color.g) && util.isNumberDefined(color.b);
};

const isCmykDefined = function(color) {
    //TODO alter this. Currently throws error to detract from people using cmyk
    //return this.isNumberDefined(color.c) && this.isNumberDefined(color.m) && this.isNumberDefined(color.y) && this.isNumberDefined(color.k);
    if (util.isNumberDefined(color.c) && util.isNumberDefined(color.m) && util.isNumberDefined(color.y) && util.isNumberDefined(color.k)) {
        throw Error('CMYK is not yet supported!');
    } else {
        return false;
    }
};

const isThreadDefined = function(color) {
    return color.thread;
};

const hex2rgb = function(hex) {
    const hexValue = hex.replace('#', '');
    return {
        r: parseInt(hexValue[0] + hexValue[hexValue[3] ? 1 : 0], 16),
        g: parseInt(hexValue[hexValue[3] ? 2 : 1] + (hexValue[3] || hexValue[1]), 16),
        b: parseInt((hexValue[4] || hexValue[2]) + (hexValue[5] || hexValue[2]), 16)
    };
};

const rgb2hex = function(rgb) {
    return `#${(
        (rgb.r < 16 ? '0' : '') + rgb.r.toString(16) +
        (rgb.g < 16 ? '0' : '') + rgb.g.toString(16) +
        (rgb.b < 16 ? '0' : '') + rgb.b.toString(16)
    ).toUpperCase()}`;
};

const threadToHex = function(color) {
    const clients = require('services/clientProvider');
    return clients.embroidery.hexForThread(color.thread);
};

const addJson = function(colors) {
    colors.forEach(color => {
        color.json = JSON.stringify(color);
    });
    return colors;
};

const addThreads = function(colors) {
    return designerContext.filterColors(colors);
};

const mapHexStringInput = function(colors) {
    return colors.map(color => {
        if (_.isString(color) && color.match(HEX_REGEX)) {
            return { hex: color };
        }
        return color;
    });
};


const filterInvalidColors = function(colors) {
    return colors.filter(color => {
        // Checks if none of the four colors are fully defined (only 1 needs to be to be valid)
        if (!util.isStringDefined(color.hex) && !isRgbDefined(color) && !isCmykDefined(color) && !isThreadDefined(color)) {
            return false;
        }
        //Checks if hex is defined properly
        if (util.isStringDefined(color.hex) && !`${color.hex}`.match(HEX_REGEX)) {
            return false;
        }
        //Checks if rgb is defined properly
        if (isRgbDefined(color) && (color.r > 255 || color.r < 0 || color.g > 255 || color.g < 0 || color.b > 255 || color.b < 0)) {
            return false;
        }
        //Checks if cmyk is defined properly
        return true;
    });
};

const addHex = function(colors) {
    colors.forEach(color => {
        if (util.isStringDefined(color.hex)) {
            return;
        } else if (isRgbDefined(color)) {
            color.hex = rgb2hex(color);
        } else if (isThreadDefined(color)) {
            color.hex = threadToHex(color);
        } else {
            //TODO define cmyk convert
            return;
        }
    });
    return colors;
};

const addRgb = function(colors) {
    colors.forEach(color => {
        if (isRgbDefined(color)) {
            return;
        } else if (util.isStringDefined(color.hex)) {
            const rgb = hex2rgb(color.hex);
            color.r = rgb.r;
            color.g = rgb.g;
            color.b = rgb.b;
        } else {
            //TODO define cmyk convert
            return;
        }
    });
    return colors;
};

const addCmyk = function(colors) {
    colors.forEach((color) => { // eslint-disable-line
        //TODO define cmyk convert
        return;
    });
    return colors;
};

//This conversion uses the luminosity method, returning the value for gray (from 0-255)
const rgb2grey = function(rgb) {
    return (rgb.r * 0.21) + (rgb.g * 0.72) + (rgb.b * 0.07);
};

const rgbstring2rgb = function(rgbstring) {
    const values = rgbstring.split(',');
    return {
        r: values[0],
        g: values[1],
        b: values[2]
    };
};
const cmyk2cmy = function(cmyk) {
    const k = cmyk.k;

    return {
        c: cmyk.c * (1 - k) + k,
        m: cmyk.m * (1 - k) + k,
        y: cmyk.y * (1 - k) + k
    };
};

const cmy2rgb = function(cmy) {
    return {
        r: 1 - cmy.c,
        g: 1 - cmy.m,
        b: 1 - cmy.y
    };
};

const cmyk2rgb = function(cmyk) {
    const cmy = cmyk2cmy(cmyk);
    return cmy2rgb(cmy);
};

const cmyk2hex = function(cmyk) {
    const rgb = cmyk2rgb(cmyk);
    return rgb2hex(rgb);
};

const rgb2cmy = function(rgb) {
    return {
        c: 1 - rgb.r,
        m: 1 - rgb.g,
        y: 1 - rgb.b
    };
};

const cmy2cmyk = function(cmy) {
    const k = Math.min(Math.min(cmy.c, cmy.m), cmy.y),
        t = 1 - k || 1e-20;

    return {
        c: (cmy.c - k) / t,
        m: (cmy.m - k) / t,
        y: (cmy.y - k) / t,
        k
    };
};

const rgb2cmyk = function(rgb) {
    const cmy = rgb2cmy(rgb);
    return cmy2cmyk(cmy);
};

const rgb2hsv = function(rgb) {
    const r = rgb.r / 255;
    const g = rgb.g / 255;
    const b = rgb.b / 255;

    const hsv = { h: 0, s: 0, v: 0 };

    let min = 0;
    let max = 0;

    if (r >= g && r >= b) {
        max = r;
        min = (g > b) ? b : g;
    } else if (g >= b && g >= r) {
        max = g;
        min = (r > b) ? b : r;
    } else {
        max = b;
        min = (g > r) ? r : g;
    }

    hsv.v = max;
    hsv.s = (max) ? ((max - min) / max) : 0;

    if (!hsv.s) {
        hsv.h = 0;
    } else {
        const delta = max - min;
        if (r === max) {
            hsv.h = (g - b) / delta;
        } else if (g === max) {
            hsv.h = 2 + (b - r) / delta;
        } else {
            hsv.h = 4 + (r - g) / delta;
        }

        hsv.h = parseInt(hsv.h * 60);
        if (hsv.h < 0) {
            hsv.h += 360;
        }
    }

    hsv.s = parseInt(hsv.s * 100);
    hsv.v = parseInt(hsv.v * 100);

    return hsv;
};

const hsv2rgb = function(hsv) {
    const rgb = { r: 0, g: 0, b: 0 };

    let h = hsv.h;
    let s = hsv.s;
    let v = hsv.v;

    if (s === 0) {
        if (v === 0) {
            rgb.r = rgb.g = rgb.b = 0;
        } else {
            rgb.r = rgb.g = rgb.b = parseInt(v * 255 / 100);
        }
    } else {
        if (h === 360) {
            h = 0;
        }
        h /= 60;

        // 100 scale
        s = s / 100;
        v = v / 100;

        const i = parseInt(h);
        const f = h - i;
        const p = v * (1 - s);
        const q = v * (1 - (s * f));
        const t = v * (1 - (s * (1 - f)));
        switch (i) {
            case 0:
                rgb.r = v;
                rgb.g = t;
                rgb.b = p;
                break;
            case 1:
                rgb.r = q;
                rgb.g = v;
                rgb.b = p;
                break;
            case 2:
                rgb.r = p;
                rgb.g = v;
                rgb.b = t;
                break;
            case 3:
                rgb.r = p;
                rgb.g = q;
                rgb.b = v;
                break;
            case 4:
                rgb.r = t;
                rgb.g = p;
                rgb.b = v;
                break;
            case 5:
                rgb.r = v;
                rgb.g = p;
                rgb.b = q;
                break;
            default:
                break;
        }

        rgb.r = parseInt(rgb.r * 255);
        rgb.g = parseInt(rgb.g * 255);
        rgb.b = parseInt(rgb.b * 255);
    }

    return rgb;
};

//TODO:This isn't incredibly accurate, update this. (this is currently unused)
const getAverageColor = function(image) {
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    ctx.drawImage(image, 0, 0, 1, 1);
    const data = ctx.getImageData(0, 0, 1, 1);
    return { r: data[0], g: data[1], b: data[2] };
};

const getAverageColorFromUrl = function(url) {
    let color;
    util.getImageFromUrl(url).then(img => color = getAverageColor(img));
    return color;
};

module.exports = {
    rgb2grey,
    isRgbDefined,
    isCmykDefined,
    hex2rgb,
    rgb2hex,
    rgbstring2rgb,
    cmyk2hex,
    cmyk2rgb,
    rgb2cmy,
    cmy2cmyk,
    cmyk2cmy,
    cmy2rgb,
    rgb2cmyk,
    rgb2hsv,
    hsv2rgb,
    mapColors: _.compose(addJson, addThreads, addCmyk, addRgb, addHex, filterInvalidColors, mapHexStringInput),
    getAverageColor,
    getAverageColorFromUrl
};