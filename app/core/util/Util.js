const $ = require('jquery');
const _ = require('underscore');

/* eslint-disable no-magic-numbers */

module.exports = {
    // Adds a mixin to a prototype. This is used to add functionality to view models.
    addMixin: function(prototype, mixin) {
        _.each(mixin, function(value, name) {
            if (_.isFunction(value)) {
                if (_.isFunction(prototype[name])) {
                    // if a function already exists, we should preserve it so that it
                    // can still be used by name inside the replacing method.
                    prototype[`${name}Base`] = prototype[name];
                }
                prototype[name] = value;
            } else if (_.isObject(value)) {
                if (!prototype.hasOwnProperty(name)) {
                    prototype[name] = _.extend({}, prototype[name], value);
                } else {
                    _.extend(prototype[name], value);
                }
            } else {
                _.extend(prototype[name], value);
            }
        });
    },

    isNumberDefined: function(value) {
        return _.isNumber(value) && !isNaN(value);
    },

    isStringDefined: function(value) {
        return _.isString(value);
    },

    // This is necessary as the javascript % operator is actually a remainder, not modulus, and does not handle negatives properly.
    mod: function(a, n) {
        return ((a % n) + n) % n;
    },

    round: function(num, places) {
        if (places > 0) {
            return +(Math.round(num + 'e+' + places) + 'e-' + places);
        } else if (places === 0) {
            return Math.round(num);
        } else {
            //TODO: FIX FOR LESS THAN 0
            //return +(Math.round(num + 'e-' + places) + 'e+' + places);
            return Math.round(num);
        }
    },

    getHypotenuse: function(x, y) {
        return Math.sqrt((x * x) + (y * y));
    },

    // This returns an array of the corners of the rectangle (rounded)
    // x, y => rectangle position (top-left corner)
    // w, h => size
    // ox, oy, => origin position
    // r => rotation (degrees for convenience)
    rotateRectangleAroundOrigin: function(x, y, w, h, ox, oy, r) {
        const corner1 = { x: x, y: y };
        const corner2 = { x: x + w, y: y };
        const corner3 = { x: x + w, y: y + h };
        const corner4 = { x: x, y: y + h };
        const rotatedCoords = [
            _.mapObject(this.rotatePointAroundOrigin(corner1.x, corner1.y, ox, oy, r), (val) => { return Math.round(val); }),
            _.mapObject(this.rotatePointAroundOrigin(corner2.x, corner2.y, ox, oy, r), (val) => { return Math.round(val); }),
            _.mapObject(this.rotatePointAroundOrigin(corner3.x, corner3.y, ox, oy, r), (val) => { return Math.round(val); }),
            _.mapObject(this.rotatePointAroundOrigin(corner4.x, corner4.y, ox, oy, r), (val) => { return Math.round(val); })
        ];
        return rotatedCoords;
    },

    // This does not do rounding and takes in degrees (for convenience)
    rotatePointAroundOrigin: function(px, py, ox, oy, rotation) {
        const deltaX = px - ox;
        const deltaY = py - oy;
        const distance = this.getHypotenuse(deltaX, deltaY);
        const angle = Math.atan2(deltaY, deltaX);
        const newAngle = angle - this.degreesToRadians(rotation);
        return {
            x: ox + distance * Math.cos(newAngle),
            y: oy + distance * Math.sin(newAngle)
        };
    },

    centerOfTwoPoints: function(ax, ay, bx, by) {
        return {
            x: (ax + bx) / 2,
            y: (ay + by) / 2
        };
    },

    centerOfRectangle: function(left, top, width, height) {
        return {
            x: left + (width / 2),
            y: top + (height / 2)
        };
    },

    arrayMax: function(values) {
        return this.recursionCalculate(values, Math.max);
    },

    arrayMin: function(values) {
        return this.recursionCalculate(values, Math.min);
    },

    recursionCalculate: function(values, func) {
        if (values.length === 0) {
            return NaN;
        } else if (values.length === 1) {
            const currentValue = values.pop();
            if (typeof currentValue === 'number') {
                return currentValue;
            } else {
                return NaN;
            }
        } else {
            const recursionValue = values.pop();
            return func(recursionValue, this.recursionCalculate(values, func));
        }
    },

    generateUUID: function() {
        let d = new Date().getTime();
        if (window.performance && typeof window.performance.now === 'function') {
            d += performance.now(); //use high-precision timer if available
        }
        const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            const r = (d + Math.random() * 16) % 16 | 0; // eslint-disable-line no-magic-numbers
            d = Math.floor(d / 16); // eslint-disable-line no-magic-numbers
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16); // eslint-disable-line no-magic-numbers
        });
        return uuid;
    },

    logError: function(err) {
        if (window.isDebug) {
            throw err;
        }
    },

    getCoordinates: function(e, identifier) {
        // Regular mouse events.
        if (_.isNumber(e.pageX)) {
            return {
                left: e.pageX,
                top: e.pageY
            };
        }

        if (e.originalEvent) {
            // Touch events.
            if (!_.isUndefined(identifier)) {
                const touch = _.find(e.originalEvent.changedTouches, function(t) {
                    return t.identifier === identifier;
                }) ||
                    _.find(e.originalEvent.touches, function(t) {
                        return t.identifier === identifier;
                    });

                if (touch) {
                    return {
                        left: touch.pageX,
                        top: touch.pageY
                    };
                }
            }

            // HTML5 native drag and drop events.
            return {
                left: e.originalEvent.changedTouches[0].pageX,
                top: e.originalEvent.changedTouches[0].pageY
            };
        }

        return {};
    },

    degreesToRadians: function(degrees) {
        return degrees * (Math.PI / 180);
    },

    radiansToDegrees: function(radians) {
        return radians * (180 / Math.PI);
    },

    applyRotationMatrix: function(x, y, radians) {
        return {
            x: (x * Math.cos(radians)) - (y * Math.sin(radians)),
            y: (x * Math.sin(radians)) + (y * Math.cos(radians))
        };
    },

    getRelativeCoordinates: function(view, inCoords) {
        const droppableOffset = view.$el.offset();
        //this should always be going to the canvas
        const canvasViewModel = view.viewModel.parent;
        const zoomFactor = canvasViewModel ? canvasViewModel.get('zoomFactor') : 1.0;
        return {
            top: (inCoords.top - droppableOffset.top) / zoomFactor,
            left: (inCoords.left - droppableOffset.left) / zoomFactor
        };
    },

    supportsCssProperty: _.memoize(function(prop) {
        const body = document.body || document.documentElement;
        const style = body.style;

        if (typeof style[prop] === 'string') {
            return true;
        }

        const prefix = ['Moz', 'Webkit', 'O', 'ms'];
        const property = prop.charAt(0).toUpperCase() + prop.substr(1);
        for (let i = 0; i < prefix.length; i++) {
            if (typeof style[prefix[i] + property] === 'string') {
                return true;
            }
        }

        return false;
    }),

    getImageFromUrl: function(url) {
        return new Promise((resolve, reject) => {
            const img = new Image();
            img.onload = function() {
                resolve(img);
            };
            img.onerror = function() {
                reject(img);
            };
            img.crossOrigin = 'Anonymous';
            img.src = url;
        });
    },

    intersects: function(dimensions1, dimensions2) {

        let top1, left1, height1, width1, bottom1, right1,
            top2, left2, height2, width2, bottom2, right2;

        top1 = dimensions1.top;
        left1 = dimensions1.left;
        height1 = dimensions1.height;
        width1 = dimensions1.width;
        bottom1 = parseFloat(top1) + parseFloat(height1);
        right1 = parseFloat(left1) + parseFloat(width1);

        top2 = dimensions2.top;
        left2 = dimensions2.left;
        height2 = dimensions2.height;
        width2 = dimensions2.width;
        bottom2 = parseFloat(top2) + parseFloat(height2);
        right2 = parseFloat(left2) + parseFloat(width2);

        //note that if a box has height/width of 0, in web terms it may as well not be there
        return !(height1 === 0 || width1 === 0 || height2 === 0 || width2 === 0 || bottom1 < top2 || top1 > bottom2 || right1 < left2 || left1 > right2);
    },

    // Takes 2 arrays of vertices to form convex polygons for collision detection
    // This uses the Seperating Axis Theorem http://www.dyn4j.org/2010/01/sat/   http://stackoverflow.com/questions/10962379/how-to-check-intersection-between-2-rotated-rectangles
    doPolygonsIntersect: function(a, b) {
        const polygons = [a, b];
        let projected, i, j, k, minA, maxA, minB, maxB;

        for (i = 0; i < polygons.length; i++) {

            // for each polygon, look at each edge of the polygon, and determine if it separates
            // the two shapes
            const polygon = polygons[i];
            for (j = 0; j < polygon.length; j++) {

                // grab 2 vertices to create an edge
                const j2 = (j + 1) % polygon.length;
                const p1 = polygon[j];
                const p2 = polygon[j2];

                // find the line perpendicular to this edge
                const normal = { x: p2.y - p1.y, y: p1.x - p2.x };

                minA = maxA = undefined;
                // for each vertex in the first shape, project it onto the line perpendicular to the edge
                // and keep track of the min and max of these values
                for (k = 0; k < a.length; k++) {
                    projected = normal.x * a[k].x + normal.y * a[k].y;
                    if (minA === undefined || projected < minA) {
                        minA = projected;
                    }
                    if (maxA === undefined || projected > maxA) {
                        maxA = projected;
                    }
                }

                // for each vertex in the second shape, project it onto the line perpendicular to the edge
                // and keep track of the min and max of these values
                minB = maxB = undefined;
                for (k = 0; k < b.length; k++) {
                    projected = normal.x * b[k].x + normal.y * b[k].y;
                    if (minB === undefined || projected < minB) {
                        minB = projected;
                    }
                    if (maxB === undefined || projected > maxB) {
                        maxB = projected;
                    }
                }

                // if there is no overlap between the projects, the edge we are looking at separates the two
                // polygons, and we know there is no overlap
                if (maxA < minB || maxB < minA) {
                    return false;
                }
            }
        }
        return true;
    },

    contains: function(dimensions1, dimensions2) {

        let top1, left1, height1, width1, bottom1, right1,
            top2, left2, height2, width2, bottom2, right2;

        top1 = dimensions1.top;
        left1 = dimensions1.left;
        height1 = dimensions1.height;
        width1 = dimensions1.width;
        bottom1 = parseFloat(top1) + parseFloat(height1);
        right1 = parseFloat(left1) + parseFloat(width1);

        top2 = dimensions2.top;
        left2 = dimensions2.left;
        height2 = dimensions2.height;
        width2 = dimensions2.width;
        bottom2 = parseFloat(top2) + parseFloat(height2);
        right2 = parseFloat(left2) + parseFloat(width2);

        //note that if a box has height/width of 0, in web terms it may as well not be there
        return !(height1 === 0 || width1 === 0 || height2 === 0 || width2 === 0) && (bottom1 > bottom2 && top1 < top2 && left1 < left2 && right1 > right2);
    },

    containsPoint: function(point, rectangle) {
        const right = rectangle.right || rectangle.left + rectangle.width;
        const bottom = rectangle.bottom || rectangle.top + rectangle.height;
        return (point.x >= rectangle.left && point.x <= right && point.y >= rectangle.top && point.y <= bottom);
    },

    //Get a random position in an area where the given item can fit
    getRandomPositionInArea: function(outerDimensions, itemSize) {
        //If there is no width or height given to the item we don't know if it will go
        //off the edge of one side so we keep the starting point at least 2/3 of the area
        //to the edge
        const variableWidthArea = itemSize.width ?
                                    outerDimensions.width - itemSize.width :
                                    outerDimensions.width * (2 / 3);
        const variableHeightArea = itemSize.height ?
                                    outerDimensions.height - itemSize.height :
                                    outerDimensions.height * (2 / 3);

        if (!outerDimensions.top) {
            outerDimensions.top = 0;
        }
        if (!outerDimensions.left) {
            outerDimensions.left = 0;
        }
        return {
            top: variableHeightArea * Math.random() + outerDimensions.top,
            left: variableWidthArea * Math.random() + outerDimensions.left
        };
    },

    getMostCommonAttributeValue: function(items, attr) {
        const counts = {};
        let best;
        _.each(items, function(item) {
            const val = item.get(attr);
            counts[val] = !counts[val] ? 1 : counts[val] + 1;
            if (_.isUndefined(best) || counts[val] > counts[best]) {
                best = val;
            }
        });
        return best;
    },

    getOverlayModel: function(overlayViewModel, canvasViewModel) {
        const overlayModule = overlayViewModel.prototype.defaults.module;
        const chromes = canvasViewModel.chromes;

        return _.find(chromes.models, function(model) {
            return model.get('module') === overlayModule;
        });
    },

    createLine: function(options) {
        $('<div>', { 'class': options.className })
            .css({
                top: options.top,
                left: options.left,
                width: options.width,
                height: options.height
            })
            .appendTo(options.el);
    },

    addText: function(options) {
        $('<span>', { 'class': options.className })
            .css({
                top: options.top,
                left: options.left,
                width: options.width
            })
            .text(options.text)
            .appendTo(options.el);
    },

    findMatchingViewModelByModelAttributes: function(viewModels, itemViewModel, attributesToMatchOn) {
        let matchingItemViewModel;
        for (let i = 0; i < viewModels.length; i++) {
            const currentItemViewModel = viewModels[i];
            let attributesMatch = true;

            for (let j = 0; j < attributesToMatchOn.length; j++) {
                const attribute = attributesToMatchOn[j];
                if (currentItemViewModel.model.attributes[attribute] !== itemViewModel.model.attributes[attribute]) {
                    attributesMatch = false;
                    break;
                }
            }

            if (attributesMatch) {
                matchingItemViewModel = currentItemViewModel;
                break;
            }
        }
        return matchingItemViewModel;
    },

    isImage: function(vm) {
        const imageModules = [
            'uploadedimage',
            'libraryimage'
        ];

        return _.contains(imageModules, vm.model.get('module'));
    }
};
