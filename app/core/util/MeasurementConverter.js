/* eslint-disable no-magic-numbers */

//Source is in millimeters
const mm2in = function(mm) {
    return mm / 25.4;
};

const mm2ft = function(mm) {
    return mm / 304.8;
};

const mm2yd = function(mm) {
    return mm / 914.4;
};

const mm2cm = function(mm) {
    return mm / 10;
};

const mm2m = function(mm) {
    return mm / 1000;
};

//Source is in centimeters
const cm2in = function(cm) {
    return cm / 2.54;
};

const cm2ft = function(cm) {
    return cm / 30.48;
};

const cm2yd = function(cm) {
    return cm / 91.44;
};

const cm2mm = function(cm) {
    return cm * 10;
};

const cm2m = function(cm) {
    return cm / 100;
};

//Source is in meters
const m2in = function(m) {
    return m / .0254;
};

const m2ft = function(m) {
    return m / .3048;
};

const m2yd = function(m) {
    return m / .9144;
};

const m2mm = function(m) {
    return m * 1000;
};

const m2cm = function(m) {
    return m * 100;
};

//Source is in inches
const in2ft = function(inch) {
    return inch / 12;
};

const in2yd = function(inch) {
    return inch / 36;
};

const in2mm = function(inch) {
    return inch * 25.4;
};

const in2cm = function(inch) {
    return inch * 2.54;
};

const in2m = function(inch) {
    return inch * .0254;
};

//Source is in feet
const ft2in = function(ft) {
    return ft * 12;
};

const ft2yd = function(ft) {
    return ft / 3;
};

const ft2mm = function(ft) {
    return ft * 304.8;
};

const ft2cm = function(ft) {
    return ft * 30.48;
};

const ft2m = function(ft) {
    return ft * .3048;
};

//Source is in yards
const yd2in = function(yd) {
    return yd * 36;
};

const yd2ft = function(yd) {
    return yd * 3;
};

const yd2mm = function(yd) {
    return yd * 914.4;
};

const yd2cm = function(yd) {
    return yd * 91.44;
};

const yd2m = function(yd) {
    return yd * .9144;
};

/*********************** End of micro-utility functions *******************************/

//Converters utilizing micro-utility functions
const convertToMm = function(size, fromUnits) {
    switch (fromUnits) {
        case 'in':
            return in2mm(size);
        case 'ft':
            return ft2mm(size);
        case 'yd':
            return yd2mm(size);
        case 'cm':
            return cm2mm(size);
        case 'm':
            return m2mm(size);
        //Could not convert, return original value.
        default:
            return size;
    }
};

const convertToCm = function(size, fromUnits) {
    switch (fromUnits) {
        case 'in':
            return in2cm(size);
        case 'ft':
            return ft2cm(size);
        case 'yd':
            return yd2cm(size);
        case 'mm':
            return mm2cm(size);
        case 'm':
            return m2cm(size);
        //Could not convert, return original value.
        default:
            return size;
    }
};

const convertToM = function(size, fromUnits) {
    switch (fromUnits) {
        case 'in':
            return in2m(size);
        case 'ft':
            return ft2m(size);
        case 'yd':
            return yd2m(size);
        case 'mm':
            return mm2m(size);
        case 'cm':
            return cm2m(size);
        //Could not convert, return original value.
        default:
            return size;
    }
};

const convertToIn = function(size, fromUnits) {
    switch (fromUnits) {
        case 'ft':
            return ft2in(size);
        case 'yd':
            return yd2in(size);
        case 'mm':
            return mm2in(size);
        case 'cm':
            return cm2in(size);
        case 'm':
            return m2in(size);
        //Could not convert, return original value.
        default:
            return size;
    }
};

const convertToFt = function(size, fromUnits) {
    switch (fromUnits) {
        case 'in':
            return in2ft(size);
        case 'yd':
            return yd2ft(size);
        case 'mm':
            return mm2ft(size);
        case 'cm':
            return cm2ft(size);
        case 'm':
            return m2ft(size);
        //Could not convert, return original value.
        default:
            return size;
    }
};

const convertToYd = function(size, fromUnits) {
    switch (fromUnits) {
        case 'in':
            return in2yd(size);
        case 'ft':
            return ft2yd(size);
        case 'mm':
            return mm2yd(size);
        case 'cm':
            return cm2yd(size);
        case 'm':
            return m2yd(size);
        //Could not convert, return original value.
        default:
            return size;
    }
};

//Overall encompassing converter
const convert = function(size, fromUnits, toUnits) {
    switch (toUnits) {
        case 'in':
            return convertToIn(size, fromUnits);
        case 'ft':
            return convertToFt(size, fromUnits);
        case 'yd':
            return convertToYd(size, fromUnits);
        case 'mm':
            return convertToMm(size, fromUnits);
        case 'cm':
            return convertToCm(size, fromUnits);
        case 'm':
            return convertToM(size, fromUnits);
        //Could not convert, return original value.
        default:
            return size;
    }
};

module.exports = {
    mm2in,
    mm2ft,
    mm2yd,
    mm2cm,
    mm2m,
    cm2in,
    cm2ft,
    cm2yd,
    cm2mm,
    cm2m,
    m2in,
    m2ft,
    m2yd,
    m2mm,
    m2cm,
    in2ft,
    in2yd,
    in2mm,
    in2cm,
    in2m,
    ft2in,
    ft2yd,
    ft2mm,
    ft2cm,
    ft2m,
    yd2in,
    yd2ft,
    yd2mm,
    yd2cm,
    yd2m,
    convertToMm,
    convertToCm,
    convertToM,
    convertToIn,
    convertToFt,
    convertToYd,
    convert
};
