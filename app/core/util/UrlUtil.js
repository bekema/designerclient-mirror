const $ = require('jquery');
const globalConfig = require('configuration/Global');

const shortenedUrlCache = {};

module.exports = {
    createUrl: function(urlString) {
        const newUrl = new $.Url(urlString);
        this.setUrlItemsBasedOnCurrentUrl(newUrl);
        return newUrl;
    },

    setUrlItemsBasedOnCurrentUrl: function(urlToSet) {
        const currentUrl = this.getCurrentUrl();

        // Mailing Services flow:
        this.setUrlItemBasedOnCurrentUrl(urlToSet, currentUrl, 'msv');

        // Saving flow:
        // Unique entity id: http://vistawiki.vistaprint.net/wiki/Uei
        this.setUrlItemBasedOnCurrentUrl(urlToSet, currentUrl, 'uei');
        // Advanced gallery: http://vistawiki.vistaprint.net/wiki/AG
        this.setUrlItemBasedOnCurrentUrl(urlToSet, currentUrl, 'ag');

        // DirectWrite roll MSR override: http://vistawiki.vistaprint.net/wiki/DirectWrite_Test
        this.setUrlItemBasedOnCurrentUrl(urlToSet, currentUrl, 'use_directwrite');

        // The original alt_doc_id which the user requested to edit from the cart (if any)
        this.setUrlItemBasedOnCurrentUrl(urlToSet, currentUrl, 'cart_alt_doc_id');
    },

    getCurrentUrl: function() {
        // Using window.location instead of document.location
        // for cross browser safety.
        // Ref: http://stackoverflow.com/questions/2430936/whats-the-difference-between-window-location-and-document-location-in-javascrip
        return new $.Url(window.location.toString());
    },

    setUrlItemBasedOnCurrentUrl: function(urlToSet, currentUrl, itemName) {
        const itemValue = currentUrl.getItem(itemName);
        if (itemValue !== undefined) {
            urlToSet.setItem(itemName, itemValue);
        }
    },

    getShortenedUrl: function(url, callback) {
        // TODO Vary the maxUrlLength by browser. No need to POST everywhere just because of IE.
        if (url.length >= globalConfig.maxUrlLength) {
            if (shortenedUrlCache[url]) {
                callback(shortenedUrlCache[url]);
                return;
            }

            // POST the long url first, and then return the url with that storageid. We can't use
            // the generic VP client-side shortener because we need the storageid to be available
            // on the image servers.
            let urlObj = new $.Url(url);
            const path = urlObj.pathname();
            $.post(path, urlObj.queryString)
                .done(function(data) {
                    urlObj = new $.Url(path);
                    urlObj.setItem('storageid', data);

                    const shortenedUrl = urlObj.toString();
                    shortenedUrlCache[url] = shortenedUrl;
                    callback(shortenedUrl);
                })
                .fail(function() {
                    // TODO: is not shortening it the right thing to do if this fails?
                    callback(url);
                });
        } else {
            callback(url);
        }
    },

    isValidUrl: function(url) {
        const reUrl = /^((https?|ftp):\/\/)?((([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,6}(:[\d]+)?)|(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(:[\d]+)?))(\/[^\s]*)?$/i;
        return reUrl.test($.trim(url));
    },

    // Changing the url forces the browser to reload the image rather than using the cached version
    appendCacheBustingToUrl: function(url) {
        return `${url}&cachedTime=${new Date().getTime()}`;
    }
};
