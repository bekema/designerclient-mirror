// This is the base `View` which makes it easy to hook up `Components`.
const _ = require('underscore');
const Backbone = require('backbone');

const View = Backbone.View.extend({

    __name__: 'View',

    profile: 'default',

    /* eslint backbone/no-constructor: 0 */
    constructor: function(options) {
        // Automatically (but optionally) associate a view with view model.
        if (options) {
            this.profile = options.profile || this.profile;
            this.viewModel = options.viewModel;
            if (this.viewModel) {
                this.listenTo(this.viewModel, 'destroy', this.remove);
            }
        }

        this.options = _.defaults({}, options, this.defaults);
        this.components = [];

        // `setup` will be triggered only once, when this view is rendered for the first time. It should be used
        // to create elements, but can be skipped when elements are rendered on the server. TODO Make it easier to override this.
        /* eslint-disable backbone/event-scope */ //eslint-plugin-backbone confuses _.once with Model.once.
        this.setup = _.once(function() {
            if (this.viewModel && !this.view) {
                let id = this.viewModel.id;
                const viewIdSuffix = this.idSuffix;
                if (viewIdSuffix) {
                    id += viewIdSuffix;
                }
                this.$el.attr('id', id);
            }
            this.trigger('setup', this);
        }.bind(this));
        /* eslint-enable backbone/event-scope */

        Backbone.View.call(this, this.options);
    },

    render: function() {
        this.setup();
        this.trigger('render', this);
        return this;
    },

    // `remove` will remove this view from the DOM, and it will also remove all of its `Components`. If there
    // are extra event handlers to override, then remove this.
    remove: function() {
        // Remove any components that have been added to this view, giving
        // them a chance to unbind any events.
        this.components.forEach(function(component) {
            // ComponentViews can remove themselves.
            if (!(component instanceof View)) {
                component.remove();
            }
        });

        Backbone.View.prototype.remove.apply(this, arguments);
    },

    addComponentWithAllProfiles: function(Constructor, options) {
        const component = new Constructor(this, options);
        this.components.push(component);
        return component;
    },

    // Usage: In a View's `initialize` method, call `this.addComponent(Component, { option : "..." });`
    // This is equivalent to calling `addComponentWithProfile` with the "default" profile.
    addComponent: function(Constructor, options) {
        return this.addComponentWithProfile('default', Constructor, options);
    },

    addComponentWithProfile: function(profiles, Constructor, options) {
        if (profiles === this.profile || Array.isArray(profiles) && _.contains(profiles, this.profile)) {
            return this.addComponentWithAllProfiles(Constructor, options);
        }
        return null;
    },

    delegateEvents: function(events) {
        // Have to call the base first, because it will undelegate internally, and we need to make sure components
        // can be re-delegated afterwards.
        Backbone.View.prototype.delegateEvents.apply(this, arguments);
        _.invoke(this.components, 'delegateEvents', events);
        return this;
    },

    undelegateEvents: function() {
        Backbone.View.prototype.undelegateEvents.apply(this, arguments);
        _.invoke(this.components, 'undelegateEvents');
        return this;
    },

    setElement: function() {
        const args = arguments;
        // Only call `setElement()` on children that are strictly components, i.e. not `ComponentViews`.
        _.each(this.components, function(component) {
            if (!(component instanceof View)) {
                component.setElement(...args);
            }
        });
        return Backbone.View.prototype.setElement.apply(this, args);
    }

});

module.exports = View;
