const View = require('views/View');

module.exports = View.extend({
    className: 'dcl-canvas-border',

    initialize: function() {
        this.listenTo(this.viewModel, 'change:borderWidth change:color', this.applyBorder);
        this.applyBorder();
    },

    applyBorder: function() {
        this.$el.css({ border: `${this.viewModel.get('borderWidth')}px solid ${this.viewModel.get('color')}`,
                       top: `${this.viewModel.get('top')}px`,
                       left: `${this.viewModel.get('left')}px` });
    }
});
