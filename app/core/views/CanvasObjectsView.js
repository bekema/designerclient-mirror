const View = require('views/View');

/* eslint-disable no-magic-numbers */

module.exports = View.extend({
    className: 'dcl-canvas-objects',

    initialize: function() {
        this.listenTo(this.viewModel, 'change', this.render);
    },

    render: function() {
        View.prototype.render.apply(this, arguments);

        this.$el.hide().empty();

        // draw all objects
        const objects = this.viewModel.get('objects');

        objects.forEach(item => {
            this.drawObjects(item, this);
        });

        this.$el.show();

        return this;
    },

    drawObjects: function(item, view) {
        const zoom = view.viewModel.parent.get('zoomFactor');
        const top = item.top;
        const left = item.left;
        const width = item.width * zoom;
        const height = zoom * item.height;

        // draw canvas object content block
        const divTag = document.createElement('div');
        divTag.setAttribute('class', item.className);
        divTag.setAttribute('fill', 'grey');
        divTag.setAttribute('style', 'left: ' + left + 'px; top: ' + top + 'px;');
        divTag.setAttribute('width', width);
        divTag.setAttribute('height', height);

        item.content.forEach(function(object) {
            const objectWidth = object.width * zoom;
            const objectHeight = object.height * zoom;

            const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
            svg.setAttribute('width', objectWidth);
            svg.setAttribute('height', objectHeight);

            if (object.value) {
                const text = document.createElementNS('http://www.w3.org/2000/svg', 'text');
                text.setAttribute('x', object.left);
                text.setAttribute('y', object.top);
                text.setAttribute('width', objectWidth);
                text.setAttribute('height', objectHeight);
                text.setAttribute('class', object.className);

                if (object.transform) {
                    text.setAttribute('transform', `scale(${zoom})`);
                }

                const textContent = document.createTextNode(object.value);
                text.appendChild(textContent);
                svg.appendChild(text);
            } else {
                const image = document.createElementNS('http://www.w3.org/2000/svg', 'image');
                image.setAttribute('x', object.left);
                image.setAttribute('y', object.top);
                image.setAttribute('width', objectWidth);
                image.setAttribute('height', objectHeight);
                image.setAttribute('class', object.className);
                image.setAttribute('href', object.imageUrl);


                if (object.transform) {
                    image.setAttribute('transform', `scale(${zoom})`);
                }

                svg.appendChild(image);
            }

            divTag.appendChild(svg);
        });

        view.$el.append(divTag);
    }
});