const View = require('views/View');
const UrlBoundImage = require('views/components/UrlBoundImage');
const Positioning = require('views/components/Positioning');

module.exports = View.extend({

    tagName: 'img',

    className: 'dcl-scene-image',

    initialize: function() {
        this.addComponentWithProfile(['default', 'preview'], UrlBoundImage);
        this.addComponentWithProfile(['default', 'preview'], Positioning, { sizeAttribute: 'realDimensions', positionAttribute: 'realDimensions' });
    }
});
