/* eslint-disable no-magic-numbers */

module.exports = {
    computed: {
        snapPosition: {
            get: function() {
                const zoom = this.parent.get('zoomFactor');
                const previewPosition = this.get('handlePosition');
                const snapBox = this.get('snapBox');

                return {
                    top: previewPosition.top - Math.round(snapBox.top * zoom),
                    left: previewPosition.left - Math.round(snapBox.left * zoom)
                };
            },
            bindings: function() {
                return [
                    ['handlePosition', 'snapBox'],
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },
        snapSize: {
            get: function() {
                const zoom = this.parent.get('zoomFactor');
                const snapBox = this.get('snapBox');

                return {
                    width: Math.round(snapBox.width * zoom),
                    height: Math.round(snapBox.height * zoom)
                };
            },
            bindings: function() {
                return [
                    'snapBox',
                    { model: this.parent, attribute: 'zoomFactor' }
                ];
            }
        },
        baselines: {
            get: function() {
                const angle = this.model.get('rotation');
                const position = this.get('handlePosition');
                const size = this.get('handleSize');

                //TODO rotation snapping
                if (angle === 0 || angle === 180) {
                    return [position.top + Math.round(angle === 180 ? 0 : size.height)];
                } else {
                    return [position.left + Math.round(angle === 90 ? 0 : size.width)];
                }
            },
            bindings: function() {
                return [
                    ['handleSize', 'handlePosition'],
                    { model: this.model, attribute: 'rotation' }
                ];
            }
        }
    }
};
