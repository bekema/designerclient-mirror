const $ = require('jquery');
const View = require('views/View');

/* eslint-disable no-magic-numbers */

module.exports = View.extend({

    initialize: function() {
        this.$el.hide();
        this.listenTo(this.viewModel, 'change:sqareWidth', this.render);
        this.listenTo(this.viewModel, 'toggleOverlay', this.onToggleOverlay);
        this.listenTo(this.viewModel.parent, 'change:zoomFactor', this.render);
    },

    className: 'grid-overlay',

    createGridline: function(top, left, width, height, isSectionLine) {
        /* eslint backbone/no-native-jquery: 0 */
        const gridLine = $("<div class='grid-line'></div>").css({
            top,
            left,
            width,
            height
        });
        if (isSectionLine) {
            gridLine.addClass('section-line');
        }
        this.$el.append(gridLine);
    },

    onToggleOverlay: function(e) {
        const showOverlay = e.showOverlay;
        this.$el.toggle(showOverlay);
    },

    render: function() {
        this.$el.empty();
        let isSectionLine, i;
        const parent = this.viewModel.parent;
        const height = parent.model.get('height') * parent.get('zoomFactor');
        const width  = parent.model.get('width') * parent.get('zoomFactor');
        const squareWidth = this.viewModel.get('squareWidth');

        // add the center horizontal line
        this.createGridline(height / 2, 0, width, 2, true);
        // start from the center and add horizontal gridlines
        for (i = height / 2; i > 0; i -= squareWidth) {
            isSectionLine = false;
            // Every 5th line should be highlighted
            if (Math.round((i - height / 2) / squareWidth) % 5 === 0) {
                isSectionLine = true;
            }
            this.createGridline(i, 0, width, 1, isSectionLine);
            this.createGridline(height - i, 0, width, 1, isSectionLine);
        }

        // add the center vertical line
        this.createGridline(0, width / 2, 2, height, true);
        // start from the center and add vertical gridlines
        for (i =  width / 2; i > 0; i -= squareWidth) {
            isSectionLine = false;
            // Every 5th line should be highlighted
            if (Math.round((i - width / 2) / squareWidth) % 5 === 0) {
                isSectionLine = true;
            }
            this.createGridline(0, i, 1, height, isSectionLine);
            this.createGridline(0, width - i, 1, height, isSectionLine);
        }

        return this;
    }
});
