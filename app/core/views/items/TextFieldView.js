const Positioning = require('views/components/Positioning');
const TextFieldTarget = require('views/components/targets/TextFieldTarget');
const UrlBoundImageWithMetaData = require('views/components/UrlBoundImageWithMetaData');
const Handle = require('views/components/handles/Handle');
const ItemView = require('views/items/ItemView');
const BoundingBoxUpdater = require('views/components/BoundingBoxUpdater');

module.exports = ItemView.extend({
    className: 'dcl-item',

    initialize: function(options) {
        options.viewType = 'text';
        ItemView.prototype.initialize.apply(this, arguments);

        // These components are all necessary in order to display the view at the correct location.
        this.addComponentWithProfile(['default', 'preview'], UrlBoundImageWithMetaData, { metaDataKey: 'X-Render-Metadata' });
        this.addComponentWithProfile(['default', 'preview'], Positioning, { sizeAttribute: false, transformOriginAttribute: 'transformOrigin' });

        //This appears to need to be positioned here amongst the components
        this.addComponent(BoundingBoxUpdater);

        // What the user interacts with.
        this.addComponent(TextFieldTarget, options);

        this.addComponent(Handle, options);
    }
});
