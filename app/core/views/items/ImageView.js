const DocumentRepository = require('DocumentRepository');
const ImageTarget = require('views/components/targets/ImageTarget');
const Handle = require('views/components/handles/Handle');
const Positioning = require('views/components/Positioning');
const ImageResolutionValidation = require('views/components/validations/ImageResolutionValidation');
const UrlBoundImage = require('views/components/UrlBoundImage');
const DigitizingImage = require('views/components/DigitizingImage');
const ItemView = require('views/items/ItemView');
const validationProvider = require('validations/ValidationProvider');
const BoundingBoxUpdater = require('views/components/BoundingBoxUpdater');
const designerContext = require('DesignerContext');
const ImageColors = require('views/components/ImageColors');
const colorConfig = require('publicApi/configuration/ui').colors.colorRetrieval;

module.exports = ItemView.extend({

    className: 'dcl-item dcl-ui-resizable',

    initialize: function(options) {
        ItemView.prototype.initialize.apply(this, arguments);

        const module = this.viewModel.model.get('module');

        options.viewType = 'image';

        // These components are all necessary in order to display the view at the correct location.
        this.addComponentWithProfile(['default', 'preview'], UrlBoundImage);
        this.addComponentWithProfile(['default', 'preview'], Positioning);

        // So we can show that the image is still being processed for threads, etc
        this.addComponentWithProfile(['default', 'preview'], DigitizingImage);

        //This appears to need to be positioned here amongst the components
        this.addComponent(BoundingBoxUpdater);

        this.addComponent(ImageTarget, options);
        this.addComponent(Handle, options);

        //TODO should this be checking the module, and should it perhaps be checking the feature manager?
        if ((module === 'UploadedImage') || (module === 'LibraryImage')) {
            //Resolution validation only applies to non-vector images
            if (!designerContext.embroidery && !this.viewModel.model.get('isVector') && validationProvider.imageResolution) {
                this.addComponent(ImageResolutionValidation, options);
            }
        }

        // reading the image colors only apply to non-embroidery products,
        // because embroidery products use thread colors that we can easily use to retrieve the colors
        if (!designerContext.embroidery && colorConfig.enabled) {
            this.addComponent(ImageColors, options);
        }
    },

    isFullUpload: function() {
        const activeCanvasViewModel = DocumentRepository.getActiveCanvasViewModel();
        return activeCanvasViewModel && activeCanvasViewModel.get('isFullUpload');
    }
});