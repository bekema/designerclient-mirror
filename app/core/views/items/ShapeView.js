const metaDataProvider = require('MetaDataProvider');
const Positioning = require('views/components/Positioning');
const ShapeTarget = require('views/components/targets/ShapeTarget');
const Handle = require('views/components/handles/Handle');
const UrlBoundImageWithMetaData = require('views/components/UrlBoundImageWithMetaData');
const ItemView = require('views/items/ItemView');
const BoundingBoxUpdater = require('views/components/BoundingBoxUpdater');

module.exports = ItemView.extend({

    className: 'dcl-item dcl-ui-resizable',

    initialize: function(options) {
        options.viewType = 'shape';
        ItemView.prototype.initialize.apply(this, arguments);

        const shapeType = options.viewModel.model.get('module');
        const previewAttributes = metaDataProvider.getMetaData(shapeType).previewAttributes.map(attribute => {
            return `model:change:${attribute}`;
        }).join(' ');

        this.addComponentWithProfile(['default', 'preview'], UrlBoundImageWithMetaData, { previewAttributes, metaDataKey: 'X-Render-Metadata' });
        this.addComponentWithProfile(['default', 'preview'], Positioning);

        //This appears to need to be positioned here amongst the components
        this.addComponent(BoundingBoxUpdater);

        this.addComponent(ShapeTarget, options);
        this.addComponent(Handle, options);
    }
});
