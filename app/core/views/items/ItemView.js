const _ = require('underscore');
const Decoration = require('views/components/Decoration');
const DesignerContext = require('DesignerContext');
const RenderAsHtmlCanvas = require('views/components/RenderAsHtmlCanvas');
const Highlightable = require('views/components/Highlightable');
const overlapConfig = require('publicApi/configuration/validations').overlap;
const OverlapValidation = require('views/components/validations/OverlapValidation');
const validationManager = require('validations/ValidationManager');
const validationProvider = require('validations/ValidationProvider');

const View = require('views/View');

module.exports = View.extend({

    tagName: 'img',

    initialize: function(options) {
        //These two are important to put first, as a lot of things depend on these two.
        this.addComponent(Decoration, options);
        this.addComponent(RenderAsHtmlCanvas, _.extend({ sizeAttribute: 'previewBoundingBox' }, options));

        this.addComponent(Highlightable, options);

        if (validationProvider.overlap && (DesignerContext.embroidery || overlapConfig[options.viewType])) {
            this.addComponent(OverlapValidation, options);
        }

        this.listenTo(validationManager, 'add', this.updateValidations);
        this.listenTo(validationManager, 'remove', this.removeValidations);
    }
});
