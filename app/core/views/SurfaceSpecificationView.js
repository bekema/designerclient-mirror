const Decoration = require('views/components/Decoration');
const OverlapValidation = require('views/components/validations/OverlapValidation');
const util = require('util/Util');
const View = require('views/View');

/* eslint-disable no-magic-numbers */

module.exports = View.extend({

    initialize: function(options) {
        this.listenTo(this.viewModel, 'change', this.render);

        // Add overlap validation for collision detection margins
        this.addComponent(Decoration, options);
        this.addComponent(OverlapValidation, options);
    },

    render: function() {
        View.prototype.render.apply(this, arguments);

        this.$el.hide().empty();

        // retrieve all margins from the ViewModel
        const margins = this.viewModel.getMargins();

        // Add margins to canvas element
        this.$el.append(this.addMargin(this.$el, margins));

        return this;
    },

    addMargin: function(viewModel, margins) {
        this.canvas = document.createElement('canvas');
        const zoom = this.viewModel.parent.get('zoomFactor');
        const dimensions = this.viewModel.get('dimensions');
        const ctx = this.canvas.getContext('2d');

        const rotation = this.viewModel.get('rotation') || 0;
        this.canvas.height = rotation > 0 ? dimensions.width : dimensions.height;
        this.canvas.width = rotation > 0 ? dimensions.height : dimensions.width;
        const me = this;

        // Draw bleed/trim margins object on the canvas
        const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        // SVG element needs an absolute height and width to work in Firefox
        svg.setAttribute('width', dimensions.width);
        svg.setAttribute('height', dimensions.height);

        const path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        let pathString = '';

        // Create new path for each margin
        for (let i = 0; i < margins.length; i++) {
            pathString += ' ' + margins[i].pathString;
        }

        path.setAttribute('d', pathString);
        path.setAttribute('stroke', '#34343');
        path.setAttribute('stroke-width', '1px');
        path.setAttribute('opacity', 0.5);
        path.setAttribute('fill', '#34343');
        path.setAttribute('fill-rule', 'evenodd');
        svg.appendChild(path);

        const xml = new XMLSerializer().serializeToString(svg);
        const svg64 = btoa(xml);
        const b64Start = 'data:image/svg+xml;base64,';

        // prepend a "header"
        const image64 = b64Start + svg64;

        // Create new image
        const source = new Image();
        source.src = image64;
        // set width and height for supporting IE11
        source.width = dimensions.width;
        source.height = dimensions.height;

        source.onload = function() {
            const width = this.width * zoom;
            const height = this.height * zoom;

            ctx.translate(me.canvas.width / 2, me.canvas.height / 2);
            ctx.rotate(util.degreesToRadians(360 - rotation));
            ctx.drawImage(source, -(this.width / 2), -(this.height / 2), width, height);

            URL.revokeObjectURL(source.src);

            viewModel.show();
        };

        // Update preview for collision detection
        const size = this.viewModel.get('previewSize');
        size.width = this.canvas.width;
        size.height = this.canvas.height;

        return this.canvas;
    },

    getImageData: function() {
        // Get Image data from canvas. RenderAsHtmlCanvas is not needed for margins, becuase the canvas will be created in MarginView
        if (!this.canvas) {
            return null;
        }

        const context = this.canvas.getContext('2d');
        return context.getImageData(0, 0, this.canvas.width, this.canvas.height);
    }

});
