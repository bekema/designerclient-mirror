const $ = require('jquery');
const View = require('views/View');
const UrlBoundImage = require('views/components/UrlBoundImage');
const Positioning = require('views/components/Positioning');
const uiConfig = require('publicApi/configuration/ui');
const designContext = require('DesignerContext');

module.exports = View.extend({

    tagName: 'img',

    className: 'dcl-design-area-scene-image',

    initialize: function() {
        //TODO temporary fix, we need to rethink background scene images :/
        if (designContext.embroidery) {
            $(uiConfig.canvas.container).css({ overflow: 'hidden' });
        }
        this.addComponentWithProfile(['default', 'preview'], UrlBoundImage);
        this.addComponentWithProfile(['default', 'preview'], Positioning, { sizeAttribute: 'dimensions', positionAttribute: 'dimensions' });
    }
});
