const $ = require('jquery');
const View = require('views/View');

module.exports = View.extend({

    initialize: function() {
        this.listenTo(this.viewModel, 'change:dimensions', this.render);
    },

    className: 'dimensions-overlay',

    createLine: function(className, top, left, width, height) {
        $('<div>', { 'class': className }) // eslint-disable-line backbone/no-native-jquery
            .css({
                top: top,
                left: left,
                width: width,
                height: height
            })
            .appendTo(this.$el);
    },

    addText: function(className, text, top, left, width) {
        $('<span>', { 'class': className }) // eslint-disable-line backbone/no-native-jquery
            .css({
                top: top,
                left: left,
                width: width
            })
            .text(text)
            .appendTo(this.$el);
    },

    render: function() {
        View.prototype.render.apply(this, arguments);
        this.$el.hide().empty();
        const dimensions = this.viewModel.get('dimensions');

        const displayTextHeight = 16;
        const displayTextWidth = 50;
        const paddingAmount = 5;

        this.addText(
            'dimensions-text text-large',
            this.viewModel.get('heightText'),
            (dimensions.height - displayTextHeight) / 2,
            dimensions.width - (displayTextWidth / 2) + paddingAmount,
            displayTextWidth);
        this.addText('dimensions-text text-large', this.viewModel.get('widthText'), dimensions.height - paddingAmount, (dimensions.width - displayTextWidth) / 2, displayTextWidth);

        /* Horizontal lines */
        this.createLine('dimensions-overlay-line horizontal-left-half', dimensions.height, 0, ((dimensions.width - displayTextWidth) / 2) - paddingAmount, 1);
        this.createLine('dimensions-overlay-line horizontal-right-half', dimensions.height, ((dimensions.width + displayTextWidth) / 2) + paddingAmount, (dimensions.width - displayTextWidth) / 2, 1);

        /* Vertical lines */
        this.createLine('dimensions-overlay-line vertical-top-half', 0, dimensions.width, 1, ((dimensions.height - displayTextHeight) / 2) - paddingAmount);
        this.createLine('dimensions-overlay-line vertical-bottom-half',
            ((dimensions.height + displayTextHeight) / 2) + paddingAmount,
            dimensions.width, 1, (dimensions.height - displayTextHeight) / 2);

        this.$el.show();
        return this;
    }
});
