const View = require('views/View');
const UrlBoundImage = require('views/components/UrlBoundImage');
const Positioning = require('views/components/Positioning');

module.exports = View.extend({

    tagName: 'img',

    className: 'product-overlay',

    initialize: function() {
        this.addComponentWithProfile(['default', 'preview'], UrlBoundImage);
        this.addComponentWithProfile(['default', 'preview'], Positioning, { sizeAttribute: 'dimensions', positionAttribute: 'dimensions' });
    }
});
