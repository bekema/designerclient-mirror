// The `Workspace` is the view for everything. It does not have a view model, so all of its components
// must support not having view models. It's responsible for handling drops that aren't on a particular canvas.
const $ = require('jquery');
const _ = require('underscore');
const View = require('views/View');
const Droppable = require('views/components/Droppable');
const MoveViewModelStrategy = require('views/components/dropping/MoveViewModelStrategy');
const uiConfig = require('publicApi/configuration/ui');
const eventBus = require('EventBus');
const events = eventBus.events;

const WINDOW_RESIZE_DELAY = 50;

const onWindowResize = function() {
    const size = { width: $(window).width(), height: $(window).height() };
    eventBus.trigger(events.windowResize, { size });
};

const onCanvasContainerResize = function() {
    const container = $(uiConfig.canvas.container);
    const size = { width: container.width(), height: container.height() };
    eventBus.trigger(events.canvasContainerResize, { size });
};


module.exports = View.extend({

    el: 'body',

    events: {
        'mousedown': 'onMouseDown',
        'touchstart': 'onTouchStart'
    },

    initialize: function() {
        this.addComponent(Droppable, {
            priority: -1,
            strategies: [new MoveViewModelStrategy()]
        });
        $(window).on('resize', _.debounce(onWindowResize.bind(this), WINDOW_RESIZE_DELAY));
        $(uiConfig.canvas.container).on('resize', _.debounce(onCanvasContainerResize.bind(this), WINDOW_RESIZE_DELAY));
    },

    onMouseDown: function() {
        eventBus.trigger(events.select, []);
    },

    onTouchStart: function() {
        eventBus.trigger(events.select, []);
    }
});
