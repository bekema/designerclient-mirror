const View = require('views/View');
const util = require('util/Util');

module.exports = View.extend({
    initialize: function() {
        this.listenTo(this.viewModel, 'change', this.render);
    },

    render: function() {
        View.prototype.render.apply(this, arguments);

        this.$el.hide().empty();

        const dimensions = this.viewModel.get('dimensions');

        const bleedTop = this.viewModel.get('bleedMarginTop');
        const bleedLeft = this.viewModel.get('bleedMarginLeft');
        const bleedBottom = this.viewModel.get('bleedMarginBottom');
        const bleedRight = this.viewModel.get('bleedMarginRight');
        const safetyTop = this.viewModel.get('safetyMarginTop');
        const safetyLeft = this.viewModel.get('safetyMarginLeft');
        const safetyBottom = this.viewModel.get('safetyMarginBottom');
        const safetyRight = this.viewModel.get('safetyMarginRight');
        const totalTop = this.viewModel.get('totalMarginTop');
        const totalBottom = this.viewModel.get('totalMarginBottom');
        const totalRight = this.viewModel.get('totalMarginRight');

        /* Safety */
        // Left
        util.createLine({
            className: 'dcl-canvas__safety-margin dcl-canvas__safety-margin--left',
            top: totalTop,
            left: bleedLeft,
            width: safetyLeft,
            height: dimensions.height - totalBottom - totalTop,
            el: this.$el
        });
        // Top
        util.createLine({
            className: 'dcl-canvas__safety-margin dcl-canvas__safety-margin--top',
            top: bleedTop,
            left: bleedLeft,
            width: dimensions.width - bleedLeft - bleedRight,
            height: safetyTop,
            el: this.$el
        });
        // Right
        util.createLine({
            className: 'dcl-canvas__safety-margin dcl-canvas__safety-margin--right',
            top: totalTop,
            left: dimensions.width - totalRight,
            width: safetyRight,
            height: dimensions.height - totalBottom - totalTop,
            el: this.$el
        });
        // Bottom
        util.createLine({
            className: 'dcl-canvas__safety-margin dcl-canvas__safety-margin--bottom',
            top: dimensions.height - totalBottom,
            left: bleedLeft,
            width: dimensions.width - bleedLeft - bleedRight,
            height: safetyBottom,
            el: this.$el
        });

        // Inner
        util.createLine({
            className: 'dcl-canvas__safety-margin dcl-canvas__safety-margin--inner',
            top: totalTop,
            left: bleedLeft + safetyLeft,
            width: dimensions.width - bleedLeft - bleedRight - safetyLeft - safetyRight,
            height: dimensions.height - totalBottom - totalTop,
            el: this.$el
        });

        /* Full Bleed */
        // Left
        util.createLine({
            className: 'dcl-canvas__bleed-margin dcl-canvas__bleed-margin--left',
            top: bleedTop,
            left: 0,
            width: bleedLeft,
            height: dimensions.height - bleedTop - bleedBottom,
            el: this.$el
        });
        // Top
        util.createLine({
            className: 'dcl-canvas__bleed-margin dcl-canvas__bleed-margin--top',
            top: 0,
            left: 0,
            width: dimensions.width,
            height: bleedTop,
            el: this.$el
        });
        // Right
        util.createLine({
            className: 'dcl-canvas__bleed-margin dcl-canvas__bleed-margin--right',
            top: bleedTop,
            left: dimensions.width - bleedRight,
            width: bleedRight,
            height: dimensions.height - bleedTop - bleedBottom,
            el: this.$el
        });
        // Bottom
        util.createLine({
            className: 'dcl-canvas__bleed-margin dcl-canvas__bleed-margin--bottom',
            top: dimensions.height - bleedBottom,
            left: 0,
            width: dimensions.width,
            height: bleedBottom,
            el: this.$el
        });

        // Inner
        util.createLine({
            className: 'dcl-canvas__bleed-margin dcl-canvas__bleed-margin--inner',
            top: safetyTop,
            left: safetyLeft,
            width: dimensions.width - bleedLeft - bleedRight,
            height: dimensions.height - totalBottom,
            el: this.$el
        });

        this.$el.show();

        return this;
    }
});
