const _ = require('underscore');
const View = require('views/View');
const ToggleClasses = require('views/components/ToggleClasses');
const Positioning = require('views/components/Positioning');
const CanvasPositioner = require('views/components/CanvasPositioner');
const metaDataProvider = require('MetaDataProvider');
const CanvasHandle = require('views/components/handles/CanvasHandle');
const Handles = require('views/components/handles/Handles');
const globalConfig = require('configuration/Global');
const ShapeCreationView = require('views/components/drawing/ShapeCreationView');
const lng = require('Localization');

module.exports = View.extend({

    initialize: function(options) {
        this.addComponentWithProfile(['default', 'preview'], Positioning, { sizeAttribute: 'size', zIndexAttribute: false });

        this.addComponentWithAllProfiles(Handles);
        this.addComponentWithAllProfiles(ShapeCreationView);
        if (globalConfig.enableCanvasInteraction) {
            this.addComponent(CanvasHandle, options);
        }

        this.addComponent(ToggleClasses, {
            viewModelAttributeToggleableClasses: {
                'enabled': 'dcl-canvas--enabled',
                'active': 'dcl-canvas--active',
                'hidden': 'dcl-canvas--hidden',
                'targetEnabled': 'dcl-canvas--drop-target',
                'dropEnabled': 'dcl-canvas--drop-enabled',
                'itemTargetsDisabled': 'dcl-canvas--targets-disabled'
            }
        });

        this.addComponent(CanvasPositioner, {});

        this.listenTo(this.viewModel, 'change:itemsHidden', this.onItemsHidden);
        // We don't need to bother with removing items, because the item views will remove themselves.

        this.$el.attr('droptarget-text', lng.getText('views.dropTargetText'));

        this.itemViews = {};
    },

    className: 'dcl-canvas',

    render: function() {

        this.setup();

        this.$el.addClass('hidden');

        if (!this.$('.dcl-canvas__underlay').length) {
            this.$el.prepend("<div class='dcl-canvas__underlay dcl-canvas__element-wrapper'></div>");
        }

        if (!this.$('.dcl-items').length) {
            this.$el.append("<div class='dcl-items dcl-canvas__element-wrapper'></div>");
        }

        if (!this.$('.dcl-decorations').length) {
            this.$el.append("<div class='dcl-decorations dcl-canvas__element-wrapper'></div>");
        }

        if (!this.$('.dcl-canvas__overlay').length) {
            this.$el.append("<div class='dcl-canvas__overlay dcl-canvas__element-wrapper'></div>");
        }

        if (!this.$('.dcl-canvas__tools').length) {
            this.$el.append("<div class='dcl-canvas__tools dcl-canvas__element-wrapper tools-overlay'></div>");
        }

        if (this.getTarget) {
            if (!this.$('.dcl-targets').length) {
                this.$el.append(this.getTarget().$el);
            } else {
                this.getTarget().setElement(this.$('.dcl-targets'));
            }
        }

        this.viewModel.itemViewModels.forEach(this.renderItemViewModel, this);

        this.viewModel.get('chromes').forEach(this.renderChrome, this);

        // Add these listeners in render(), rather than initialize, because we don't want to
        // accidentally add something and then call render which will add it again.
        this.listenTo(this.viewModel.itemViewModels, 'remove', this.onViewModelRemoved);
        this.listenTo(this.viewModel.itemViewModels, 'add', this.renderItemViewModel);
        this.listenTo(this.viewModel.chromes, 'add', this.renderChrome);

        // For smoother loading appearance, canvas stays hidden until now.
        this.$el.removeClass('hidden');

        return View.prototype.render.apply(this, arguments);
    },

    renderItemViewModel: function(itemViewModel) {
        const itemView = metaDataProvider.getView(itemViewModel);

        // This is hooked up for contrast validation.
        itemView.parent = this;

        this.itemViews[itemViewModel.id] = itemView;

        this.$('.dcl-items').append(itemView.render().$el);

        // Don't need to render the handles or decorations -- they render when the item view renders.
        if (this.getTarget && itemView.getTarget) {
            this.getTarget().$el.append(itemView.getTarget().$el);
        }

        if (this.getHandle && itemView.getHandle) {
            this.getHandle().$el.append(itemView.getHandle().$el);
        }

        if (itemView.getDecorationElement) {
            this.$('.dcl-decorations').append(itemView.getDecorationElement());
        }

        _.defer(() => itemViewModel.trigger('appendedtodom'));
    },

    onViewModelRemoved: function(itemViewModel) {
        //the view will take care of itself, but we don't want to keep a reference
        delete this.itemViews[itemViewModel.id];
    },

    renderChrome: function(chrome) {
        const chromeView = metaDataProvider.getView(chrome);
        chromeView.render();

        const type = chrome.get('type') || 'underlay';

        this.$(`.dcl-canvas__${type}`).append(chromeView.$el);
    },

    onItemsHidden: function() {
        const cssValue = this.viewModel.get('itemsHidden') ? 'none' : 'block';
        this.$('.dcl-items').css({ display: cssValue });
    }
});
