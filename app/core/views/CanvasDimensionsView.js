const View = require('views/View');

module.exports = View.extend({

    tagName: 'canvas',

    className: 'dcl-canvas-dimensions',

    initialize: function() {
        this.listenTo(this.viewModel, 'change:color change:margin change:enabled change:lineWidth', this.drawDimensions.bind(this));
        this.listenTo(this.viewModel.parent, 'change:size', this.drawDimensions.bind(this));
        this.drawDimensions();
    },

    drawDimensions: function() {
        const ctx = this.el.getContext('2d');

        const widthText = this.viewModel.get('displayableCanvasDimensions').width;
        const heightText = this.viewModel.get('displayableCanvasDimensions').height;
        const width = this.viewModel.parent.get('size').width;
        const height = this.viewModel.parent.get('size').height;
        const color = this.viewModel.get('color');
        const margin = this.viewModel.get('margin');
        const lineWidth = this.viewModel.get('lineWidth');
        const fontMargin = 10;
        const fontHeight = this.viewModel.get('fontHeight');
        const fontWidth = this.viewModel.get('fontWidth');
        //Lines draw centered, so half the width goes into the margin

        const showTop = this.viewModel.get('showTop');
        const showBottom = this.viewModel.get('showBottom');
        const showLeft = this.viewModel.get('showLeft');
        const showRight = this.viewModel.get('showRight');

        const topMargin = -this.viewModel.get('top');
        const leftMargin = -this.viewModel.get('left');

        this.el.width = this.viewModel.get('width');
        this.el.height = this.viewModel.get('height');

        ctx.beginPath();
        ctx.lineWidth = lineWidth;
        ctx.strokeStyle = color;
        if (showTop) {
            const y = fontHeight / 2;
            ctx.strokeText(widthText, leftMargin + (width / 2) - (ctx.measureText(widthText).width / 2), y + (fontHeight / 2) - 1);
            ctx.moveTo(leftMargin, y);
            ctx.lineTo(leftMargin + (width / 2) - (ctx.measureText(widthText).width / 2) - fontMargin, y);
            ctx.moveTo(leftMargin + width, y);
            ctx.lineTo(leftMargin + (width / 2) + (ctx.measureText(widthText).width / 2) + fontMargin, y);
        }
        if (showBottom) {
            const y = topMargin + margin + height;
            ctx.strokeText(widthText, leftMargin + (width / 2) - (ctx.measureText(widthText).width / 2), y + (fontHeight / 2) - 1);
            ctx.moveTo(leftMargin, y);
            ctx.lineTo(leftMargin + (width / 2) - (ctx.measureText(widthText).width / 2) - fontMargin, y);
            ctx.moveTo(leftMargin + width, y);
            ctx.lineTo(leftMargin + (width / 2) + (ctx.measureText(widthText).width / 2) + fontMargin, y);
        }
        if (showLeft) {
            const x = fontWidth / 2;
            ctx.strokeText(heightText, x - ctx.measureText(heightText).width / 2, topMargin + (height / 2) - (fontHeight / 2) + fontMargin - 1);
            ctx.moveTo(x, topMargin);
            ctx.lineTo(x, topMargin + (height / 2) - (fontHeight / 2) - fontMargin);
            ctx.moveTo(x, topMargin + height);
            ctx.lineTo(x, topMargin + (height / 2) + (fontHeight / 2) + fontMargin);
        }
        if (showRight) {
            const x = leftMargin + margin + width;
            ctx.strokeText(heightText, x - ctx.measureText(heightText).width / 2, topMargin + (height / 2) - (fontHeight / 2) + fontMargin - 1);
            ctx.moveTo(x, topMargin);
            ctx.lineTo(x, topMargin + (height / 2) - (fontHeight / 2) - fontMargin);
            ctx.moveTo(x, topMargin + height);
            ctx.lineTo(x, topMargin + (height / 2) + (fontHeight / 2) + fontMargin);
        }
        ctx.stroke();

        this.$el.css({ top: `${this.viewModel.get('top')}px`, left: `${this.viewModel.get('left')}px` });
    }
});
