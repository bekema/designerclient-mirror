const View = require('views/View');

module.exports = View.extend({

    tagName: 'canvas',

    className: 'dcl-canvas-ruler',

    initialize: function() {
        this.listenTo(this.viewModel, 'change:lines change:color change:length', this.drawRuler.bind(this));
        this.listenTo(this.viewModel.parent, 'change:size', this.drawRuler.bind(this));
        this.drawRuler();
    },

    drawRuler: function() {
        const width = this.viewModel.parent.get('size').width;
        const height = this.viewModel.parent.get('size').height;
        const lines = this.viewModel.get('lines');
        const color = this.viewModel.get('color');
        const length = this.viewModel.get('length');
        const lineWidth = this.viewModel.get('lineWidth');
        const margin = this.viewModel.get('margin');

        //This.el is the canvas
        this.el.width = this.viewModel.get('width');
        this.el.height = this.viewModel.get('height');
        const ctx = this.el.getContext('2d');

        ctx.beginPath();
        ctx.lineWidth = lineWidth;
        ctx.strokeStyle = color;

        for (let i = 1; i <= lines; i++) {
            //Drawing left and right sides
            const y = (height * i / (lines + 1)) + length + margin;
            ctx.moveTo(0, y);
            ctx.lineTo(length, y);
            ctx.moveTo(width + length + (margin * 2), y);
            ctx.lineTo(width + (length * 2) + (margin * 2), y);
            //Drawing top and bottom sides
            const x = (width * i / (lines + 1)) + length + margin;
            ctx.moveTo(x, 0);
            ctx.lineTo(x, length);
            ctx.moveTo(x, height + length + (margin * 2));
            ctx.lineTo(x, height + (length * 2) + (margin * 2));
        }

        ctx.stroke();

        this.$el.css({ top: `${this.viewModel.get('top')}px`, left: `${this.viewModel.get('left')}px` });
    }
});
