const $ = require('jquery');
const Component = require('views/components/Component');
const canvasMarginStrategy = require('strategies/CanvasMarginStrategy');
const uiConfig = require('publicApi/configuration/ui');
const eventBus = require('EventBus');
const events = eventBus.events;

module.exports = Component.extend({

    initialize: function() {
        this.strategy = uiConfig.canvas.marginStrategy || canvasMarginStrategy;
        this.listenTo(this.viewModel, 'change:maxChromeDimensions change:size', this.position);
        eventBus.on(events.windowResize, this.position.bind(this));
    },

    position: function() {
        const canvas = this.viewModel;
        const container = $(uiConfig.canvas.container);
        const containerSize = { width: container.width(), height: container.height() };
        const css = this.strategy({ canvas, document: canvas.parent, containerSize });
        this.$el.css(css);
    }
});
