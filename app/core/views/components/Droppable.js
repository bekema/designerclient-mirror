// The `Droppable` component is designed to handle the `drop` event that is caused by `Draggable` components. This
// component should also work if there is no parent view model.
const Component = require('views/components/Component');
const dragAndDrop = require('DragAndDrop');

module.exports = Component.extend({

    defaults: {
        strategies: [],
        priority: 0
    },

    events: {
        'dcldrop': 'onDrop'
    },

    initialize: function() {
        this.listenTo(this.view, 'render', this.onRender);
    },

    onRender: function() {
        const strategies = this.options.strategies;
        this.view.droppable = new dragAndDrop.Droppable(this.view.$el, {
            priority: this.options.priority,
            accepts: draggable => strategies.some(strategy => strategy.accepts(draggable))
        });
    },

    // Helper that will take page-level coordinates and figure out where on this model it maps to.
    getPositionFromPageCoordinates: function(x, y) {
        const droppableOffset = this.view.$el.offset();
        let zoomFactor = this.viewModel.get('zoomFactor');
        if (!zoomFactor && this.viewModel.parent) {
            zoomFactor = this.viewModel.parent.get('zoomFactor');
        }
        return {
            top: (y - droppableOffset.top) / zoomFactor,
            left: (x - droppableOffset.left) / zoomFactor
        };
    },

    // `ui` is optional and will have positional information.
    onDrop: function(e, ui) {
        // If I'm handling this drop, don't let it bubble up any higher.
        e.stopPropagation();
        e.preventDefault();

        const destinationCoordinates = this.getPositionFromPageCoordinates(e.pageX, e.pageY);
        const viewModel = this.viewModel;

        // Break out of the each loop for the first successful strategy
        this.options.strategies.some(strategy => strategy.onDrop(viewModel, ui, destinationCoordinates));
    }
});
