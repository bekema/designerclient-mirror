const $ = require('jquery');
const _ = require('underscore');
const Component = require('views/components/Component');

module.exports = Component.extend({
    initialize: function() {
        if (!_.isFunction(this.view.getDecorationElement)) {
            throw new Error('View must have getDecorationElement support');
        }
        this.listenTo(this.viewModel, 'highlight', this.highlight);
        this.listenTo(this.viewModel, 'unhighlight', this.removeHighlights);

        this.listenTo(this.viewModel, 'drag', this.onDrag);
        this.listenTo(this.viewModel, 'dragstop', this.onDragStop);
    },

    remove: function() {
        delete this.$helper;
        Component.prototype.remove.apply(this, arguments);
    },

    onDrag: function(ui) {
        this.$helper = ui.helper;
    },

    onDragStop: function() {
        delete this.$helper;
    },

    highlight: function(highlightInfo) {
        if (!highlightInfo.id || !highlightInfo.status) {
            throw 'A highlight must specify a unique id and a status';
        } else {
            this.updateHighlights(highlightInfo);
        }
    },

    //get alternate highlight containers (like for dragging or resizing)
    getAlternateHighlightContainers: function() {
        if (this.$helper) {
            return this.$helper.find(`.handle[data-source-element=${this.viewModel.id}]`);
        }

        return null;
    },

    getHighlightElements: function(container, highlight) {
        return container.find(`[data-highlight=${highlight.id}]`);
    },

    getStatusClass: function(highlightInfo) {
        let statusClass = 'highlight-hidden';
        switch (highlightInfo.status) {
            case 'warning':
                statusClass = 'dcl-highlight-warning';
                break;
            case 'error':
                statusClass = 'highlight-error';
                break;
        }

        return `item-highlight ${statusClass}`;
    },

    getItemArea: function() {
        const size = this.viewModel.get('previewSize');

        return {
            top: 0,
            left: 0,
            width: size.width,
            height: size.height
        };
    },

    getHighlightCss: function(area) {
        const size = this.viewModel.get('previewSize');

        const height = area.height || size.height;
        const width = area.width || size.width;
        const top = area.top || 0;
        const left = area.left || 0;

        return {
            width,
            height,
            top,
            left
        };
    },

    updateHighlights: function(highlightInfo) {
        const me = this;
        let $containers, $highlightContainer, $oldHighlightContainer;

        $containers = this.getAlternateHighlightContainers();
        if (!$containers || !$containers.length) {
            $containers = this.view.getDecorationElement();
        }

        const statusClass = me.getStatusClass(highlightInfo);
        this.removeHighlights(highlightInfo);

        $containers.each(function() {
            $oldHighlightContainer = $(this).find('.highlight-container');
            $highlightContainer = $('<div />', {
                'class': 'highlight-container'
            });

            //default to the entire item if there's no areas defined
            const areas = highlightInfo.areas || [me.getItemArea()];
            areas.forEach(function(area) {
                const css = me.getHighlightCss(area);
                const $highlight = $('<div />').css(css).attr({
                    'class': statusClass,
                    'data-highlight': highlightInfo.id
                });

                $highlightContainer.append($highlight);
            });

            if ($oldHighlightContainer.length) {
                $oldHighlightContainer.replaceWith($highlightContainer);
            } else {
                $(this).append($highlightContainer);
            }
        });
    },

    removeHighlights: function(highlightInfo) {
        const me = this;
        const $alternateContainers = this.getAlternateHighlightContainers();

        me.getHighlightElements(this.view.getDecorationElement(), highlightInfo).remove();
        if ($alternateContainers) {
            $alternateContainers.each(function() {
                const $highlights = me.getHighlightElements($(this), highlightInfo);
                $highlights.remove();
            });
        }
    }
});
