// ## Snappable
// This can be used in addition to the `Draggable` component in order to have views snap to the edges of other views.
const $ = require('jquery');
const _ = require('underscore');
const Component = require('views/components/Component');

module.exports = Component.extend({

    defaults: {
        getContainerElement: function() {
            return this.view.$el.parent();
        }
    },

    events: {
        'dragstart': 'onDragStart',
        'drag': 'onDrag',
        'dragstop': 'onDragStop'
    },

    initialize: function() {
        this.guidelines = [];
        this.strategies = this.options.strategies;
    },

    remove: function() {
        this.removeGuidelines();
        Component.prototype.remove.apply(this, arguments);
    },

    onDragStart: function(e, ui) {
        _.invoke(this.strategies, 'start', this.viewModel, e, ui);
        this.guidelineContainerElement = this.options.getContainerElement.apply(this).get(0);
    },

    onDrag: function(e, ui) {
        const delta = {
            left: ui.position.left - ui.originalPosition.left,
            top: ui.position.top - ui.originalPosition.top
        };

        const results = _.invoke(this.strategies, 'getSnapsAndGuidelines', this.viewModel, delta, e, ui);

        let guidelines = [];
        let snappedLeft = false;
        let snappedTop = false;
        _.each(results, function(result) {
            if (result) {
                const snaps = result.snaps;
                if (!snappedLeft && _.isNumber(snaps.left)) {
                    ui.position.left += snaps.left;
                    guidelines = guidelines.concat(result.guidelines.left);
                    snappedLeft = true;
                }
                if (!snappedTop && _.isNumber(snaps.top)) {
                    ui.position.top += snaps.top;
                    guidelines = guidelines.concat(result.guidelines.top);
                    snappedTop = true;
                }
            }
        });
        this.toggleGuidelines(guidelines);
    },

    toggleGuidelines: function(guidelineInfos) {
        let i = 0;
        for (; i < guidelineInfos.length; i++) {
            const guidelineInfo = guidelineInfos[i];
            let guidelineElement = this.guidelines[i];
            if (!guidelineElement) {
                guidelineElement = document.createElement('div');
                guidelineElement.className = 'snap-guideline';
                this.guidelineContainerElement.appendChild(guidelineElement);
                this.guidelines.push(guidelineElement);
            }
            guidelineElement.style.top = guidelineInfo.top + 'px';
            guidelineElement.style.left = guidelineInfo.left + 'px';
            guidelineElement.style.width = guidelineInfo.width + 'px';
            guidelineElement.style.height = guidelineInfo.height + 'px';
            guidelineElement.style.display = '';
        }
        for (; i < this.guidelines.length; i++) {
            this.guidelines[i].style.display = 'none';
        }
    },

    removeGuidelines: function() {
        $('.snap-guideline').remove();
        delete this.guidelineContainerElement;
        this.guidelines = [];
    },

    onDragStop: function(e, ui) {
        if ($('.snap-guideline:visible').length) {
            this.triggerGlobal('analytics:track', this.viewModel, 'MoveSnap');
        }

        _.invoke(this.strategies, 'stop', this.viewModel, e, ui);
        this.removeGuidelines();
    }
});
