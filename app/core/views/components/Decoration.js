const ComponentView = require('views/components/ComponentView');
const Positioning = require('views/components/Positioning');
const ToggleClasses = require('views/components/ToggleClasses');

module.exports = ComponentView.extend({

    className: 'dcl-decoration',

    initialize: function(options) {

        this.listenTo(this.view, 'render', this.render);

        // Publish a method on the view. Anything is free to use it, but they should first check to make sure it's there.
        this.view.getDecorationElement = function() {
            return this.$el;
        }.bind(this);

        this.addComponent(Positioning, { positionAttribute: options.positionAttribute || 'handlePosition', sizeAttribute: options.sizeAttribute || 'handleSize' });

        this.listenTo(this.viewModel, 'drag resize', this.onAction);
        this.listenTo(this.viewModel, 'rotate', this.onRotate);
        this.listenTo(this.viewModel, 'dragstop resizestop', this.onActionStop);

        this.addComponent(ToggleClasses, {
            // This is a collection of view model attributes -> class names, where
            // the class names will be toggled on and off when the view model attributes change. If any
            // of the attributes use the same class name, then that class name will be added if any of the
            // attributes are true.
            viewModelAttributeToggleableClasses: {
                'resizable': 'dcl-ui-resizable',
                'errorOverlap': 'dcl-outline-warning',
                'OutsideBounds': 'dcl-outline-warning',
                'errorOverlayOverlap': 'dcl-outline-warning',
                'editing': 'dcl-editing',
                'imageresolution-warning': 'dcl-outline-warning',
                'imageresolution-error': 'dcl-outline-error',
                'selected': 'dcl-selected',
                'dragging': 'dcl-dragging',
                'over': 'dcl-over'
            }
        });
    },

    render: function() {
        this.$el.attr('id', `${this.viewModel.id}-decoration`);
        return ComponentView.prototype.render.apply(this, arguments);
    },

    onAction: function(e) {
        // Direct DOM access for performance, since this is called on every drag and resize.
        this.el.style.left = `${e.handlePosition.left}px`;
        this.el.style.top = `${e.handlePosition.top}px`;
        if (e.handleSize) {
            this.el.style.width = `${e.handleSize.width}px`;
            this.el.style.height = `${e.handleSize.height}px`;
        }
    },

    onRotate: function(e) {
        this.el.style.transform = `rotate(-${e.rotation}deg)`;
    },

    onActionStop: function() {
        const handlePosition = this.viewModel.get('handlePosition');
        this.el.style.left = `${handlePosition.left}px`;
        this.el.style.top = `${handlePosition.top}px`;

        const handleSize = this.viewModel.get('handleSize');
        this.el.style.width = `${handleSize.width}px`;
        this.el.style.height = `${handleSize.height}px`;
    }
});
