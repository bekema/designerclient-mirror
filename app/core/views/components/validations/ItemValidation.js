const $ = require('jquery');
const _ = require('underscore');
const Component = require('views/components/Component');

module.exports = Component.extend({
    // Override these properties in subclasses as needed
    defaults: {
        validateOnDrag: true,
        validateOnRender: true,
        validateOnChangePreviewDimensions: true,
        positionAttribute: 'previewBoundingBox', // what attribute (on the view model) to look at for the image position
        sizeAttribute: 'previewBoundingBox' // what attribute (on the view model) to look at for the image size
    },

    initialize: function(options) {
        this.positionAttribute = options.positionAttribute;
        this.sizeAttribute = options.sizeAttribute;

        this.listenTo(this.viewModel, 'updateAllValidations', this.updateValidation);
        if (this.options.validateOnRender) {
            this.listenTo(this.view, 'render', this.updateValidation);
        }

        if (options.validateOnChangePreviewDimensions) {
            const changePreviewDimensionsEvent = `change:${this.positionAttribute} change:${this.sizeAttribute}`;
            this.listenTo(this.viewModel, changePreviewDimensionsEvent, this.updateValidation);
        }

        if (options.validateOnDrag) {
            this.listenTo(this.viewModel, 'drag', this.onDrag);
        } else {
            this.listenTo(this.viewModel, 'drag', this.hideValidation);
        }
    },

    onDrag: function(dragInfo) {
        const delta = this.getDimensions(dragInfo.delta);
        const box = _.extend({}, dragInfo.originalBoundingBox);
        const dimensions = {
            left: box.left + delta.left,
            top: box.top + delta.top,
            width: box.width,
            height: box.height
        };
        if (this.isOutsideParent(dimensions)) {
            this.hideValidation();
        } else {
            const validation = this.updateValidation(dimensions, dragInfo.handlePosition);
            if (validation.successful) {
                this.hideValidation();
            }
        }
    },

    updateValidation: function() {
        // Override in sub class

        // This method should return a validation result object like this:
        /*
        return {
            status: "warning", // or "error" or "success"
        }
        */

        return { severity: 'success' };
    },

    hideValidation: $.noop,

    //This needs to take into consideration drag and resize, so we can't use existing isOutsideParent functionality
    isOutsideParent: function(dimensions) {
        const parent = this.viewModel.parent;
        if (parent) {
            const parentDimensions = parent.get('size');
            return dimensions.left + dimensions.width <= 0 || dimensions.top + dimensions.height <= 0 || dimensions.left >= parentDimensions.width || dimensions.top >= parentDimensions.height;
        } else {
            return false;
        }
    },

    getDimensions: function(proxyDimensions) {
        const position = this.viewModel.get(this.positionAttribute) || this.viewModel.get('position');
        const size = this.viewModel.get(this.sizeAttribute) || this.viewModel.get('size');
        const defaultDimensions = {
            left: position.left,
            top: position.top,
            width: size.width,
            height: size.height
        };

        return $.extend(defaultDimensions, proxyDimensions);
    }
});
