const _ = require('underscore');
const ItemValidation = require('views/components/validations/ItemValidation');
const validationProvider = require('validations/ValidationProvider');

const VALIDATION_NAME = 'OutsideBounds';

const setHighlight = function(view, isOverlapping, overlapAttr) {
    if (isOverlapping) {
        view.viewModel.set(overlapAttr, true);
    } else {
        view.viewModel.unset(overlapAttr);
    }
};

module.exports = ItemValidation.extend({

    initialize: function(options) {
        ItemValidation.prototype.initialize.apply(this, arguments);
        this.margin = options.margin;
    },

    hideValidation: function() {
        this.viewModel.trigger('unhighlight', { id: VALIDATION_NAME });
    },

    toggleValidation: function(isOverlapping) {
        setHighlight(this.view, isOverlapping, VALIDATION_NAME);
    },

    updateValidation: function(dimensions) {
        dimensions = _.extend({}, this.viewModel.get('previewBoundingBox'), dimensions);
        const validation = validationProvider.outsideBounds.validate(this.viewModel, dimensions, this.margin);
        this.toggleValidation(!validation.successful);

        return validation;

    }
});
