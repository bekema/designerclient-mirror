const _ = require('underscore');
const ItemValidation = require('views/components/validations/ItemValidation');
const documentRepository = require('DocumentRepository');
const validationProvider = require('validations/ValidationProvider');

module.exports = ItemValidation.extend({
    defaults: _.extend({}, ItemValidation.prototype.defaults, {
        validateOnRender: false,
        validateOnDrag: false,
        validateOnChangePreviewDimensions: false,
        previousValidationResult: {}
    }),

    initialize: function() {
        ItemValidation.prototype.initialize.apply(this, arguments);
        this.listenTo(this.viewModel, 'change:previewSize', this.updateValidation);
        this.listenTo(this.viewModel.model, 'change:naturalDimensions change:crop', this.updateValidation);
    },

    setHighlight: function(status) {
        if (this.viewModel.get('imageresolution-warning') && status === 'error' ||
            this.viewModel.get('imageresolution-error') && status === 'warning') {
            this.hideValidation();
        }

        this.viewModel.set(`imageresolution-${status}`, true);
    },

    hideValidation: function() {
        this.viewModel.unset('imageresolution-warning');
        this.viewModel.unset('imageresolution-error');
    },

    // don't fire validation if the result has not changed
    shouldSuppressUnchangedValidationStatus: function(result) {
        if (result.successful && this.options.previousValidationResult.successful) {
            return true;
        } else {
            return false;
        }
    },

    updateValidation: function() {
        if (!documentRepository.getActiveCanvasViewModel()) {
            return false;
        }

        const validation = validationProvider.imageResolution.validate(this.viewModel);

        if (this.shouldSuppressUnchangedValidationStatus(validation)) {
            return false;
        }

        this.options.previousValidationResult = validation;

        if (validation.successful) {
            this.hideValidation();
        } else if (validation.severity === 'warning') {
            this.setHighlight('warning');
        } else if (validation.severity === 'error') {
            this.setHighlight('error');
        }

        return validation;
    }
});
