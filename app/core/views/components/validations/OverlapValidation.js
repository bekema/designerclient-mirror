// `OverlapValidation` is a component that can be added to a view in order to have it perform pixel by pixel comparisons
// with other views with the same component when it is dragged or repositioned. Views that do have an overlap will be flagged as such.
const _ = require('underscore');
const ItemValidation = require('views/components/validations/ItemValidation');
const validationManager = require('validations/ValidationManager');
const validationProvider = require('validations/ValidationProvider');

/* eslint-disable no-magic-numbers */

// This is a registry of all of the views that have this component. This makes it possible for one instance of the component
// to compare itself against other instances. It is keyed by view model id.
const viewRegistry = {};
// A cache of all of the existing overlaps (keyed by view model id).
const existingOverlapsCache = {};

const VALIDATION_NAME = 'errorOverlap';

const setHighlight = function(view, isOverlapping) {
    if (isOverlapping) {
        view.viewModel.set(VALIDATION_NAME, true);
    } else {
        view.viewModel.unset(VALIDATION_NAME);
    }
};

module.exports = ItemValidation.extend({

    initialize: function() {
        ItemValidation.prototype.initialize.apply(this, arguments);
        if (!_.isFunction(this.view.getImageData)) {
            throw new Error('View must have getImageData support');
        }

        if (!_.isFunction(this.view.getDecorationElement)) {
            throw new Error('View must have getDecorationElement support');
        }

        // Register this component's view so that all the other views with `OverlapValidation` can interact with it.
        viewRegistry[this.viewModel.id] = this.view;
        this.listenTo(this.viewModel, 'htmlcanvas:updated', this.updateValidation);
    },

    // `remove` is the most important function in a component. When a model is deleted, this will get called, and
    // every conceivable reference that this component has to that model must be removed, or the model will leak.
    remove: function() {
        delete viewRegistry[this.viewModel.id];
        delete existingOverlapsCache[this.viewModel.id];

        _.each(viewRegistry, (view) => {
            const overlaps = existingOverlapsCache[view.viewModel.id];
            if (_.contains(overlaps, this.viewModel.id)) {
                existingOverlapsCache[view.viewModel.id] = _.without(overlaps, this.viewModel.id);
                if (existingOverlapsCache[view.viewModel.id].length < 1) {
                    const validationComponent = validationManager.getValidation({ id: view.viewModel.id, type: 'itemViewModel', name: 'Overlap' });
                    if (validationComponent) {
                        validationComponent.hideValidation();
                    }
                }
            }
        });

        ItemValidation.prototype.remove.apply(this, arguments);
    },

    updateOverlaps: function(view, overlappedViews) {
        // Views that currently overlap with this view
        const currentOverlapIds = _.map(overlappedViews, function(overlappedView) {
            return overlappedView.viewModel.id;
        });

        // Views that previously overlapped with this view
        const previousOverlapIds = existingOverlapsCache[view.viewModel.id];

        if (previousOverlapIds && previousOverlapIds.length > 0) {
            // Views that no longer overlap with this view, but previously did
            const differences = _.difference(previousOverlapIds, currentOverlapIds);

            // Remove this view from their overlap lists
            differences.forEach((previouslyOverlapping) => {
                let otherOverlaps = existingOverlapsCache[previouslyOverlapping];
                otherOverlaps = _.without(otherOverlaps, view.viewModel.id);
                existingOverlapsCache[previouslyOverlapping] = otherOverlaps;

                // If this view was the only thing in the other view's overlap list,
                // the other view no longer has an overlap validation error
                if (!otherOverlaps.length) {
                    const validationComponent = this.getValidationComponentByViewModelId(previouslyOverlapping);
                    if (validationComponent) {
                        validationComponent.hideValidation();
                    }
                }
            });
        }

        existingOverlapsCache[view.viewModel.id] = currentOverlapIds;

        // Add this view to the overlap lists of each view it overlaps with
        currentOverlapIds.forEach((currentOverlapId) => {
            if (!existingOverlapsCache[currentOverlapId]) {
                existingOverlapsCache[currentOverlapId] = [];
            }

            if (!_.contains(existingOverlapsCache[currentOverlapId], view.viewModel.id)) {
                existingOverlapsCache[currentOverlapId].push(view.viewModel.id);
            }
        });
    },

    hideValidation: function() {
        this.toggleValidation(false);
    },

    toggleValidation: function(isOverlapping) {
        setHighlight(this.view, isOverlapping);
    },

    updateValidation: function(dimensions) {
        const draggedViewModels = this.dragInfo ? this.dragInfo.viewModels : null;
        const validation = validationProvider.overlap.validate(this.view, viewRegistry, dimensions, draggedViewModels);
        const overlaps = !validation.successful;
        this.updateOverlaps(this.view, overlaps);
        this.toggleValidation(overlaps);
        return validation;
    }
});
