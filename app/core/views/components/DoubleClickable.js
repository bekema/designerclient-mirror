// This component makes it possible to double click an item to fire an event
const Component = require('views/components/Component');
const eventBus = require('EventBus');

module.exports = Component.extend({
    events: {
        'dblclick': 'onDoubleClick'
    },

    initialize: function(options) {
        this.event = options.event;
    },

    onDoubleClick: function() {
        if (!this.viewModel.get('selectable')) {
            return;
        }
        eventBus.trigger(this.event, this.viewModel);
    }
});
