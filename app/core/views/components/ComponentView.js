// A `ComponentView` should be used as a component of a View that has its own element. The canonical
// example of this is the `Target`.
const _ = require('underscore');
const View = require('views/View');

module.exports = View.extend({
    /* eslint backbone/no-constructor: 0 */
    constructor: function(view, options) {
        this.view = view;
        this.viewModel = view.viewModel;
        this.options = _.extend({}, this.defaults, options);
        View.apply(this, Array.prototype.slice.call(arguments, 1));
    }

});
