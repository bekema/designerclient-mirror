// The `Resizable` component should be used to resize items and sometimes move them around on the canvas. It uses the `SelectionManager` in
// order to resize / move all selected items at once.
const $ = require('jquery');
const commandDispatcher = require('CommandDispatcher');
const Component = require('views/components/Component');
const selectionManager = require('SelectionManager');
const resize = require('Resize');
const rebindOptionsMethods = require('views/utilities/rebindOptionsMethods');
const eventBus = require('EventBus');
const events = eventBus.events;

const viewModelRegistry = {};

module.exports = Component.extend({

    defaults: {
        resizeCommandType: 'resize', // Which resize command to call
        maintainProportions: false,
        getContainerElement: function() {
            return this.$el.parent();
        },
        getResizableElementClones: function() {
            return this.$el.clone();
        },
        getResizableElementOriginals: function() {
            return this.$el;
        },
        handles: function() {
            return this.viewModel.get('handles');
        },
        originalAspectRatio: function() {
            // Use the true aspect ratio so that if you resize down to the min size and then back up the aspect ratio won't have
            // any changes due to rounding.
            return this.viewModel.model.get('width') / this.viewModel.model.get('height');
        }
    },

    initialize: function() {
        this.listenTo(this.viewModel, 'change:handles', this.updateHandles);
        this.listenTo(this.view, 'render', this.onRender);
        this.listenToGlobal('cancelaction', this.onCancel);
        this.cancelled = false;
        viewModelRegistry[this.viewModel.id] = this;

        rebindOptionsMethods(this);
    },

    remove: function() {
        Component.prototype.remove.apply(this, arguments);
        delete viewModelRegistry[this.viewModel.id];
    },

    updateHandles: function() {
        // Don't need to do any DOM changes if it's not selected, since that means the handles aren't showing.
        if (this.view.resizable && this.viewModel.get('selected')) {
            this.view.resizable.handles(this.options.handles);
        }
    },

    onCancel: function() {
        if (!this.cancelled) {
            this.cancelled = true;
            $(document).mouseup();
        }
    },

    onRender: function() {
        this.view.resizable = new resize.Resizable(
            this.$el, {
                handles: this.options.handles,
                helper: this.createResizableHelperLayer.bind(this),
                appendTo: this.options.getContainerElement,
                originalAspectRatio: this.options.originalAspectRatio,
                maintainProportions: this.options.maintainProportions,
                minWidth: this.options.minWidth,
                minHeight: this.options.minHeight,
                start: function() {
                    // Can't resize something that isn't selected.
                    if (!this.viewModel.get('resizable') || !this.viewModel.get('selected')) {
                        return false;
                    }

                    // Don't allow resizing more than one image at a time.
                    if (selectionManager.length > 1) {
                        return false;
                    }

                    // Don't allow resizing if a selected item is either not registered as resizable or if it belongs to a different parent.
                    if (selectionManager.find(function(vm) {
                        return viewModelRegistry[vm.id] && vm.parent === this.viewModel;
                    }, this)) {
                        return false;
                    }

                    const viewModels = selectionManager.toArray();

                    // Since this.$el is the resizable element, other components that interact with it may
                    // want to get access to the resize view models.
                    const rotation = viewModels[0].model.get('rotation');
                    this.$el.data({ viewModels, rotation });
                    this.currentViewModels = viewModels;

                    // Hide all of the resizable elements, since they"re being cloned and added to the helper layer
                    selectionManager.forEach(function(vm) {
                        viewModelRegistry[vm.id].options.getResizableElementOriginals().css('visibility', 'hidden');
                        vm.trigger('resizestart');
                    }, this);

                    eventBus.trigger(events.resizeStart, this.currentViewModels);

                    this.cancelled = false;
                }.bind(this),

                resize: this.onResize.bind(this),

                stop: this.onStop.bind(this)
            });
    },

    createResizableHelperLayer: function() {
        // Calculate position with clientTop and clientLeft to account for borders, margins, padding
        const $layer = $("<div class='dcl-resizable-layer dcl-canvas__element-wrapper'></div>").css({
            top: 0,
            left: 0
        });

        // Attach any selected views to our layer so that we resize.
        selectionManager.forEach(function(vm) {
            // Create a new view based off the existing view
            const component = viewModelRegistry[vm.id];

            // Get original position of resizable element
            const clones = component.options.getResizableElementClones();

            clones.css('visibility', 'visible');
            $layer.css({ zIndex: vm.model.get('zIndex') });
            $layer.append(clones);

        }, this);

        return $layer[0];
    },

    onResize: function(e, ui) {
        if (this.cancelled) {
            return false;
        }

        const resizeResults = [];

        this.currentViewModels.forEach(function(vm) {
            const previewPosition = vm.get('previewPosition');
            const handlePosition = vm.get('handlePosition');
            const previewSize = vm.get('previewSize');
            const handleSize = vm.get('handleSize');

            const delta = {
                left: ui.position.left - ui.originalPosition.left,
                top: ui.position.top - ui.originalPosition.top,
                width: ui.size.width - ui.originalSize.width,
                height: ui.size.height - ui.originalSize.height
            };

            const resizePreviewPosition = {
                left: previewPosition.left + delta.left,
                top: previewPosition.top + delta.top
            };

            const resizePreviewSize = {
                width: previewSize.width + delta.width,
                height: previewSize.height + delta.height
            };

            const resizeHandlePosition = {
                left: handlePosition.left + delta.left,
                top: handlePosition.top + delta.top
            };

            const resizeHandleSize = {
                width: handleSize.width + delta.width,
                height: handleSize.height + delta.height
            };

            const resizeInfo = {
                helper: ui.helper,
                originalPosition: previewPosition,
                previewPosition: resizePreviewPosition,
                handlePosition: resizeHandlePosition,
                previewSize: resizePreviewSize,
                handleSize: resizeHandleSize,
                delta: delta
            };

            resizeResults.push(resizeInfo);

            vm.trigger('resize', resizeInfo);
        }.bind(this));

        return resizeResults;
    },

    // `ui` is optional and will have positional information.
    onStop: function(e, ui) {
        const zoomFactor = this.viewModel.parent.get('zoomFactor');
        const eventData = {
            viewModels: this.currentViewModels,
            delta: {
                top: (ui.position.top - ui.originalPosition.top) / zoomFactor,
                left: (ui.position.left - ui.originalPosition.left) / zoomFactor,
                width: (ui.size.width - ui.originalSize.width) / zoomFactor,
                height: (ui.size.height - ui.originalSize.height) / zoomFactor
            }
        };

        if (!this.cancelled) {
            commandDispatcher.trigger(this.options.resizeCommandType, eventData);
        }

        this.currentViewModels.forEach(function(vm) {
            viewModelRegistry[vm.id].options.getResizableElementOriginals().css('visibility', 'visible');
            vm.trigger('resizestop');
        }.bind(this));

        eventBus.trigger(events.resizeStop, this.currentViewModels);
        this.$el.removeData('viewModels');
        delete this.currentViewModels;
    }
});
