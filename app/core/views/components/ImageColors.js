const Component = require('views/components/Component');
const util = require('util/Util');
const colorManager = require('ColorManager');
const colorConverter = require('util/ColorConverter');
const colorConfig = require('publicApi/configuration/ui').colors.colorRetrieval;

/* eslint-disable no-magic-numbers */

module.exports = Component.extend({

    initialize: function() {
        this.listenTo(this.viewModel, `loadimage`, this.setDominantImageColors);
    },

    setDominantImageColors: function() {
        const imageUri = this.viewModel.get('url');
        const hexColors = [];

        if (colorConfig.colorCount > 10) {
            // to optimize speed, we set the colorCount to 10 even when the config is higher
            colorConfig.colorCount = 10;
        }

        util.getImageFromUrl(imageUri)
            .then(image => colorManager.getColorPalette(image, colorConfig.colorCount, colorConfig.colorQuality))
            .then(colorPalette => {
                colorPalette.forEach(color => {
                    const rgb = {
                        r: color[0],
                        g: color[1],
                        b: color[2]
                    };
                    hexColors.push(colorConverter.rgb2hex(rgb));
                });

                this.viewModel.set({ colors: hexColors });
            });
    }
});