const $ = require('jquery');
const Component = require('views/components/Component');
const defaultInProgressTemplate = require('ui/widgets/templates/upload/UploadingImage.tmp');
const localization = require('Localization');

module.exports = Component.extend({

    initialize: function() {
        this.listenTo(this.viewModel, 'change:percentComplete', this.updatePercent, this);
        this.listenTo(this.viewModel, 'change:state', this.updateState, this);
        this.listenTo(this.viewModel, 'change:url model:change:digitizedId', this.updateImage, this);
        this.listenTo(this.viewModel, 'change:previewPosition change:previewSize change:handlePosition change:handleSize', this.styleLoader, this);
        this.listenTo(this.viewModel, 'destroy', this.removeLoadingSpinner, this);

        this.loader = defaultInProgressTemplate({
            percentComplete: 0,
            message: localization.getText('imageProcessingSteps.imagePreparation.message')
        });

        this.$loader = $(this.loader);
    },

    events: {
        'load': 'onLoadImage'
    },

    updatePercent: function() {
        const percentComplete = this.viewModel.get('percentComplete');
        this.$loader.find('.dcl-uploading-percentage').text(Math.round(percentComplete) + '%');
        this.updatePathLoading(percentComplete);
    },

    updatePathLoading: function(percentComplete) {
        this.$loader.find('.dcl-path-loading').attr('stroke-dashoffset', this.calcDashOffset(percentComplete));
    },

    calcDashOffset: function(val) {
        const completePercentage = 100;
        const r = this.$loader.find('.dcl-path-loading').attr('r');
        const c = Math.PI * (r * 2);

        if (val < 0) { val = 0; }
        if (val > completePercentage) { val = completePercentage; }

        const dashOffsetValue = ((completePercentage - val) / completePercentage) * c;
        return dashOffsetValue;
    },

    styleLoader: function() {
        const position = this.viewModel.get('previewBoundingBox');
        if (!position) { return; }

        const attributes = {
            top: position.top,
            left: position.left,
            width: position.width,
            height: position.height,
            zIndex: this.$el.css('z-index') + 1
        };

        this.$loader.css(attributes);
    },

    updateState() {
        this.$loader.find('.dcl-image-process-message').text(this.viewModel.get('state'));
    },

    appendLoadingSpinner: function() {
        if (this.$el.parent().find('.dcl-thumb').length === 1) { return; }

        this.$el.parent().prepend(this.$loader);
        this.styleLoader();
    },

    removeLoadingSpinner: function() {
        this.$loader.remove();
    },

    updateImage: function() {
        // If digitization is not complete we need to indicate that to the preview.
        if (this.viewModel.get('digitizationNeeded')) {
            this.appendLoadingSpinner();
            this.$el.addClass('dcl-image-processing');
        } else {
            this.removeLoadingSpinner();
            this.$el.removeClass('dcl-image-processing');
        }
    },

    onLoadImage: function() {
        this.updateImage();
    }
});
