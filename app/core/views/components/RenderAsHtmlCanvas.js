const Component = require('views/components/Component');
const util = require('util/Util');

/* eslint-disable no-magic-numbers */

const canvases = {};

const getImageData = function() {
    const canvas = canvases[this.viewModel.id];

    if (!canvas || canvas.height === 0 || canvas.width === 0) {
        return null;
    }

    const context = canvas.getContext('2d');
    return context.getImageData(0, 0, canvas.width, canvas.height);
};

const getHtmlCanvas = function() {
    return canvases[this.viewModel.id] || null;
};

module.exports = Component.extend({

    initialize: function() {
        this.updateCanvas();
        this.listenTo(this.viewModel, `change:previewBoundingBox`, this.updateCanvas);

        this.view.getImageData = getImageData.bind(this);
        this.view.getHtmlCanvas = getHtmlCanvas.bind(this);
    },

    remove: function() {
        Component.prototype.remove.apply(this, arguments);
        delete canvases[this.viewModel.id];
    },

    updateCanvas: function() {
        const img = this.view.el;
        img.crossOrigin = 'Anonymous';
        const canvasSize = this.viewModel.get('previewBoundingBox');
        const size = this.viewModel.get('previewSize');

        if (size.width > 0 && size.height > 0 && img.naturalWidth > 0 && img.naturalHeight > 0) {
            const rotation = this.viewModel.model.get('rotation') || 0;
            const canvas = document.createElement('canvas');
            canvas.height = Math.round(canvasSize.height);
            canvas.width = Math.round(canvasSize.width);
            const context = canvas.getContext('2d');
            context.translate(Math.round(canvasSize.width) / 2, Math.round(canvasSize.height) / 2);
            context.rotate(util.degreesToRadians(360 - rotation));
            context.drawImage(img, -(size.width / 2), -(size.height / 2), size.width, size.height);

            canvases[this.viewModel.id] = canvas;

            this.viewModel.trigger('htmlcanvas:populated');
        }

        this.viewModel.trigger('htmlcanvas:updated');
    }
});
