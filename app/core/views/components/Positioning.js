// The `Positioning` component is about as simple as a component can be. It's just responsible for
// making sure that views are always positioned and sized correctly. Typically, the views should
// be positioned absolutely, but that should be accomplished via css. This can be applied to views that
// only have a size (like the `CanvasView`), or that only need a position (like the `TextFieldView`).
const Component = require('views/components/Component');

module.exports = Component.extend({

    defaults: {
        // Override these to be `false` if you do not want them to be set.
        sizeAttribute: 'previewSize',
        positionAttribute: 'previewPosition',
        zIndexAttribute: 'zIndex',
        rotationAttribute: 'rotation',
        transformOriginAttribute: false
    },

    // Components are initialized as part of view initialization. This should not contain any UI setup, but
    // is the appropriate spot to add event handlers to the view or view model that this component is operating.
    // should not contain any UI setup, but should contain all the event handling.
    // Components will automatically have `this.view`, `this.viewModel`, and `this.options` set by the time
    // this is called.
    initialize: function() {
        // `this.options` will use the values from `defaults`, unless the view overrides them when creating the component.
        if (this.options.positionAttribute) {
            // It is almost always better to use `listenTo` instead of `on`, because `stopListening` is called
            // automatically on `remove`, so you don't have to worry about causing any memory leaks by forgetting
            // event handlers.
            this.listenTo(this.viewModel, `change:${this.options.positionAttribute}`, this.updatePosition);
        }

        if (this.options.sizeAttribute) {
            this.listenTo(this.viewModel, `change:${this.options.sizeAttribute}`, this.updateSize);
        }

        if (this.options.zIndexAttribute) {
            this.listenTo(this.viewModel.model, `change:${this.options.zIndexAttribute}`, this.updateZIndex);
        }

        if (this.options.rotationAttribute) {
            this.listenTo(this.viewModel.model, `change:${this.options.rotationAttribute}`, this.updateRotation);

        }

        if (this.options.transformOriginAttribute) {
            this.listenTo(this.viewModel, `change:${this.options.transformOriginAttribute}`, this.updateTransformOrigin);
        }

        this.listenTo(this.view, 'render', this.onRender);
    },

    // Components are modules that modify a view's element. Since they do not render anything on their own,
    // they don't use `render()`. Rather, they bind to the `render` event triggered by their view.
    onRender: function() {
        this.updatePosition();
        this.updateSize();
        this.updateZIndex();
        this.updateRotation();
    },

    updateSize: function() {
        const size = this.viewModel.get(this.options.sizeAttribute);
        if (size) {
            // `this.$el` is set automatically to match `this.view.$el`
            this.$el.css(size);
        }
    },

    updateZIndex: function() {
        const zIndex = this.viewModel.model.get(this.options.zIndexAttribute);
        if (zIndex) {
            this.$el.css('z-index', zIndex);
        }
    },

    updatePosition: function() {
        const position = this.viewModel.get(this.options.positionAttribute);
        if (position) {
            this.$el.css(position);
        }
    },

    updateRotation: function() {
        const rotation = this.viewModel.model.get(this.options.rotationAttribute);
        if (rotation || rotation === 0) {
            this.$el.css('transform', `rotate(-${rotation}deg)`);
        }

        if (this.options.transformOriginAttribute) {
            this.updateTransformOrigin();
        }
    },

    updateTransformOrigin: function() {
        const origin = this.viewModel.get(this.options.transformOriginAttribute);
        if (origin) {
            this.$el.css('transform-origin', origin);
        }
    }
});
