const $ = require('jquery');
const Snappable = require('views/components/Snappable');
const Target = require('views/components/targets/Target');
const TextSnappingStrategy = require('views/components/snapping/TextStrategy');
const VerticalAlignmentStrategy = require('views/components/snapping/VerticalAlignmentStrategy');

module.exports = Target.extend({

    initialize: function(options) {
        Target.prototype.initialize.call(this, options);

        this.viewModel.on('change:previewSize change:relativePreviewPosition change:selected', this.sizePreviewTarget, this);

        this.addComponent(Snappable, {
            getContainerElement: this.getDecorationsContainerElement,
            strategies: [new TextSnappingStrategy(), new VerticalAlignmentStrategy()]
        });

        this.$target = $('<div></div>', { 'class': 'dcl-target__text-target', 'unselectable': 'on' });
    },

    render: function() {
        this.$el.addClass('dcl-target--text');
        this.$el.append(this.$target);
        return Target.prototype.render.apply(this, arguments);
    },

    sizePreviewTarget: function() {
        if (this.viewModel.get('selected')) {
            this.$target.css({
                width: '100%',
                height: '100%',
                left: 0,
                top: 0
            });
        } else {
            const size = this.viewModel.get('previewSize');
            const pos = this.viewModel.get('relativePreviewPosition');

            this.$target.css({
                width: size.width,
                height: size.height,
                left: pos.left,
                top: pos.top
            });
        }
    }
});
