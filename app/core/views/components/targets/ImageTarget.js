const ImageSnappingStrategy = require('views/components/snapping/ImageStrategy');
const Snappable = require('views/components/Snappable');
const Target = require('views/components/targets/Target');

module.exports = Target.extend({

    initialize: function() {
        Target.prototype.initialize.apply(this, arguments);

        this.addComponent(Snappable, {
            getContainerElement: this.getDecorationsContainerElement,
            strategies: [new ImageSnappingStrategy()]
        });
    }
});
