// The `Target` is what the user actually interacts with. We need it because some items aren't clickable,
// yet are on top of other items, so just doing regular event delegation wouldn't work. The example of this
// is a full bleed branding image overlayed on top of the entire document. We also need it because sometimes the
// target has different dimensions than the image - e.g. text fields with overhang.
const BoundingBoxUpdater = require('views/components/BoundingBoxUpdater');
const ComponentView = require('views/components/ComponentView');
const DoubleClickable = require('views/components/DoubleClickable');
const Draggable = require('views/components/Draggable');
const itemConfig = require('publicApi/configuration/core/items');
const OutsideBoundsValidation = require('views/components/validations/OutsideBoundsValidation');
const Positioning = require('views/components/Positioning');
const Selectable = require('views/components/Selectable');
const ToggleClasses = require('views/components/ToggleClasses');
const validationConfig = require('publicApi/configuration/validations');
const validationProvider = require('validations/ValidationProvider');

module.exports = ComponentView.extend({

    defaults: {
        containerSelector: '.dcl-canvas'
    },

    className: 'dcl-target',

    initialize: function(options) {
        const me = this;

        this.listenTo(this.view, 'render', this.render);

        // These functions are defined inline because we want them to be properly scoped to "this" handle. Since
        // they're going to be used by components of this handle, that wouldn't happen by default.
        this.view.getTarget = function() {
            return me;
        };

        this.getHandleArea = function() {
            return me.$el;
        };

        this.getDraggableElements = function() {
            return me.$el;
        };

        this.getContainerElement = function() {
            return me.$el.closest(me.options.containerSelector);
        };

        this.getItemsContainerElement = function() {
            return me.$el.closest(me.options.containerSelector).find('.dcl-items');
        };

        this.getDecorationsContainerElement = function() {
            return me.$el.closest(me.options.containerSelector).find('.dcl-decorations');
        };

        this.getVisibleElementOriginals = function() {
            return me.view.$el.add(me.$el);
        };

        // The actual cloned img src hasn't necessarily been updated if we've been performing actions very quickly,
        // because the src doesn't get updated until the XHR returns. So components that are showing clones need to
        // use this method instead of doing it themselves. This particular method should be used by resizable, which
        // wants to display both the handle and the original.
        this.getVisibleElementClones = function() {
            const clone = me.view.$el.clone();
            return clone.add(me.$el.clone());
        };

        this.getViewElementClone = function() {
            const clone = me.view.$el.clone();
            return clone;
        };

        this.addComponent(Positioning, { positionAttribute: 'handlePosition', sizeAttribute: 'handleSize' });
        this.addComponent(Selectable, { getFocusableElement: this.getHandleArea });

        // Updates the bounding box based on the DOM
        this.addComponent(BoundingBoxUpdater, {
            viewModelEvents: 'change:handlePosition change:handleSize change:transformOrigin appendedtodom',
            setAttribute: 'handleBoundingBox'
        });

        // This is so that we can bring the handle infront of everything while we are selecting only
        this.addComponent(ToggleClasses, {
            viewModelAttributeToggleableClasses: {
                'selecting': 'selecting'
            }
        });

        if (itemConfig[options.viewType].doubleClick) {
            this.addComponent(DoubleClickable, { event: itemConfig[options.viewType].doubleClick });
        }

        if (validationConfig.outsideBounds[options.viewType].enabled && validationProvider.outsideBounds) {
            this.addComponent(OutsideBoundsValidation, { margin: validationConfig.outsideBounds[options.viewType].margin });
        }

        this.addDraggable();
    },

    addDraggable: function() {
        // This is a good pattern to follow when creating components that need access to other elements. We don't want
        // to assume that any other view or component has set up its DOM before render() is called, so this allows us
        // to defer the search for those elements until the component actually needs them.
        this.addComponent(Draggable, {
            getDraggableElements: this.getDraggableElements,
            getHelperElements: this.getViewElementClone,
            getOriginalElements: this.getVisibleElementOriginals,
            // Have to add draggables here so that they will respect z-index ordering.
            getContainerElement: this.getItemsContainerElement
        });
    },

    render: function() {
        this.$el.attr('id', `${this.viewModel.id}-handle`);
        return ComponentView.prototype.render.apply(this, arguments);
    }
});
