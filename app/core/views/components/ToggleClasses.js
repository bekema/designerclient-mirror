const _ = require('underscore');
const Component = require('views/components/Component');

module.exports = Component.extend({

    defaults: {
        viewModelAttributeToggleableClasses: {
            // "example-attr": "example-classname"
        }
    },

    initialize: function() {
        const vm = this.viewModel;
        this.listenTo(this.view, 'render', this.onRender);

        // Clone this so that it can't be modified after initialization.
        this.attributeToClass = _.clone(this.options.viewModelAttributeToggleableClasses);
        // Create an object so that we can do a reverse lookup of class name to attribute.
        this.classToAttributes = {};
        _.each(this.attributeToClass, function(className, attr) {
            const attrs = this.classToAttributes[className] || (this.classToAttributes[className] = []);
            attrs.push(attr);
        }, this);

        _.each(this.attributeToClass, function(className, changedAttr) {
            this.listenTo(vm, `change:${changedAttr}`, function() {
                // When an attribute changes, we still have to check all of the other attributes that are candidates
                // for modifying the same class name, because we will leave the class name on if any of the attributes
                // responsible for it are true.
                const shouldToggleClass = this.classToAttributes[className].reduce(function(result, attr) {
                    return result || vm.get(attr);
                }, false);
                // Force convert to boolean so that we treat undefined as false (toggleClass requires a boolean as its second arg)
                this.$el.toggleClass(className, !!shouldToggleClass);
            });
        }, this);
    },

    onRender: function() {
        _.each(this.classToAttributes, function(attributes, className) {
            const shouldToggleClass = attributes.some(attr => this.viewModel.get(attr));
            this.$el.toggleClass(className, !!shouldToggleClass);
        }, this);
    }
});
