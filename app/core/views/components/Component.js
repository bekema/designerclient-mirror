// A `Component` is used to modify the behavior or appearance of a parent view. Internally, it is also a
// Backbone View - this makes it easier to manage events. The `View` code makes sure that a component's `$el`
// is always the same as the parent view's.
const _ = require('underscore');
const Backbone = require('backbone');

module.exports = Backbone.View.extend({

    __name__: 'Component',

    // Override the `constructor` so that it mimics a view but uses the view's element and view model instead
    // of creating its own.
    /* eslint backbone/no-constructor: 0 */
    constructor: function(view, options) {
        this.cid = _.uniqueId('component');
        this.options = _.extend({}, this.defaults, options);
        this.view = view;
        this.viewModel = view.viewModel;
        // Don't call delegateEvents from inside the component. The view will
        // call delegateEvents on its own, and that will take care of calling it on the child components.
        this.setElement(this.view.$el, false);
        this.initialize.call(this, this.options);
    },

    // Override `remove` so that it doesn't remove the DOM element - that's the View's responsibility.
    remove: function() {
        this.stopListening();
    }

});
