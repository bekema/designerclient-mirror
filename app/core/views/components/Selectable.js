// This component makes it possible to select an individual item view model.
const $ = require('jquery');
const _ = require('underscore');
const Component = require('views/components/Component');
const selectionManager = require('SelectionManager');
const focusManager = require('behavior/FocusManager');
const eventBus = require('EventBus');
const events = eventBus.events;

module.exports = Component.extend({

    defaults: {
        className: 'dcl-selected',
        multiSelectClassName: 'dcl-multiselected',
        // This is the element that can get focus and can cause the view model to get selected.
        getFocusableElement: function() {
            return this.$el;
        },
        // This is the element that is shown when the view model is already selected, and it just
        // needs to make sure that the element is not inappropriately deselected.
        getSelectedElement: function() {
            return this.$el;
        }
    },

    initialize: function() {
        this.listenTo(this.viewModel, 'change:selectable', this.onChangeSelectable);
        this.listenTo(selectionManager, 'select', this.onSelectionManagerSelect);
        this.listenTo(this.view, 'render', this.onRender);

        this.dragging = false;
        this.draggingfromlockeditem = false;
        this.triggerItemFocus = _.debounce(this.triggerItemFocus, 0);
    },

    delegateEvents: function() {
        this.undelegateEvents();

        // This is a handle's area element, which is the element that captures focus.
        const $focusable = this.options.getFocusableElement.call(this);
        $focusable.on('mousedown.selectable' + this.cid, _.bind(this.onPointerDown, this));
        $focusable.on('touchstart.selectable' + this.cid, _.bind(this.onPointerDown, this));
        $focusable.on('touchend.selectable' + this.cid, _.bind(this.onTouchEnd, this));
        $focusable.on('mouseenter.selectable' + this.cid, _.bind(this.onMouseEnter, this));
        $focusable.on('mouseleave.selectable' + this.cid, _.bind(this.onMouseLeave, this));
        $focusable.on('focus.selectable' + this.cid, _.bind(this.onFocus, this));
        $focusable.on('vpfocus.selectable' + this.cid, _.bind(this.onVpFocus, this));
        $focusable.on('mouseup.selectable' + this.cid, _.bind(this.onMouseUp, this));
        focusManager.register($focusable[0]);

        // This is the element that is displayed when the element is already selected, which is
        // used to maintain focus.
        const $selected = this.options.getSelectedElement.call(this);
        if ($selected[0] !== $focusable[0]) {
            $selected.on('mousedown.selectable' + this.cid, _.bind(this.onPointerDown, this));
            $selected.on('touchstart.selectable' + this.cid, _.bind(this.onPointerDown, this));
            $selected.on('touchend.selectable' + this.cid, _.bind(this.onTouchEnd, this));
            $selected.on('mouseup.selectable' + this.cid, _.bind(this.onMouseUp, this));
            $selected.on('vpfocus.selectable' + this.cid, _.bind(this.onVpFocus, this)); // Register this for the sake of IE8.
            focusManager.register($selected[0], $focusable[0]);
        }

        $selected.on('dragstart.selectable' + this.cid, _.bind(this.onDragStart, this));
        $selected.on('dragstop.selectable' + this.cid, _.bind(this.onDragStop, this));
        this.delegatedEvents = true;
    },

    undelegateEvents: function() {
        if (this.delegatedEvents) {
            const $focusable = this.options.getFocusableElement.call(this);
            $focusable.off('.selectable' + this.cid);
            focusManager.remove($focusable[0]);

            const $selected = this.options.getSelectedElement.call(this);
            if ($selected[0] !== $focusable[0]) {
                $selected.off('.selectable' + this.cid);
                focusManager.remove($selected[0]);
            }
            this.delegatedEvents = false;
        }
    },

    remove: function() {
        this.undelegateEvents(); // Specifically we want to make sure that this element is removed from the focus manager.
        Component.prototype.remove.apply(this, arguments);
    },

    onRender: function() {
        this.onChangeSelectable();
    },

    onMouseEnter: function() {
        if (this.viewModel.get('selectable')) {
            this.viewModel.set('over', true);
        }
    },

    onMouseLeave: function() {
        this.viewModel.unset('over');
    },

    onPointerDown: function(e) {
        e.preventDefault();
        e.stopPropagation();
        this.viewModel.set('selecting', true);
        // The ignoreFocus thing is because IE8 triggers focus on mousedown on areas, which
        // is inconsistent and unexpected.
        this.ignoreFocus = true;
        this.xStart = e.pageX;
        this.yStart = e.pageY;

        // Select the item if not selected, otherwise if there is multiselect then deselect it
        if (!this.viewModel.get('selected')) {
            this.select(e);
        } else if (e.ctrlKey || e.shiftKey) {
            // We don't want to deselect if we are dragging,
            // OnPointerUp we check whether a drag was occuring before deselecting
            this.deselecting = true;
        }

        eventBus.trigger(events.manageCanvases, { activeCanvas: this.viewModel.parent.model.id });

        _.defer(_.bind(function() {
            this.ignoreFocus = false;
        }, this));
    },

    onDragStart: function() {
        if (this.viewModel.model.get('locked')) {
            this.select([]);
            this.draggingfromlockeditem = true;
            this.triggerGlobal('dragstartfromlockeditem', this.xStart, this.yStart, this.viewModel.parent.id);
        } else {
            this.dragging = true;
        }
    },

    onDragStop: function() {
        this.dragging = false;
        this.draggingfromlockeditem = false;
        this.deselecting = false;
        this.viewModel.set('selecting', false);
    },

    pointerUp: function(e) {
        if (this.viewModel.get('selected') && !this.dragging && !this.draggingfromlockeditem) {
            if (!this.deselecting) {
                // Don't do an actual focus, as that may cause unwanted scrolling behavior, and the
                // focus manager will keep track of what actually has focus. This will result in the "onFocus" handler getting called.
                const focusEvent = new $.Event('vpfocus', { ctrlKey: e.ctrlKey, shiftKey: e.shiftKey }); //propogate the ctrl/shift key information
                $(e.target).trigger(focusEvent); // Have to do e.target because IE sometimes fires the event handler for the wrong element while still setting the correct target element.
                this.triggerItemFocus();
            } else {
                this.deselect(e);
            }
        }
        // dragstop doesn't always fire, but if mouseup has happened, it should have.
        this.dragging = false;
        this.draggingfromlockeditem = false;
        this.deselecting = false;
        this.viewModel.set('selecting', false);
    },

    onTouchEnd: function(e) {
        this.onMouseLeave(); // Touch devices don't trigger mouse leave reliably.
        this.pointerUp(e);
    },

    onMouseUp: function(e) {
        this.pointerUp(e);
    },

    onVpFocus: function(e) {
        // Selection was a success, so broadcast that.
        if (this.select(e)) {
            this.triggerItemFocus();
        }
    },

    onFocus: function(e) {
        if (!this.ignoreFocus) { // IE8 calls focus on mousedown
            this.onVpFocus(e);
        }
    },

    // This gets debounced in initialize() so that pointerUp + focus don't call it twice.
    triggerItemFocus: function() {
        // Contextual toolbar listens to this.
        this.viewModel.trigger('item:focus');
    },

    select: function(e) {
        if (this.viewModel.get('selectable')) {
            if (!this.viewModel.get('selected')) {
                selectionManager.select(this.viewModel, { multi: e.shiftKey || e.ctrlKey });
            }
            return true;
        }
        return false;
    },

    deselect: function(e) {
        if (e.ctrlKey || e.shiftKey) {
            selectionManager.deselect(this.viewModel);
        }
    },

    onChangeSelectable: function() {
        if (this.viewModel.get('selectable')) {
            this.$el.addClass('dcl-ui-selectable');
            this.delegateEvents();
        } else {
            this.$el.removeClass('dcl-ui-selectable');
            this.undelegateEvents();
        }
    },

    // Should this just be using view model attributes and relying on ToggleClasses for adding classes?
    onSelectionManagerSelect: function() {
        const isSelected = !!this.viewModel.get('selected');
        this.$el.toggleClass(this.options.className, isSelected);

        const isMultiSelected = (isSelected && selectionManager.length > 1);
        this.$el.toggleClass(this.options.multiSelectClassName, isMultiSelected);
    }
});
