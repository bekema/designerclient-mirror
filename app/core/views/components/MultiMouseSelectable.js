// `MultiMouseSelectable` can be applied to a nested item collection in order to make it so that one or more items
// can be selected when the user creates a drag area that includes the positions of those items.
const $ = require('jquery');
const Component = require('views/components/Component');
const selectionManager = require('SelectionManager');
const util = require('util/Util');

//Track whether or not we are dragging
let dragging = false;

module.exports = Component.extend({
    // TODO: What to do about touch events? If we add touch support, we'll make
    // it difficult for pinch gestures to work, since we'll likely
    // have to preventDefault on touchstart.
    events: {
        'mousedown': 'onMouseDown'
    },

    initialize: function(options) {
        if (!this.viewModel.itemViewModels) {
            throw new Error('Cannot add the MultiMouseSelectable component without an itemViewModels collection.');
        }
        //Move this here so that we can drop the select outside of the canvas
        $(document).on('mouseup', this.onMouseUp.bind(this));
        $(document).on('mousemove', this.onMouseMove.bind(this));
        // Make sure we can handle both DOM and jQuery elements as arguments.
        this.eventTarget = $(options.eventTarget).get(0);
        this.listenToGlobal('dragstartfromlockeditem', this.dragStartFromLockedItem.bind(this));
    },

    dragStartFromLockedItem: function(x, y, id) {
        if (this.viewModel.id === id) {
            this.target = this.eventTarget;
            this.x = x;
            this.y = y;

            if (!dragging) {
                //drag started
                dragging = true;
            }

        }
    },

    onMouseDown: function(e) {
        e.preventDefault();

        //if shift/ctrl keys are down then don't empty selectionManager onMouseDown to allow multiselect
        if (e.shiftKey || e.ctrlKey) {
            e.stopPropagation();
        }

        this.target = e.target;
        this.x = e.pageX;
        this.y = e.pageY;

        //drag started
        dragging = true;
    },

    onMouseMove: function(e) {
        // Don't create any selections that didn't start by mousing down on this component's view.
        if (dragging && (this.target === this.eventTarget)) {
            if (!this.$selectBox) {
                //create the select-box
                this.$selectBox = $('<div />', { 'class': 'dcl-canvas__select-box' }).appendTo(this.$el);
            }

            //calculate select-box dimensions
            const offset = this.view.$el.offset();

            const selectDimensions = {
                left: Math.max((Math.min(this.x, e.pageX) - offset.left), 0),
                right: Math.min(Math.max(this.x, e.pageX) - offset.left, this.view.$el.width()),
                top: Math.max((Math.min(this.y, e.pageY) - offset.top), 0),
                bottom: Math.min((Math.max(this.y, e.pageY) - offset.top), this.view.$el.height())
            };
            selectDimensions.width = selectDimensions.right - selectDimensions.left;
            selectDimensions.height = selectDimensions.bottom - selectDimensions.top;

            //update select-box dimensions
            this.$selectBox.css({
                top: selectDimensions.top,
                left: selectDimensions.left,
                width: selectDimensions.width,
                height: selectDimensions.height
            });

            // We want to select any items that are at least partially contained within the selectbox
            // rectangle that we just created by dragging the mouse.
            this.viewModel.itemViewModels.forEach(function(ivm) {
                //get item dimensions
                const dimensions = ivm.get('previewBoundingBox');

                //if the select-box and item handle box area intersect then set the "over" state
                // right now this uses contains, so that it only selects when the item is completely contained
                // alternatively you can use intersect, so that it selects when it intersets
                if (util.contains(selectDimensions, dimensions) && ivm.get('selectable')) {
                    if (!util.contains(dimensions, selectDimensions)) {
                        ivm.set('over', true);
                    }
                } else {
                    ivm.unset('over');
                }
            });
        }
    },

    // This still gets triggered when mousing up inside of an item handle, because the canvas handle contains
    // item handles. It also successfully deselects items because a single click creates an empty rectangle.
    onMouseUp: function(e) {
        // Don't create any selections that didn't start by mousing down on this component's view. Anything
        // else can handle selection itself. See the related `Selectable` component.
        if (this.target === this.eventTarget) {
            e.stopPropagation();

            const selectedItemViewModels = this.viewModel.itemViewModels.filter(function(ivm) {
                if (ivm.get('over')) {
                    ivm.unset('over');
                    return true;
                }
                return false;
            });

            //Add selected items to the selectionManager
            selectionManager.select(selectedItemViewModels, {
                multi: e.shiftKey || e.ctrlKey //since we check e.shiftKey || e.ctrlKey on mousedown
            });

            //Remove the selectbox
            dragging = false;
            if (this.$selectBox) {
                this.$selectBox.remove();
                this.$selectBox = null;
            }
        }
        delete this.target;
    }
});
