// This is the use case where an item is being dragged around the canvas.
const eventBus = require('EventBus');
const events = eventBus.events;

module.exports = function() {
    this.accepts = function(draggable) {
        return draggable.data('image');
    };

    this.onDrop = function(dropViewModel, ui, destinationCoordinates) {
        if (!ui.draggable) {
            return false;
        }

        const image = ui.draggable.data('image');
        if (image) {
            // If we are dropping on an full bleed image then ask the user what they want to do
            //TODO: Move the logic for calcluating full bleed out of here - probably to util.js
            if (dropViewModel.model.get('module') !== 'models/placeholder' &&
                (dropViewModel.model.get('left') <= 0 &&
                    dropViewModel.model.get('top') <= 0 &&
                        dropViewModel.model.get('top') + dropViewModel.model.get('height') >= dropViewModel.parent.model.get('height') &&
                            dropViewModel.model.get('left') + dropViewModel.model.get('width') >= dropViewModel.parent.model.get('width'))) {
                eventBus.trigger(events.openAddOrReplaceImageDialog, {
                    viewModel: dropViewModel,
                    newImage: image,
                    destinationCoordinates
                });
            } else {
                eventBus.trigger(events.replaceImage, dropViewModel, image.toJSON());
            }
            return true;
        }
        return false;
    };
};
