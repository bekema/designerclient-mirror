// This is the use case where a shape is dragged onto the canvas
const commandDispatcher = require('CommandDispatcher');

module.exports = function() {
    this.accepts = function(draggable) {
        return draggable.data('shapeType');
    };

    this.onDrop = function(dropViewModel, ui, destinationCoordinates) {
        if (!ui.draggable) {
            return false;
        }

        const shapeType = ui.draggable.data('shapeType');
        if (shapeType) {
            commandDispatcher.createShape({
                viewModel: dropViewModel,
                shapeType,
                attributes: {
                    top: destinationCoordinates.top,
                    left: destinationCoordinates.left
                }
            });
            return true;
        }

        return false;
    };
};
