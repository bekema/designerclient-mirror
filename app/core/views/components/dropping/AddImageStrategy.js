// This is the use case where an item is being dragged onto the canvas.
const eventBus = require('EventBus');
const events = eventBus.events;

module.exports = function() {
    this.accepts = function(draggable) {
        return draggable.data('image');
    };

    this.onDrop = function(dropViewModel, ui, destinationCoordinates) {
        if (!ui.draggable) {
            return false;
        }

        const image = ui.draggable.data('image');
        const fileType = ui.draggable.data('fileType');
        if (image) {
            eventBus.trigger(events.addImage, {
                image,
                fileType,
                viewModel: dropViewModel,
                selectNewModels: true,
                attributes: {
                    top: destinationCoordinates.top,
                    left: destinationCoordinates.left
                }
            });
            return true;
        }
        return false;
    };
};
