// This is the use case where an item is being dragged from one canvas to another
const $ = require('jquery');
const commandDispatcher = require('CommandDispatcher');

module.exports = function() {
    this.accepts = function(draggable) {
        return draggable.data('dragViewModel');
    };

    this.onDrop = function(dropViewModel, ui) {
        if (!ui.draggable) {
            return false;
        }

        const draggableData = ui.draggable.data();
        if (draggableData.dragViewModel) {
            const dragViewModel = draggableData.dragViewModel;

            const originalCanvasViewModel = dragViewModel.parent;
            const newCanvasViewModel = dropViewModel;

            if (newCanvasViewModel.get('module') !== 'CanvasViewModel' || originalCanvasViewModel === newCanvasViewModel) {
                return false;
            }

            const dragZoomFactor = dragViewModel.parent.get('zoomFactor');

            const $canvas = $(`#${newCanvasViewModel.get('id')}`);
            const mousePositionOnCanvas = {
                top: ui.draggable.inputPosition.top - $canvas.offset().top,
                left: ui.draggable.inputPosition.left - $canvas.offset().left
            };
            const position = {
                top: mousePositionOnCanvas.top - ui.draggable.clickOffset.top,
                left: mousePositionOnCanvas.left - ui.draggable.clickOffset.left
            };

            commandDispatcher.changeCanvas({
                originalCanvasViewModel,
                newCanvasViewModel,
                viewModels: draggableData.viewModels,
                position: {
                    top: position.top / dragZoomFactor,
                    left: position.left / dragZoomFactor
                },
                selectNewModels: true
            });

            return true;
        }
        return false;
    };
};
