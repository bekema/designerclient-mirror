// This is the use case where an item is being dragged around the canvas.
const commandDispatcher = require('CommandDispatcher');

module.exports = function() {
    this.accepts = function(draggable) {
        return draggable.data('dragViewModel');
    };

    this.onDrop = function(dropViewModel, ui) {
        if (!ui.draggable) {
            return false;
        }

        const draggableData = ui.draggable.data();
        if (draggableData.dragViewModel) {
            const dragViewModel = draggableData.dragViewModel;

            let deltaTop, deltaLeft;
            const dragZoomFactor = dragViewModel.parent.get('zoomFactor');

            deltaTop = ui.position.top - ui.originalPosition.top;
            deltaLeft = ui.position.left - ui.originalPosition.left;

            commandDispatcher.move({
                viewModels: draggableData.viewModels,
                delta: {
                    top: deltaTop / dragZoomFactor,
                    left: deltaLeft / dragZoomFactor
                }
            });

            return true;
        }
        return false;
    };
};
