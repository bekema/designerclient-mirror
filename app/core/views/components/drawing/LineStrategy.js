const ShapeStrategy = require('views/components/drawing/ShapeStrategy');

module.exports = ShapeStrategy.extend({

    svgType: function() {
        return 'line';
    },

    updateAttributes: function({ startPosition, endPosition, offset, parentSize }) {
        const dimensions = this.getDimensions({ startPosition, endPosition, offset, parentSize });
        const shapeElement = this.$el.children()[0];
        const zoom = this.canvasViewModel.get('zoomFactor');

        shapeElement.setAttribute('stroke-width', this.model.strokeWidth * zoom);
        shapeElement.setAttribute('x1', 0);
        shapeElement.setAttribute('y1', dimensions.height / 2);
        shapeElement.setAttribute('x2', dimensions.width);
        shapeElement.setAttribute('y2', dimensions.height / 2);

        this.model.left = dimensions.left / zoom;
        this.model.top = (startPosition.y - offset.top) / zoom;
        this.model.width = dimensions.width / zoom;
        this.model.height = this.model.strokeWidth;

        this.$el.css({
            position: 'absolute',
            top: startPosition.y - offset.top - dimensions.height / 2,
            left: dimensions.left,
            width: dimensions.width,
            height: dimensions.height
        });
    },

    getDimensions: function({ startPosition, endPosition, offset, parentSize }) {
        const dimensions = ShapeStrategy.prototype.getDimensions.call(this, { startPosition, endPosition, offset, parentSize });
        dimensions.height = this.model.strokeWidth * this.canvasViewModel.get('zoomFactor');
        return dimensions;
    }
});