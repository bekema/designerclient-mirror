const ShapeStrategy = require('views/components/drawing/ShapeStrategy');

// This seems to be a close to perfect match to itemPreview
const RADIUS_FACTOR = 20;

module.exports = ShapeStrategy.extend({

    svgType: function() {
        return 'rect';
    },

    initializeShape: function(shape) {
        ShapeStrategy.prototype.initializeShape.call(this, shape);

        const zoom = this.canvasViewModel.get('zoomFactor');

        shape.setAttribute('rx', this.model.strokeWidth * zoom / RADIUS_FACTOR);
        shape.setAttribute('ry', this.model.strokeWidth * zoom / RADIUS_FACTOR);
    }
});