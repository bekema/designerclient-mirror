const $ = require('jquery');
const ComponentView = require('views/components/ComponentView');
const EllipseStrategy = require('views/components/drawing/EllipseStrategy');
const LineStrategy = require('views/components/drawing/LineStrategy');
const RectangleStrategy = require('views/components/drawing/RectangleStrategy');
const util = require('util/Util');

const strategies = {
    RectangleStrategy,
    EllipseStrategy,
    LineStrategy
};

module.exports = ComponentView.extend({

    defaults: {
        containerSelector: '.canvas'
    },

    className: 'shape-creator',

    events: {
        'mousedown': 'onMouseDown',
        'touchstart': 'onMouseDown'
    },

    initialize: function() {
        this.listenTo(this.view, 'render', this.onRender);
        this.listenToGlobal('enableDrawable', this.onEnableDrawable);
        this.listenToGlobal('disableDrawable', this.onDisableDrawable);
        this.listenToGlobal('cancelaction', this.onCancel);
        this.eventTarget = this.el;
    },

    onRender: function() {
        this.view.$el.append(this.$el);
    },

    onCancel: function() {
        this.triggerGlobal('disableDrawable');
    },

    onEnableDrawable: function(shape) {
        if (this.viewModel.get('enabled')) {
            if (!this.$el.is(':visible')) {
                this.$el.show();
            }
            this.shape = shape;
        }
    },

    onDisableDrawable: function() {
        this.$el.hide();

        if (this.svgElement) {
            this.svgElement.remove();
            this.svgElement = null;
            this.dragging = false;
        }

        $(document).off('.shapedrawing');
    },

    onMouseDown: function(e) {
        const coords = util.getCoordinates(e);

        this.startPosition = {
            x: coords.left,
            y: coords.top
        };

        if (!this.dragging) {
            $(document).on('mouseup.shapedrawing touchend.shapedrawing', this.onMouseUp.bind(this));
            $(document).on('mousemove.shapedrawing touchmove.shapedrawing', this.onMouseMove.bind(this));

            this.dragging = true;
        }
    },

    onMouseMove: function(e) {
        const offset = this.view.$el.offset();
        const { left: x, top: y } = util.getCoordinates(e, null);

        if (!this.svgElement) {
            this.svgElement = new strategies[`${this.shape}Strategy`]({ viewModel: this.viewModel, shape: this.shape });
            this.$el.append(this.svgElement.$el);
        }

        const endPosition = {
            x,
            y
        };

        const parentSize = {
            width: this.$el.width(),
            height: this.$el.height()
        };

        const startPosition = this.startPosition;
        const fixedAspect = e.shiftKey;

        this.svgElement.updateAttributes({ startPosition, endPosition, offset, parentSize, fixedAspect });
    },

    onMouseUp: function(e) {
        e.stopPropagation();
        $(document).off('.shapedrawing');

        this.dragging = false;

        if (this.svgElement) {
            this.svgElement.createShape();
            this.svgElement.remove();
            this.svgElement = null;
        }

        this.triggerGlobal('disableDrawable');
    }
});
