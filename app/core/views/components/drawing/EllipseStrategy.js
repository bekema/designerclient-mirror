const ShapeStrategy = require('views/components/drawing/ShapeStrategy');

module.exports = ShapeStrategy.extend({

    svgType: function() {
        return 'ellipse';
    },

    updateAttributes: function(options) {
        const dimensions = this.getDimensions(options);

        ShapeStrategy.prototype.updateAttributes.call(this, options);

        const halfWidth = dimensions.width / 2;
        const halfHeight = dimensions.height / 2;
        const shapeElement = this.$el.children()[0];

        const strokeWidthOffset = (this.model.strokeWidth * this.canvasViewModel.get('zoomFactor')) / 2;

        shapeElement.setAttribute('cx', Math.round(halfWidth + strokeWidthOffset));
        shapeElement.setAttribute('cy', Math.round(halfHeight + strokeWidthOffset));
        shapeElement.setAttribute('rx', Math.floor(halfWidth));
        shapeElement.setAttribute('ry', Math.floor(halfHeight));
    }
});