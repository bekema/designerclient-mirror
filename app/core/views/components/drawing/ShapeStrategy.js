const _ = require('underscore');
const Backbone = require('backbone');
const commandDispatcher = require('CommandDispatcher');
const Shapes = require('publicApi/configuration/core/items').shape.defaults;

module.exports = Backbone.View.extend({

    svgType: function() {
        throw new Error('You must define the svg type');
    },

    initialize: function({ viewModel, shape }) {
        this.canvasViewModel = viewModel;
        this.model = _.clone(Shapes[shape]);

        const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        this.setElement(svg);

        const svgNS = svg.namespaceURI;
        const shapeElement = document.createElementNS(svgNS, this.svgType());
        this.initializeShape(shapeElement);
        this.shape = shape;

        svg.appendChild(shapeElement);
    },

    initializeShape: function(shape) {
        const zoom = this.canvasViewModel.get('zoomFactor');
        const offset = this.model.strokeWidth * zoom / 2;

        shape.setAttribute('x', offset);
        shape.setAttribute('y', offset);
        shape.setAttribute('stroke-width', this.model.strokeWidth * zoom);

        if (this.model.fillColor) {
            shape.setAttribute('fill', this.model.fillColor);
        }

        if (this.model.strokeColor) {
            shape.setAttribute('stroke', this.model.strokeColor);
        }
    },

    updateAttributes: function({ startPosition, endPosition, offset, parentSize, fixedAspect }) {
        const zoom = this.canvasViewModel.get('zoomFactor');
        const strokeWidth = this.model.strokeWidth * zoom;
        const shapeElement = this.$el.children()[0];

        const dimensions = this.getDimensions({ startPosition, endPosition, offset, parentSize, fixedAspect });

        shapeElement.setAttribute('width', dimensions.width);
        shapeElement.setAttribute('height', dimensions.height);

        this.offset = offset;

        this.model.left = dimensions.left / zoom;
        this.model.top = dimensions.top / zoom;
        this.model.width = dimensions.width / zoom;
        this.model.height = dimensions.height / zoom;

        this.$el.css({
            position: 'absolute',
            top: Math.round(dimensions.top - strokeWidth / 2),
            left: Math.round(dimensions.left - strokeWidth / 2),
            width: Math.round(dimensions.width + strokeWidth),
            height: Math.round(dimensions.height + strokeWidth)
        });
    },

    getDimensions: function({ startPosition, endPosition, offset, parentSize, fixedAspect }) {
        this.offset = offset;

        const dimensions = {
            left: Math.max((Math.min(startPosition.x, endPosition.x) - offset.left), 0),
            right: Math.min(Math.max(startPosition.x, endPosition.x) - offset.left, parentSize.width),
            top: Math.max((Math.min(startPosition.y, endPosition.y) - offset.top), 0),
            bottom: Math.min((Math.max(startPosition.y, endPosition.y) - offset.top), parentSize.height)
        };

        dimensions.width = dimensions.right - dimensions.left;
        dimensions.height = dimensions.bottom - dimensions.top;

        // TODO: Make this work when drawing in directions other than right and down
        if (fixedAspect) {
            dimensions.width = Math.max(dimensions.width, dimensions.height);
            dimensions.height = dimensions.width;
        }

        return dimensions;
    },

    remove: function() {
        this.$el.remove();
    },

    createShape: function() {
        if (!this.shape) { return; }

        commandDispatcher.createShape({
            viewModel: this.canvasViewModel,
            shape: this.shape,
            attributes: this.model
        });
    }
});