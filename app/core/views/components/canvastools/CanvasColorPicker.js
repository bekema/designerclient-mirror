const $ = require('jquery');
const ComponentView = require('views/components/ComponentView');
const CanvasToolColorPicker = require('ui/tools/color/CanvasToolColorPicker');
const canvasColorPickerButtonTemplate = require('ui/tools/templates/buttons/HoverButton.tmp');

module.exports = ComponentView.extend({
    className: 'dcl-canvas-tools__color-picker dcl-canvas-tools__colors-box dcl-color-choice-button--remove-color',

    tagName: 'div',

    events: {
        'click': 'onClick'
    },

    initialize: function() {
        this.listenTo(this.viewModel.model.items, 'add change', this.updateButtonBackground);
        this.listenTo(this.viewModel.model.items, 'remove', this.setDefaultButtonBackground);
        this.canvasToolColorPicker = new CanvasToolColorPicker({
            parent: this,
            viewModel: this.viewModel
        });

        $(document).on('click', this.onClick.bind(this));
        $(document).on('click', this.hide.bind(this));
    },

    setDefaultButtonBackground: function() {
        // TODO: Get the real background - from the scene or using color thief.
        this.$el.css({ backgroundColor: '#FFFFFF' });
        this.$el.addClass('dcl-color-choice-button--remove-color');
    },

    updateButtonBackground: function(item) {
        if (item.get('isBackground')) {
            this.setPickerBackgroundColor(item.get('fillColor'));
            this.$el.removeClass('dcl-color-choice-button--remove-color');
        }
    },

    stopPropagation: function(e) {
        e.stopPropagation();
    },

    setPickerBackgroundColor: function(color) {
        this.$el.css({
            backgroundColor: color
        });
    },

    hide: function() {
        $('.dcl-canvas-tools__color-picker').removeClass('dcl-button--hover');
        $('.dcl-canvas').removeClass('dcl-canvas-editing-enabled');
        $('.dcl-canvas__tools').removeClass('dcl-canvas__element-wrapper--editing');
    },

    show: function() {
        this.hide();
        this.$el.parents('.dcl-canvas').addClass('dcl-canvas-editing-enabled');
        if (!this.$el.hasClass('dcl-button--hover')) {
            this.$el.addClass('dcl-button--hover');
            this.canvasToolColorPicker.show();
            $('.dcl-canvas__tools').addClass('dcl-canvas__element-wrapper--editing');
        }
    },

    onClick: function(e) {
        if ($(e.target).hasClass('dcl-button--hover')) {
            this.hide();
        } else {
            this.show();
            this.stopPropagation(e);
        }
    },

    render: function() {
        this.$el.html(canvasColorPickerButtonTemplate);
        this.$el.append(this.canvasToolColorPicker.render().el);

        this.setDefaultButtonBackground();
        ComponentView.prototype.render.apply(this, arguments);

        return this;
    }
});
