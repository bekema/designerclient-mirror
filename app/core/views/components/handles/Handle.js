const _ = require('underscore');
const Handles = require('views/components/handles/Handles');
const Resizable = require('views/components/Resizable');
const coreConfig = require('publicApi/configuration/core/items');
const Positioning = require('views/components/Positioning');
const BoundingBoxUpdater = require('views/components/BoundingBoxUpdater');
const Draggable = require('views/components/Draggable');
const Selectable = require('views/components/Selectable');
const Rotatable = require('views/components/Rotatable');

//TODO: A bunch of this is a copy from Target - can we leverage Target better?
module.exports = Handles.extend({
    className: 'dcl-handle',

    defaults: {
        containerSelector: '.dcl-canvas'
    },

    initialize: function(options) {
        const me = this;
        this.view.getHandle = function() {
            return me;
        };
        this.getHandleArea = function() {
            return me.$el;
        };

        this.getDraggableElements = function() {
            return me.$el;
        };

        this.getViewElementClone = function() {
            const clone = me.view.$el.clone();
            return clone;
        };

        this.getVisibleElementOriginals = function() {
            return me.view.$el.add(me.$el);
        };

        this.getItemsContainerElement = function() {
            return me.$el.closest(me.options.containerSelector).find('.dcl-items');
        };

        this.getVisibleElementClones = function() {
            const clone = me.view.$el.clone();
            return clone.add(me.$el.clone());
        };

        this.addComponent(Positioning, { positionAttribute: 'handlePosition', sizeAttribute: 'handleSize' });

        this.addComponent(Resizable, this.getResizeOptions(options));

        // Updates the bounding box based on the DOM
        this.addComponent(BoundingBoxUpdater, {
            viewModelEvents: 'change:handlePosition change:handleSize change:transformOrigin appendedtodom',
            setAttribute: 'handleBoundingBox'
        });
        this.addComponent(Selectable, { getFocusableElement: this.getHandleArea });

        this.addDraggable();

        if (coreConfig[options.viewType].rotatable) {
            this.addComponent(Rotatable, _.extend({}, coreConfig[options.viewType], options));
        }

        return Handles.prototype.initialize.apply(this, arguments);
    },

    addDraggable: function() {
        this.addComponent(Draggable, {
            getDraggableElements: this.getDraggableElements,
            getHelperElements: this.getViewElementClone,
            getOriginalElements: this.getVisibleElementOriginals,
            getContainerElement: this.getItemsContainerElement
        });
    },

    render: function() {
        this.$el.attr('id', `${this.viewModel.id}-control`);
        return Handles.prototype.render.apply(this, arguments);
    },

    getResizeOptions: function(options) {
        return _.extend(coreConfig[options.viewType], options, {
            maintainProportions: options.viewType === 'image', // all others do not maintain proportions
            getResizableElementClones: this.getVisibleElementClones,
            getResizableElementOriginals: this.getVisibleElementOriginals,
            getContainerElement: this.getItemsContainerElement
        });
    }
});