const $ = require('jquery');
const ComponentView = require('views/components/ComponentView');
const Positioning = require('views/components/Positioning');
const Droppable = require('views/components/Droppable');
const ChangeCanvasStrategy = require('views/components/dropping/ChangeCanvasStrategy');
const MoveViewModelStrategy = require('views/components/dropping/MoveViewModelStrategy');
const AddImageStrategy = require('views/components/dropping/AddImageStrategy');
const AddShapeStrategy = require('views/components/dropping/AddShapeStrategy');
const MultiMouseSelectable = require('views/components/MultiMouseSelectable');
const eventBus = require('EventBus');
const events = eventBus.events;

module.exports = ComponentView.extend({

    className: 'dcl-targets dcl-canvas__element-wrapper',

    events: {
        'mousedown' : 'clickedCanvas',
        'touchstart' : 'clickedCanvas',
        'mousedown .handles': 'preventDefault',
        // Don't allow the href clicks on areas to change the browser hash
        'click area': 'preventDefault'
    },

    initialize: function() {
        this.listenTo(this.view, 'render', this.render);
        this.listenTo(this.viewModel, 'change:enabled', this.toggleEnabled);

        // Publish a public method on the view so that the CanvasView can add this handle to the DOM.
        this.view.getTarget = function() {
            return this;
        }.bind(this);

        this.addComponent(Positioning, { sizeAttribute: 'size', positionAttribute: false, zIndexAttribute: false });
        this.addComponent(MultiMouseSelectable, { eventTarget: this.$el });
        this.addComponent(Droppable, { strategies: [new ChangeCanvasStrategy, new MoveViewModelStrategy, new AddImageStrategy, new AddShapeStrategy] });

        eventBus.on(`${events.dragStart} ${events.resizeStart} ${events.rotateStart}`, this.toggleTargetVisibility.bind(this, true));
        eventBus.on(`${events.dragStop} ${events.resizeStop} ${events.rotateStop}`, this.toggleTargetVisibility.bind(this, false));

        //Sometimes focusing on things outside of the viewable canvas can cause the canvas to scroll. This breaks everything so lets not let it happen
        this.$el.scroll(function(e) {
            $(e.currentTarget).scrollTop(0);
            $(e.currentTarget).scrollLeft(0);
        });

        this.$el.on('dcldragenter', this.dragEnter.bind(this));
        this.$el.on('dcldragleave', this.dragLeave.bind(this));
        this.$el.on('dcldragactivate', this.enableTargets.bind(this));
        this.$el.on('dcldragdeactivate dcldrop', this.disableTargets.bind(this));
    },

    clickedCanvas: function() {
        eventBus.trigger(events.manageCanvases, { activeCanvas: this.viewModel.model.id });
    },

    preventDefault: function(e) {
        e.preventDefault();
    },

    toggleEnabled: function() {
        if (this.viewModel.get('enabled')) {
            this.droppable.enable();
        } else {
            this.droppable.disable();
        }
    },

    toggleTargetVisibility: function(toggle) {
        // This attribute causes the targets div to be hidden so that
        // hover events don't occur during drag, resize, and rotate
        this.viewModel.set('itemTargetsDisabled', toggle);
    },

    enableTargets: function(event, eventData) {
        this.viewModel.set('dropEnabled', this.validTarget(eventData));
    },

    disableTargets: function() {
        this.viewModel.unset('dropEnabled');
        this.viewModel.unset('targetEnabled');
    },

    dragEnter: function(event, eventData) {
        const valid = this.validTarget(eventData);
        this.viewModel.set('targetEnabled', valid);
    },

    validTarget: function(eventData) {
        let valid = true;
        const draggableData = eventData.draggable.data();
        if (draggableData.dragViewModel) {
            const itemParentViewModel  = draggableData.dragViewModel.parent;

            valid = itemParentViewModel !== this.viewModel;
        }

        return valid;
    },

    dragLeave: function() {
        this.viewModel.unset('targetEnabled');
    }
});
