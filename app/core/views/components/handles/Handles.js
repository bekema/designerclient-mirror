const ComponentView = require('views/components/ComponentView');

module.exports = ComponentView.extend({
    className: 'dcl-handles dcl-canvas__element-wrapper',

    initialize: function() {
        const me = this;
        this.view.getHandle = function() {
            return me;
        };
        this.listenTo(this.view, 'render', this.render);
    },

    render: function() {
        this.view.$el.append(this.$el);
        return ComponentView.prototype.render.apply(this, arguments);
    }
});
