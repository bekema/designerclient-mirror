const Backbone = require('backbone');
const Component = require('views/components/Component');

module.exports = Component.extend({

    initialize: function() {
        this.listenTo(this.viewModel, 'change:url', this.updateImage, this);
        this.listenTo(this.view, 'setup', this.onSetup, this);
    },

    events: {
        'load': 'onLoadImage'
    },

    onSetup: function() {
        if (!this.$el.is('img')) {
            throw new Error('UrlBoundImage must be applied to a view with an img element');
        }

        this.updateImage();
    },

    updateImage: function() {
        //If we shouldnt be updating images now then update them as soon as we can again
        if (this.viewModel.parent.get('disableImageUpdates')) {
            this.stopListening(this.viewModel.parent, 'change:disableImageUpdates');
            this.listenToOnce(this.viewModel.parent, 'change:disableImageUpdates', function() {
                this.updateImage();
            });
            return;
        }

        const url = this.viewModel.get('url');
        this.makeImageRequest(url);
    },

    makeImageRequest: function(url) {
        this.$el.attr('src', url);
    },

    onLoadImage: function() {
        this.triggerViewModelEvent();
    },

    // Do this once the image itself is finally loaded. May be overridden.
    triggerViewModelEvent: function() {
        const zoom = this.viewModel.parent.get('zoomFactor');

        // "atomic" means don't trigger computed attribute changes until *after* this function is complete.
        Backbone.atomic(function() {
            // Reset any sizing data that was waiting for the image reload.
            //TODO I'm pretty sure temporaryPositionDelta isn't really used
            this.viewModel.set({
                temporarySizeDelta: null,
                temporaryPositionDelta: null
            });
            // Sometimes this.el.width returns a value when this.$el.width() does not.
            this.viewModel.trigger('loadimage', {
                width: this.el.naturalWidth / zoom,
                height: this.el.naturalHeight / zoom
            });
        }, this);
    }
});
