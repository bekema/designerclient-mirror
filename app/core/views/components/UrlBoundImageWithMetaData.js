// This component extends the `UrlBoundImage` to first make an XHR
// request for the image in order to read any headers that might be in the response. Then it loads the image
// itself, which should be cached as a result of the previous request. This triggers an additional event
// on the view model, `loadimagewithmetadata`, once the headers (metadata) and the image have both been loaded.
const Backbone = require('backbone');
const UrlBoundImage = require('views/components/UrlBoundImage');

module.exports = UrlBoundImage.extend({

    initialize: function() {
        this.currentRequest = 0;
        this.lastResponse = 0;
        UrlBoundImage.prototype.initialize.apply(this, arguments);
    },

    makeImageRequest: function(url) {
        const currentRequest = this.currentRequest;
        this.currentRequest++;
        const me = this;

        // IE10 does not support drawing cross origin images on a canvas.
        // All of this nonsense with XMLHttpRequest, blobs, and object URLs
        // fixes that issue.
        const xhr = new XMLHttpRequest;
        xhr.onload = function() {
            if (me.objectUrl) {
                URL.revokeObjectURL(me.objectUrl);
            }

            me.objectUrl = URL.createObjectURL(this.response);

            // If any AJAX requests return out of order, ignore out of date ones.
            if (me.lastResponse > currentRequest) {
                return;
            }
            me.lastResponse = currentRequest;
            // Trigger the event with the current meta data.
            const metaData = xhr.getResponseHeader(me.options.metaDataKey);
            if (metaData) {
                me.triggerViewModelEvent = function() {
                    // "atomic" means don't trigger computed attribute changes until *after* this function is complete.
                    Backbone.atomic(function() {
                        const viewModel = me.viewModel;
                        viewModel.set({ temporarySizeDelta: null, temporaryPositionDelta: null });
                        viewModel.trigger('loadimage', { metaData: JSON.parse(metaData) });
                    });
                };
            } else {
                me.triggerViewModelEvent = UrlBoundImage.prototype.triggerViewModelEvent;
            }

            me.$el.attr('src', me.objectUrl);
        };
        xhr.open('GET', url, true);
        xhr.responseType = 'blob';
        xhr.send();
    }
});
