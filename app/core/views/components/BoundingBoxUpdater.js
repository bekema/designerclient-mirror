// This component is used to update the boundingBox properties of the view model
// using the DOM API, which conveniently takes into account transforms
const $ = require('jquery');
const Component = require('views/components/Component');

const defaultEvents = ['appendedtodom', 'loadimage'];

const updateBoundingBox = function() {
    const rect = this.view.el.getBoundingClientRect();
    const canvasPosition = this.view.$el.parent().offset();
    const scrollTop = $(window).scrollTop();
    const scrollLeft = $(window).scrollLeft();

    const boundingBox = {
        left: rect.left - canvasPosition.left + scrollLeft,
        top: rect.top - canvasPosition.top + scrollTop,
        width: rect.width,
        height: rect.height
    };

    this.viewModel.set(this.options.setAttribute, boundingBox);
};

module.exports = Component.extend({

    defaults: {
        modelEvents: ['change:rotation'],
        viewModelEvents: ['change:previewPosition', 'change:previewSize', 'change:transformOrigin'],
        setAttribute: 'previewBoundingBox'
    },

    initialize: function() {
        const updateFunction = updateBoundingBox.bind(this);

        this.listenTo(this.viewModel, defaultEvents.concat(this.options.viewModelEvents).join(' '), updateFunction);
        this.listenTo(this.viewModel.model, this.options.modelEvents.join(' '), updateFunction);
    }
});
