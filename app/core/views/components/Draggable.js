// The `Draggable` component should be used to move items around on the canvas. It uses the `SelectionManager` in
// order to move all selected items at once.
const $ = require('jquery');
const _ = require('underscore');
const Component = require('views/components/Component');
const selectionManager = require('SelectionManager');
const dragAndDrop = require('DragAndDrop');
const rebindOptionsMethods = require('views/utilities/rebindOptionsMethods');
const eventBus = require('EventBus');
const events = eventBus.events;

// A note about casing: I capitalize constructors and camel case singletons and libraries.
// There are times where we need to operate on multiple views and components simultaneously.
// This registry lets us take the view models which are managed by the `SelectionManager`,
// and use those to find the relevant draggable components.
const viewModelRegistry = {};

// We allow dragging for these viewModels
const getDraggableViewModels = function(currentViewModel) {
    return selectionManager.toArray().filter((vm) => {
        return vm.get('draggable') && viewModelRegistry[vm.id] &&
                (vm === currentViewModel || vm.parent === currentViewModel.parent);
    });
};

module.exports = Component.extend({

    defaults: {
        selectorToIgnore: '.dcl-ui-resizable-handle, .dcl-ui-resizable-resizing',
        // This is the element that the helper will be appended to.
        getContainerElement: function() {
            return this.$el.parent();
        },
        // The elements to display as the helper.
        getHelperElements: function() {
            return this.$el.clone();
        },
        // The original elements to hide while the drag is taking place.
        getOriginalElements: function() {
            return this.$el;
        },
        // The elements that the drag can be triggered on.
        getDraggableElements: function() {
            return this.$el;
        }
    },

    initialize: function() {
        this.listenTo(this.view, 'render', this.onRender);
        this.cancelled = false;
        viewModelRegistry[this.viewModel.id] = this;

        rebindOptionsMethods(this);
    },

    remove: function() {
        Component.prototype.remove.apply(this, arguments);
        delete viewModelRegistry[this.viewModel.id];
    },

    onCancel: function() {
        if (!this.cancelled) {
            this.cancelled = true;
            // This is a "hack" to stop dragging.
            this.$el.mouseup();
        }
    },

    createDraggableHelperLayer: function() {
        // Create the draggable layer and copy certain properties on to it from its container.
        const $layer = $('<div></div>', { 'class': 'draggable-layer dcl-canvas__element-wrapper' });
        $layer.css({ top: 0, left: 0, 'z-index': 'auto' });

        // Find viewModels that are registered as draggable and belong to this parent.
        const viewModels = getDraggableViewModels(this.viewModel);

        // Attach views for these viewModels to our layer.
        viewModels.forEach(function(vm) {
            const proxy = viewModelRegistry[vm.id].options.getHelperElements().css('visibility', 'visible');
            proxy.attr('data-source-element', vm.id);
            // We don't need these when dragging! Actually the entire handle will be hidden
            // unless there are validation problems.
            proxy.find('.dcl-ui-resizable-handle').remove();
            const rotation = vm.model.get('rotation');
            proxy.data({ rotation });
            $layer.append(proxy);
        }.bind(this));

        return $layer;
    },

    onRender: function() {
        this.$el.addClass('ui-draggable');

        const draggables = this.options.getDraggableElements();

        // Hook up the draggable functionality.
        this.view.draggable = new dragAndDrop.Draggable(
            draggables, {
                selectorToIgnore: this.options.selectorToIgnore,
                appendTo: this.options.getContainerElement,
                helper: this.createDraggableHelperLayer.bind(this),
                translate: true,
                updateHelper: function({ helper, position }) {
                    helper.children().each(function() {
                        // We're not touching the actual css top and left of the children, so position will just be the amount that the draggable
                        // layer has moved, which is the amount we want to adjust by.
                        this.style.transform = `translate(${position.left}px, ${position.top}px) rotate(-${$(this).data().rotation}deg)`;
                    });
                }.bind(this),
                start: function() {
                    // Can't drag something that isn't draggable or selected.
                    if (!this.viewModel.get('draggable') || !this.viewModel.get('selected')) {
                        return false;
                    }

                    // Find viewModels that are registered as draggable and belong to this parent.
                    const viewModels = getDraggableViewModels(this.viewModel);

                    // There are no draggable viewModels
                    if (viewModels.length === 0) {
                        return false;
                    }
                    const views = viewModels.map((viewModel) => {
                        return viewModelRegistry[viewModel.id].view.view; //get the view associated with the handle (which this is a component of)
                    });

                    draggables.data({
                        viewModels,
                        views,
                        dragViewModel: this.viewModel
                    });
                    // Also keep track of the list of view models so we can access it internally without going through jQuery.data() - this
                    // is a performance optimization. Note that we can't just use the selection manager's view models because that could
                    // change mid-drag.
                    this.currentViewModels = viewModels;

                    // Hide all of the draggable elements, since they're getting cloned and added to the helper layer.
                    viewModels.forEach(function(vm) {
                        const draggableElements = viewModelRegistry[vm.id].options.getOriginalElements();
                        draggableElements.css('visibility', 'hidden');
                        vm.set('dragging', true);
                        vm.trigger('dragstart');
                    }, this);

                    // Make sure the action can be cancelled.
                    this.listenToGlobal('cancelaction', this.onCancel);
                    this.cancelled = false;
                    eventBus.trigger(events.dragStart, this.currentViewModels);
                }.bind(this),

                drag: function(e, ui) {
                    // This is part of a hack to cancel dragging on `esc`. I suspect that we may need to just write our
                    // own draggable (potentially that would give us better touch support too), and we wouldn't be dependent
                    // on jquery-ui.
                    if (this.cancelled) {
                        return false;
                    }

                    // Trigger a `drag` event on each view model with the UI data modified to be more appropriate to the view model.
                    _.each(this.currentViewModels, function(vm) {
                        const previewPosition = vm.get('previewPosition');
                        const previewBoundingBox = vm.get('previewBoundingBox');
                        const handlePosition = vm.get('handlePosition');

                        const delta = {
                            left: ui.position.left - ui.originalPosition.left,
                            top: ui.position.top - ui.originalPosition.top
                        };

                        const dragPreviewPosition = {
                            left: previewPosition.left + delta.left,
                            top: previewPosition.top + delta.top
                        };

                        const dragHandlePosition = {
                            left: handlePosition.left + delta.left,
                            top: handlePosition.top + delta.top
                        };

                        const dragInfo = {
                            helper: ui.helper,
                            originalPosition: previewPosition,
                            originalBoundingBox: previewBoundingBox,
                            previewPosition: dragPreviewPosition,
                            handlePosition: dragHandlePosition,
                            delta,
                            viewModels: this.currentViewModels
                        };

                        vm.trigger('drag', dragInfo);
                    }, this);
                }.bind(this),

                stop: function() {
                    // Clean up after the drag.
                    _.each(this.currentViewModels, function(vm) {
                        const registeredViewModel = viewModelRegistry[vm.id];
                        if (registeredViewModel) {
                            const draggableElements = registeredViewModel.options.getOriginalElements();
                            draggableElements.css('visibility', 'visible');
                            vm.unset('dragging');
                            vm.trigger('dragstop');
                        }
                    }, this);

                    eventBus.trigger(events.dragStop, this.currentViewModels);

                    delete this.currentViewModels;
                    this.$el.removeData(['viewModels', 'dragViewModel', 'views']);
                }.bind(this)
            });
    }
});
