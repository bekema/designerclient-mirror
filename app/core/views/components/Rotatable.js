/* eslint-disable no-magic-numbers */

const commandDispatcher = require('CommandDispatcher');
const $ = require('jquery');
const Component = require('views/components/Component');
const util = require('util/Util');
const rebindOptionsMethods = require('views/utilities/rebindOptionsMethods');
const eventBus = require('EventBus');
const events = eventBus.events;

// TODO: these should be configurable by the merchant
const SNAP_THRESHOLD = 5;
const SNAP_ANGLES = [0, 90, 180, 270];

module.exports = Component.extend({
    defaults: {
        snapWithShift: false,

        snapEnabled: function(e) {
            return (this.options.snapWithShift && e.shiftKey) || (!this.options.snapWithShift && !e.shiftKey);
        }
    },

    initialize: function() {
        this.listenTo(this.viewModel, 'model:change:rotation change:handleSize dragstop resizestop', this.updateHandle);
        this.listenTo(this.view, 'render', this.render);

        this.$dragSurface = $(document);

        this.grab = this.grab.bind(this);
        this.drop = this.drop.bind(this);
        this.rotation = this.rotation.bind(this);

        rebindOptionsMethods(this);
    },

    remove: function() {
        Component.prototype.remove.apply(this, arguments);
    },

    render: function() {
        this.$handle = $(`<div class="dcl-rotatable-handle">
                            <svg class="dcl-rotatable-handle__icon">
                                <use class="dcl-icon__use" xlink:href="#dcl-icon-rotate" />
                            </svg>
                            <div class="dcl-rotatable-handle__line"></div>
                          </div>`);
        this.$handle.on('mousedown', this.grab);
        this.$handle.on('touchstart', this.grab);
        this.$el.append(this.$handle);
        this.updateHandle();
    },

    cancel: function() {
        this.angle = this.viewModel.model.get('rotation');
        this.target.css('transform', `rotate(-${this.angle}deg)`);
        this.target.css('transform-origin', '50% 50%');
        this.$dragSurface.off(`.rotatable${this.cid}`);
    },

    updateHandle: function() {
        this.$handle.css('top', this.$el.height() + 15);
    },

    rotation: function(e) {
        const { left: x, top: y } = util.getCoordinates(e);
        const radians = Math.atan2(x - this.imageX, y - this.imageY);
        this.angle = util.mod(radians * (180 / Math.PI), 360);

        if (this.options.snapEnabled(e)) {
            SNAP_ANGLES.some(snap => {
                if (snap === 0) {
                    if (this.angle > (360 - SNAP_THRESHOLD) || this.angle < SNAP_THRESHOLD) {
                        this.angle = snap;
                        return true; // break out of loop
                    }
                } else {
                    if (this.angle > (snap - SNAP_THRESHOLD) && this.angle < (snap + SNAP_THRESHOLD)) {
                        this.angle = snap;
                        return true; // break out of loop
                    }
                }
            });
        }

        this.target.css('transform', `rotate(-${this.angle}deg)`);
        this.target.css('transform-origin', this.viewModel.get('transformOrigin'));
        this.$el.css('transform', `rotate(-${this.angle}deg)`);
        this.viewModel.trigger('rotate', { rotation: this.angle });
    },

    startRotation: function() {
        this.$dragSurface.on(`mousemove.rotatable${this.cid}`, this.rotation);
        this.$dragSurface.on(`touchmove.rotatable${this.cid}`, this.rotation);
        this.$dragSurface.on(`mouseup.rotatable${this.cid}`, this.drop);
        this.$dragSurface.on(`touchend.rotatable${this.cid}`, this.drop);

        const parentCoords = this.$el.parent().offset();

        this.target = $(`#${this.viewModel.get('id')}`);
        this.angle = this.viewModel.model.get('rotation');
        this.imageX = parentCoords.left + this.viewModel.get('handlePosition').left + (this.viewModel.get('handleSize').width / 2);
        this.imageY = parentCoords.top + this.viewModel.get('handlePosition').top + (this.viewModel.get('handleSize').height / 2);

        this.viewModel.trigger('rotatestart');
        eventBus.trigger(events.rotateStart, this.viewModel);
    },

    stopRotation: function() {
        this.$dragSurface.off(`.rotatable${this.cid}`);

        if (this.angle !== this.viewModel.model.get('rotation')) {
            commandDispatcher.changeAttributes({ viewModel: this.viewModel, attributes: { rotation: this.angle } });
        }

        this.viewModel.trigger('rotatestop');
        eventBus.trigger(events.rotateStop, this.viewModel);
    },

    grab: function(e) {
        e.preventDefault();
        e.stopPropagation();
        this.startRotation();
    },

    drop: function() {
        this.stopRotation();
    }
});
