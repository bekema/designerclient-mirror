const _ = require('underscore');
const config = require('configuration/components/Snapping');

/* eslint-disable no-magic-numbers */

const createAboveBaseline = function(box1, box2, boxes, minLeft, maxRight, distance, heightToLastBaseline, viewModelHeight, i) {
    const above = box1.baseline - distance;
    if (above - heightToLastBaseline + viewModelHeight > box1.top) {
        return null;
    }

    for (let k = i - 1; k >= 0; k--) {
        const box = boxes[k];
        // The next closest box is far enough above that this baseline is safe.
        if (box.bottom < above - heightToLastBaseline) {
            break;
        }

        // Don't create baselines that would cause overlaps.
        if (box.right > minLeft && box.left < maxRight) {
            return null;
        }
    }

    return {
        baseline: above,
        left: minLeft,
        right: maxRight,
        distance,
        type: 'above'
    };
};

const createBelowBaseline = function(box1, box2, boxes, minLeft, maxRight, distance, heightToFirstBaseline, viewModelHeight, j) {
    if (Math.abs(distance) < heightToFirstBaseline) {
        return null;
    }

    const below = box2.baseline + distance;

    for (let k = j + 1; k < boxes.length; k++) {
        const box = boxes[k];
        // The next closest box is far enough below that this baseline is safe.
        if (box.top > below - heightToFirstBaseline + viewModelHeight) {
            break;
        }

        // Don't create baselines that would cause overlaps.
        if (box.right > minLeft && box.left < maxRight) {
            return null;
        }
    }

    return {
        baseline: below,
        left: minLeft,
        right: maxRight,
        distance,
        type: 'below'
    };
};

const createCenterBaseline = function(box1, box2, boxes, minLeft, maxRight, heightToFirstBaseline, viewModelHeight) {
    const distance = Math.round((box2.baseline - box1.baseline) / 2);
    const center = box1.baseline + distance;

    if (center - heightToFirstBaseline < box1.bottom || center - heightToFirstBaseline + viewModelHeight > box2.top) {
        return null;
    }

    return {
        baseline: center,
        left: minLeft,
        right: maxRight,
        distance,
        type: 'center'
    };
};

const getBaselines = function(viewModel) {
    let i, j, vm, box;
    const occupiedBaselines = {};
    // Short circuit because I'm not supporting non-zero rotation.
    const rotation = viewModel.model.get('rotation');
    if (rotation !== 0) {
        return occupiedBaselines;
    }

    let boxes = [];

    const viewModelHeight = viewModel.get('previewSize').height;
    const viewModelTop = viewModel.get('previewPosition').top;
    const baselines = viewModel.get('baselines');
    const heightToFirstBaseline = baselines[0] - viewModelTop;
    const heightToLastBaseline = baselines[baselines.length - 1] - viewModelTop;

    // Which other view models should we consider?
    const viewModels = viewModel.parent.itemViewModels.filter(function(itemViewModel) {
        return itemViewModel !== viewModel
            && itemViewModel.has('baselines')
            && !itemViewModel.get('selected')
            && itemViewModel.model.get('rotation') === rotation;
    });

    const addBox = function(previewBox, baseline, baselineIndex) {
        previewBox.top += previewBox.lineHeight * baselineIndex;

        // TODO Optimization: Insert in sorted order.
        boxes.push({
            left: previewBox.left,
            right: previewBox.right,
            top: previewBox.top,
            bottom: previewBox.top + previewBox.lineHeight,
            baseline
        });

        // Mark each of these baselines as occupied - we'll need this when determining what guidelines to show.
        occupiedBaselines[baseline] = baseline;
        // Due to unavoidable rounding errors, I'm creating some "close enough" baselines.
        occupiedBaselines[baseline - 1] = baseline;
        occupiedBaselines[baseline + 1] = baseline;
    };

    // Construct a sorted list of all possible boxes (top, right, left, bottom, and baseline), where we create one box for every row
    // of text on the design.
    for (i = 0; i < viewModels.length; i++) {
        vm = viewModels[i];
        const previewPosition = vm.get('previewPosition');
        const previewSize = vm.get('previewSize');
        box = {
            top: previewPosition.top,
            left: previewPosition.left,
            right: previewPosition.left + previewSize.width,
            lineHeight: previewSize.height / vm.get('baselines').length
        };

        _.each(vm.get('baselines'), _.partial(addBox, box));
    }

    boxes = _.sortBy(boxes, function(b) {
        return b.top;
    });

    const targets = [];
    let target;

    for (i = 0; i < boxes.length - 1; i++) {
        const box1 = boxes[i];

        for (j = i + 1; j < boxes.length; j++) {
            const box2 = boxes[j];
            // At least part of box1 must be directly above box2, and they must not be vertically overlapping.
            if (box2.right > box1.left && box2.left < box1.right && box2.top >= box1.baseline) {

                const minLeft = Math.min(box1.left, box2.left);
                const maxRight = Math.max(box1.right, box2.right);
                const distance = box2.baseline - box1.baseline;

                target = createAboveBaseline(box1, box2, boxes, minLeft, maxRight, distance, heightToLastBaseline, viewModelHeight, i);
                if (target) {
                    targets.push(target);
                }

                target = createBelowBaseline(box1, box2, boxes, minLeft, maxRight, distance, heightToFirstBaseline, viewModelHeight, j);
                if (target) {
                    targets.push(target);
                }

                target = createCenterBaseline(box1, box2, boxes, minLeft, maxRight, heightToFirstBaseline, viewModelHeight);
                if (target) {
                    targets.push(target);
                }

                break;
            }
        }
    }

    return {
        targets,
        occupiedBaselines
    };
};

const getGuidelines = function(target, left, right, maxHeight, occupiedBaselines) {
    const guidelines = [];
    let i, occupiedBaseline;
    const minLeft = Math.min(target.left, left);
    const maxRight = Math.max(target.right, right);

    const createGuideline = function(at) {
        const actualBaseline = occupiedBaselines[at];
        if (_.isNumber(actualBaseline)) {
            guidelines.push({
                height: 1,
                left: minLeft - 15,
                width: maxRight - minLeft + 30,
                top: actualBaseline
            });
            return actualBaseline;
        }
        return null;
    };

    if (target.distance > 0) {
        // Show guidelines for the existing baselines that this now lines up with.
        for (i = target.baseline + target.distance; i < maxHeight;) {
            occupiedBaseline = createGuideline(i);
            if (!_.isNumber(occupiedBaseline)) {
                break;
            }
            i = occupiedBaseline + target.distance;
        }

        for (i = target.baseline - target.distance; i > 0;) {
            occupiedBaseline = createGuideline(i);
            if (!_.isNumber(occupiedBaseline)) {
                break;
            }
            i = occupiedBaseline - target.distance;
        }
    }

    // Create the actual guideline.
    guidelines.push({
        height: 1,
        left: minLeft - 15,
        width: maxRight - minLeft + 30,
        top: target.baseline
    });
    return guidelines;
};

const VerticalAlignmentStrategy = function() { };
_.extend(VerticalAlignmentStrategy.prototype, {

    start: function(viewModel) {
        this.cache = getBaselines(viewModel);
    },

    stop: function() {
        delete this.cache;
    },

    getSnapsAndGuidelines: function(viewModel, delta) {
        if (!this.cache) {
            throw new Error('Strategy has not been started');
        }

        const position = viewModel.get('snapPosition');
        const size = viewModel.get('snapSize');

        const left = position.left + delta.left;
        const right = position.left + size.width + delta.left;
        const baselines = viewModel.get('baselines');
        const firstBaseline = baselines[0] + delta.top;
        const lastBaseline = baselines[baselines.length - 1] + delta.top;

        let bestTarget, bestAbsoluteDistance;
        _.each(this.cache.targets, function(target) {
            if (left < target.right && right > target.left) {
                const baseline = target.type === 'above' ? lastBaseline : firstBaseline;
                const distance = target.baseline - baseline;
                const absoluteDistance = Math.abs(distance);
                if (absoluteDistance < config.snapThreshold) {
                    if (!bestTarget || absoluteDistance < bestAbsoluteDistance) {
                        bestAbsoluteDistance = absoluteDistance;
                        bestTarget = target;
                    }
                }
            }
        });

        if (!bestTarget) {
            return null;
        }

        return {
            snaps: {
                top: bestTarget.baseline - (bestTarget.type === 'above' ? lastBaseline : firstBaseline),
                left: null
            },
            guidelines: {
                top: getGuidelines(bestTarget, left, right, viewModel.parent.get('size').height, this.cache.occupiedBaselines),
                left: []
            }
        };
    }
});

module.exports = VerticalAlignmentStrategy;
