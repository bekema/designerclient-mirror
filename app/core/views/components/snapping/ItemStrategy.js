const _ = require('underscore');
const config = require('configuration/components/Snapping');
const View = require('views/View');
const _without = require('lodash-compat/array/without');

/* eslint-disable no-magic-numbers */

module.exports = View.extend({
    getRules: function(viewModel, otherViewModel) {
        if (viewModel === otherViewModel) {
            return null;
        }

        // Don't snap to anything selected, because it's taking part in the action.
        if (viewModel.get('selected')) {
            return null;
        }

        // If rotation is something other than right angles, it can't be snapped
        //TODO rotation snapping
        const currentRotation = viewModel.model.get('rotation');
        if (currentRotation % 90 !== 0) {
            return null;
        }

        //TODO rotation snapping
        const axis = currentRotation % 180 === 0 ? 'x' : 'y';
        const position = viewModel.get('snapPosition');
        const size = viewModel.get('snapSize');
        const baselines = viewModel.get('baselines');

        // Don't snap to a field with a different rotation.
        if (currentRotation !== otherViewModel.model.get('rotation')) {
            return null;
        }

        if (axis === 'x') {
            return {
                left: [position.left],
                right: [position.left + size.width],
                baseline: baselines
            };
        } else {
            return {
                top: [position.top],
                bottom: [position.top + size.height],
                baseline: baselines
            };
        }
    },

    filterTargets: function(targets, coordinateNames) {
        let bestTargets = [];

        _.each(coordinateNames, function(coordinateName) {
            if (targets[coordinateName]) {
                const currentTarget = targets[coordinateName];
                const currentAbsoluteDistance = Math.abs(currentTarget.distance);
                const bestTarget = bestTargets[0]; // All the targets will be the same distance away, so we only need to compare the first.
                if (!bestTarget) {
                    bestTargets.push(currentTarget);
                    return;
                }

                const bestAbsoluteDistance = Math.abs(bestTarget.distance);
                // If we've found a better (closer) target...
                if (currentAbsoluteDistance < bestAbsoluteDistance) {
                    bestTargets = [currentTarget];
                    return;
                }

                // If we've found a target that is the same distance AND the same direction, then push it into the list. This
                // is what lets us show two guidelines at once if we're going to be lined up with both at once.
                if (currentTarget.distance === bestTarget.distance) {
                    bestTargets.push(currentTarget);
                }
            }
        });

        return bestTargets;
    },

    getBestTargets: function(cache, candidate) {
        const bestTargets = {};

        _.each(cache, function(target) {
            _.each(target.rules, function(coordinates, coordinateName) {
                _.each(coordinates, function(targetCoordinate) {
                    const currentCoordinate = candidate[coordinateName];
                    if (_.isUndefined(currentCoordinate)) {
                        return;
                    }
                    const distance = targetCoordinate - currentCoordinate;
                    const absoluteDistance = Math.abs(distance);
                    if (absoluteDistance < config.snapThreshold) {
                        const currentBest = bestTargets[coordinateName];
                        if (!currentBest || absoluteDistance < Math.abs(currentBest.distance)) {
                            bestTargets[coordinateName] = {
                                distance,
                                currentCoordinate,
                                targetCoordinate,
                                guidelineCoordinates: {
                                    minLeft: Math.min(candidate.left, target.position.left),
                                    maxRight: Math.max(candidate.right, target.position.right),
                                    minTop: Math.min(candidate.top, target.position.top),
                                    maxBottom: Math.max(candidate.bottom, target.position.bottom)
                                }
                            };
                        } else if (absoluteDistance === Math.abs(currentBest.distance)) {
                            currentBest.guidelineCoordinates = {
                                minLeft: Math.min(currentBest.guidelineCoordinates.minLeft, target.position.left),
                                maxRight: Math.max(currentBest.guidelineCoordinates.maxRight, target.position.right),
                                minTop: Math.min(currentBest.guidelineCoordinates.minTop, target.position.top),
                                maxBottom: Math.max(currentBest.guidelineCoordinates.maxBottom, target.position.bottom)
                            };
                        }
                    }
                });
            });
        });

        return bestTargets;
    },

    getGuidelines: function(positionalProperty, actualCoordinate, targets) {
        return targets.map(function(target) {
            // x-based alignment: vertical guideline
            if (positionalProperty === 'left') {
                return {
                    // Adjust for the margin of the handle being -1 (which itself makes sure the border is at the right spot)
                    left: target.targetCoordinate + (Math.round(actualCoordinate) < Math.round(target.currentCoordinate) ? 0 : -1),
                    top: target.guidelineCoordinates.minTop - 10,
                    height: target.guidelineCoordinates.maxBottom - target.guidelineCoordinates.minTop + 20,
                    width: 1
                };
            } else {
                return {
                    // y-based alignment: Guideline shows horizontally
                    left: target.guidelineCoordinates.minLeft - 10,
                    top: target.targetCoordinate + (Math.round(actualCoordinate) < Math.round(target.currentCoordinate) ? 0 : -1),
                    height: 1,
                    width: target.guidelineCoordinates.maxRight - target.guidelineCoordinates.minLeft + 20
                };
            }
        });
    },

    start: function(viewModel) {
        //Ignore all if this is a curved wordart item
        if (config.isSnappingDisabled(viewModel)) {
            this.cache = [];
        } else {
            this.cache = _(viewModel.parent.itemViewModels.toArray())
            .filter(function(siblingViewModel) {
                // Ignore siblings that we don't want to snap to
                if (config.ignoreSnappingTo(siblingViewModel)) {
                    return false;
                }

                //Get all siblings that this viewmodel is configured to snap to in the config file
                const module = viewModel.get('module');

                if (module !== null) {
                    return _.contains(config.snapOptions[module], siblingViewModel.get('module'));
                }

                return false;
            })
            .map(function(siblingViewModel) {
                const rules = this.getRules(siblingViewModel, viewModel);
                if (!rules) {
                    return null;
                }
                const position = siblingViewModel.get('previewPosition') || siblingViewModel.get('handlePosition');
                const size = siblingViewModel.get('previewSize') || siblingViewModel.get('handleSize');
                return {
                    rules,
                    position: {
                        left: position.left,
                        top: position.top,
                        right: position.left + size.width,
                        bottom: position.top + size.height
                    }
                };
            }, this)
            .valueOf();

            this.cache = _without(this.cache, null);
        }

        // Add a snap line in the middle of the canvas.
        if (!config.isSnappingToCenterDisabled(viewModel)) {
            const parentSize = viewModel.parent.get('size');
            this.addSnapLinesAlongCenter(parentSize);
        }
    },

    addSnapLinesAlongCenter: function(parentSize) {
        this.cache.push({
            position: {
                top: 0,
                left: Math.round(parentSize.width / 2),
                bottom: parentSize.height,
                right: parentSize.width
            },
            rules: {
                center: [Math.round(parentSize.width / 2)]
            }
        });
    },

    addSnapLinesOnDocumentEdge: function(parentSize) {
        this.cache.push({
            position: {
                top: 0,
                left: 0,
                bottom: parentSize.height,
                right: parentSize.width
            },
            rules: {
                top: [0],
                left: [0],
                bottom: [parentSize.height],
                right: [parentSize.width]
            }
        });
    },

    addSnapLinesOnSafteyMargin: function(safetyMarginDimensions, safetyMarginWidth) {
        this.cache.push({
            position: {
                top: safetyMarginWidth,
                left: safetyMarginWidth,
                bottom: safetyMarginWidth + safetyMarginDimensions.height,
                right: safetyMarginWidth + safetyMarginDimensions.width
            },
            rules: {
                top: [safetyMarginWidth],
                left: [safetyMarginWidth],
                bottom: [safetyMarginWidth + safetyMarginDimensions.height],
                right: [safetyMarginWidth + safetyMarginDimensions.width]
            }
        });
    },

    stop: function() {
        delete this.cache;
    },

    getCandidate: function(viewModel, delta) {
        const position = viewModel.get('snapPosition');
        const size = viewModel.get('snapSize');

        const candidate = {
            left: position.left + delta.left,
            right: position.left + size.width + delta.left,
            center: position.left + Math.round(size.width / 2) + delta.left,
            top: position.top + delta.top,
            bottom: position.top + size.height + delta.top,
            baseline: viewModel.get('baselines')[0],
            rotation: viewModel.model.get('rotation')
        };

        if (candidate.rotation === 0 || candidate.rotation === 180) {
            candidate.baseline += delta.top;
        } else {
            candidate.baseline += delta.left;
        }

        return candidate;
    },

    setSnapsAndGuidelines: function(candidate, bestTargets) {
        let positionalProperty = 'left';
        let baselineProperty = 'top';
        let filterableCoordinateNames = ['left', 'right', 'center'];
        if (candidate.rotation === 90 || candidate.rotation === 270) {
            positionalProperty = 'top';
            baselineProperty = 'left';
            filterableCoordinateNames = ['top', 'bottom'];
        }

        const filteredTargets = this.filterTargets(bestTargets, filterableCoordinateNames);
        if (filteredTargets.length === 0 && !bestTargets.baseline) {
            return null;
        }

        const snaps = { top: null, left: null };
        const guidelines = { top: [], left: [] };
        if (filteredTargets.length > 0) {
            snaps[positionalProperty] = filteredTargets[0].distance;
            guidelines[positionalProperty] = this.getGuidelines(positionalProperty, candidate[positionalProperty], filteredTargets);
        }
        if (bestTargets.baseline && candidate.baseline) {
            snaps[baselineProperty] = bestTargets.baseline.distance;
            guidelines[baselineProperty] = this.getGuidelines(baselineProperty, candidate[baselineProperty], [bestTargets.baseline]);
        }

        return {
            snaps,
            guidelines
        };
    },

    getSnapsAndGuidelines: function(viewModel, delta) {
        if (!this.cache) {
            throw new Error('Strategy has not been started');
        }

        const candidate = this.getCandidate(viewModel, delta);
        const bestTargets = this.getBestTargets(this.cache, candidate);

        return this.setSnapsAndGuidelines(candidate, bestTargets);
    }
});
