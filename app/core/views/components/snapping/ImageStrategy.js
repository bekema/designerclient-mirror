const _ = require('underscore');
const config = require('configuration/components/Snapping');
const ItemStrategy = require('views/components/snapping/ItemStrategy');
/* eslint-disable no-magic-numbers */

module.exports = ItemStrategy.extend({
    start: function(viewModel) {
        ItemStrategy.prototype.start.apply(this, arguments);

        // Add a snap line in the middle of the canvas.
        const parentSize = viewModel.parent.get('size');

        this.cache.push({
            position: {
                top: 0,
                left: Math.round(parentSize.width / 2),
                bottom: parentSize.height,
                right: parentSize.width
            },
            rules: {
                centerY: [Math.round(parentSize.width / 2)]
            }
        });

        this.cache.push({
            position: {
                top: Math.round(parentSize.height / 2),
                left: 0,
                bottom: Math.round(parentSize.height / 2),
                right: parentSize.width
            },
            rules: {
                centerX: [Math.round(parentSize.height / 2)]
            }
        });
    },

    getClosestDistance: function(distances) {
        let bestDistance = null;
        _.each(distances, function(distance) {
            const absoluteDistance = Math.abs(distance);
            if (bestDistance === null || absoluteDistance < Math.abs(bestDistance)) {
                bestDistance = distance;
            }
        });
        return bestDistance;
    },

    getCandidate: function(viewModel, delta) {
        const position = viewModel.get('handlePosition');
        const size = viewModel.get('handleSize');

        const candidate = {
            left: position.left + delta.left,
            right: position.left + size.width + delta.left,
            centerY: position.left + Math.round(size.width / 2) + delta.left,
            centerX: position.top + Math.round(size.height / 2) + delta.top,
            top: position.top + delta.top,
            bottom: position.top + size.height + delta.top,
            baseline: viewModel.get('baselines')[0] + delta.top,
            rotation: viewModel.model.get('rotation'),
            viewModel: viewModel
        };

        //TODO rotation snapping
        if (candidate.rotation === 0 || candidate.rotation === 180) {
            candidate.baseline += delta.top;
        } else {
            candidate.baseline += delta.left;
        }

        return candidate;
    },

    setSnapsAndGuidelines: function(candidate, bestTargets) {
        let positionalProperty = 'left';
        let baselineProperty = 'top';
        let filterableCoordinateNames = ['left', 'right', 'centerY'];
        //TODO rotation snapping
        if (candidate.rotation === 90 || candidate.rotation === 270) {
            positionalProperty = 'top';
            baselineProperty = 'left';
            filterableCoordinateNames = ['top', 'bottom', 'centerX'];
        }

        const filteredTargets = this.filterTargets(bestTargets, filterableCoordinateNames);

        const snaps = { top: null, left: null };
        const guidelines = { top: [], left: [] };

        // if the item is rotated, then don't snap to anything:
        //TODO: fix up rotation and snapping.
        if (candidate.rotation % 90 !== 0) {
            return {
                snaps,
                guidelines
            };
        }

        if (filteredTargets.length > 0) {
            snaps[positionalProperty] = filteredTargets[0].distance;
            guidelines[positionalProperty] = this.getGuidelines(positionalProperty, candidate[positionalProperty], filteredTargets);
        }
        if (bestTargets.baseline) {
            snaps[baselineProperty] = bestTargets.baseline.distance;
            guidelines[baselineProperty] = this.getGuidelines(baselineProperty, candidate[baselineProperty], [bestTargets.baseline]);
        }
        if (candidate.rotation === 90 || candidate.rotation === 270) {
            if (bestTargets.centerY) {
                snaps[baselineProperty] = bestTargets.centerY.distance;
                guidelines[baselineProperty] = this.getGuidelines(baselineProperty, candidate[baselineProperty], [bestTargets.centerY]);
            }
        } else {
            if (bestTargets.centerX) {
                snaps[baselineProperty] = bestTargets.centerX.distance;
                guidelines[baselineProperty] = this.getGuidelines(baselineProperty, candidate[baselineProperty], [bestTargets.centerX]);
            }
        }

        //snap to full bleed line or safety margin, not in between
        //If not safety margin then just snap to the full bleed line
        const leftCandidates = [];
        const topCandidates = [];
        const fullBleedSize = candidate.viewModel.parent.get('size');
        const fullBleedLeftDistance = -candidate.left;
        const fullBleedRightDistance = fullBleedSize.width - candidate.right;
        const fullBleedTopDistance = -candidate.top;
        const fullBleedBottomDistance = fullBleedSize.height - candidate.bottom;

        const canvasSafeArea = candidate.viewModel.parent.get('zoomedSafeArea');
        const safetyMarginLeftDistance = canvasSafeArea.left - candidate.left;
        const safetyMarginRightDistance = canvasSafeArea.left + canvasSafeArea.width - candidate.right;
        const safetyMarginTopDistance = canvasSafeArea.top - candidate.top;
        const safetyMarginBottomDistance = canvasSafeArea.top + canvasSafeArea.height - candidate.bottom;

        if (safetyMarginLeftDistance > -config.snapThreshold && candidate.left > -config.snapThreshold) {
            leftCandidates.push(safetyMarginLeftDistance);
            leftCandidates.push(fullBleedLeftDistance);
        }
        if (safetyMarginRightDistance < config.snapThreshold && candidate.right < fullBleedSize.width + config.snapThreshold) {
            leftCandidates.push(safetyMarginRightDistance);
            leftCandidates.push(fullBleedRightDistance);
        }
        if (safetyMarginTopDistance > -config.snapThreshold && candidate.top > -config.snapThreshold) {
            topCandidates.push(safetyMarginTopDistance);
            topCandidates.push(fullBleedTopDistance);
        }
        if (safetyMarginBottomDistance < config.snapThreshold && candidate.bottom < fullBleedSize.height + config.snapThreshold) {
            topCandidates.push(safetyMarginBottomDistance);
            topCandidates.push(fullBleedBottomDistance);
        }

        const closestTop = this.getClosestDistance(topCandidates);
        const closestLeft = this.getClosestDistance(leftCandidates);

        if (closestTop) {
            snaps.top = closestTop;
            guidelines.top = [];
        }
        if (closestLeft) {
            snaps.left = closestLeft;
            guidelines.left = [];
        }

        return {
            snaps,
            guidelines
        };
    }
});
