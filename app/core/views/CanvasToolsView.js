const View = require('views/View');
const CanvasColorPicker = require('views/components/canvastools/CanvasColorPicker');

module.exports = View.extend({
    className: 'dcl-canvas-tools',

    initialize: function() {
        this.colorPicker = new CanvasColorPicker({ viewModel: this.viewModel, parent: this });
        this.$el.append(this.colorPicker.render().el);
    },

    render: function() {
        this.$el.css({ top: `${this.viewModel.get('top')}px`, left: `${this.viewModel.get('left')}px` });
        return View.prototype.render.apply(this, arguments);
    }
});
