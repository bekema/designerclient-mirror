const _ = require('underscore');

// binds methods specified in defaults and options to the context of
// the class that defines them.
module.exports = function(context) {
    const options = context.options;
    _.each(options, (value, key) => {
        if (_.isFunction(value)) {
            options[key] = options[key].bind(context);
        }
    });
};
