// The `CommandDispatcher` is a simple pub/sub singleton that is reponsible for handling all actions
// that will result in changing document models. Views should trigger events using this.
const $ = require('jquery');
const _ = require('underscore');
const Backbone = require('backbone');

let subscribedEvents = {};
const dispatcher = _.extend({}, Backbone.Events);

module.exports = {

    // Subscribe commands to events. This can either be an object of { `eventName` : `commandModule` }, or two arguments: `eventName` and `commandModule`.
    subscribe: function(events) {
        // Convert string arguments into an object.
        if (_.isString(events)) {
            const name = events;
            const module = arguments[1];
            events = {};
            events[name] = module;
        }

        _.each(events, (commandModule, eventName) => {
            if (subscribedEvents[eventName] && subscribedEvents[eventName][commandModule]) {
                throw new Error(`${commandModule} has already been attached to the ${eventName} event`);
            }
        });

        const commandFn = (Command, e, commandDeferred) => {
            const cmd = new Command();
            const result = cmd.execute(e);
            // TODO Let the commands resolve this internally if they're actually doing something async.
            if (commandDeferred) {
                commandDeferred.resolve(result);
            }
        };

        _.each(events, (moduleName, eventName) => {
            if (!subscribedEvents[eventName]) {
                subscribedEvents[eventName] = {};
            }

            const Command = require(`commands/${moduleName}`);

            // Create a callback handler to execute the command.
            subscribedEvents[eventName][moduleName] = _.partial(commandFn, Command);

            // And actually hook up the events.
            dispatcher.on(eventName, subscribedEvents[eventName][moduleName]);

            // Add a method shorthand to use in place of trigger
            this[eventName] = _.bind(this.trigger, this, eventName);
        });
    },

    // Unsubscribe a command from an event. Passing no arguments will unsubscribe all events.
    unsubscribe: function(eventName, commandModule) {
        if (!arguments.length) {
            dispatcher.off();
            subscribedEvents = {};
        }
        if (subscribedEvents[eventName] && subscribedEvents[eventName][commandModule]) {
            dispatcher.off(eventName, subscribedEvents[eventName][commandModule]);
            delete subscribedEvents[eventName][commandModule];
        }
    },

    // Triggering a command returns a promise that is resolved when the command is complete.
    trigger: function(command, options) {
        const deferred = new $.Deferred();
        dispatcher.trigger(command, options, deferred);
        return deferred.promise();
    }
};
