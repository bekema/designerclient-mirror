const _ = require('underscore');
const uiConfig = require('publicApi/configuration/ui');
const colorConverter = require('util/ColorConverter');
const documentRepository = require('DocumentRepository');
const eventBus = require('EventBus');
const events = eventBus.events;
const util = require('util/Util');
const ColorThief = require('util/ColorThief');
const colorThief = new ColorThief();
const globalConfig = require('configuration/Global');
const designerContext = require('DesignerContext');
const clients = require('services/clientProvider');

const DEFAULT_PALETTE_COLORS = 10;
const DEFAULT_PALETTE_COLORS_QUALITY = 5;

// colorHistory should be an array of objects that consist of hex, r, g, b, a, c, m, y, and k
// TODO add a, c, m, y, and k
const colorHistory = [];
const dominantSceneColors = {};
let enabled = false;

const addToHistory = function(color) {
    const index = _.findIndex(colorHistory, (c => c.hex === color.hex));
    if (index < 0) {
        color.json = JSON.stringify(color);
        colorHistory.unshift(color);
    } else {
        const c = colorHistory[index];
        colorHistory.splice(index, 1);
        colorHistory.unshift(c);
    }
};

const getPrintStandardColors = function() {
    return colorConverter.mapColors(uiConfig.colors.palettes.print);
};

const getEmbroideryStandardColors = function() {
    const threadDefaultColors = clients.embroidery.threads.map(thread => ({ thread: thread.id }));

    const colors = uiConfig.colors.palettes.embroidery || threadDefaultColors;
    return colorConverter.mapColors(colors);
};

const standardColors = {
    print: getPrintStandardColors,
    embroidery: getEmbroideryStandardColors
};

module.exports = {

    enable: function() {
        if (!enabled) {
            eventBus.on(events.colorSelected, addToHistory);
        }
        enabled = true;
    },

    disable: function() {
        if (enabled) {
            eventBus.off(events.colorSelected, addToHistory);
        }
        enabled = false;
    },

    getHistory: function() {
        return colorHistory;
    },

    getProvidedColors: function() {
        return new Promise(resolve => {
            if (!this.defaultColors) {
                this.defaultColors = standardColors[designerContext.designTechnology]();
            }
            resolve(this.defaultColors);
        });
    },

    //TODO: Perhaps find a way to preserve cmyk and hex colors when assigning to itemViewModels (so that we can retrieve both)
    getAllColorsOnCanvas: function() {
        const colors = [];
        documentRepository.getAllCanvasViewModels().forEach(canvas => {
            canvas.itemViewModels.forEach(itemViewModel => {
                itemViewModel.get('colors').filter(hex => hex).forEach(hex => {
                    const color = { hex };
                    color.json = JSON.stringify(color);
                    colors.push(color);
                });
            });
        });
        return _.uniq(colors, false, color => color.hex);
    },

    getDominantSceneColor: function(canvasViewModel) {
        if (dominantSceneColors[canvasViewModel.id]) {
            return Promise.resolve(dominantSceneColors[canvasViewModel.id]);
        } else {
            if (!globalConfig.scenes) {
                return null;
            }
            //Canvases start at ID 1, so subtract 1 to get the proper array mapping
            const sceneUri = globalConfig.scenes[canvasViewModel.model.id - 1].designAreaSceneUri;
            const encodedUri = window.encodeURIComponent(sceneUri);
            const imageUri = `https://rendering.documents.cimpress.io/v1/dcl/preview?width=300&scene=${encodedUri}`;
            return util.getImageFromUrl(imageUri)
                .then(image => {
                    const color = colorThief.getColor(image);
                    dominantSceneColors[canvasViewModel.id] = color;
                    return color;
                });
        }
    },

    getColorPalette: function(image, colorCount = DEFAULT_PALETTE_COLORS, colorQuality = DEFAULT_PALETTE_COLORS_QUALITY) {
        return colorThief.getPalette(image, colorCount, colorQuality);
    },

    getMostVisibleDefaultColor: function(canvasViewModel) {
        // Override always takes priority
        if (uiConfig.colors.defaults.override) {
            return Promise.resolve(uiConfig.colors.defaults.override);
        }
        return this.getDominantSceneColor(canvasViewModel)
            .then(dominantSceneColor => {
                if (!dominantSceneColor) {
                    return uiConfig.colors.defaults.dark;
                } else {
                    const rgb = {
                        r: dominantSceneColor[0],
                        g: dominantSceneColor[1],
                        b: dominantSceneColor[2]
                    };

                    const grey = colorConverter.rgb2grey(rgb);
                    if (grey <= 127) { // eslint-disable-line no-magic-numbers
                        return uiConfig.colors.defaults.light;
                    } else {
                        return uiConfig.colors.defaults.dark;
                    }
                }
            });
    }
};
