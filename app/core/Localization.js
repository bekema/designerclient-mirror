const i18n = require('i18next');
const localizationFile = require('publicApi/configuration/localization');

module.exports = {

    init: function() {
        i18n.init({
            fallbackLng: 'en',
            resources: localizationFile,
            ns: {
                namespaces: ['translation'],
                defaultNs: 'translation'
            }
        });
    },

    getText: function(key) {
        const translation = i18n.t(key);
        if (translation === key) {
            // TODO: throw error or retrieve translation
        }

        return translation;
    },

    changeLanguage: function() {
        i18n.changeLanguage(localizationFile.language);
    },

    addNewKeyValue: function(lng, key, value) {
        i18n.addResource(lng, 'translation', key, value);
    }
};