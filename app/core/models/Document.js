const Backbone = require('backbone');

module.exports = Backbone.NestedModel.extend({

    __name__: 'Document',

    defaults: {
        module: 'Document'
    },

    constructor: function() {
        this.canvases = new Backbone.NestedCollection();
        Backbone.NestedModel.apply(this, arguments);
    }

});
