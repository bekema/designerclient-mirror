const UploadedImage = require('models/UploadedImage');

module.exports = UploadedImage.extend({
    defaults: {
        'module': 'EmbroideryImage'
    }
});
