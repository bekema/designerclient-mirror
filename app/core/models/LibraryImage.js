const Item = require('models/Item');

module.exports = Item.extend({
    defaults: {
        'module': 'LibraryImage'
    }
});
