const _ = require('underscore');
const Item = require('models/Item');

module.exports = Item.extend({

    __name__: 'TextField',

    defaults: _.extend({}, Item.prototype.defaults, {
        'module': 'textfield'
    })
});
