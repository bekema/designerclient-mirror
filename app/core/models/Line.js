const _ = require('underscore');
const Item = require('models/Item');

module.exports = Item.extend({

    __name__: 'Line',

    defaults: _.extend({}, Item.prototype.defaults, {
        'module': 'line'
    })
});
