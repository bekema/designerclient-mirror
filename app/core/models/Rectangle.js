const _ = require('underscore');
const Item = require('models/Item');

module.exports = Item.extend({

    __name__: 'Rectangle',

    defaults: _.extend({}, Item.prototype.defaults, {
        'module': 'rectangle'
    })
});
