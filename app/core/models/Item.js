const Backbone = require('backbone');

module.exports = Backbone.NestedModel.extend({
    __name__: 'Item'
});
