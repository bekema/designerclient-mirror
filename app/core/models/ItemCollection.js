const Backbone = require('backbone');

module.exports = Backbone.NestedCollection.extend({
    comparator: function(item) {
        return item.get('zIndex');
    },

    byModule: function(moduleName) {
        return this.where({
            module: moduleName
        });
    }
});
