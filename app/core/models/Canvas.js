const _ = require('underscore');
const Backbone = require('backbone');
const ItemCollection = require('models/ItemCollection');

module.exports = Backbone.NestedModel.extend({

    //TODO what is this __name__ property and why does it exist?
    __name__: 'Canvas',

    constructor: function() {
        this.items = new ItemCollection();
        Backbone.NestedModel.apply(this, arguments);
    },

    getBackgroundColor: function() {
        const backgroundItem = _.find(this.items.models, model => model.get('isBackground'));

        //TODO this should probably come from the scene, or something not defaulted to white?
        return backgroundItem ? backgroundItem.get('fillColor') : '#FFFFFF';
    }
});
