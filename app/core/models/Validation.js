const Backbone = require('backbone');
const util = require('util/Util');

module.exports = Backbone.NestedModel.extend({
    defaults: {
        validationId: '', //Should be a generatedUUID unless it is the ID of another viewModel
        name: '', //Name of the validation
        type: '', //The type of validation (itemViewModel, upload, document, etc)
        message: '', //Message related to the validation
        severity: '', //Severity (currently only "warning" and "success" is recognized)
        acknowledged: false, //Indicates whether the user has acknowledged the validation
        location: {}, //x, y coordinates or a string indicating a known location
        data: {} //A location to put extra data about the validation.
    },

    initialize: function(options) {
        if (!options.validationId) {
            this.set('validationId', util.generateUUID());
        }
        if (options.data && options.data.itemViewModel) {
            this.listenTo(options.data.itemViewModel, 'destroy', this.remove);
        }
    },

    // Called when the view is destroyed
    remove: function() {
        this.destroy();
    }
});
