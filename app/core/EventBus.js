const Backbone = require('backbone');
const _ = require('underscore');
const events = require('publicApi/events');

const eventBus = {};
_.extend(eventBus, Backbone.Events);

module.exports = {
    on() { eventBus.on(...arguments); },
    off(eventName, callback) { eventBus.off(eventName, callback); },
    trigger(eventName, args) { eventBus.trigger(eventName, args); },
    events
};
