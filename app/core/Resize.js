// ## Resizable
// This library manages resizing and replaces jQueryUI's resizable.
// It shares a few snippets from jQuery UI's resizable, and is thus covered by the MIT license?

/* eslint-disable no-magic-numbers */

const $ = require('jquery');
const _ = require('underscore');
const util = require('util/Util');

const Resizable = function(el, options) {
    const me = this;

    this.$el = $(el);
    this.touchIdentifier = null;

    let handles = {};

    // Default values
    const defaultOptions = {
        appendTo: function() {
            return 'body';
        },
        handles: 'all',
        maxHeight: null,
        maxWidth: null,
        minHeight: 8,
        minWidth: 8,
        containResize: false,
        originalAspectRatio: null,
        maintainProportions: false,
        // callbacks
        resize: null,
        start: null,
        stop: null
    };

    const adjustMinSizesToAspectRatio = function(aspectRatio) {
        const adjustedMinWidth =  options.minHeight * aspectRatio;
        const adjustedMinHeight = options.minWidth / aspectRatio;

        if (adjustedMinWidth >= options.minWidth && adjustedMinHeight < options.minHeight) {
            me.minWidth = options.minHeight * aspectRatio;
            me.minHeight = options.minHeight;
        } else {
            me.minHeight = options.minWidth / aspectRatio;
            me.minWidth = options.minWidth;
        }
    };

    const cacheData = function(e) {
        me.position = me.$el.position();
        me.previousPosition = _.clone(me.position);
        me.originalPosition = _.clone(me.position);
        me.size = {
            width: me.$el.width(),
            height: me.$el.height()
        };
        me.previousSize = _.clone(me.size);
        me.originalSize = _.clone(me.size);
        me.originalInputPosition = util.getCoordinates(e, me.touchIdentifier);

        if (e.data.axis) {
            me.axis = e.data.axis;
        }

        //If this maintains proportions it will always use the original aspect ratio -
        //otherwise it will calcuate it on mousedown
        //Then the min-sizes are readjusted to those of the current aspect ratio
        if (options.maintainProportions) {
            me.aspectRatio = (options.originalAspectRatio ? options.originalAspectRatio() : (me.originalSize.width / me.originalSize.height)) || 1;
        } else {
            me.aspectRatio = (me.originalSize.width / me.originalSize.height) || 1;
            me.minWidth = options.minWidth;
            me.minHeight = options.minHeight;
        }
    };

    const updateCache = function(data) {
        if (_.isNumber(data.left)) {
            me.previousPosition.left = me.position.left;
            me.position.left = data.left;
        }
        if (_.isNumber(data.top)) {
            me.previousPosition.top = me.position.top;
            me.position.top = data.top;
        }
        if (_.isNumber(data.height)) {
            me.previousSize.height = me.size.height;
            me.size.height = data.height;
        }
        if (_.isNumber(data.width)) {
            me.previousSize.width = me.size.width;
            me.size.width = data.width;
        }
    };

    // Adjust the dimensions to respect the aspect ratio.
    // `dx` and `dy` are the amount thats the mouse position has moved on this event.
    const resizeProportionally = function(data, e, dx, dy, rotation) {
        // Use the new coordinates if they've changed, or fall back to the old unchanged ones.
        const width = _.isNumber(data.width) ? data.width : me.size.width;
        const height = _.isNumber(data.height) ? data.height : me.size.height;
        let newdx, newdy;

        // We will want to restrict the height if the cursor is closest to the N or S edge of the item.
        // We will otherwise want to restrict the width. Essentially the user should always have their cursor resting
        // on the edge of the item, so dragging away from the item will always make it bigger, and towards smaller.
        let restrictHeight;
        if (data.width / data.height > me.aspectRatio) {
            restrictHeight = true;
        } else {
            restrictHeight = false;
        }

        // Always restrict the growth of the dimensions: if the amount that we need to adjust a direction
        // is greater than the amount that they've changed the opposing direction, then just decrease the
        // amount that they have changed that direction.
        if (restrictHeight) {
            data.height = width / me.aspectRatio;
            newdy = dy - (height - data.height);
            if (/n|ne|nw/.test(me.axis)) {
                newdy = dy + (height - data.height);
            } else {
                newdy = dy - (height - data.height);
            }
        } else {
            data.width = height * me.aspectRatio;
            if (/w|sw|nw/.test(me.axis)) {
                newdx = dx + (width - data.width);
            } else {
                newdx = dx - (width - data.width);
            }
        }
        data = me.updateSize[me.axis](e, newdx || dx, newdy || dy, rotation || 0);
        return data;
    };

    // Ensure that dimensions are within the min and max width and height (if specified)
    const respectSize = function(data, e, dx, dy, rotation) {
        const isMaxWidth = _.isNumber(data.width) && options.maxWidth && (options.maxWidth < data.width),
            isMaxHeight = _.isNumber(data.height) && options.maxHeight && (options.maxHeight < data.height),
            isMinWidth = _.isNumber(data.width) && me.minWidth && (me.minWidth > data.width),
            isMinHeight = _.isNumber(data.height) && me.minHeight && (me.minHeight > data.height);

        let newdx, newdy;

        if (isMinWidth) {
            newdx = me.originalSize.width - me.minWidth;
        }
        if (isMinHeight) {
            newdy = me.originalSize.height - me.minHeight;
        }
        if (isMaxWidth) {
            newdx = me.maxWidth - me.originalSize.width;
        }
        if (isMaxHeight) {
            newdy = me.maxHeight - me.originalSize.height;
        }

        if (newdx && /e|se|ne/.test(me.axis)) {
            newdx = -newdx;
        }

        if (newdy && /s|sw|se/.test(me.axis)) {
            newdy = -newdy;
        }

        if (newdx || newdy) {
            data = me.updateSize[me.axis](e, newdx || dx, newdy || dy, rotation || 0);
        }

        if (options.containResize) {
            if (data.left === undefined) {
                data.left = me.position.left;
            }
            if (data.top === undefined) {
                data.top = me.position.top;
            }
            if (data.left < 0) {
                data.width += data.left;
                data.left = 0;
            }
            if (data.top < 0) {
                data.height += data.top;
                data.top = 0;
            }
            if (data.left + data.width > options.maxWidth) {
                data.width = options.maxWidth - data.left;
            }
            if (data.top + data.height > options.maxHeight) {
                data.height = options.maxHeight - data.top;
            }
        }

        if (!data.width && !data.height && !data.left && data.top) {
            data.top = null;
        } else if (!data.width && !data.height && !data.top && data.left) {
            data.left = null;
        }

        return data;
    };

    // Quick little helper method to return the object with positional coordinates that we'll expose to the consumer.
    const getEventData = function() {
        return {
            originalSize: me.originalSize,
            size: me.size,
            previousSize: me.previousSize,
            originalPosition: me.originalPosition,
            position: me.position,
            previousPosition: me.previousPosition,
            helper: me.$helper,
            handle: me.currentHandle.attr('data-handle'),
            options: options
        };
    };

    const unbindResizeHandler = function(fn) {
        $(document).off('mousemove.vpresize touchmove.vpresize', fn);
        /* eslint-disable no-use-before-define */  //circular reference between onResizeStop and unbindResizeHandler
        $(document).off('mouseup.vpresize touchend.vpresize', onResizeStop);
        /* eslint-enable no-use-before-define */
    };

    const onResize = function(e) {
        // Short circuit touch events that aren't for the touch that started the resize.
        const eventShouldNotCauseResize = me.touchIdentifier && !e.originalEvent.changedTouches.some(touch => {
            return touch.identifier === me.touchIdentifier;
        });

        if (eventShouldNotCauseResize) {
            return;
        }

        if (!me.currentHandle) {
            me.currentHandle = $(e.target).closest('.dcl-ui-resizable-handle');
            if (options.start && options.start(e, getEventData()) === false) {
                me.currentHandle = null;
                unbindResizeHandler();
                return;
            }

            if (options.helper) {
                me.$helper = $(options.helper(e));
            }

            if (me.$helper) {
                me.$helper.appendTo(options.appendTo());
            }

            // For consistency with the resize event, don't bubble this.
            me.$el.triggerHandler('resizestart', getEventData());
        }

        if (me.currentHandle) {
            e.preventDefault();

            const currentPosition = util.getCoordinates(e, me.touchIdentifier);
            let dx = currentPosition.left - me.originalInputPosition.left;
            let dy = currentPosition.top - me.originalInputPosition.top;
            const rotation = util.degreesToRadians(me.$el.data().rotation);
            if (rotation) {
                const rotated = util.applyRotationMatrix(dx, dy, rotation);
                dx = rotated.x;
                dy = rotated.y;
            }
            const updateSize = me.updateSize[me.axis];
            if (!updateSize) {
                return;
            }

            let newData = updateSize(e, dx, dy, rotation || 0);

            // Maintain proportions when specified in the options, or when the shift key is pressed
            if (options.maintainProportions || e.shiftKey) {
                // Adjust the min sizes here rather than in cacheData() because the shift key can be on and off during the course
                // of the same resize.
                adjustMinSizesToAspectRatio(me.aspectRatio);
                newData = resizeProportionally(newData, e, dx, dy, rotation || 0);
            } else {
                me.minWidth = options.minWidth;
                me.minHeight = options.minHeight;
            }

            // Ensure that new data is within min and max dimensions
            newData = respectSize(newData, e, dx, dy, rotation || 0);
            updateCache(newData);

            // Have to trigger resize before updating css, so that any resize handlers have the opportunity to change the position.
            // Use triggerHandler() so that this event doesn't bubble up to the window and trigger page resize functionality.
            me.$el.triggerHandler('resize', getEventData());

            const resizeInfo = options.resize(e, getEventData())[0];

            if (me.$helper) {
                // Only resize elements that have the 'dcl-ui-resizable' class.
                me.$helper.find('.dcl-ui-resizable').each(function() {
                    this.style.left = `${resizeInfo.handlePosition.left}px`;
                    this.style.top = `${resizeInfo.handlePosition.top}px`;
                    this.style.width = `${resizeInfo.handleSize.width}px`;
                    this.style.height = `${resizeInfo.handleSize.height}px`;
                });
            } else {
                me.$el.css({
                    left: me.position.left,
                    top: me.position.top,
                    width: me.size.width,
                    height: me.size.height
                });
            }
        }
    };

    const onResizeStop = function(e) {
        if (me.touchIdentifier) {
            // A different touch event has ended than the one we are tracking, don't stop!
            if (!e.originalEvent.changedTouches.some(t => {
                return t.identifier === me.touchIdentifier;
            })) {
                return false;
            }
        }

        me.$el.removeClass('dcl-ui-resizable-resizing');
        unbindResizeHandler(onResize);
        if (me.currentHandle) {
            // For consistency with the resize event, don't bubble this.
            me.$el.triggerHandler('resizestop', getEventData());
            if (options.stop) {
                options.stop(e, getEventData());
            }
            me.activeHandle = null;

            if (me.$helper) {
                me.$helper.remove();
                delete me.$helper;
            }

            me.currentHandle = null;
            me.touchIdentifier = null;
        }
    };

    const onResizeStart = function(e) {
        e.preventDefault();
        me.$el.addClass('dcl-ui-resizable-resizing');
        cacheData(e);
    };

    const onMouseDown = function(e) {
        if (e.which !== 1) {
            return;
        }

        onResizeStart(e);
        $(document).on('mousemove.vpresize', onResize);
        $(document).on('mouseup.vpresize', onResizeStop);
    };

    const onTouchStart = function(e) {
        const eventedElement = this;
        const touch = _.find(e.originalEvent.changedTouches, function(t) {
            return $(t.target).closest(eventedElement).length > 0;
        });

        // This should never happen - invalid touch?
        if (!touch) {
            return;
        }

        me.touchIdentifier = touch.identifier;

        onResizeStart(e);
        $(document).on('touchmove.vpresize', onResize);
        $(document).on('touchend.vpresize', onResizeStop);
    };

    const createHandles = function() {
        me.$el.find('.dcl-ui-resizable-handle').remove();

        // Create resize handles
        handles = _.isFunction(options.handles) ? options.handles() : options.handles;

        if (handles === 'all') {
            handles = 'n,e,s,w,se,sw,ne,nw';
        }
        if (handles === 'corners') {
            handles = 'nw,ne,sw,se';
        }

        const axes = handles.split(',');
        handles = {};

        for (let i = 0; i < axes.length; i++) {
            const axis = $.trim(axes[i]);
            if (axis) {
                const hname = 'dcl-ui-resizable-' + axis;
                const $handle = $("<div class='dcl-ui-resizable-handle " + hname + "'></div>").attr('data-handle', axis);

                $handle.on('mousedown.vpresize', { axis: axis }, onMouseDown);
                // NOTE: In IE11 on tablets, mousedown is fired on touch events (GD-165602)
                $handle.on('touchstart.vpresize', { axis: axis }, onTouchStart);

                //Insert into internal handles object and append to element
                handles[axis] = $handle;
                me.$el.append($handle);
            }
        }
    };

    const create = function() {
        me.$el.addClass('dcl-ui-resizable');

        // Future consideration: jQueryUI also handles cases where the resizable
        // element cannot hold child nodes (e.g. textarea, input, select, etc).
        // If, at some point, the Studio canvas needs to support these types of
        // elements (e.g. for designing websites), we'd need to add that logic here.

        createHandles();
    };

    const initialize = function() {
        options = _.defaults(options || {}, defaultOptions);

        if (options.data) {
            me.$el.data(options.data);
        }

        create(options);
    };

    // jQuery-style interface for getting or setting the handles
    this.handles = function(value) {
        if (_.isUndefined(value)) {
            return handles;
        } else {
            options.handles = value;
            createHandles();
        }
    };

    // A dictionary of functions that update the position and size information based on the selected axis (n, s, e, w, etc)
    this.updateSize = {
        e: function(event, dx, dy, rotation) {
            return {
                left: me.originalPosition.left - (dx * (Math.cos(rotation + Math.PI) / 2 + .5)),
                width: me.originalSize.width + dx,
                top: me.originalPosition.top + (dx * (Math.sin(rotation + Math.PI) / 2))
            };
        },
        w: function(event, dx, dy, rotation) {
            return {
                left: me.originalPosition.left + (dx * (Math.cos(rotation) / 2 + .5)),
                width: me.originalSize.width - dx,
                top: me.originalPosition.top - (dx * (Math.sin(rotation) / 2))
            };
        },
        n: function(event, dx, dy, rotation) {
            return {
                left: me.originalPosition.left - (dy * (Math.cos(rotation + Math.PI / 2) / 2)),
                height: me.originalSize.height - dy,
                top: me.originalPosition.top + (dy * (Math.sin(rotation + Math.PI / 2) / 2 + .5))
            };
        },
        s: function(event, dx, dy, rotation) {
            return {
                left: me.originalPosition.left + (dy * (Math.cos(rotation + Math.PI * 3 / 2) / 2)),
                height: me.originalSize.height + dy,
                top: me.originalPosition.top - (dy * (Math.sin(rotation + Math.PI * 3 / 2) / 2 + .5))
            };
        },
        se: function(event, dx, dy, rotation) {
            const s = me.updateSize.s(event, dx, dy, rotation);
            const e = me.updateSize.e(event, dx, dy, rotation);
            return {
                left: s.left + e.left - me.originalPosition.left,
                top: s.top + e.top - me.originalPosition.top,
                width: e.width,
                height: s.height
            };
        },
        sw: function(event, dx, dy, rotation) {
            const s = me.updateSize.s(event, dx, dy, rotation);
            const w = me.updateSize.w(event, dx, dy, rotation);
            return {
                left: s.left + w.left - me.originalPosition.left,
                top: s.top + w.top - me.originalPosition.top,
                width: w.width,
                height: s.height
            };
        },
        ne: function(event, dx, dy, rotation) {
            const n = me.updateSize.n(event, dx, dy, rotation);
            const e = me.updateSize.e(event, dx, dy, rotation);
            return {
                left: n.left + e.left - me.originalPosition.left,
                top: n.top + e.top - me.originalPosition.top,
                width: e.width,
                height: n.height
            };
        },
        nw: function(event, dx, dy, rotation) {
            const n = me.updateSize.n(event, dx, dy, rotation);
            const w = me.updateSize.w(event, dx, dy, rotation);
            return {
                left: n.left + w.left - me.originalPosition.left,
                top: n.top + w.top - me.originalPosition.top,
                width: w.width,
                height: n.height
            };
        }
    };

    initialize();
};

module.exports = {
    Resizable
};
