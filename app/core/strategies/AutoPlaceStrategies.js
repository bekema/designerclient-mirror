const CropStrategy = require('strategies/CropStrategy');
const ScaleToFitStrategy = require('strategies/ScaleToFitStrategy');
const StretchStrategy = require('strategies/StretchStrategy');

module.exports = {
    CropStrategy,
    ScaleToFitStrategy,
    StretchStrategy
};