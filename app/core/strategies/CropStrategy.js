module.exports = {
    getAttributes: function(canvasSize, imageSize, currentAttributes) {
        const crop = {
            left: 0,
            top: 0,
            right: 0,
            bottom: 0
        };

        const imageRatio = imageSize.width / imageSize.height;
        const canvasRatio = canvasSize.width / canvasSize.height;
        if (imageRatio < canvasRatio) {
            const heightCrop = 1 - (imageRatio / canvasRatio);
            crop.top = heightCrop / 2;
            crop.bottom = heightCrop / 2;
        } else if (imageRatio > canvasRatio) {
            const widthCrop = 1 - (canvasRatio / imageRatio);
            crop.left = widthCrop / 2;
            crop.right = widthCrop / 2;
        }
        currentAttributes.crop = crop;
        currentAttributes.top = 0;
        currentAttributes.left = 0;
        currentAttributes.height = canvasSize.height;
        currentAttributes.width = canvasSize.width;

        return currentAttributes;
    }
};
