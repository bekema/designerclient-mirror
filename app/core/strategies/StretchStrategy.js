module.exports = {
    getAttributes: function(canvasSize, imageSize, currentAttributes) {
        currentAttributes.width = canvasSize.width;
        currentAttributes.height = canvasSize.height;
        currentAttributes.left = 0;
        currentAttributes.top =  0;

        return currentAttributes;
    }
};
