//Add a little give to the auto check to insure it wont hide anything.
const AUTO_GIVE = 10;

module.exports = function({ canvas, document, containerSize }) {

    //First, determine the base margins necessary (should never be less than these values)
    const chromeSize = canvas.get('maxChromeDimensions');
    let top = Math.max(0, -chromeSize.top);
    let left = Math.max(0, -chromeSize.left);
    let right = Math.max(0, -chromeSize.right);
    let bottom = Math.max(0, -chromeSize.bottom);

    //Then, determine what margins we can make auto.
    const singleCanvas = document.canvasViewModels.length === 1;
    const canvasSize = canvas.get('size');
    const heightAuto = singleCanvas && ((Math.max(top, bottom) * 2) + canvasSize.height) < containerSize.height - AUTO_GIVE;
    const widthAuto = ((Math.max(left, right) * 2) + canvasSize.width) < containerSize.width - AUTO_GIVE;

    //Setting these as pixels as auto height doesn't really work
    if (heightAuto) {
        top = (containerSize.height - canvasSize.height) / 2;
        bottom = 0;
    }
    if (widthAuto) {
        left = 'auto';
        right = 'auto';
    }

    return {
        'margin-top': top,
        'margin-left': left,
        'margin-right': right,
        'margin-bottom': bottom
    };
};
