module.exports = {
    getAttributes: function(canvasSize, imageSize, currentAttributes) {
        const widthRatio = imageSize.width / canvasSize.width;
        const heightRatio = imageSize.height / canvasSize.height;
        const ratio = Math.max(widthRatio, heightRatio);

        currentAttributes.width = imageSize.width / ratio;
        currentAttributes.height = imageSize.height / ratio;
        currentAttributes.left = (canvasSize.width - currentAttributes.width) / 2;
        currentAttributes.top =  (canvasSize.height - currentAttributes.height) / 2;

        return currentAttributes;
    }
};