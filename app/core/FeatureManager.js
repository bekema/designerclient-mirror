const CanvasBackground = require('features/CanvasBackground');
const PdfProof = require('features/PdfProof');

module.exports = {
    canvasBackgroundColor: CanvasBackground,
    generatePdf: PdfProof
};