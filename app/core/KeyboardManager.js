// ## KeyboardManager

// The `KeyboardManager` is a singleton responsible for listening to any keyboard events on the document, and triggering
// relevant global events. Examples include checking whether the `shift` key is down, listening for `ctrl+A` to select all
// items, or listening for `esc` to cancel any actions currently in progress.

const $ = require('jquery');
const _ = require('underscore');
const Backbone = require('backbone');
const eventBus = require('EventBus');
const events = eventBus.events;

const keyboardManager = _.extend({}, Backbone.Events);

/* eslint-disable no-magic-numbers */

const onKeyDown = function(e) {
    switch (e.which) {
        case 8: // Prevent `backspace` from navigating back.
            if (e.target.tagName !== 'INPUT' && !e.target.attributes.contenteditable) {
                e.preventDefault();
            }
            break;
        case 9: //tab
            keyboardManager.triggerGlobal('tab', e);
            break;
        case 27: // `esc` triggers a global event to cancel the current action.
            keyboardManager.triggerGlobal('cancelaction', e);
            break;
        case 37: //`left arrow`
        case 38: //`up arrow`
        case 39: //`right arrow`
        case 40: //`down arrow`
            keyboardManager.triggerGlobal('arrowkeydown', e);
            break;
        case 46: // `delete`
            eventBus.trigger(events.deleteItem, e);
            break;
        case 65: //a
            if (e.ctrlKey) {
                eventBus.trigger(events.selectAll, e);
            }
            break;
        case 67: //c
            if (e.ctrlKey) {
                keyboardManager.triggerGlobal('copy', e);
            }
            break;
        case 83: //s
            if (e.ctrlKey) {
                keyboardManager.triggerGlobal('save', e);
                e.preventDefault();
            }
            break;
        case 86: //v
            if (e.ctrlKey) {
                keyboardManager.triggerGlobal('paste', e);
            }
            break;
        case 88: //x
            if (e.ctrlKey) {
                keyboardManager.triggerGlobal('cut', e);
            }
            break;
        case 89: //y
            if (e.ctrlKey) {
                eventBus.trigger(events.redo, e);
            }
            break;
        case 90: //z
            if (e.ctrlKey) {
                eventBus.trigger(events.undo, e);
            }
            break;
        case 48: // 0
        case 96: // 0
            if (e.ctrlKey && !e.shiftKey) {
                e.preventDefault();
                eventBus.trigger(events.zoomReset, e);
            }
            break;
        case 107: //+
        case 187: //+
            if (e.ctrlKey) {
                e.preventDefault();
                eventBus.trigger(events.zoomIn, e);
            }
            break;
        case 109: //-
        case 189: //-
            if (e.ctrlKey && !e.shiftKey) {
                e.preventDefault();
                eventBus.trigger(events.zoomOut, e);
            }
            break;
        case 219: //[
            if (e.ctrlKey) {
                keyboardManager.triggerGlobal('previouscanvas', e);
            }
            break;
        case 221: //[
            if (e.ctrlKey) {
                keyboardManager.triggerGlobal('nextcanvas', e);
            }
            break;
    }

    keyboardManager.trigger('keydown', e);
};

const onKeyUp = function(e) {
    switch (e.which) {
        case 37: //`left arrow`
        case 38: //`up arrow`
        case 39: //`right arrow`
        case 40: //`down arrow`
            keyboardManager.triggerGlobal('arrowkeyup', e);
            break;
    }
    keyboardManager.trigger('keyup', e);
};

const onKeyPress = function(e) {
    const key = String.fromCharCode(e.which);
    if (!e.ctrlKey && !e.metaKey && !e.altKey
        && e.which // FireFox reports a tab as "0" which is actually a non whitespace character
        && key.match(/[\S ]/ig)) {
        keyboardManager.triggerGlobal('inserttext', key, e);
    }
    keyboardManager.trigger('keypress', e);
};

keyboardManager.enable = function() {
    $(document).keydown(onKeyDown);
    $(document).keypress(onKeyPress);
    $(document).keyup(onKeyUp);
};

keyboardManager.disable = function() {
    $(document).off('keydown', onKeyDown);
    $(document).off('keypress', onKeyPress);
    $(document).off('keyup', onKeyUp);
};

module.exports = keyboardManager;
