const lng = require('Localization');
const util = require('util/Util');

const fixUpDocument = (doc, surfaceSpecifications) => {
    if (!doc.id) {
        doc.id = util.generateUUID();
    }

    doc.module = 'Document';
    doc.sku = surfaceSpecifications.mcpSku;

    doc.canvases && doc.canvases.forEach(canvas => {
        canvas.module = 'Canvas';
    });

    return doc;
};

const validateDocument = (document, surfaceSpecifications) => {
    const messages = [];

    // validate surfaces and canvases
    if (surfaceSpecifications.surfaces.length !== document.canvases.length) {
        messages.push(lng.getText('errors.surfaceValidationSurfaces'));
    }

    // validate width and height doc with widthInMm and heightInMm surfaceSpecifications
    surfaceSpecifications.surfaces.forEach((surface, index) => {
        if (document.canvases[index]) {
            const canvas = document.canvases[index];

            if (surface.widthInMm !== canvas.width || surface.heightInMm !== canvas.height) {
                messages.push(lng.getText('errors.surfaceValidationSizes'));
            }

            // validate surfaces
            if (surface.name.toUpperCase() !== canvas.name.toUpperCase()) {
                messages.push(lng.getText('errors.surfaceValidationNames'));
            }
        }
    });

    if (messages.length) {
        throw Error(messages.join(', '));
    }
};

const createDocument = (document, surfaceSpecifications) => {
    if (document) {
        return fixUpDocument(document, surfaceSpecifications);
    }

    // create canvases based on the surfaces
    const canvases = surfaceSpecifications.surfaces.map((surface, index) => {
        return {
            module: 'Canvas',
            name: surface.name,
            height: surface.heightInMm,
            width: surface.widthInMm,
            id: index + 1, // to start with id 1
            items: []
        };
    });

    return {
        module: 'Document',
        name: '',
        id: util.generateUUID(),
        sku: surfaceSpecifications.mcpSku,
        documentReference: '',
        canvases
    };
};

module.exports = {
    createDocument,
    validateDocument
};
