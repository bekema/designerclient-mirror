const designerContext = require('DesignerContext');

class CanvasBackground {
    get enabled() {
        return !designerContext.embroidery;
    }
}

module.exports = new CanvasBackground();