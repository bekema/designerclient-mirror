const designerContext = require('DesignerContext');

class PdfProof {
    get enabled() {
        return !designerContext.embroidery;
    }
}

module.exports = new PdfProof();