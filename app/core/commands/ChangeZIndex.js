const _ = require('underscore');
const Command = require('commands/Command');

module.exports = Command.extend({

    name: 'change-zindex',

    // Requires a `model` and an `attributes` hash
    apply: function(options) {
        if (!options.selectedItem) {
            return;
        }

        options.items = options.items || options.selectedItem.parent.itemViewModels;

        this[options.action](options.items, options.selectedItem);
    },

    bringToFront: function(items, selectedItem) {
        // set new z-index current selected object
        const newZIndex = items.max(item => item.model.get('zIndex')).model.get('zIndex');

        items.filter(item => item.model.get('zIndex') >= selectedItem.model.get('zIndex')).forEach(item => {
            // send all the other items backward
            item.model.set({ zIndex: item.model.get('zIndex') - 1 });
        });

        // set selected item to front
        selectedItem.model.set({ zIndex: newZIndex });
    },

    sendToBack: function(items, selectedItem) {
        const filteredItems = items.filter(item => !item.model.get('zIndexLock') && item.model.get('zIndex') <= selectedItem.model.get('zIndex'));
        // set new z-index current selected object
        const newZIndex = Math.min(...filteredItems.map(item => item.model.get('zIndex')));

        filteredItems.forEach(item => {
            // send all the other items forward
            item.model.set({ zIndex: item.model.get('zIndex') + 1 });
        });

        // set selected item to z-index 1 (first index)
        selectedItem.model.set({ zIndex: newZIndex });
    },

    bringForward: function(items, selectedItem) {
        const selectedZIndex = selectedItem.model.get('zIndex');
        const higherItems = items.filter(item => !item.model.get('zIndexLock') && item.model.get('zIndex') > selectedZIndex);
        const itemToSwapWith = _.min(higherItems, item => item.model.get('zIndex'));
        if (itemToSwapWith) {
            selectedItem.model.set('zIndex', selectedZIndex + 1);
            itemToSwapWith.model.set('zIndex', selectedZIndex);
        }
    },

    sendBackward: function(items, selectedItem) {
        const selectedZIndex = selectedItem.model.get('zIndex');
        const lowerItems = items.filter(item => !item.model.get('zIndexLock') && item.model.get('zIndex') < selectedZIndex);
        const itemToSwapWith = _.max(lowerItems, item => item.model.get('zIndex'));
        if (itemToSwapWith) {
            selectedItem.model.set('zIndex', selectedZIndex - 1);
            itemToSwapWith.model.set('zIndex', selectedZIndex);
        }
    }
});
