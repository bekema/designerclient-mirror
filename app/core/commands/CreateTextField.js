// Create a new `TextField` and add it to the specified model. It will be added
// using the most common properties of the text fields on the given model.
const _ = require('underscore');
const Command = require('commands/Command');
const AddCommand = require('commands/Add');
const textFieldConfig = require('publicApi/configuration/core/items').text.defaults;
const util = require('util/Util');
const clients = require('services/clientProvider');
const designerContext = require('DesignerContext');
const coreConfig = require('publicApi/configuration/core/items');

/* eslint-disable no-magic-numbers */

module.exports = Command.extend({

    name: 'create-text-field',

    // Takes one option - "model" - which is the model that we will add the new text field to.
    apply: function(options) {
        let width, height, itemSize, top, left;
        const parent = options.viewModel.model;
        const items = parent.items.byModule('TextField');

        const safeArea = options.viewModel.get('safeArea');

        //Font size is based on safe area
        let fontSize = util.getMostCommonAttributeValue(items, 'fontSize') || Math.round(Math.min(safeArea.height, safeArea.width) / 5);
        if (fontSize > coreConfig.text.maxFontSize) {
            fontSize = coreConfig.text.maxFontSize;
        } else if (fontSize < coreConfig.text.minFontSize) {
            fontSize = coreConfig.text.minFontSize;
        }

        //Set width or height (based on text orientation) to 2/3 of safeArea width of height
        //TODO this does something weird with randomly placing it, it doesn't go as far down as it should
        if (options.attributes && options.attributes.writingMode === 'vertical-rl') {
            height = safeArea.height * 2 / 3;
            width = 0;
            itemSize = { width: null, height };
        } else {
            width = safeArea.width * 2 / 3;
            itemSize = { width, height: null };
        }

        //Make sure we have a working font
        const fontFamily = util.getMostCommonAttributeValue(items, 'fontFamily') || clients.font.defaultFont.FontFamily;

        //Place the text field anywhere in the canvas making sure it doesnt go out of the safety margin
        const position = util.getRandomPositionInArea(safeArea, itemSize);
        top = position.top;
        left = position.left;

        // Flatten the color object down for the current context
        if (_.isObject(textFieldConfig.fontColor)) {
            textFieldConfig.fontColor = textFieldConfig.fontColor[designerContext.fontFlavor];
        }

        const attributes =
        _.defaults(_.clone(options.attributes) || {}, {
            horizontalAlignment: util.getMostCommonAttributeValue(items, 'horizontalAlignment'),
            fontSize,
            fontColor: util.getMostCommonAttributeValue(items, 'fontColor'),
            fontFamily,
            fontItalic: util.getMostCommonAttributeValue(items, 'fontItalic'),
            fontUnderline: util.getMostCommonAttributeValue(items, 'fontUnderline'),
            fontBold: util.getMostCommonAttributeValue(items, 'fontBold'),
            fontStrikeout: util.getMostCommonAttributeValue(items, 'fontStrikeout'),
            width,
            height,
            top,
            left,
            userCreated: true
        }, textFieldConfig);

        // Don't execute the add command, because we don't need it to create internal diffs or do any logging.
        // We just want it to do its work and leave the rest to us.
        const addCommand = new AddCommand();
        const newItems = addCommand.apply({
            model: parent,
            items: [attributes],
            selectNewModels: true
        });

        this.triggerGlobal('analytics:track', newItems.itemViewModels, 'Insert');
    }
});
