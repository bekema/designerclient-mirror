// This is the base `Command`, which should be extended when creating any behavior that
// modifies a model. Make sure to override `apply` and not `execute`.
const _ = require('underscore');
const Backbone = require('backbone');
const documentRepository = require('DocumentRepository');
const _cloneDeep = require('lodash-compat/lang/cloneDeep');
const _zipObject = require('lodash-compat/array/zipObject');
const eventBus = require('EventBus');
const events = eventBus.events;

const Command = function() {
    this.rootRepository = documentRepository;
    if (this.initialize) {
        this.initialize(...arguments);
    }
};

Command.extend = Backbone.View.extend;

// Given a model, update the tree so that it has the same ancestral structure as
// the model. Return the branch that should contain the model.
const getBranchToModify = function(model, tree) {
    let ancestry, root, depth;
    ancestry = [];
    root = model;

    while (root) {
        ancestry.push(root.id);
        if (root.collection) {
            ancestry.push(root.collection.name);
        }
        root = root.parent;
    }

    let branch = tree;
    for (depth = ancestry.length - 1; depth > 0; depth--) {
        const id = ancestry[depth];
        if (!branch[id]) {
            branch = branch[id] = {};
        } else {
            branch = branch[id];
        }
    }
    return branch;
};

const applyWithListeners = function(fn) {
    let changed = false;
    // Prepare to store a set of instructions about how to undo this command.
    const instructions = {};

    const onChange = function(model) {
        changed = true;
        const branch = getBranchToModify(model, instructions);
        const changedKeys = Object.keys(model.changedAttributes() || {});
        // Note the order of the extend -- subsequent changes are always overwritten by the first change.
        branch[model.id] = _.extend(
            {
                // Store whatever attributes were changed.
                _change: _cloneDeep(_.pick(model.previousAttributes(), changedKeys)),
                // Store any newly added attributes that we'll have to unset. This is stored as a
                // hash of { key1: undefined, key2: undefined } so that it can be easily extended by
                // subsequent changes.
                _unset: _zipObject(_.difference(changedKeys, Object.keys(model.previousAttributes() || {})), true)
            },
            branch[model.id]);
        // This is a special case for undoing view models, where we always want to leave selected set.
        if (model.get('selected')) {
            const instruction = branch[model.id];
            instruction._change.selected = true;
            delete instruction._unset.selected;
        }
        // Another special case, never store active canvas changes in history
        if (model.get('module') === 'CanvasViewModel') {
            const instruction = branch[model.id];
            delete instruction._change.active;
        }
    };

    const onAdd = function(model) {
        // Since we're create a set of instructions to undo, when something was added, we want to store it for later removal.
        changed = true;
        const branch = getBranchToModify(model, instructions);
        branch[model.id] = _.extend(
            {
                _remove: true
            },
            branch[model.id]);
    };

    // If something is removed we're going to keep track of how to re-add it.
    const onRemove = function(model) {
        changed = true;
        const branch = getBranchToModify(model, instructions);
        branch[model.id] = _.extend(
            {
                // Clone it in case the JSON has an object in it - we don't want those stored by reference.
                _add: _cloneDeep(model.toJSON())
            },
            branch[model.id]);
    };

    // IDEA: Don't force global registration -- just register the things passed into the options with the name
    // 'model', 'viewModel', 'models', or 'viewModels'. But then what about items that are added to models and
    // subsequently modified?
    this.listenTo(this.rootRepository, 'change', onChange);
    this.listenTo(this.rootRepository, 'add', onAdd);
    this.listenTo(this.rootRepository, 'remove', onRemove);

    // Actually invoke the passed in function.
    let result;
    Backbone.atomic(function() {
        result = fn();
    });

    this.stopListening();

    return {
        result,
        instructions: changed ? instructions : false
    };
};

const patch = function(branch, instructions) {
    _.each(instructions, function(val, key) {
        if (key === '_change') {
            branch.set(val);
        } else if (key === '_unset') {
            _.each(val, function(value, attr) {
                branch.unset(attr);
            });
        } else if (key === '_remove') {
            branch.destroy();
        } else if (branch[key] && branch[key] instanceof Backbone.NestedCollection) {
            _.each(val, function(obj, id) {
                const model = branch[key].get(id);
                if (model) {
                    patch(model, obj);
                }
                if (obj._add) {
                    // Create the new model with any attributes that would've been
                    // changed after the fact.
                    branch[key].add(_.extend(obj._add, obj._change));
                }
            });
        }
    });
};

// View models don't do adds and removes, those are created automatically by the patch.
const patchViewModel = function(branch, instructions) {
    _.each(instructions, function(val, key) {
        // Treat adds as changes - the view model is created when adding the model, but
        // the add ensures that it will get the correct properties.
        // TODO Is this right? It seems to work, but it seems fragile and like keys could be incorrectly overwritten.
        if (key === '_change' || key === '_add') {
            branch.set(val);
        } else if (key === '_unset') {
            _.each(val, function(value, attr) {
                branch.unset(attr);
            });
        } else if (branch[key] && branch[key] instanceof Backbone.NestedCollection) {
            _.each(val, function(obj, id) {
                const model = branch[key].get(id);
                if (model) {
                    patchViewModel(model, obj);
                }
            });
        }
    });
};

_.extend(Command.prototype, Backbone.Events, {

    defaults: {
        triggerGlobalEvent: true,
        applyOnly: false
    },

    // Subclasses should override `apply` to do the actual operations on the model.
    apply: function() {

    },

    execute: function(options) {
        // Only copy the options that are defined in defaults.
        this.options = _.extend({}, this.defaults, _.pick(options, Object.keys(this.defaults)));

        // Short-circuit all the change listening. This is used by composite commands and turns
        // undo() and redo() into no-ops.
        if (this.options.applyOnly) {
            return this.apply(options);
        }

        // If the command is being called for a second time, we can just apply the instructions, rather than re-executing it.
        if (!_.isUndefined(this.instructions)) {
            // Only allow redoing commands after they've been undone.
            if (this.undone) {
                this.redo();
            }
        } else {
            const applied = applyWithListeners.call(this, function() {
                return this.apply(options);
            }.bind(this));

            // If there are no instructions for undoing this command, that means nothing happened,
            // so don't bother saying that it was executed.
            if (applied.instructions) {
                this.instructions = {
                    undo: applied.instructions
                };
                if (this.options.triggerGlobalEvent) {
                    eventBus.trigger(events.executeCommand, this);
                }
            }
            return applied.result;
        }
    },

    redo: function() {
        if (!_.isUndefined(this.instructions)) {
            // Don't need to listen to the changes on a redo, we already have them
            // stored from when we initially executed it.
            Backbone.atomic(function() {
                _.each(this.instructions.redo, function(instructions, rootId) {
                    const root = this.rootRepository.getDocument(rootId);
                    if (root) {
                        patch(root, instructions);
                    }
                }, this);

                _.each(this.instructions.redo, function(instructions, rootId) {
                    const rootViewModel = this.rootRepository.getDocumentViewModel(rootId);
                    if (rootViewModel) {
                        patchViewModel(rootViewModel, instructions);
                    }
                }, this);
            }.bind(this));

            this.undone = false;
            if (this.options.triggerGlobalEvent) {
                eventBus.trigger(events.executeCommand, this);
            }
        }
    },

    // Undoes the changes that were generated when the command was executed.
    undo: function() {
        if (!_.isUndefined(this.instructions)) {
            // If we've already generated redo instructions, just undo the changes
            // atomically, otherwise undo them with monitoring.
            const fn = this.instructions.redo ? Backbone.atomic : applyWithListeners;

            const applied = fn.call(this, function() {
                _.each(this.instructions.undo, function(instructions, rootId) {
                    const root = this.rootRepository.getDocument(rootId);
                    if (root) {
                        patch(root, instructions);
                    }
                }, this);

                _.each(this.instructions.undo, function(instructions, rootId) {
                    const rootViewModel = this.rootRepository.getDocumentViewModel(rootId);
                    if (rootViewModel) {
                        patchViewModel(rootViewModel, instructions);
                    }
                }, this);
            }.bind(this));

            if (applied) {
                this.instructions.redo = applied.instructions;
            }

            this.undone = true;
            if (this.options.triggerGlobalEvent) {
                eventBus.trigger(events.undoCommand, this);
            }
        }
    }
});

module.exports = Command;
