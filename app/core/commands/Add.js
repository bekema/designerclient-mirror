const _ = require('underscore');
const Backbone = require('backbone');
const selectionManager = require('SelectionManager');
const documentRepository = require('DocumentRepository');
const ViewModel = require('viewModels/ViewModel');
const Command = require('commands/Command');

const getNextValue = function(parent, propertyName) {
    if (parent.items.length === 0) {
        return 1;
    }
    const value = parent.items.max(function(item) {
        return item.get(propertyName);
    });
    if (value === -Infinity) {
        return 1;
    }
    return value.get(propertyName) + 1;
};

module.exports = Command.extend({

    name: 'add',

    apply: function(options) {
        const parent = options.model || options.viewModel.model;
        const items = [];

        // Sort items by zIndex so that when we add them it will add in the correct order
        const sortedItems = _.sortBy(options.items, function(item) {
            if (!(item instanceof Backbone.Model)) {
                return item.zIndex;
            }
            return item.get('zIndex');
        });

        sortedItems.forEach(function(item, index) {
            if (!(item instanceof Backbone.Model)) {
                const Constructor = require('models/' + item.module);
                item = new Constructor(item);
            }

            items.push(item);
            // Ids *must* be unique.
            while (!item.has('id') || parent.items.get(item.get('id'))) {
                item.set('id', _.uniqueId('item_'));
            }

            // Make sure everything has a tab index. Only text fields and wordart will persist this, but it's
            // still useful on the client for everything. TODO: Should this match zIndex?
            if (!item.has('tabIndex') || parent.items.findWhere({ 'tabIndex': item.get('tabIndex') })) {
                item.set('tabIndex', getNextValue(parent, 'tabIndex'));
            }

            // Make sure that zIndex is unique and that everything has it, increasing it sequentially if necessary.
            const requestedZIndex = item.get('zIndex');
            let conflictingItem;
            if (requestedZIndex) {
                conflictingItem = parent.items.findWhere({ 'zIndex': requestedZIndex });
            }
            if (!requestedZIndex || conflictingItem) {
                item.set('zIndex', getNextValue(parent, 'zIndex'));
                item.set('zIndexLock', false);
            }

            // Add item to canvas; causes CanvasViewModel.onAddItem to instantiate item's ViewModel.
            if (options.viewModelAttributes && options.viewModelAttributes[index]) {
                parent.items.add(item, { viewModelAttributes: options.viewModelAttributes[index] });
            } else {
                parent.items.add(item);
            }
        });

        const itemViewModels = [];
        const canvasViewModel = documentRepository.getCanvasViewModel(ViewModel.getViewModelId(parent));
        if (canvasViewModel) {
            for (let i = 0; i < items.length; i++) {
                // Models and view models have different ids (view models have stricter uniqueness requirements).
                const id = ViewModel.getViewModelId(items[i]);
                const itemViewModel = canvasViewModel.itemViewModels.get(id);
                if (itemViewModel) {
                    itemViewModels.push(itemViewModel);
                }
            }

            if (options.selectNewModels) {
                selectionManager.select(itemViewModels);
            }
        }

        return { items, itemViewModels };
    }
});
