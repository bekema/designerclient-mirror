// The `Resize` command takes in a list of `models` and a `delta` object that describes
// how much to move the `top`, `left`, `width`, and `height` of each model. It also adjusts the
// `initialHeight` and `initialWidth` of the rotation object.
const _ = require('underscore');
const Command = require('commands/Command');

/* eslint-disable no-magic-numbers */

module.exports = Command.extend({

    name: 'resize',

    // For recalculating the initialHeight and initialWidth
    // properties based on rotation angle
    getModNinetyRotationAttributes: function({ rotation, initialHeight, initialWidth, resizedDimensions }) {
        if (rotation === 90 || rotation === 270) {
            initialHeight = resizedDimensions.width;
            initialWidth = resizedDimensions.height;
        } else {
            initialHeight = resizedDimensions.height;
            initialWidth = resizedDimensions.width;
        }
        return { rotation, initialHeight, initialWidth };
    },

    // Non-90 degree rotated elements can only be resized if we maintain the aspect ratio
    getRotationAttributes: function({ rotation, initialHeight, initialWidth, dimensionsBeforeResizing, resizedDimensions }) {
        // This is here so we can just do the simple case, without additional computation
        if (rotation % 90 === 0) {
            return this.getModNinetyRotationAttributes({ rotation, initialHeight, initialWidth, resizedDimensions });
        } else {
            const scaleFactor = resizedDimensions.height / dimensionsBeforeResizing.height;
            initialHeight *= scaleFactor;
            initialWidth *= scaleFactor;
            return { rotation, initialHeight, initialWidth };
        }
    },

    apply: function(options) {
        const viewModels = options.viewModels || [options.viewModel];
        _.each(viewModels, function(viewModel) {

            const model = viewModel.model;

            const top = model.get('top') + options.delta.top;
            const left = model.get('left') + options.delta.left;
            const width = model.get('width') + options.delta.width;
            const height = model.get('height') + options.delta.height;
            const rotation = model.get('rotation');
            const initialHeight = model.get('initialHeight');
            const initialWidth = model.get('initialWidth');

            const dimensionsBeforeResizing = {
                height: model.get('height'),
                width: model.get('width')
            };

            const resizedDimensions = {
                height,
                width
            };

            const rotationAttributes = this.getRotationAttributes({ rotation, initialHeight, initialWidth, dimensionsBeforeResizing, resizedDimensions });

            viewModel.set('temporarySizeDelta', {
                width: options.delta.width,
                height: options.delta.height
            });

            model.set({
                top,
                left,
                width,
                height,
                rotation: rotationAttributes.rotation,
                initialHeight: rotationAttributes.initialHeight,
                initialWidth: rotationAttributes.initialWidth
            });
        }, this);
    }
});
