const _ = require('underscore');
const Backbone = require('backbone');
const Command = require('commands/Command');
const documentRepository = require('DocumentRepository');
const util = require('util/Util');

/* eslint-disable no-magic-numbers */

module.exports = Command.extend({

    name: 'crop-image',

    apply: function(options) {
        const viewModel = options.viewModel;

        if (!options.unlocked) {
            this.lockedCropApply(viewModel, options.crop);
        } else {
            this.unlockedCropApply(viewModel, options.crop);
        }
    },

    crop: function(dimensions, crop) {
        return {
            height: dimensions.height * (1 - (crop.top + crop.bottom)),
            width: dimensions.width * (1 - (crop.left + crop.right))
        };
    },

    getAspectRatio: function(dimensions) {
        return dimensions.width / dimensions.height;
    },

    /**
    * Locked cropping.
    * This should maintain the height and width of the model.
    */

    //TODO: Do we actually need this? (does this make things more confusing, unlocked crop apply can do the same thing given the right values)
    lockedCropApply: function(viewModel, newCrop) {
        const model = viewModel.model;

        const newAttributes = {
            crop: newCrop
        };

        this.trackChangingAttributes(model, newAttributes);

        model.set(newAttributes);
    },

    areAttributesChanging: function(model, newAttributes) {
        _.any(newAttributes, function(newAttribute, attributeName) {
            return !_.isEqual(model.get(attributeName), newAttribute);
        });
    },

    trackChangingAttributes: function(model, newAttributes) {
        if (this.areAttributesChanging(model, newAttributes)) {
            Backbone.Events.triggerGlobal('analytics:track', 'CropComplete');
        }
    },

    centerResizedImage: function(model, imageDimensions) {
        const containerDimensions = { height: model.get('height'), width: model.get('width') };
        const deltaWidth = imageDimensions.width - containerDimensions.width;
        const deltaHeight = imageDimensions.height - containerDimensions.height;
        return {
            top: model.get('top') - deltaHeight / 2,
            left: model.get('left') - deltaWidth / 2
        };
    },

    /**
    * Unlocked cropping resizes the image. The height and width of the
    * model is not maintained.
    */
    unlockedCropApply: function(viewModel, newCrop) {
        const model = viewModel.model;
        // need size of full original image, not just what it had been after previous cropping
        const naturalDimensions = viewModel.model.get('naturalDimensions');
        const croppedDimensions = this.crop(naturalDimensions, newCrop);

        // scaling...
        const result = this.scaleRectangle(model, croppedDimensions);

        const newAttributes = {
            crop: newCrop,
            initialHeight: result.scaledDimensions.height,
            initialWidth: result.scaledDimensions.width,
            height: result.scaledDimensions.height,
            width: result.scaledDimensions.width,
            top: result.adjustedTopLeft.top,
            left: result.adjustedTopLeft.left
        };

        this.trackChangingAttributes(model, newAttributes);

        model.set(newAttributes);
    },

    scaleRectangle: function(model, newDimensions) {
        const modelDimensions = {
            height: model.get('height'),
            width: model.get('width')
        };

        // first we need to check if we can crop the image
        // by keeping the size of the image similar
        let scaledDimensions = this.hypotenuseScaling(modelDimensions, newDimensions);

        const activeCanvasViewModel = documentRepository.getActiveCanvas();
        const canvasDimensions = {
            height: activeCanvasViewModel.get('height'),
            width: activeCanvasViewModel.get('width')
        };

        let adjustedTopLeft;
        // check to see if we can fit the image in our canvas
        if (scaledDimensions.height < canvasDimensions.height &&
            scaledDimensions.width < canvasDimensions.width) {
            adjustedTopLeft = this.centerResizedImage(model, scaledDimensions);

            // this method prevents the image from overflowing
            this.positionImageWithinCanvas(adjustedTopLeft, scaledDimensions, canvasDimensions);

            return {
                scaledDimensions: scaledDimensions,
                adjustedTopLeft: adjustedTopLeft
            };
        }

        // else if we can't fit the image, then scale it down so it does fit
        scaledDimensions = this.fitToRectangleScaling(canvasDimensions, newDimensions);
        adjustedTopLeft = this.centerResizedImage(model, scaledDimensions);
        return {
            scaledDimensions: scaledDimensions,
            adjustedTopLeft: adjustedTopLeft
        };
    },

    // adjusting top and left by checking top/left values combined with height and width
    positionImageWithinCanvas: function(topLeft, scaledDimensions, canvasDimensions) {
        topLeft.top = (topLeft.top < 0) ? 0 : topLeft.top;
        topLeft.left = (topLeft.left < 0) ? 0 : topLeft.left;

        const right = canvasDimensions.width - (scaledDimensions.width + topLeft.left);
        topLeft.left = (right < 0) ? topLeft.left + right : topLeft.left;

        const bottom = canvasDimensions.height - (scaledDimensions.height + topLeft.top);
        topLeft.top = (bottom < 0) ? topLeft.top + bottom : topLeft.top;

        return topLeft;
    },

    // hypotenuse scaling uses the hypotenuse of the two dimensions to calculate a scale factor
    hypotenuseScaling: function(oldDimensions, newDimensions) {
        const oldHypotenuse = util.getHypotenuse(oldDimensions.width, oldDimensions.height);
        const newHypotenuse = util.getHypotenuse(newDimensions.width, newDimensions.height);

        const scaleFactor = newHypotenuse / oldHypotenuse;
        return {
            height: newDimensions.height / scaleFactor,
            width: newDimensions.width / scaleFactor
        };
    },

    fitToRectangleScaling: function(oldDimensions, newDimensions) {
        const newAspectRatio = newDimensions.width / newDimensions.height;
        if (newAspectRatio > 1) {
            return {
                width: oldDimensions.width,
                height: oldDimensions.width * (1 / newAspectRatio)
            };
        } else {
            return {
                height: oldDimensions.height,
                width: oldDimensions.height * newAspectRatio
            };
        }
    }
});
