const Command = require('commands/Command');
const CreateImageCommand = require('commands/CreateImage');

module.exports = Command.extend({

    name: 'autoplace-image',

    apply: function(options) {
        options.attributes = options.strategy.getAttributes(options.viewModel.get('canvasSize'), options.image, options.attributes);
        options.attributes.autoPlaced = true;

        const createImageCommand = new CreateImageCommand();
        createImageCommand.apply(options);
    }
});
