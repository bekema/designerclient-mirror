const _ = require('underscore');
const Resize = require('commands/Resize');

module.exports = Resize.extend({

    name: 'resize-image',

    //TODO: I am also confused as to the point of this file.
    apply: function(options) {
        const viewModels = options.viewModels || [options.viewModel];
        _.each(viewModels, function(viewModel) {

            const model = viewModel.model;

            const top = model.get('top');
            const left = model.get('left');
            const width = model.get('width');
            const height = model.get('height');

            const croppedPosition = viewModel.get('unzoomedScaleToFitPosition');
            const croppedSize = viewModel.get('unzoomedScaleToFitSize');

            const widthRatio = width / croppedSize.width;
            const heightRatio = height / croppedSize.height;

            const deltaWidth = options.delta.width * widthRatio;
            const deltaHeight = options.delta.height * heightRatio;

            const deltaLeft = (croppedPosition.left - ((croppedPosition.left - left) * (width + deltaWidth) / width) + options.delta.left) - left;
            const deltaTop = (croppedPosition.top - ((croppedPosition.top - top) * (height + deltaHeight) / height) + options.delta.top) - top;

            // Call the base to deal with the rotation and temporary sizing.
            Resize.prototype.apply.call(
                this,
                {
                    viewModels: [viewModel],
                    delta: {
                        width: deltaWidth,
                        height: deltaHeight,
                        top: deltaTop,
                        left: deltaLeft
                    }
                });
        }, this);

        this.triggerGlobal('analytics:track', options.viewModels, 'Resize');
    }
});
