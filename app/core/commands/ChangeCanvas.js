// The `ChangeCanvas` command takes in a list of `viewModels`, the CanvasViewModel for
// the canvas being moved to, and a position for the new items.
const Command = require('commands/Command');
const AddCommand = require('commands/Add');
const DeleteCommand = require('commands/Delete');

module.exports = Command.extend({

    name: 'changecanvas',

    apply: function(options) {
        const viewModels = options.viewModels || [options.viewModel];
        const newModels = [];

        viewModels.forEach(function(viewModel) {
            const model = viewModel.model;
            const clonedModel = model.clone();

            //TODO: update positioning, this stacks the images
            clonedModel.set({
                top: options.position.top,
                left: options.position.left
            });

            newModels.push(clonedModel);
        });

        // Don't execute the add or delete command, because we don't need it to create internal diffs or do any logging.
        // We just want it to do its work and leave the rest to us.
        const addCommand = new AddCommand();
        addCommand.apply({
            model: options.newCanvasViewModel.model,
            items: newModels,
            selectNewModels: options.selectNewModels
        });

        const deleteCommand = new DeleteCommand();
        deleteCommand.apply({ viewModels });

        this.triggerGlobal('analytics:track', options.viewModels, 'ChangeCanvas');
    }
});
