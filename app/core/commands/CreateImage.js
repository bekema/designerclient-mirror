// Create a new `Image` and add it to the specified model.
const _ = require('underscore');
const Backbone = require('backbone');
const Command = require('commands/Command');
const AddCommand = require('commands/Add');
const addImage = require('behavior/AddImage');
const itemConfig = require('publicApi/configuration/core/items').image;
const processingStepsManager = require('uploads/processingStepsManager');
const uploadsUrlBuilder = require('services/clientProvider').upload.uploadsUrlBuilder;
const eventBus = require('EventBus');
const events = eventBus.events;

/* eslint-disable no-magic-numbers */

module.exports = Command.extend({

    name: 'create-image',

    //Takes two options - `viewModel` - which is the what the new image will be added to (e.g. a canvas) and `image` - the data for the new image.
    //Returns the newly created image view model.
    apply: function(options) {

        options.attributes = options.attributes || {};

        const parent = options.viewModel.model;
        const image = options.image;

        // We'll want to make sure that images don't end up outside the safety margin unless specifically requested.
        const safeArea = options.viewModel.get('safeArea');
        const maxWidth = safeArea.width;
        const maxHeight = safeArea.height;

        // TODO Should these be set before the command is called?
        const defaultAttributes = addImage.getDesignerImageAttributes(image);
        let { top, left, width, height } = options.attributes;

        //TODO: ROTATION
        const { width: imageWidth, height: imageHeight } = image;

        if (!_.isNumber(height) || !_.isNumber(width)) {
            const canvasAspectRatio = safeArea.width / safeArea.height;
            const aspectRatio = imageWidth / imageHeight;
            const maxSize = Math.min(maxHeight, maxWidth);

            // TODO: pull this out into a common function since nearly identical code lives in UploadView
            if (canvasAspectRatio > 1 && aspectRatio > 1) {
                // image and canvas are wide - make it as tall as possible
                height = maxHeight * itemConfig.initialSize;
                width = height * aspectRatio;
                if (width > maxWidth) {
                    width = maxWidth;
                    height = width / aspectRatio;
                }
            } else if (canvasAspectRatio <= 1 && aspectRatio <= 1) {
                // image and canvas are tall - make it as wide as possible
                width = maxWidth * itemConfig.initialSize;
                height = width / aspectRatio;
                if (height > maxHeight) {
                    height = maxHeight;
                    width = height * aspectRatio;
                }
            } else {
                // best alternative when orientations of image and canvas don't match
                width = maxSize * itemConfig.initialSize * (aspectRatio > 1 ? 1 : aspectRatio);
                height = maxSize * itemConfig.initialSize * (aspectRatio > 1 ? 1 / aspectRatio : 1);
            }

            // If we didn't know the height of the image, we want the new image to appear centered on the cursor,
            // which was probably the position used to determine top and left.
            if (_.isNumber(top)) {
                top -= height / 2;
            }

            if (_.isNumber(left)) {
                left -= width / 2;
            }
        }

        if (!_.isNumber(top)) {
            // Randomly position the image within the safe area.
            top = ((maxHeight - height) * Math.random()) + (safeArea.top);
        }

        if (!_.isNumber(left)) {
            left = ((maxWidth - width) * Math.random()) + (safeArea.left);
        }

        const attributes = _.extend(
            defaultAttributes,
            options.attributes,
            {
                height,
                width,
                initialHeight: height,
                initialWidth: width,
                top,
                left,
                userCreated: true,
                url: options.image.previewUrl,
                previewUrl: options.image.previewUrl,
                xtiUrl: options.image.xtiUrl,
                printUrl: options.image.printUrl,
                fileType: options.fileType,
                naturalDimensions: {
                    width: image.width,
                    height: image.height
                },
                requestId: options.image.requestId, // we need the request id in order to raise an event to the account adapter to save our data for next time
                uploadId: options.image.uploadId, // we need the upload id in order to properly process embroidery images that were not processed
                digitizedId: options.image.digitizedId // Embroidery images have a digitization id that is used for preview, when process is completed.
            });

        // Don't execute the add command, because we don't need it to create internal diffs or do any logging.
        // We just want it to do its work and leave the rest to us.
        const addCommand = new AddCommand();
        const added = addCommand.apply({
            model: parent,
            items: [attributes],
            selectNewModels: options.selectNewModels
        });

        this.triggerGlobal('analytics:track', added.itemViewModels, 'Insert');

        const viewModel = added.itemViewModels[0];

        if (viewModel.get('digitizationNeeded')) {
            const upload = new Backbone.Model({
                requestId: viewModel.model.get('requestId'),
                pages: [
                    {
                        uploadId: viewModel.model.get('uploadId')
                    }
                ]
            });

            processingStepsManager.executeUploadSteps(upload, viewModel)
                .then(() => {
                    eventBus.trigger(events.uploadEmbroideryProcessComplete, upload);

                    viewModel.model.set({
                        digitizedId: upload.get('pages')[0].digitizedId,
                        xtiUrl: uploadsUrlBuilder.getOriginalUrl(upload.get('pages')[0].digitizedId)
                    });
                });
        }

        return viewModel;
    }
});
