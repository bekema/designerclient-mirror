const Command = require('commands/Command');

module.exports = Command.extend({

    name: 'change-attributes',

    // Requires a `model` and an `attributes` hash
    // Or 'viewModel' and 'attributes / viewModelAttributes'
    apply: function(options) {
        if ((options.models || options.model) && (options.viewModels || options.viewModel)) {
            throw new Error('Pass either models or viewmodels, not both');
        }

        if (options.models || options.model) {
            const models = options.models || [options.model];
            models.forEach(function(model) {
                model.set(options.attributes);
            });
        } else if (options.viewModels || options.viewModel) {
            const viewModels = options.viewModels || [options.viewModel];
            viewModels.forEach(function(viewModel) {
                viewModel.model.set(options.attributes);
                viewModel.set(options.viewModelAttributes);
            });
        }
    }
});
