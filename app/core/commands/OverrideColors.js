const Command = require('commands/Command');

module.exports = Command.extend({
    name: 'override-colors',

    apply: function(options) {
        options.viewModel.model.set({ colorOverrides: options.colorOverrides });
    }
});