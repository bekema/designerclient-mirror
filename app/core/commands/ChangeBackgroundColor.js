const _ = require('underscore');
const Command = require('commands/Command');
const AddCommand = require('commands/Add');
const DeleteCommand = require('commands/Delete');
const shapeConfig = require('publicApi/configuration/core/items').shape;
const defaults = shapeConfig.defaults;

const removeBackground = function(currentBackground) {
    const deleteCommand = new DeleteCommand();
    deleteCommand.apply({
        viewModel: { model: currentBackground }
    });
};

const addBackground = function(model, attributes) {
    const addCommand = new AddCommand();
    addCommand.apply({
        model,
        items: [attributes]
    });
};

const setAttributes = function({ model, color }) {
    const attributes = _.extend({}, defaults.Rectangle, {
        left: 0,
        top: 0,
        strokeWidth: 0,
        width: model.get('width'),
        height: model.get('height'),
        isBackground: true,
        zIndex: -1001,
        zIndexLock: true
    });

    if (color) {
        attributes.fillColor = color.hex;
    }

    return attributes;
};

const getCurrentBackground = function(model) {
    return _.find(model.items.models, (m) => m && m.get('isBackground'));
};

module.exports = Command.extend({
    name: 'change-background-color',

    apply: function(options) {
        const currentBackground = getCurrentBackground(options.model);
        const attributes = setAttributes(options);

        if (!options.color && currentBackground) {
            return removeBackground(currentBackground);
        }

        if (currentBackground) {
            return currentBackground.set(attributes);
        }

        if (options.color) {
            return addBackground(options.model, attributes);
        }
    }
});
