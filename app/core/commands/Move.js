// The `Move` command takes in a list of `viewModels` and a `delta` object that describes
// how much to move the `top` and `left` of each model.
const Command = require('commands/Command');
const DeleteCommand = require('commands/Delete');
const eventBus = require('EventBus');
const events = eventBus.events;

const isViewModelOutOfBounds = function(viewModel) {
    const parentModel = viewModel.parent.model;
    const model = viewModel.model;

    const calculatedBounds = {
        top: model.get('top'),
        left: model.get('left'),
        bottom: model.get('top') + model.get('height'),
        right: model.get('left') + model.get('width')
    };

    return calculatedBounds.bottom < 0
            || calculatedBounds.right < 0
            || calculatedBounds.left > parentModel.get('width')
            || calculatedBounds.top > parentModel.get('height');
};

module.exports = Command.extend({

    name: 'move',

    apply: function(options) {
        // track viewModels that are out of parent bounds
        const outOfBoundsViewModels = [];
        options.viewModels.forEach(function(viewModel) {
            const top = viewModel.model.get('top') + options.delta.top;
            const left = viewModel.model.get('left') + options.delta.left;
            viewModel.model.set({
                top,
                left
            });

            if (isViewModelOutOfBounds(viewModel)) {
                outOfBoundsViewModels.push(viewModel);
            }
        });

        if (options.viewModels.length) {
            eventBus.trigger(events.manageCanvases, { activeCanvas: options.viewModels[0].parent.model.id });
        }

        //delete any models moved outside of the bounds of their parent
        if (outOfBoundsViewModels.length > 0) {
            const deleteCommand = new DeleteCommand();
            deleteCommand.apply({
                viewModels: outOfBoundsViewModels
            });
        }

        this.triggerGlobal('analytics:track', options.viewModels, 'Move');
    }
});
