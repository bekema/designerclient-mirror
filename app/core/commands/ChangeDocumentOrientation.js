const Command = require('commands/Command');
const documentRepository = require('DocumentRepository');

module.exports = Command.extend({

    name: 'rotate-document',

    apply: function() {
        const doc = documentRepository.getActiveDocument();
        doc.set({ alternativeOrientation: !doc.get('alternativeOrientation') });

        const canvases = documentRepository.getAllCanvasViewModels();
        canvases.forEach(canvas => {
            canvas.chromes.models.forEach(model => {
                if (model.get('rotation') === 0) {
                    model.set({ rotation: 90 });
                } else {
                    model.set({ rotation: 0 });
                }
            });
        });

        doc.canvases.forEach(model => {
            model.set({ height: model.get('width'), width: model.get('height') });
        });
    }
});
