// Create a new shape and add it to the specified model.
const _ = require('underscore');
const Command = require('commands/Command');
const AddCommand = require('commands/Add');
const shapeConfig = require('publicApi/configuration/core/items').shape;
const defaults = shapeConfig.defaults;

const updatePositionAndSize = (canvas, attributes) => {
    const { width: maxWidth, height: maxHeight } = canvas.get('safeArea');

    if (!_.isNumber(attributes.width) || !_.isNumber(attributes.height)) {
        attributes.width = attributes.height = Math.min(maxWidth, maxHeight) * shapeConfig.initialSize;

        if (attributes.module === 'Line') {
            // fix up the height for lines
            attributes.height = 1;
        }

        if (_.isNumber(attributes.top) && attributes.module !== 'Line') {
            attributes.top -= attributes.height / 2;
        }
        if (_.isNumber(attributes.left)) {
            attributes.left -= attributes.width / 2;
        }
    }

    if (!_.isNumber(attributes.top)) {
        attributes.top = maxHeight / 2 - attributes.height / 2;
    }

    if (!_.isNumber(attributes.width)) {
        attributes.left = maxWidth / 2 - attributes.width / 2;
    }
};

module.exports = Command.extend({
    name: 'create-shape',

    apply: function(options) {
        const attributes = _.extend({}, defaults[options.shapeType], options.attributes);

        updatePositionAndSize(options.viewModel, attributes);

        const addCommand = new AddCommand();
        const newItems = addCommand.apply({
            model: options.viewModel.model,
            items: [attributes],
            selectNewModels: true
        });

        this.triggerGlobal('analytics:track', newItems.itemViewModels, 'Insert');
    }
});