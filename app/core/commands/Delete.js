const _ = require('underscore');
const Command = require('commands/Command');

module.exports = Command.extend({

    name: 'delete',

    apply: function(options) {
        const viewModels = options.viewModels || [options.viewModel];
        const models = viewModels.map(viewModel => viewModel.model);
        _.invoke(models, 'destroy');
        this.triggerGlobal('analytics:track', viewModels, 'Delete');
    }
});
