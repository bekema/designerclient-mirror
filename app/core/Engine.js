const _ = require('underscore');
const $ = require('jquery');
const Backbone = require('backbone');
const metaDataProvider = require('MetaDataProvider');
const documentFactory = require('DocumentFactory');
const viewModelFactory = require('ViewModelFactory');
const Document = require('models/Document');
const documentRepository = require('DocumentRepository');
const Workspace = require('views/Workspace');
const commandDispatcher = require('CommandDispatcher');
const config = require('Configuration');
const globalConfig = require('configuration/Global');
const selectionManager = require('SelectionManager');
const zoomable = require('behavior/Zoomable');
const canvasManager = require('CanvasManager');

// Loads and enables any modules that have been specified as enabled in config/behavior.
const enableBehaviors = function() {
    _.each(config.behavior, function(val, behavior) {
        // Behaviors can be configured as simple { string : boolean } pairs, in which case, the module is assumed
        // to be in the behavior directory.
        if (_.isBoolean(val) && val) {
            require(`behavior/${behavior}`).enable();
        // More complex behaviors should specify an `enabled` boolean.
        } else if (val && val.enabled) {
            val.module.enable();
        }
    });
};

// Hooks up all of { event : command } subscriptions specified in config/commands.
const configureCommands = function() {
    commandDispatcher.subscribe(config.commands);
};

const createCanvasFromViewModel = function(canvasViewModel, index) {
    const uiConfig = require('publicApi/configuration/ui');

    documentRepository.addCanvasViewModel(canvasViewModel);

    //TODO Make divider work with embroidery and more tied to canvases (perhaps make it a viewModel or something)
    const dividerTemplate = uiConfig.canvas.dividerTemplate || "<div class='dcl-canvas-divider'></div>";

    if (index !== 0) {
        $(uiConfig.canvas.container).append(dividerTemplate);
    }

    metaDataProvider.getView(canvasViewModel).render().$el.appendTo(uiConfig.canvas.container);

    // Initialize zoom factor based on browser width
    zoomable.initializeZoomFactor(canvasViewModel);
};

// Create the document model and view model.
const createModels = function({ document, surfaceSpecifications, canvasObjects, scenes }) {
    this.doc = new Document(document);

    this.docViewModel = viewModelFactory.createDocumentViewModel(this.doc, surfaceSpecifications, scenes, canvasObjects);
    this.docViewModel.canvasViewModels.forEach(createCanvasFromViewModel);
    const canvasConfig = require('publicApi/configuration/ui').canvas;
    canvasManager.manageCanvases(canvasConfig);
};

const filterCustomizableSurfaces = function(surfaces) {
    return surfaces.filter(surface =>
        !surface.fulfillerMetadata || !surface.fulfillerMetadata.some(metadata => metadata.key.toLowerCase() === 'iscustomizable' && metadata.value.toLowerCase() === 'false'));
};

// The Engine isn't a model or a view. It's just a "class" that supports Backbone events.
const Engine = function() {};
_.extend(Engine.prototype, Backbone.Events, {
    // Call `Engine.start()` in order to make the editor usable.
    // TODO It should also support the promise pattern.
    start: function({ document, surfaceSpecifications, scenes, canvasObjects }) {

        // delete not customizable surfaces
        surfaceSpecifications.surfaces = filterCustomizableSurfaces(surfaceSpecifications.surfaces);

        document = documentFactory.createDocument(document, surfaceSpecifications);
        documentFactory.validateDocument(document, surfaceSpecifications);

        enableBehaviors();
        configureCommands();

        metaDataProvider.registerMetaData(config.metadata);

        //TODO this probably shouldn't be how we store the scenes...
        globalConfig.scenes = scenes;

        // Create the document model and view model.
        createModels.call(this, { document, surfaceSpecifications, scenes, canvasObjects });

        // Setup the "master" view that is attached to the body.
        this.workspace = new Workspace({ viewModel: this.docViewModel });
        this.workspace.render();

        // Trigger an event to notify that the engine has started.
        this.triggerGlobal('startcomplete');

        selectionManager.manage(this.docViewModel.canvasViewModels.models);

        Backbone.Events.triggerGlobal('editorready');
    }
});

module.exports = Engine;
