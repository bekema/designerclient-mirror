const _ = require('underscore');
const eventBus = require('EventBus');
const events = eventBus.events;

const UPLOADS = 'uploads';
const DOCUMENTREFERENCE = 'documentReference';

const initLocalStorage = function(itemName, defaultValue) {
    if (!localStorage.getItem(itemName)) {
        localStorage.setItem(itemName, JSON.stringify(defaultValue));
    }
};

const getItem = function(itemName) {
    return JSON.parse(localStorage.getItem(itemName));
};

const setItem = function(itemName, value) {
    localStorage.setItem(itemName, JSON.stringify(value));
};

class AccountAdapter {
    constructor() {
        initLocalStorage(UPLOADS, []);
        initLocalStorage(DOCUMENTREFERENCE, {});

        eventBus.on(events.uploadComplete, this.storeUpload, this);
        eventBus.on(events.uploadDelete, this.deleteUpload, this);
        eventBus.on(events.uploadPostStepsComplete, this.updateMetaData, this);
        eventBus.on(events.uploadEmbroideryProcessComplete, this.updateMetaData, this);
    }

    verifyUploadsUrlBuilderExists() {
        if (!this.uploadsUrlBuilder) {
            const uploadClient = require('services/clientProvider').upload;
            this.uploadsUrlBuilder = uploadClient.uploadsUrlBuilder;
        }
    }

    updateMetaData(model) {
        const storedUploads = getItem(UPLOADS);
        let uploadIndex = null;
        for (let i = 0, len = storedUploads.length; i < len; i++) {
            if (storedUploads[i].requestId === model.get('requestId')) {
                uploadIndex = i;
                break;
            }
        }
        if (uploadIndex !== null) {

            this.verifyUploadsUrlBuilderExists();

            const updateItem = storedUploads[uploadIndex];
            _.each(updateItem.pages, (upload) => {
                upload.preparedId = model.get('pages')[0].preparedId;
                upload.cleanId = model.get('pages')[0].cleanId;
                upload.digitizedId = model.get('pages')[0].digitizedId;
                model.get('pages')[0].xtiUrl = this.uploadsUrlBuilder.getOriginalUrl(upload.digitizedId);
                upload.xtiUrl = model.get('pages')[0].xtiUrl;
                upload.uploadId = model.get('pages')[0].uploadId;
            });

            setItem(UPLOADS, storedUploads);
        }
    }

    getUploads() {
        return Promise.resolve(JSON.parse(localStorage.getItem(UPLOADS)));
    }

    storeUpload(uploadGroup) {
        const storableUploadGroup = {
            fileName: uploadGroup.get('fileName'),
            fileType: uploadGroup.get('fileType'),
            requestId: uploadGroup.get('requestId'),
            pages: []
        };
        _.each(uploadGroup.get('pages'), (upload) => {
            storableUploadGroup.pages.push({
                uploadId: upload.uploadId,
                width: upload.width,
                height: upload.height
            });
        });
        const storedUploads = getItem(UPLOADS);
        storedUploads.push(storableUploadGroup);
        setItem(UPLOADS, storedUploads);
    }

    deleteUpload(requestId) {
        const storedUploads = getItem(UPLOADS);
        let uploadIndex = null;
        for (let i = 0, len = storedUploads.length; i < len; i++) {
            if (storedUploads[i].requestId === requestId) {
                uploadIndex = i;
                break;
            }
        }
        if (uploadIndex !== null) {
            storedUploads.splice(uploadIndex, 1);
            setItem(UPLOADS, storedUploads);
        }
    }

    saveDocument(documentJson) {
        const documents = getItem(DOCUMENTREFERENCE);
        documents[documentJson.documentReference] = documentJson;
        setItem(DOCUMENTREFERENCE, documents);

        return Promise.resolve(documentJson);
    }

    loadDocument(documentReference) {
        return Promise.resolve(getItem(DOCUMENTREFERENCE)[documentReference]);
    }

    getAllDocuments() {
        if (_.isEmpty(getItem(DOCUMENTREFERENCE))) {
            return Promise.resolve([]);
        }

        const documents = [];
        _.each(getItem(DOCUMENTREFERENCE), document => {
            documents.push(document);
        });

        return Promise.resolve(documents);
    }
}

module.exports = AccountAdapter;
