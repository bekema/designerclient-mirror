const $ = require('jquery');
const tokenProvider = require('services/authTokens/TokenProvider');

const makeRequest = (url, authToken, data) => {
    return Promise.resolve(
        $.ajax({
            url,
            type: 'POST',
            headers: {
                Authorization: authToken
            },
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'json',
            processData: false
        }));
};

class CrispifyAdapter {
    constructor({ url }) {
        this.baseUrl = url;
    }

    denoiseImage(imageUrl, DenoisingLevel) {
        const data = {
            imageUrl,
            DenoisingLevel
        };

        const url = `${this.baseUrl}/v2/denoise`;

        return tokenProvider.getToken('crispify')
            .then(token => makeRequest(url, token, data));
    }

    superResolutionImage(imageUrl) {
        const data = {
            imageUrl
        };
        const url = `${this.baseUrl}/v2/superResolution`;

        return tokenProvider.getToken('crispify')
            .then(token => makeRequest(url, token, data));
    }
}

CrispifyAdapter.validateOptions = function({ url }) {
    const errors = [];

    if (!url) {
        errors.push('url required');
    }

    if (errors.length) {
        throw Error(errors.join('; '));
    }
};

module.exports = CrispifyAdapter;