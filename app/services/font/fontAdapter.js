const $ = require('jquery');
const context = require('DesignerContext');

const getFonts = (fontRepositoryUrl) => {
    const url = 'https://misc-rendering.documents.cimpress.io/fontlist';
    const data = {
        fontRepositoryUrl,
        fontFlavor: context.fontFlavor
    };

    return Promise.resolve($.getJSON(url, data));
};

class FontAdapter {
    constructor({ availableFonts, fontRepositoryUrl, defaultFont }) {
        this.fontRepositoryUrl = fontRepositoryUrl;
        this._defaultFontName = defaultFont.toLowerCase() || '';
        this.availableFonts = (availableFonts || []).map(font => font.toLowerCase());
    }

    initializeFontData(allFonts) {
        const filteredFonts = this.availableFonts && this.availableFonts.length
            ? allFonts.filter(font => this.availableFonts.indexOf(font.FontFamily.toLowerCase()) >= 0)
            : allFonts;

        this.fonts = filteredFonts.length ? filteredFonts : allFonts;

        this._defaultFontName = this.fontNamesLowerCase.indexOf(this._defaultFontName) >= 0
            ? this._defaultFontName
            : this.fontNamesLowerCase[0];
    }

    initialize() {
        if (this.initialized) {
            return;
        }

        this.initialized = true;

        return getFonts(this.fontRepositoryUrl)
            .then(fontList => this.initializeFontData(fontList));
    }

    get defaultFont() {
        return this.fontMapping[this._defaultFontName];
    }

    get fontNames() {
        if (!this._fontNames) {
            this._fontNames = this.fonts.map(fontData => fontData.FontFamily);
        }
        return this._fontNames;
    }

    get fontNamesLowerCase() {
        if (!this._fontNamesLowerCase) {
            this._fontNamesLowerCase = this.fontNames.map(fontName => fontName.toLowerCase());
        }
        return this._fontNamesLowerCase;

    }

    get fontMapping() {
        if (!this._fontMap) {
            this._fontMap = this.fonts.reduce((fontMap, font) => {
                fontMap[font.FontFamily.toLowerCase()] = font;
                return fontMap;
            }, {});
        }
        return this._fontMap;
    }
}

module.exports = FontAdapter;
