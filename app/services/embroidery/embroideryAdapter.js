const $ = require('jquery');
const _ = require('underscore');
const colorConverter = require('util/ColorConverter');
const tokenProvider = require('services/authTokens/TokenProvider');

const makeGetRequest = function(url) {
    return Promise.resolve(
        $.ajax({
            url,
            type: 'GET',
            contentType: 'application/json',
            processData: false
        })
    );
};

const makePostRequest = function({ url, token, data, dataType }) {
    return Promise.resolve($.ajax({
        url,
        type: 'POST',
        headers: {
            Authorization: token
        },
        data,
        dataType,
        processData: false,
        contentType: 'application/json'

    }));
};

class EmbroideryAdapter {
    constructor({ embroideryUrl, cleanImageUrl, digitizationUrl }) {
        this.baseUrl = embroideryUrl;
        this.cleanImageUrl = cleanImageUrl;
        this.digitizationUrl = digitizationUrl;
    }

    translateThreadToColorInfo() {
        this.colorInfoList = this.threads.map(thread => {
            const rgb = colorConverter.hex2rgb(thread.rgb);
            const mappingThread = _.find(this.threadMapping, (item) => item.id === thread.id);

            const translatedThread = {
                Red: rgb.r,
                Green: rgb.g,
                Blue: rgb.b
            };

            if (mappingThread) {
                translatedThread.ThreadID = mappingThread.vpColorId;
            }

            return translatedThread;
        });
    }

    loadThreadData() {
        const requests = [];

        requests.push(makeGetRequest(`${this.baseUrl}/threads`)
            .then(threadData => this.fullThreadList = threadData)
        );

        requests.push(makeGetRequest(`${this.baseUrl}/standard-palette`)
            .then(paletteList => this.paletteList = paletteList)
        );

        requests.push(makeGetRequest(`${this.baseUrl}/columbus-mapping`)
            .then(threadData => this.threadMapping = threadData)
        );

        return Promise.all(requests)
            .then(this.translateThreadToColorInfo.bind(this));
    }

    get threads() {
        if (!this._threads) {
            this._threads = this.fullThreadList.filter(threadData => this.paletteList.indexOf(threadData.id) > -1);  //eslint-disable-line no-magic-numbers
        }

        return this._threads;
    }

    interrogate(xtiUrl, colorMapping) {
        const url = `${this.baseUrl}/dcl/interrogate?xtiUrl=${xtiUrl}&vpColorMappingUrl=${colorMapping}`;

        return makeGetRequest(url);
    }

    threadForHex(hexColor) {
        const thread = _.find(this.threads, (item) => item.rgb === hexColor);
        return thread ? thread.id : null;
    }

    hexForThread(threadId) {
        const thread = _.find(this.threads, (item) => item.id === threadId);
        return thread ? thread.rgb : null;
    }

    cleanImage(preparedImageUrl) {
        return tokenProvider.getToken('cleanimage').then(token =>
        makePostRequest({
            url: `${this.cleanImageUrl}?fileUrl=${preparedImageUrl}`,
            token: token,
            data: JSON.stringify(this.colorInfoList),
            dataType: 'json'
        }));
    }

    digitizeImage(ImageUrl) {
        // We have to provide a background color, we are sending transparent white
        // the digitization service will not work without a background color if the image is transparent
        // clean image *always* provides a transparent color
        // taking the color from the image gives us a black transparent color, which does not work....
        // so we are just hard coding it here...because this does.
        const fakeTransparency = 0x00FFFFFF;

        const data = {
            ImageUrl,
            BackgroundColor: fakeTransparency,
            ThreadProfileId: 'columbus',
            ColorInfoList: this.colorInfoList
        };
        return tokenProvider.getToken('digitization').then(token =>
        makePostRequest({
            url: this.digitizationUrl,
            token: token,
            data: JSON.stringify(data),
            dataType: 'text'
        }));
    }
}

module.exports = EmbroideryAdapter;
