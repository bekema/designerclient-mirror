const $ = require('jquery');
const tokenProvider = require('services/authTokens/TokenProvider');

const requestPdf = function(documentInstructionsUrl, url, token) {
    const data = {
        DocumentInstructionsUrl: documentInstructionsUrl,
        PriorityOverride: 1
    };

    return Promise.resolve(
        $.ajax({
            url,
            type: 'POST',
            headers: {
                Authorization: token
            },
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'json',
            processData: false
        }));
};

class PrepressAdapter {
    constructor({ url }) {
        this.baseUrl = url;
    }

    getProof(documentInstructionsUrl) {
        return tokenProvider.getToken('prepress').then(token =>
        requestPdf(documentInstructionsUrl, this.baseUrl, token)
            .then(response => Promise.resolve(response.Output)));
    }
}

PrepressAdapter.validateOptions = function({ url }) {
    const errors = [];

    if (!url) {
        errors.push('url required');
    }

    if (errors.length) {
        throw Error(errors.join('; '));
    }
};

module.exports = PrepressAdapter;