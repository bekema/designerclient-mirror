const $ = require('jquery');
const tokenProvider = require('services/authTokens/TokenProvider');

const TEMP_PDF_PROFILE_URL = 'https://uploads.documents.cimpress.io/v1/uploads/12aaf249-7f95-4810-9517-b0434576cc1f~110';

const makeRequest = (url, token, data) => {
    return Promise.resolve(
        $.ajax({
            url,
            type: 'POST',
            headers: {
                Authorization: token
            },
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'json',
            processData: false
        }));
};

class PreflightAdapter {
    constructor({ url }) {
        this.baseUrl = url;
    }

    validatePdf(pdfUrl) {
        const data = {
            PdfUrl: pdfUrl,
            ProfileUrl: TEMP_PDF_PROFILE_URL
        };

        const url = `${this.baseUrl}/preflight/v1`;

        return tokenProvider.getToken('preflight')
            .then(token =>  makeRequest(url, token, JSON.stringify(data)));
    }
}

PreflightAdapter.validateOptions = function({ url }) {
    const errors = [];

    if (!url) {
        errors.push('url required');
    }

    if (errors.length) {
        throw Error(errors.join('; '));
    }
};

module.exports = PreflightAdapter;
