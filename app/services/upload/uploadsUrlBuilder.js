const UPLOAD_ID_TEMPLATE_PARAMETER = '{uploadId}';

class UploadsUrlBuilder {
    constructor({ originalUrlTemplate, previewUrlTemplate, thumbnailUrlTemplate, printUrlTemplate }) {
        this.originalUrlTemplate = originalUrlTemplate;
        this.previewUrlTemplate = previewUrlTemplate;
        this.thumbnailUrlTemplate = thumbnailUrlTemplate;
        this.printUrlTemplate = printUrlTemplate;
    }

    getOriginalUrl(uploadId) {
        return this.originalUrlTemplate.replace(UPLOAD_ID_TEMPLATE_PARAMETER, uploadId);
    }

    getPreviewUrl(uploadId) {
        return this.previewUrlTemplate.replace(UPLOAD_ID_TEMPLATE_PARAMETER, uploadId);
    }

    getPrintUrl(uploadId) {
        return this.printUrlTemplate.replace(UPLOAD_ID_TEMPLATE_PARAMETER, uploadId);
    }

    getThumbnailUrl(uploadId) {
        return this.thumbnailUrlTemplate.replace(UPLOAD_ID_TEMPLATE_PARAMETER, uploadId);
    }
}

UploadsUrlBuilder.validateOptions = function({ originalUrlTemplate, previewUrlTemplate, thumbnailUrlTemplate, printUrlTemplate }) {
    const errors = [];
    if (!originalUrlTemplate) {
        errors.push('originalUrlTemplate required');
    }

    if (originalUrlTemplate && originalUrlTemplate.indexOf(UPLOAD_ID_TEMPLATE_PARAMETER) === -1) { // eslint-disable-line
        errors.push(`originalUrlTemplate must contain the parameter ${UPLOAD_ID_TEMPLATE_PARAMETER}`);
    }
    if (!thumbnailUrlTemplate) {
        errors.push('thumbnailUrlTemplate required');
    }

    if (thumbnailUrlTemplate && thumbnailUrlTemplate.indexOf(UPLOAD_ID_TEMPLATE_PARAMETER) === -1) { // eslint-disable-line
        errors.push(`thumbnailUrlTemplate must contain the parameter ${UPLOAD_ID_TEMPLATE_PARAMETER}`);
    }

    if (!previewUrlTemplate) {
        errors.push('previewUrlTemplate required');
    }

    if (previewUrlTemplate && previewUrlTemplate.indexOf(UPLOAD_ID_TEMPLATE_PARAMETER) === -1) { // eslint-disable-line
        errors.push(`previewUrlTemplate must contain the parameter ${UPLOAD_ID_TEMPLATE_PARAMETER}`);
    }

    if (!printUrlTemplate) {
        errors.push('printUrlTemplate required');
    }

    if (printUrlTemplate && printUrlTemplate.indexOf(UPLOAD_ID_TEMPLATE_PARAMETER) === -1) { // eslint-disable-line
        errors.push(`printUrlTemplate must contain the parameter ${UPLOAD_ID_TEMPLATE_PARAMETER}`);
    }

    return errors;
};


module.exports = UploadsUrlBuilder;
