const $ = require('jquery');
const _ = require('underscore');
const eventBus = require('EventBus');
const events = eventBus.events;
const validationProvider = require('validations/ValidationProvider');
const util = require('util/Util');
const UploadsUrlBuilder = require('services/upload/UploadsUrlBuilder');

require('imports?define=>false!blueimp-file-upload');

const PDF_MIME_TYPE = 'application/pdf';
const MERCHANT_TEMPLATE_PARAMETER = '{merchant}';

class UploadAdapter {
    constructor({ form, dropZone, pdfUploadUrl, imageUploadUrl, originalUrlTemplate, previewUrlTemplate, thumbnailUrlTemplate, printUrlTemplate, merchantId }) {
        const $form = $(form);
        const $dropZone = $(dropZone);

        this.uploadsUrlBuilder = new UploadsUrlBuilder({ originalUrlTemplate, previewUrlTemplate, thumbnailUrlTemplate, printUrlTemplate });

        // default PDF url to the image URL if it's not specified
        pdfUploadUrl = pdfUploadUrl || imageUploadUrl;

        imageUploadUrl = imageUploadUrl.replace(MERCHANT_TEMPLATE_PARAMETER, merchantId);
        this.imageUploadUrl = imageUploadUrl;

        pdfUploadUrl = pdfUploadUrl.replace(MERCHANT_TEMPLATE_PARAMETER, merchantId);

        this.promiseHashtable = {};
        $form.fileupload({
            dataType: 'json',
            dropZone: $dropZone,
            pasteZone: $dropZone,
            resetForm: true,
            paramName: 'file[]'
        }).on('fileuploaddone', (e, response) => {
            const promise = this.getPromise(response);
            promise.resolve(this.parseResponse(response.result, response.requestId));
            this.deletePromise(response);
        }).on('fileuploadfail', (e, response) => {
            const promise = this.getPromise(response);
            promise.reject(response);
            this.deletePromise(response);
        }).on('fileuploadprogress', (e, response) => {
            eventBus.trigger(events.uploadProgress, { loaded: response.loaded, total: response.total, requestId: response.requestId });
        }).on('fileuploadsubmit', (e, response) => {
            if (validationProvider.uploadSize) {
                const validation = validationProvider.uploadSize.validate(response);
                if (!validation.successful) {
                    return false; // Returning false blocks the upload :)
                }
            }
            const properUrl = response.files[0].type === PDF_MIME_TYPE ? pdfUploadUrl : imageUploadUrl;
            $form.fileupload('option', 'url', properUrl);
            this.createPromise(response);
        });
    }

    retrieveUploads(previousUploads) {
        const retrievedUploads = previousUploads.map((uploadGroup) => {
            uploadGroup.uploadComplete = true;
            uploadGroup.pages = uploadGroup.pages.map((upload, i) => {
                upload.uploadIndex = i + 1;
                upload.printUrl = this.uploadsUrlBuilder.getPrintUrl(upload.uploadId);
                upload.originalUrl = this.uploadsUrlBuilder.getOriginalUrl(upload.uploadId);
                upload.previewUrl = this.uploadsUrlBuilder.getPreviewUrl(upload.uploadId);
                upload.thumbnailUrl = this.uploadsUrlBuilder.getThumbnailUrl(upload.uploadId);
                upload.xtiUrl = this.uploadsUrlBuilder.getOriginalUrl(upload.digitizedId);
                upload.requestId = uploadGroup.requestId;

                return upload;
            });
            return uploadGroup;
        });
        return Promise.resolve(retrievedUploads);
    }

    getPromise(response) {
        return this.promiseHashtable[response.requestId];
    }

    deletePromise(response) {
        this.promiseHashtable[response.requestId] = undefined;
    }

    createPromise(response) {
        const promise = new Promise((resolve, reject) => {
            response.requestId = util.generateUUID();
            this.promiseHashtable[response.requestId] = { promise: this, resolve, reject };
        });

        eventBus.trigger(events.uploadNew, { requestId: response.requestId, promise, file: response.files[0] });
    }

    parseResponse(response, requestId) {
        return {
            fileName: response[0].uploadedFileName,
            fileType: response[0].originalFileContentType,
            requestId: requestId,
            pages: _.map(response, (upload, i) => {
                const obj = {
                    pageNumber: i + 1,
                    uploadTime: upload.processingTimeEnd,
                    fileSize: upload.originalFileSize,
                    uploadId: upload.uploadId,
                    originalUrl: this.uploadsUrlBuilder.getOriginalUrl(upload.uploadId),
                    printUrl: this.uploadsUrlBuilder.getPrintUrl(upload.uploadId),
                    previewUrl: this.uploadsUrlBuilder.getPreviewUrl(upload.uploadId),
                    thumbnailUrl: this.uploadsUrlBuilder.getThumbnailUrl(upload.uploadId),
                    height: upload.printPixelHeight,
                    width: upload.printPixelWidth
                };

                return obj;
            })
        };
    }

    uploadFileBlob(fileBlob, fileName) {
        // TODO - it feels wrong to expose another URL paramter, but it feels even more wrong to do this...
        // perhaps we manage all the query string data parameters internally instead of by provided url?
        const url = this.imageUploadUrl.split('?')[0];

        const fd = new FormData();
        fd.append('fname', fileName);
        fd.append('file', fileBlob);

        return Promise.resolve($.ajax({
            url: `${url}?process={type: 'storeOnly'}`,
            type: 'POST',
            data: fd,
            processData: false,
            contentType: false
        }));
    }
}

UploadAdapter.validateOptions = function({ form, dropZone, imageUploadUrl, pdfUploadUrl, originalUrlTemplate, previewUrlTemplate, thumbnailUrlTemplate, printUrlTemplate, merchantId }) {
    const errors = UploadsUrlBuilder.validateOptions({ originalUrlTemplate, previewUrlTemplate, thumbnailUrlTemplate, printUrlTemplate });

    if (!form) {
        errors.push('form element or selector required');
    }

    const $form = $(form);

    if (form && !$form.length) {
        errors.push('form did not resolve to an actual element');
    }

    if ($form.length && $form.prop('tagName') !== 'FORM') {
        errors.push(`form must resolve to a form element, not a ${$form.prop('tagName')} element`);
    }

    if (dropZone && !$(dropZone).length) {
        errors.push('dropZone did not resolve to an actual element');
    }

    if (!imageUploadUrl) {
        errors.push('imageUploadUrl required');
    }

    if (imageUploadUrl && imageUploadUrl.indexOf(MERCHANT_TEMPLATE_PARAMETER) === -1) { // eslint-disable-line
        errors.push(`imageUploadUrl must contain the parameter ${MERCHANT_TEMPLATE_PARAMETER}`);
    }

    if (pdfUploadUrl && pdfUploadUrl.indexOf(MERCHANT_TEMPLATE_PARAMETER) === -1) { // eslint-disable-line
        errors.push(`pdfUploadUrl must contain the parameter ${MERCHANT_TEMPLATE_PARAMETER}`);
    }

    if (!merchantId) {
        errors.push('merchantId required');
    }

    if (errors.length) {
        throw Error(errors.join('; '));
    }
};

module.exports = UploadAdapter;
