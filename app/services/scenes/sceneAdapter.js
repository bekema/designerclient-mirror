const $ = require('jquery');
const tokenProvider = require('services/authTokens/TokenProvider');

class SceneAdapter {
    constructor({ url }) {
        this.baseUrl = url;
    }

    getScenes(surfaces) {
        return tokenProvider.getToken('scene')
            .then(token => Promise.all(surfaces.map(surface => this.getSceneForSurface(surface, token))));
    }

    getSceneForSurface(surfaceId, token) {
        return this.query(surfaceId, token)
            .then(response => this.getSceneLink(response))
            .then(link => this.getSceneData(link, token));
    }

    query(surfaceId, token) {
        const url = `${this.baseUrl}/v1/surfaceScenesQuery`;

        return new Promise((resolve, reject) => {
            $.ajax({
                type: 'GET',
                url,
                headers: {
                    Authorization: token
                },
                data: {
                    surfaceId
                },
                cache: false
            }).then(resolve,
                function(jqXhr) {
                    reject({ statusCode: jqXhr.status, response: jqXhr.responseJSON });
                }
            );
        });
    }

    getSceneLink(queryResponse) {
        if (!queryResponse.results) {
            return Promise.reject('Surface scene query failed');
        }

        if (!queryResponse.results.length) {
            return null;
        }

        // TODO: Are these assumptions about the structure going to hold across all products?
        return queryResponse.results[0].links[0].href;
    }

    getSceneData(url, token) {
        if (!url) {
            return null;
        }

        return new Promise((resolve, reject) => {
            $.ajax({
                type: 'GET',
                url,
                headers: {
                    Authorization: token
                },
                cache: false
            }).then(
                resolve,
                jqXhr => reject({ statusCode: jqXhr.statusCode, response: jqXhr.responseJSON })
            );
        });
    }
}

SceneAdapter.validateOptions = function({ url }) {
    const errors = [];

    if (!url) {
        errors.push('url required');
    }

    if (errors.length) {
        throw Error(errors.join('; '));
    }
};

module.exports = SceneAdapter;
