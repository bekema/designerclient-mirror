const $ = require('jquery');
const SERVICE_NAME_TEMPLATE_PARAMETER = '{service}';

/**  Token Contract  **/
/*
    Tokens recieved by this adapter from endpoints specified above must return in the following JSON format:
    {
        ValidTo: 'some date in UTC',
        TokenType: 'Bearer',
        IdToken: 'some long token'
    }
*/
const validateToken = token => {
    if (!token.TokenType || !token.IdToken) {
        throw Error('Token does not follow specified contract');
    }

    return token;
};

const getToken = (urlTemplate, serviceName) => {
    const url = urlTemplate.replace(SERVICE_NAME_TEMPLATE_PARAMETER, encodeURIComponent(serviceName));
    return Promise.resolve($.get({
        url,
        dataType: 'json'
    })).catch(function() { throw Error(`Failed to retrieve auth token for ${serviceName}`); });
};

class TokenRetrievalAdapter {

    constructor({ urlTemplate }) {
        this.urlTemplate = urlTemplate;
    }

    getAuthToken(serviceName) {
        return getToken(this.urlTemplate, serviceName)
            .then(validateToken);
    }
}

TokenRetrievalAdapter.validateOptions = function({ urlTemplate }) {
    if (!urlTemplate) {
        throw Error('urlTemplate required');
    }

    if (urlTemplate && urlTemplate.indexOf(SERVICE_NAME_TEMPLATE_PARAMETER) === -1) { // eslint-disable-line
        throw Error(`urlTemplate must contain the parameter ${SERVICE_NAME_TEMPLATE_PARAMETER}`);
    }
};

module.exports = TokenRetrievalAdapter;