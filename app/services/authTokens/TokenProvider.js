const cachedTokens = {};

module.exports = {
    initialize: function(authSettings) {
        Object.keys(authSettings.tokens).forEach(key => cachedTokens[key] = authSettings.tokens[key]);
    },

    getCachedToken : function(serviceName) {
        return cachedTokens[serviceName];
    },

    tokenInvalid : function(token) {
        return !token || (token.ValidTo && (new Date(token.ValidTo).getTime() < Date.now()));
    },

    // Returns a promise
    getToken : function(serviceName) {
        const token = this.getCachedToken(serviceName);
        const clients = require('services/clientProvider');

        if (this.tokenInvalid(token)) {
            return clients.tokenRetrieval.getAuthToken(serviceName)
            .then(response => {
                cachedTokens[serviceName] = response;
                return Promise.resolve(`${response.TokenType} ${response.IdToken}`);
            });
        } else {
            return Promise.resolve(`${token.TokenType} ${token.IdToken}`);
        }
    }
};