const DesignerContext = require('DesignerContext');

class RenderingAdapter {
    constructor({ url }) {
        this.baseUrl = url;
    }

    renderPreview(renderingInstructionsUrl, size, surfaceName, scene) {
        return `${this.baseUrl}?${this.renderSize(size)}&instructions_uri=${encodeURIComponent(renderingInstructionsUrl)}` +
            `${surfaceName && !DesignerContext.embroidery ? `${encodeURIComponent('&surface=')}` +
            `${surfaceName}` : ''}${scene ? `&scene=${encodeURIComponent(scene)}` : ''}`;
    }

    renderSize(size) {
        return `${size.width ? `${'width=' + size.width}` : ''}${size.height ? `${'&height=' + size.height}` : ''}`;
    }
}

module.exports = RenderingAdapter;