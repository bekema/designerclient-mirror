const $ = require('jquery');
const tokenProvider = require('services/authTokens/TokenProvider');

class SurfaceSpecificationsAdapter {
    constructor({ url }) {
        this.baseUrl = url;
    }

    setToken(token) {
        this.authToken = token;
    }

    getSurfaceSpecifications(mcpSku) {
        return tokenProvider.getToken('surfaceSpecifications')
            .then(token => this.setToken(token))
            .then(() => this.query(mcpSku))
            .then(response => this.getId(response))
            .then(id => this.getSurfaces(id));
    }

    query(mcpSku) {
        const url = `${this.baseUrl}/v1/surfaceSpecificationsQuery?mcpSku=${mcpSku}`;

        return new Promise((resolve, reject) => {
            $.ajax({
                type: 'GET',
                url,
                headers: {
                    Authorization: this.authToken
                },
                cache: false
            }).then(
                resolve,
                jqXhr => reject({ statusCode: jqXhr.status, response: jqXhr.responseJSON })
            );
        });
    }

    getId(queryResponse) {
        if (!queryResponse.results || !queryResponse.results.length) {
            return Promise.reject('Surface specification query returned 0 results');
        }
        return queryResponse.results[0].id;
    }

    getSurfaces(id) {
        const url = `${this.baseUrl}/v2/surfaceSpecifications/${id}`;

        return new Promise((resolve, reject) => {
            $.ajax({
                type: 'GET',
                url,
                headers: {
                    Authorization: this.authToken
                },
                cache: false
            }).then(
                resolve,
                jqXhr => reject({ statusCode: jqXhr.statusCode, response: jqXhr.responseJSON })
            );
        });
    }
}

SurfaceSpecificationsAdapter.validateOptions = function({ url }) {
    const errors = [];

    if (!url) {
        errors.push('url required');
    }

    if (errors.length) {
        throw Error(errors.join('; '));
    }
};

module.exports = SurfaceSpecificationsAdapter;