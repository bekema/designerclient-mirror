const options = require('publicApi/configuration/services/clients');
const authOptions = require('publicApi/configuration/services/authentication');
const tokenProvider = require('services/authTokens/TokenProvider');

const publicApi = {};

const initialize = function() {
    delete publicApi.initialize;

    tokenProvider.initialize(authOptions);

    Object.keys(options).forEach(key => {
        const Constructor = options[key].Constructor;
        const serviceOptions = options[key];

        if (!Constructor) { return; }

        try {
            Constructor.validateOptions && Constructor.validateOptions(serviceOptions);
            publicApi[key] = new Constructor(serviceOptions);
        } catch (e) {
            // TODO: notify through the error module (that doesn't yet exist)
            console.warn(`Service client '${key}' failed to validate options: ${e.message}`); // eslint-disable-line
        }
    });
};

publicApi.initialize = initialize;

module.exports = publicApi;
