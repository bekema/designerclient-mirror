const $ = require('jquery');
const designerContext = require('DesignerContext');

class PreviewAdapter {
    constructor({ url }) {
        this.renderUrl = url;
    }

    render(renderParams) {
        const queryString = $.param(renderParams);
        return `${this.renderUrl}?${queryString}`;
    }

    renderImage(renderOptions) {
        const params = PreviewAdapter.buildImageRenderParams(renderOptions);
        return this.render(params);
    }

    renderTextField(renderOptions) {
        const params = PreviewAdapter.buildTextRenderParams(renderOptions);
        return this.render(params);
    }

    renderShape(renderOptions) {
        const params = PreviewAdapter.buildShapeRenderParams(renderOptions);
        return this.render(params);
    }

    static getPixelToMmRatio(zoom) {
        return `${(1 / zoom)}mm`;
    }

    static buildImageRenderParams({ zoom, width, height, url, crop, colorOverrides, digitizedId }) {
        const json = [
            {
                url,
                position: {
                    top: '0mm',
                    left: '0mm',
                    width: `${width}mm`,
                    height: `${height}mm`
                },
                crop,
                zIndex: 1,
                itemType: 'image'
            }
        ];

        let technology = designerContext.designTechnology;
        if (designerContext.embroidery && digitizedId) {
            // TODO Intentially hard coded - this is temporary requirement until we use MCP digitization
            json[0].vpThreadMapping = 'http://embroidery.documents.cimpress.io/columbus-mapping';
            json[0].colorOverrides = colorOverrides;

            // Send zero crops for embroidery preview.
            json[0].crop = {
                left: 0,
                right: 0,
                top: 0,
                bottom: 0
            };
        } else {
            //we are not in embroidery or we do not have the digitized image yet, so let's preview it in print until digitization is completed.
            technology = designerContext.defaultTechnology();
        }

        return this.normalizedParameters(json, zoom, technology);
    }

    static buildFontStyle({ fontBold, fontUnderline, fontItalic, fontStrikeout }) {
        const styles = [];
        if (fontBold) {
            styles.push('bold');
        }
        if (fontUnderline) {
            styles.push('underline');
        }
        if (fontItalic) {
            styles.push('italic');
        }
        if (fontStrikeout) {
            styles.push('strikeout');
        }
        if (styles.length === 0) {
            return 'normal';
        } else {
            return styles.join();
        }
    }

    static buildTextRenderParams({ width, zoom, fontSize, fontColor, innerHTML, horizontalAlignment, fontBold, fontUnderline, fontItalic, fontStrikeout, fontFamily }) {
        const clients = require('services/clientProvider');
        const json = [
            {
                position: {
                    top: '0mm',
                    left: '0mm',
                    width: `${width}mm`
                },
                content: innerHTML,
                fontSize: `${fontSize}pt`,
                fontStyle: this.buildFontStyle({ fontBold, fontUnderline, fontItalic, fontStrikeout }),
                fontColor: designerContext.embroidery && fontColor.indexOf('thread') > -1 ? fontColor : designerContext.itemColor(fontColor), // eslint-disable-line no-magic-numbers
                fontFamily,
                horizontalAlignment,
                zIndex: 1,
                itemType: 'textfield',
                fontRepositoryUrl: clients.font.fontRepositoryUrl
            }
        ];

        return this.normalizedParameters(json, zoom);
    }

    static buildShapeRenderParams({ module, zoom, fillColor, width, height, strokeColor, strokeWidth, strokeLineJoin, strokeLineCap  }) {
        const json = [
            {
                color: `rgb(${fillColor})`,
                position: {
                    top: '0mm',
                    left: '0mm',
                    width: `${width}mm`,
                    height: `${height}mm`
                },
                stroke: {
                    color: `rgb(${strokeColor})`,
                    thickness: `${strokeWidth}mm`,
                    linejoin: strokeLineJoin,
                    linecap: strokeLineCap
                },
                zIndex: 1,
                itemType: module.toLowerCase()
            }
        ];

        if (module === 'Line') {
            json[0].start = {
                x: '0mm',
                y: '0mm'
            };
            json[0].end = {
                x: `${width}mm`,
                y: '0mm'
            };

            delete json[0].position;
            delete json[0].color;
        }

        return this.normalizedParameters(json, zoom);
    }

    static normalizedParameters(json, zoom, technology = designerContext.designTechnology) {
        return {
            json: JSON.stringify(json),
            pixel: this.getPixelToMmRatio(zoom),
            technology
        };

    }
}

PreviewAdapter.validateOptions = function({ url }) {
    if (!url) {
        throw Error('url required');
    }
};

module.exports = PreviewAdapter;