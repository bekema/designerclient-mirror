const _ = require('underscore');

// Rotation must be flipped as they expect clockwise rotation whereas we use counterclockwise
const MAXROTATION = 360;

const convertToUdsPosition = function({ left, top, width, height }) {

    return {
        x: `${left}mm`,
        y: `${top}mm`,
        width: `${width}mm`,
        height: `${height}mm`
    };
};

const convertToDocumentPosition = function({ y, x, width, height }) {

    return {
        top: parseFloat(y.replace('mm', '')),
        left: parseFloat(x.replace('mm', '')),
        width: parseFloat(width.replace('mm', '')),
        height: parseFloat(height.replace('mm', ''))
    };
};

const convertToUdsStroke = function({ strokeColor, strokeWidth, strokeLineCap, strokeLineJoin }) {

    return {
        color: `rgb(${strokeColor})`,
        thickness: `${strokeWidth}mm`,
        lineCap: strokeLineCap,
        lineJoin: strokeLineJoin
    };
};

const convertToDocumentStroke = function({ color, thickness, lineCap, lineJoin }) {

    return {
        strokeColor: color.replace(/[()]/g, '').replace('rgb', ''),
        strokeWidth: parseFloat(thickness.replace('mm', '')),
        strokeLineCap: lineCap,
        strokeLineJoin: lineJoin
    };
};

const decorateLine = function(shape, { left, top, width }) {
    shape.start = {
        x: `${left}mm`,
        y: `${top}mm`
    };
    shape.end = {
        x: `${left + width}mm`,
        y: `${top}mm`
    };
};

const convertToDocumentModule = function(moduleString) {
    return moduleString.charAt(0).toUpperCase() + moduleString.slice(1);
};

const convertToUds = function(items) {

    return items.map(item => {
        const shape = {
            id: item.id,
            type: item.module.toLowerCase(),
            zIndex: item.zIndex,
            position: convertToUdsPosition(item),
            color: `rgb(${item.fillColor})`,
            stroke: convertToUdsStroke(item),
            rotationAngle: MAXROTATION - item.rotation
        };

        if (item.module === 'Line') {
            decorateLine(shape, item);
            delete shape.position;
            delete shape.color;
        }

        return shape;
    });
};

const convertToDocument = function(items, metadataObjects) {
    return items.map((item) => {
        const shape = {
            id: item.id,
            module: convertToDocumentModule(item.type),
            rotation: MAXROTATION - item.rotationAngle,
            zIndex: item.zIndex,
            zIndexLock: false,
            isBackground: false
        };

        let shapePosition, shapeColor = { };
        const stroke = convertToDocumentStroke(item.stroke);

        if (item.type === 'line') {
            shapePosition = {
                left: parseFloat(item.start.x.replace('mm', '')),
                top: parseFloat(item.start.y.replace('mm', '')),
                width: parseFloat(item.end.x.replace('mm', '')) - parseFloat(item.start.x.replace('mm', ''))
            };
        } else {
            shapeColor = { fillColor: item.color.replace(/[()]/g, '').replace('rgb', '') };
            shapePosition = convertToDocumentPosition(item.position);
        }

        // find object in metadata
        const metadataShape = _.find(metadataObjects.template, object => object.id === item.id);
        if (metadataShape) {
            shape.isBackground = metadataShape.background;
            shape.zIndexLock = metadataShape.zIndexLock;       
        }

        return _.extend(shape, shapePosition, stroke, shapeColor);
    });
};

module.exports = {
    convertToUds,
    convertToDocument
};