const _ = require('underscore');
const cropConverter = require('services/uds/converter/cropConverter');
const designerContext = require('DesignerContext');

// Rotation must be flipped as they expect clockwise rotation whereas we use counterclockwise
const MAXROTATION = 360;

const getImageUrl = function(item) {
    return designerContext.embroidery ? item.xtiUrl : (item.printUrl || item.url);
};

const attachEmbroideryAttributes = function(item, image) {
    // TODO Intentially hard coded - this is temporary requirement until we use MCP digitization
    image.vpThreadMapping = 'http://embroidery.documents.cimpress.io/vbu-mapping';
    if (item.colorOverrides) {
        image.colorOverrides = item.colorOverrides;
    }
};

const getCropFractions = function(item) {
    return designerContext.embroidery
        ?
            { top: 0, left: 0, bottom: 0, right: 0 }
        :
            cropConverter.convertToUds(item.crop);
};

const convertToUds = function(items) {
    return items.map((item) => {
        const image = {
            id: item.id,
            url: getImageUrl(item),
            position: {
                x: `${item.left}mm`,
                y: `${item.top}mm`,
                width: `${item.width}mm`,
                height: `${item.height}mm`
            },
            cropFractions: getCropFractions(item),
            zIndex: item.zIndex,
            rotationAngle: MAXROTATION - item.rotation,
            originalSourceUrl: item.oldPrintUrl
        };

        if (designerContext.embroidery) {
            attachEmbroideryAttributes(item, image);
        }

        if (item.previewUrl) {
            image.comparisonImages = [
                {
                    key: 'Primary',
                    url: item.previewUrl
                }
            ];
        }

        return image;
    });
};

var convertToDocument = function(items, metadataObjects) {
    return items.map((item, index) => {
        var image = {
            id: item.id,
            module: 'UploadedImage',
            fileType: '',
            locked: false,
            tabIndex: index,
            top: parseFloat(item.position.y.replace('mm', '')),
            left: parseFloat(item.position.x.replace('mm', '')),
            initialHeight: parseFloat(item.position.height.replace('mm', '')),
            initialWidth: parseFloat(item.position.width.replace('mm', '')),
            previewUrl: item.url,
            url: item.url,
            printUrl: item.url,
            width: parseFloat(item.position.width.replace('mm', '')),
            height: parseFloat(item.position.height.replace('mm', '')),
            rotation: MAXROTATION - parseFloat(item.rotationAngle),
            naturalDimensions: {},
            zIndex: item.zIndex,
            zIndexLock: false,
            autoPlaced: false,
            crop: cropConverter.convertToDocument(item.cropFractions),
            comparisonImages: []
        };

        if (designerContext.embroidery) {
            image.module = 'EmbroideryImage';
            image.xtiUrl = item.url;
            image.digitizedId = '23'; // Please don't ask
            attachEmbroideryAttributes(item, image);
        }

        if (image.comparisonImages.length > 0) {
            image.comparisonImages = item.comparisonImages.map(imageElement => ({ key: imageElement.key, previewUrl: imageElement.url }));
        }

        // find object in metadata
        const templateImage = _.find(metadataObjects.template, object => object.id === item.id);
        if (templateImage) {
            image.zIndexLock = templateImage.locks.zIndex;
            image.locked = templateImage.locks.transform;
            image.isBackground = templateImage.background;
        }

        const metadataImage = _.find(metadataObjects.dclMetadata, object => object.id === item.id);
        if (metadataImage) {
            image.naturalDimensions = metadataImage.naturalDimensions;
            image.fileType = metadataImage.mimeType;
        }

        if (item.originalSourceUrl) {
            const crispify = {
                crispify: metadataImage.imageTransformations.crispify,
                crispifyUrl: item.url,
                oldPreviewUrl: item.originalSourceUrl,
                oldPrintUrl: item.originalSourceUrl
            };

            return _.extend(image, crispify);
        }

        return image;
    });
};

module.exports = {
    convertToUds,
    convertToDocument
};