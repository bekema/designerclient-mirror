const convertToUds = function(dclDocument) {
    const template = [];
    const dclMetadata = [];

    dclDocument.canvases.map((canvas) => {
        canvas.items.forEach(item => {
            if (item.module === 'UploadedImage') {
                const templateImage = {
                    id: item.id,
                    locks: {
                        edit: item.restricted,
                        transform: item.locked,
                        zIndex: item.zIndexLock
                    }
                };

                const dclImage = {
                    id: item.id,
                    mimeType: item.fileType,
                    naturalDimensions: item.naturalDimensions,
                    imageTransformations: {
                        crispify: item.crispify
                    }
                };

                dclMetadata.push(dclImage);
                template.push(templateImage);
            }

            if (item.module === 'TextField') {
                const templateTextField = {
                    id: item.id,
                    placeholder: item.placeholder,
                    locks: {
                        edit: item.restricted,
                        transform: item.locked,
                        zIndex: item.zIndexLock
                    }
                };

                template.push(templateTextField);
            }

            if (item.module === 'Line' || item.module === 'Ellipse' || item.module === 'Rectangle') {
                const templateShape = {
                    id: item.id,
                    background: item.isBackground,
                    locks: {
                        edit: item.restricted,
                        transform: item.locked,
                        zIndex: item.zIndexLock
                    }
                };

                template.push(templateShape);
            }
        });
    });

    return {
        template,
        dclMetadata
    };
};

module.exports = {
    convertToUds
};