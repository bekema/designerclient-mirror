const convert = function(value) {
    // contract requires this to be a string
    return value ? `${value}` : '0';
};

const convertToUds = function(crop) {
    if (!crop) {
        return null;
    }

    if (!crop.top && !crop.left && !crop.right && !crop.bottom) {
        return null;
    }

    const converted = {
        top: convert(crop.top),
        left: convert(crop.left),
        right: convert(crop.right),
        bottom: convert(crop.bottom)
    };

    return converted;
};

var convertToDocument = function(crop) {
    const defaultCrop = {
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
    };

    if (!crop) {
        return defaultCrop;
    }

    if (!crop.top && !crop.left && !crop.right && !crop.bottom) {
        return defaultCrop;
    }

    var converted = {
        top: parseFloat(convert(crop.top)),
        left: parseFloat(convert(crop.left)),
        right: parseFloat(convert(crop.right)),
        bottom: parseFloat(convert(crop.bottom))
    };

    return converted;
};


module.exports = {
    convertToUds,
    convertToDocument
};