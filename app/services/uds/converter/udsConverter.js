const canvasConverter = require('services/uds/converter/canvasConverter');
const metaDataConverter = require('services/uds/converter/metaDataConverter');

const convertToUds = function(designDocument) {
    const clients = require('services/clientProvider');
    const udsDocument = {
        version: '2.0',
        sku: designDocument.sku,
        fontRepositoryUrl: clients.font.fontRepositoryUrl,
        document: { },
        metadata: { }
    };

    const surfaceNodeName = designDocument.alternativeOrientation ? 'panels' : 'surfaces';
    udsDocument.document[surfaceNodeName] = canvasConverter.convertToSurface(designDocument.canvases);

    // add non existing attributes to document metadata
    udsDocument.metadata = metaDataConverter.convertToUds(designDocument);

    if (designDocument.alternativeOrientation) {
        udsDocument.projectionId = 'ROTATE_270';
    }

    return udsDocument;
};

const convertToDocument = function(udsDocument, documentReference) {
    const document = {
        module: 'Document',
        name: 'document',
        documentReference,
        sku: udsDocument.sku,
        canvases: canvasConverter.convertToCanvas(udsDocument.document.surfaces, udsDocument)
    };

    return document;
};

module.exports = {
    convertToUds,
    convertToDocument
};