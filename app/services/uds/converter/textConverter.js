const _ = require('underscore');
const designerContext = require('DesignerContext');

const MAXROTATION = 360;

const convertToUdsPosition = function({ left, top, width, height }) {

    return {
        x: `${left}mm`,
        y: `${top}mm`,
        width: `${width}mm`,
        height: `${height}mm`
    };
};

const convertToDocumentPosition = function({ y, x, width, height }) {

    return {
        top: parseFloat(y.replace('mm', '')),
        left: parseFloat(x.replace('mm', '')),
        width: parseFloat(width.replace('mm', '')),
        height: parseFloat(height.replace('mm', ''))
    };
};

const convertToUdsFontStyle = function({ fontBold, fontUnderline, fontItalic, fontStrikeout }) {
    const styles = [];
    if (fontBold) {
        styles.push('bold');
    }
    if (fontUnderline) {
        styles.push('underline');
    }
    if (fontItalic) {
        styles.push('italic');
    }
    if (fontStrikeout) {
        styles.push('strikeout');
    }
    if (styles.length === 0) {
        return 'normal';
    } else {
        return styles.join();
    }
};

const convertToDocumentFontStyle = function(styles) {
    var documentStyles = {
        fontBold: false,
        fontUnderline: false,
        fontItalic: false,
        fontStrikeout: false
    };

    if (styles.search('bold') >= 0) {
        documentStyles.fontBold = true;
    }

    if (styles.search('strikeout') >= 0) {
        documentStyles.fontStrikeout = true;
    }

    if (styles.search('italic') >= 0) {
        documentStyles.fontItalic = true;
    }

    if (styles.search('underline') >= 0) {
        documentStyles.fontUnderline = true;
    }
    return documentStyles;
};

// This will omit any text fields that contain no innerHTML, as they are just blank/placeholder text fields
const convertToUds = function(items) {
    return items.filter(item => item.innerHTML)
                .map(item => {
                    const textField = {
                        position: convertToUdsPosition(item),
                        horizontalAlignment: item.horizontalAlignment,
                        verticalAlignment: item.verticalAlignment,
                        textFields: [
                            {
                                id: item.id,
                                fontFamily: item.fontFamily,
                                fontStyle: convertToUdsFontStyle(item),
                                fontSize: `${item.fontSize}pt`,
                                content: item.innerHTML, 
                                color: designerContext.embroidery && item.fontColor.indexOf('thread') > -1 // eslint-disable-line no-magic-numbers
                                ? item.fontColor : designerContext.itemColor(item.fontColor)
                            }
                        ],
                        zIndex: item.zIndex,
                        rotationAngle: MAXROTATION - item.rotation
                    };
                    return textField;
                });
};

const convertToDocument = function(items, metadataObjects) {
    return items.map((item, index) => {
        const textArea = {
            id: item.textFields[0].id,
            fontColor: item.textFields[0].color.replace(/[()]/g, '').replace('rgb', ''),
            fontFamily: item.textFields[0].fontFamily,      
            fontSize: parseFloat(item.textFields[0].fontSize),
            horizontalAlignment: item.horizontalAlignment,
            verticalAlignment: item.verticalAlignment,
            innerHTML: item.textFields[0].content,
            locked: false,
            module: 'TextField',
            placeholder: 'defaultPlaceholder',
            rotation: MAXROTATION - parseFloat(item.rotationAngle),
            tabIndex: index,
            writingMode: 'horizontal-tb',
            zIndex: item.zIndex,
            zIndexLock: false
        };
        
        if (item.textFields[0].color.indexOf('thread') > -1) { // eslint-disable-line no-magic-numbers
            textArea.fontColor = item.textFields[0].color; 
        }

        // find object in metadata
        
        if (item.textFields.length > 0) {
            const metadataTextArea = _.find(metadataObjects.template, object => object.id === item.textFields[0].id);
            if (metadataTextArea) {
                textArea.placeholder = metadataTextArea.placeholder;
                textArea.zIndexLock = metadataTextArea.locks.zIndex;
                textArea.locked = metadataTextArea.locks.edit; 
            }
        }
        
        const styles = convertToDocumentFontStyle(item.textFields[0].fontStyle);
        const textAreaPosition =  convertToDocumentPosition(item.position);

        return _.extend(styles, textArea, textAreaPosition);
    });
};

module.exports = {
    convertToUds,
    convertToDocument
};