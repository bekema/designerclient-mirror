const imageConverter = require('services/uds/converter/imageConverter');
const textConverter = require('services/uds/converter/textConverter');
const shapeConverter = require('services/uds/converter/shapeConverter');

const getItemsByModules = function(items, moduleOption) {
    const moduleNames = moduleOption.moduleNames || [moduleOption.moduleName];

    return moduleNames
        .map(moduleName => items.filter(item => item.module === moduleName))
        .reduce((previous, current) => current.concat(previous));
};

const convertToSurface = function(canvases) {
    return canvases.map((canvas) => {
        const surface = {
            name: canvas.name,
            width: `${canvas.width}mm`,
            height: `${canvas.height}mm`,

            textAreas: textConverter.convertToUds(getItemsByModules(canvas.items, { moduleName: 'TextField' })),
            images: imageConverter.convertToUds(getItemsByModules(canvas.items, { moduleNames: ['UploadedImage', 'EmbroideryImage'] })),
            shapes: shapeConverter.convertToUds(getItemsByModules(canvas.items, { moduleNames: ['Line', 'Ellipse', 'Rectangle'] }))
        };
        return surface;
    });
};

const convertToCanvas = function(surfaces, udsDocument) {
    return surfaces.map((surface, index) => {
        const canvas = {
            module: 'Canvas',
            name: surface.name,
            height: parseFloat(surface.height.replace('mm', '')),
            width: parseFloat(surface.width.replace('mm', '')),
            id: index + 1,
            items: []
        };

        const images = imageConverter.convertToDocument(surface.images, udsDocument.metadata);
        const textAreas = textConverter.convertToDocument(surface.textAreas, udsDocument.metadata);
        const shapes = shapeConverter.convertToDocument(surface.shapes, udsDocument.metadata);

        canvas.items = images.concat(textAreas, shapes);

        return canvas;
    });
};

module.exports = {
    convertToSurface,
    convertToCanvas
};