const $ = require('jquery');
const documentRepository = require('DocumentRepository');
const util = require('util/Util');
const udsConverter = require('services/uds/converter/udsConverter');
const tokenProvider = require('services/authTokens/TokenProvider');

const makeUdsRequest = function(url, data, type) {
    return tokenProvider.getToken('uds')
        .then(token => Promise.resolve($.ajax({
            url,
            type: type,
            headers: {
                Authorization: token
            },
            data,
            dataType: 'json',
            processData: false,
            contentType: 'application/json'
        })
    ));
};

class UdsAdapter {
    constructor({ url }) {
        this.baseUrl = url;
    }

    save(documentJson) {
        const documentReference = documentRepository.getActiveDocument().get('documentReference');
        if (!documentReference) {
            const udsDoc = udsConverter.convertToUds(documentJson);
            const id = util.generateUUID();
            const url = `${this.baseUrl}/documents/${id}`;

            return makeUdsRequest(url, JSON.stringify(udsDoc), 'POST');
        } else {
            return this.loadReference(documentReference)
                .then(response => this.update(documentJson, response.cimDocUrl));
        }
    }

    update(documentJson, url) {
        const udsDoc = udsConverter.convertToUds(documentJson);

        return makeUdsRequest(url, JSON.stringify(udsDoc), 'PUT');
    }

    loadReference(url) {
        return makeUdsRequest(url, null, 'GET');
    }

    loadDocument(documentReference) {
        const url = documentReference.cimDocUrl;

        return makeUdsRequest(url, null, 'GET');
    }

    retrieve(id) {
        return id;
    }
}

UdsAdapter.validateOptions = function({ url }) {
    if (!url) {
        throw Error('url required');
    }
};

module.exports = UdsAdapter;