const clients = require('services/clientProvider');
const getDocumentJson = require('publicApi/documentJson');
const documentRepository = require('DocumentRepository');

// documentJson (object {}): object containing the documentJson.
// surface (int): identifying number for each canvas, canvas "1" stands for the first canvas. for the BC this is the front.
// scene (string): this is the scene url that we need to append to the rendering service url.
module.exports = function previewDocument(documentJson, size, surfaceId, scene) {
    let surfaceName;
    const documentReference = documentRepository.getActiveDocument().get('documentReference');
    const canvases = documentRepository.getAllCanvasViewModels();

    // when there's a scene, then don't use the surface, becuase the scene will handle the surface
    if (!scene) {
        // base the surface on the selected canvas viewModel
        surfaceId = surfaceId || documentRepository.getActiveCanvasViewModel().model.id;
        surfaceName = canvases[surfaceId - 1].model.get('name');
    }

    if (!size) {
        // set width on default value 1000
        // recommandation Zachary Lynn
        size = {
            width: 1000,
            height: 1000
        };
    }

    documentJson = documentJson || getDocumentJson();
    if (documentReference) {
        return clients.uds.save(documentJson)
            .then(() => clients.uds.loadReference(documentReference))
                .then(response => clients.rendering.renderPreview(response.renderingInstructionsUrl, size, surfaceName, scene));
    } else {
        return clients.uds
            .save(documentJson)
            .then(response => {
                documentRepository.getActiveDocument().set({ documentReference: response.documentReferenceUrl });
                return clients.rendering.renderPreview(response.previewInstructionSourceUrl, size, surfaceName, scene);
            });
    }
};