/* utility methods */
const generatePdfProof = require('publicApi/pdfProof');
const getDocumentJson = require('publicApi/documentJson');
const saveDocument = require('publicApi/saveDocument');
const previewDocument = require('publicApi/previewDocument');

/* managers, dispatchers, clients */
const buttons = require('publicApi/buttons');
const canvasInfo = require('publicApi/canvasInfo');
const clients = require('services/clientProvider');
const commandDispatcher = require('CommandDispatcher');
const documentRepository = require('DocumentRepository');
const eventBus = require('EventBus');
const selectionManager = require('SelectionManager');
const validationManager = require('validations/ValidationManager');

module.exports = {
    canvasInfo,
    getDocumentJson,
    generatePdfProof,
    saveDocument,
    previewDocument,

    clients,
    commandDispatcher,
    selectionManager,
    eventBus,
    buttons,
    validationManager,
    documentRepository
};
