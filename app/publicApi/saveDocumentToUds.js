const clients = require('services/clientProvider');
const getDocumentJson = require('publicApi/documentJson');
const documentRepository = require('DocumentRepository');

module.exports = function saveDocumentToUds(documentJson) {
    documentJson = documentJson || getDocumentJson();
    return clients.uds
        .save(documentJson)
        .then(response => {
            if (response) {
                documentRepository.getActiveDocument().set({ documentReference: response.documentReferenceUrl });
            }
        });
};
