const $ = require('jquery');
const documentRepository = require('DocumentRepository');

module.exports = {
    getCanvasBoundingClientRect: (id) => {
        const canvasViewModel = documentRepository.getAllCanvasViewModels()[id - 1];
        return $(`#${canvasViewModel.id}`)[0].getBoundingClientRect();
    },
    getCanvasOffsetInContainer: (id) => {
        const canvasViewModel = documentRepository.getAllCanvasViewModels()[id - 1];
        const $canvas = $(`#${canvasViewModel.id}`);
        return { left: $canvas.offsetLeft, top: $canvas.offsetTop };
    }
};
