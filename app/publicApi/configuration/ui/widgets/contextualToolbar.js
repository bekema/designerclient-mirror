const _ = require('underscore');
const _merge = require('lodash-compat/object/merge');
const ConfigurationBase = require('publicApi/configuration/ConfigurationBase');
const ColorPickerCustom = require('ui/tools/color/ColorPickerCustom');
const ColorPickerProvidedChoices = require('ui/tools/color/ColorPickerProvidedChoices');
const ColorPickerCanvasChoices = require('ui/tools/color/ColorPickerCanvasChoices');

const mergeOrAddSingleButton = function(defaultButtons, buttonOptions) {
    if (!buttonOptions.name) { return; }

    // if it's in the defaultConfig list, merge into that,
    // otherwise add it
    const existingButton = _(defaultButtons).find({ name: buttonOptions.name });

    if (existingButton) {
        _merge(existingButton, buttonOptions);
    } else {
        defaultButtons.push(existingButton);
    }
};

const mergeButtons = function(defaultButtons, buttons) {
    if (!buttons) { return; }

    buttons.forEach(button => {
        if (_.isObject(button)) {
            mergeOrAddSingleButton(defaultButtons, button);
        } else if (_.isFunction(button) || _.isString(button)) {
            // assume a constructor function or the name of a built-in button
            defaultButtons.push(button);
        }
    });
};

const defaults = {
    enabled: true,
    padding: 15,
    containerElement: 'body',
    positionBelow: false,

    Buttons: [
        { name: 'crop' },
        { name: 'crispify' },
        { name: 'textfontfamily' },
        { name: 'textsize' },
        { name: 'fillcolorpicker',
            childViewOptions: {
                colorAttribute: 'fillColor',
                views: [
                    { constructor: ColorPickerCanvasChoices, default: true },
                    { constructor: ColorPickerProvidedChoices },
                    { constructor: ColorPickerCustom }]
            } },
        { name: 'strokecolorpicker',
            childViewOptions: {
                colorAttribute: 'strokeColor',
                views: [
                    { constructor: ColorPickerCanvasChoices, default: true },
                    { constructor: ColorPickerProvidedChoices },
                    { constructor: ColorPickerCustom }]
            } },
        { name: 'colorpicker',
            childViewOptions: {
                colorAttribute: 'fontColor',
                views: [
                    { constructor: ColorPickerCanvasChoices, default: true },
                    { constructor: ColorPickerProvidedChoices },
                    { constructor: ColorPickerCustom }]
            } },
        { name: 'embroideryColors' },
        { name: 'textbold' },
        { name: 'textitalic' },
        { name: 'strokewidth' },
        { name: 'hover',
            iconName: 'sendback',
            childViewOptions: {
                Buttons: [
                { name: 'bringtofront', text: 'bringToFront' },
                { name: 'sendtoback', text: 'sendToBack' }
                ] }
        },
        { name: 'delete' },
        { name: 'hover',
            title: 'more',
            childViewOptions: {
                Buttons: [
                { name: 'textunderline', text: 'underline' },
                { name: 'textstrikeout', text: 'strikeout' },
                { name: 'textleftalign', text: 'leftAlign' },
                { name: 'textcenteralign', text: 'centerAlign' },
                { name: 'textrightalign', text: 'rightAlign' }
                ] }
        },
        { name: 'textedit' }
    ]
};

class ContextualToolbarConfiguration extends ConfigurationBase {
    constructor(options) {
        super();
        // Babel doesn't get the prototype correctly in the constructor
        // in IE10. This is redundant, but not harmful
        ConfigurationBase.call(this, options);
    }

    /*
     * This is a specialized version of merge, since buttons cannot just be
     * deep-merged by lodash. Instead, each item in the options.button list
     * should be merged individually with the existing default buttons list.
     */
    merge(options) {
        mergeButtons(this.buttons, options.buttons);
        delete options.buttons;

        super.merge(options);
    }
}

module.exports = new ContextualToolbarConfiguration(defaults);
