const ConfigurationBase = require('publicApi/configuration/ConfigurationBase');

const defaults = {
    zoom: {
        enabled: false
    },

    addText: {
        enabled: false
    },

    editText: {
        enabled: false
    },

    changeScene: {
        enabled: false
    },

    addShape: {
        enabled: false
    },

    generatePdf: {
        enabled: false
    },

    saveDocument: {
        enabled: false
    },

    previewDocument: {
        enabled: false
    },

    uploadList: {
        enabled: false,
        templates: {
            uploadInProgress: false,
            uploadComplete: false,
            validations: false,
            autoPlace: false
        }
    },

    uploadButton: {
        enabled: false
    },

    validationOverlay: {
        enabled: true,
        containerElement: 'body'
    }
};

module.exports = new ConfigurationBase(defaults);
