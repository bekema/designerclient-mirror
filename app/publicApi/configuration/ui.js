const UiConfiguration = require('publicApi/configuration/UiConfiguration');

const defaults = {
    canvas: {
        container: '#canvases',
        showMargins: true,
        enabledCanvas: 0, // 0 means all are enabled (canvas IDs start at 1)
        visibleCanvas: 0,
        activeCanvas: 1,
        useProductScenes: false,
        chromes: {
            dimensions: {
                enabled: true,
                color: null,
                precision: 1,
                measurement: 'in',
                margin: 35,
                lineWidth: 1,
                showTop: false,
                showRight: false,
                showBottom: true,
                showLeft: true
            },
            ruler: {
                enabled: true,
                color: null,
                lineWidth: 1,
                length: 10,
                lines: 5,
                margin: 1
            },
            border: {
                enabled: true,
                color: null,
                lineWidth: 1
            },
            canvasTools: {
                enabled: true,
                left: 0,
                top: -25
            }
        }
    },

    colors: {
        defaults: {
            light: '#C0C0C0',
            dark: '#000000',
            override: null
        },
        palettes: {
            print: [
                '#FFFFFF', '#C0C0C0', '#808080', '#404040', '#000000',
                '#99CCFF', '#00CCFF', '#0099FF', '#0000FF', '#003399',
                '#00FF00', '#00CC00', '#CCFFCC', '#009900', '#006600',
                '#FFFFCC', '#FFFF00', '#FFC000', '#FF6600', '#663300',
                '#FF9999', '#FF3300', '#FF0000', '#990000', '#660000',
                '#FFCCFF', '#FF66FF', '#FF0099', '#990066', '#800080'
            ]
        },
        colorRetrieval: {
            enabled: true,
            colorCount: 10, // 10 is maximum color count
            colorQuality: 5
        }
    },

    useDefaultIcons: true,

    zoomStrategy: {
        resizeEnabled: true,
        initialWidth: .9,
        initialHeight: .9,
        initialZoom: 1,
        maxZoom: 2,
        minZoom: .25,
        zoomIncrement: .25
    },

    crop: {
        CropModalView: require('ui/widgets/CropModalView')
    },

    embroidery: {
        EmbroideryModificationView: require('ui/widgets/embroidery/EmbroideryModificationView')
    }
};

module.exports = new UiConfiguration(defaults);
