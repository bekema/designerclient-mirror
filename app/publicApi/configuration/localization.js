const ConfigurationBase = require('publicApi/configuration/ConfigurationBase');

const defaults = {
    language: 'en',
    en: {
        translation: {
            text: {
                defaultPlaceholder: 'Type text here'
            },
            views: {
                dropTargetText: 'Drop here to place item.'
            },
            widgets: {
                addButtonTextField: 'Add text',
                uploadFileButton: 'Add images',
                cancelButton: 'Cancel',
                cropModal: {
                    description: 'Select the part of the image you want to show.',
                    cropButton: 'Crop'
                },
                getPdfProofButton: 'Get PDF proof',
                saveDocumentButton: 'Save',
                previewDocumentButton: 'Preview',
                addArtworkButton: 'Add artwork',
                uploadStateTextReady: 'Ready to be placed',
                autoPlaceArtworkText: 'Auto-place artwork',
                uploadStateTextUploading: 'Uploading...',
                dismissButton: 'Dismiss',
                closeButton: 'Close',
                cropModalTitle: 'Crop-modal',
                embroidery: {
                    colorHeader: 'List of Colors',
                    modalHeader: 'Embroidery Colors',
                    resetColors: 'Reset Colors'
                },
                addShapesHeader: 'Shapes',
                zoomButtons: {
                    in: 'Zoom in',
                    out: 'Zoom out'
                },
                canvasHistoryActions: {
                    undo: 'Undo',
                    redo: 'Redo'
                },
                orientationChange: 'Orientation',
                dragAndDropShapes: 'Drag and drop shapes'
            },
            tools: {
                buttons: {
                    apply: {
                        text: 'Apply',
                        title: 'Apply'
                    },
                    crop: {
                        text: 'Crop',
                        title: 'Crop'
                    },
                    crispify: {
                        text: 'Crispify',
                        title: 'Crispify'
                    },
                    delete: {
                        text: 'Delete',
                        title: 'Delete'
                    },
                    colorPicker: {
                        text: 'Color Picker',
                        title: 'Color Picker'
                    },
                    fillColorPicker: {
                        text: 'Fill Color Picker',
                        title: 'Fill Color Picker'
                    },
                    strokeColorPicker: {
                        text: 'Stroke Color Picker',
                        title: 'Stroke Color Picker'
                    },
                    bold: {
                        text: 'Bold',
                        title: 'Bold'
                    },
                    italic: {
                        text: 'Italic',
                        title: 'Italic'
                    },
                    underline: {
                        text: 'Underline',
                        title: 'Underline'
                    },
                    strikeout: {
                        text: 'Strikethrough',
                        title: 'Strikeout'
                    },
                    leftAlign: {
                        text: 'Align Left',
                        title: 'Left Align Text'
                    },
                    centerAlign: {
                        text: 'Align Center',
                        title: 'Center Align Text'
                    },
                    rightAlign: {
                        text: 'Align Right',
                        title: 'Right Align Text'
                    },
                    rotateLeft: {
                        text: 'Rotate Left',
                        title: 'Rotate Left'
                    },
                    rotateRight: {
                        text: 'Rotate Right',
                        title: 'Rotate Right'
                    },
                    bringForward: {
                        text: 'Bring Forward',
                        title: 'Bring Forward'
                    },
                    sendBackward: {
                        text: 'Send Backward',
                        title: 'Send Backward'
                    },
                    bringToFront: {
                        text: 'Bring To Front',
                        title: 'Bring To Front'
                    },
                    sendToBack: {
                        text: 'Send To Back',
                        title: 'Send To Back'
                    },
                    more: {
                        text: 'More',
                        title: 'More'
                    },
                    threadcolors: {
                        text: 'Thread Colors',
                        title: 'Thread Colors'
                    }
                },
                color: {
                    providedColors: 'Standard Colors',
                    recentColors: 'Recent Colors',
                    canvasColors: 'Colors on Design',
                    customColor: 'Custom Colors'
                }
            },
            imageProcessingSteps: {
                digitization: {
                    name: 'Digitize Image',
                    message: 'Creating Threads...'
                },
                imagePreparation: {
                    name: 'Prepare for Clean Image',
                    message: 'Preparing Image...'
                },
                cleanImage: {
                    name: 'Clean Image',
                    message: 'Sharpening Image...'
                }
            },
            errors: {
                surfaceValidationSurfaces: 'The number of surfaces don\'t match the document canvases',
                surfaceValidationSizes: 'The surface sizes don\'t match the document canvas sizes',
                surfaceValidationNames: 'The surface names don\'t match the document canvas names'
            },

            validations: {
                outsideBounds: {
                    warning: 'An object is out of bounds.'
                },
                overlap: {
                    warning: 'There is overlap that may be unintended.',
                    error: 'This overlap will prevent this product from printing properly. Remove the overlap.'
                },
                uploadSize: {
                    error: 'The uploaded image was too large! Try uploading a smaller image.'
                },
                imageResolution: {
                    warning: 'Your image has low resolution and may appear blurry when printed. For a clearer image, choose one with a higher resolution.',
                    error: 'Your image has low resolution and won’t print well. Please replace it with an image that has a higher resolution.'
                }
            }
        }
    },
    nl: {
        translation: {
            //TODO replace
            text: {
                defaultPlaceholder: 'Enter Your Text'
            },
            views: {
                dropTargetText: 'Sleep hier om item te plaatsen.'
            },
            widgets: {
                addButtonTextField: 'Voeg tekstveld toe',
                uploadFileButton: 'Bestanden uploaden',
                cancelButton: 'Annuleren',
                cropModal: {
                    description: 'Selecteer het gedeelte van de afbeelding die je wilt gebruiken.',
                    cropButton: 'Bijsnijden'
                },
                getPdfProofButton: 'PDF voorbeeld',
                saveDocumentButton: 'Opslaan',
                previewDocumentButton: 'Voorbeeld',
                addArtworkButton: 'Afbeeldingen toevoegen',
                uploadStateTextReady: 'Klaar om te plaatsen',
                autoPlaceArtworkText: 'Auto-place ontwerp',
                uploadStateTextUploading: 'Uploaden...',
                dismissButton: 'Stoppen',
                closeButton: 'Sluiten',
                cropModalTitle: 'Bijsnijden-modal',
                embroidery: {
                    colorHeader: 'Lijst met kleuren',
                    modalHeader: 'Borduurwerk kleuren',
                    resetColors: 'Kleuren resetten'
                },
                addShapesHeader: 'Vormen',
                zoomButtons: {
                    in: 'In zoomen',
                    out: 'Uit zoomen'
                },
                canvasHistoryActions: {
                    undo: 'Undo',
                    redo: 'Redo'
                },
                orientationChange: 'Pagina\'s draaien',
                dragAndDropShapes: 'Sleep de vorm rechts'
            },
            tools: {
                buttons: {
                    apply: {
                        text: 'Toepassen',
                        title: 'Toepassen'
                    },
                    crop: {
                        text: 'Bijsnijden',
                        title: 'Bijsnijden'
                    },
                    crispify:{
                        text: 'Crispify',
                        title: 'Crispify'
                    },
                    delete: {
                        text: 'Verwijderen',
                        title: 'Verwijderen'
                    },
                    colorPicker: {
                        text: 'Kleur kiezen',
                        title: 'Kleur kiezen'
                    },
                    fillColorPicker: {
                        text: 'Fill Kleur kiezen',
                        title: 'Fill Kleur kiezen'
                    },
                    strokeColorPicker: {
                        text: 'Stroke Kleur kiezen',
                        title: 'Stroke Kleur kiezen'
                    },
                    bold: {
                        text: 'Bold',
                        title: 'Bold'
                    },
                    italic: {
                        text: 'Italic',
                        title: 'Italic'
                    },
                    underline: {
                        text: 'Onderstrepen',
                        title: 'Onderstrepen'
                    },
                    strikeout: {
                        text: 'Doorhalen',
                        title: 'Doorhalen'
                    },
                    leftAlign: {
                        text: 'Links uitlijnen',
                        title: 'Links uitlijnen'
                    },
                    centerAlign: {
                        text: 'Centreren',
                        title: 'Centreren'
                    },
                    rightAlign: {
                        text: 'Rechts uitlijnen',
                        title: 'Rechts uitlijnen'
                    },
                    rotateLeft: {
                        text: 'Linksom roteren',
                        title: 'Linksom roteren'
                    },
                    rotateRight: {
                        text: 'Rechtsom roteren',
                        title: 'Rechtsom roteren'
                    },
                    //TODO replace
                    bringForward: {
                        text: 'Bring Forward',
                        title: 'Bring Forward'
                    },
                    sendBackward: {
                        text: 'Send Backward',
                        title: 'Send Backward'
                    },
                    bringToFront: {
                        text: 'Bring To Front',
                        title: 'Bring To Front'
                    },
                    sendToBack: {
                        text: 'Send To Back',
                        title: 'Send To Back'
                    },
                    more: {
                        text: 'More',
                        title: 'More'
                    },
                    threadcolors: {
                        text: 'draad Kleuren',
                        title: 'draad Kleuren'
                    }
                },
                color: {
                    providedColors: 'Aanbevolen kleuren',
                    recentColors: 'Recente gebruikte kleuren',
                    //TODO replace
                    canvasColors: 'Canvas Colors',
                    customColor: 'Aangepaste kleur'
                }
            },
            errors: {
                surfaceValidationSurfaces: 'Het aantal surfaces is niet hetzelfde als de document canvases',
                surfaceValidationSizes: 'De maten van de surfaces zijn niet hetzelfde als de document canvas maten',
                surfaceValidationNames: 'De namen van de surfaces zijn niet hetzelfde als de document canvas namen'
            },

            validations: {
                outsideBounds: {
                    warning: 'An object is out of bounds'
                },
                overlap: {
                    warning: 'There is overlap that may be unintended',
                    error: 'There is overlap that will prevent this product from printing properly. Remove the overlap.'
                },
                uploadSize: {
                    error: 'The size of the uploaded image was too large! Try uploading a smaller image'
                },
                imageResolution: {
                    warningMessage: 'De afbeelding die u probeert te uploaden heeft een lage resolutie, hierdoor kan de kwalitiet tegenvallen. Probeer een afbeelding met een hogere resolutie aub',
                    errorMessage: 'De afbeelding die u probeert te uploaden heeft een te lage resolutie. Probeer een andere afbeelding aub'
                }
            }
        }
    }
};

module.exports = new ConfigurationBase(defaults);
