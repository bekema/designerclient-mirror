const ConfigurationBase = require('publicApi/configuration/ConfigurationBase');

const AccountAdapter = require('services/account/accountAdapter');
const PrepressAdapter = require('services/prepress/prepressAdapter');
const EmbroideryAdapter = require('services/embroidery/embroideryAdapter');
const CrispifyAdapter = require('services/crispify/crispifyAdapter');
const PreflightAdapter = require('services/preflight/preflightAdapter');
const PreviewAdapter = require('services/preview/previewAdapter');
const RenderingAdapter = require('services/rendering/RenderingAdapter');
const UdsAdapter = require('services/uds/udsAdapter');
const UploadAdapter = require('services/upload/uploadAdapter');
const FontAdapter = require('services/font/fontAdapter');
const SceneAdapter = require('services/scenes/sceneAdapter');
const surfaceSpecificationsAdapter = require('services/surfaceSpecifications/surfaceSpecificationsAdapter');
const TokenRetrievalAdapter = require('services/authTokens/TokenRetrievalAdapter');

const defaults = {
    crispify: {
        Constructor: CrispifyAdapter,
        url: 'https://gpu.images.documents.cimpress.io/crispify'
    },

    prepress: {
        Constructor: PrepressAdapter,
        url: 'https://prepress.documents.cimpress.io/v1'
    },

    preflight: {
        Constructor: PreflightAdapter,
        url: 'https://preflight.prepress.documents.cimpress.io'
    },

    preview: {
        Constructor: PreviewAdapter,
        url: 'https://misc-rendering.documents.cimpress.io/dcl/itempreview'
    },

    rendering: {
        Constructor: RenderingAdapter,
        url: 'http://rendering.documents.cimpress.io/v1/uds/preview'
    },

    uds: {
        Constructor: UdsAdapter,
        url: 'https://uds.documents.cimpress.io'
    },

    upload: {
        Constructor: UploadAdapter,
        imageUploadUrl: 'https://uploads.documents.cimpress.io/v1/uploads?process={type:"image", "arguments":{"storeOriginal":"true"}}&tenant={merchant}',
        // PDF uploads store their original so that preflight checks can be run on it
        pdfUploadUrl: 'https://uploads.documents.cimpress.io/v1/uploads?process={type:"multipagePdfImage", "arguments": {"storeOriginal": true}}&tenant={merchant}',
        originalUrlTemplate: 'https://uploads.documents.cimpress.io/v1/uploads/{uploadId}/original',
        printUrlTemplate: 'https://uploads.documents.cimpress.io/v1/uploads/{uploadId}',
        previewUrlTemplate: 'https://uploads.documents.cimpress.io/v1/uploads/{uploadId}/preview',
        thumbnailUrlTemplate: 'https://uploads.documents.cimpress.io/v1/uploads/{uploadId}/thumb'
    },

    font: {
        Constructor: FontAdapter,
        fontRepositoryUrl: 'http://fonts.documents.cimpress.io/prod',
        defaultFont: 'actor'
    },

    account: {
        Constructor: AccountAdapter
    },

    surfaceSpecifications: {
        Constructor: surfaceSpecificationsAdapter,
        url: 'https://surface.products.cimpress.io'
    },

    scene: {
        Constructor: SceneAdapter,
        url: 'https://scene.products.cimpress.io'
    },

    embroidery: {
        Constructor: EmbroideryAdapter,
        embroideryUrl: 'https://embroidery.documents.cimpress.io',
        cleanImageUrl: 'https://cleanimage.docreview.documents.cimpress.io/v1',
        digitizationUrl: 'https://digitization.docreview.documents.cimpress.io/v1'
    },

    tokenRetrieval: {
        Constructor: TokenRetrievalAdapter,
        urlTemplate: '/getAuthToken/{service}'
    }
};

module.exports = new ConfigurationBase(defaults);
