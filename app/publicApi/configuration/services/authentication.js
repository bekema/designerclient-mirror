const ConfigurationBase = require('publicApi/configuration/ConfigurationBase');

const defaults = {
    // tokens can be prepopulated here during configuration so that
    // the back end can serve up all the tokens it already has without
    // a bunch of round trips
    tokens: {
        /*
        surfaceSpecifications: {
            TokenType: 'Basic',
            IdToken: 'sOmE_iDtOkEN'
        }
        */
    }
};

module.exports = new ConfigurationBase(defaults);