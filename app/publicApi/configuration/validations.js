const ConfigurationBase = require('publicApi/configuration/ConfigurationBase');
const uploadSizeStrategy = require('validations/UploadSize');
const outsideBoundsStrategy = require('validations/OutsideBounds');
const imageResolutionStrategy = require('validations/ImageResolution');
const overlapStrategy = require('validations/Overlap');

const defaults = {
    outsideBounds: {
        strategy: outsideBoundsStrategy,
        enabled: true,
        overlay: true,
        image: {
            enabled: true,
            margin: false
        },
        text: {
            enabled: true,
            margin: true
        },
        shape: {
            enabled: false,
            margin: false
        }
    },
    overlap: {
        //TODO I'm not happy with how this is configured, but I can't come up with a more clever way to do this
        //Presently, it only cares about text overlap with other items, unless it is embroidery, where it does it for everything
        strategy: overlapStrategy,
        enabled: true,
        overlay: true,
        text: true,
        image: true,
        shape: true,
        pixelTolerance: 0,
        alphaTolerance: 0
    },
    imageResolution: {
        strategy: imageResolutionStrategy,
        enabled: true,
        overlay: true,
        warningThreshold: 0.75,
        errorThreshold: 0.25,
        productDpi: 300
    },
    uploadSize: {
        strategy: uploadSizeStrategy,
        enabled: true,
        overlay: true,
        maxSize: 320000000
    }
};

module.exports = new ConfigurationBase(defaults);
