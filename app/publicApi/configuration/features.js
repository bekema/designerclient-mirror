const ConfigurationBase = require('publicApi/configuration/ConfigurationBase');

const defaults = {
    autoPlace: {
        strategy: 'CropStrategy'
    }
};

module.exports = new ConfigurationBase(defaults);
