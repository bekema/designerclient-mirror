const ConfigurationBase = require('publicApi/configuration/ConfigurationBase');
const events = require('publicApi/events');

const defaults = {
    text: {
        doubleClick: events.doubleClick,
        minWidth: 15,
        minFontSize: 5,
        maxFontSize: 500,
        rotatable: true,
        defaults: {
            horizontalAlignment: 'center',
            fontSize: '12',
            fontColor: {
                print: '#000000',
                embroidery: '#282E2E'
            },
            fontUnderline: false,
            fontBold: false,
            fontItalic: false,
            fontStrikeout: false,
            rotation: 0,
            // TODO: localize
            clickText: 'Add text',
            height: 5,
            width: 50,
            top: 0,
            left: 0,
            placeholder: 'defaultPlaceholder',
            innerHTML: '',
            label: 'Text Field',
            module: 'TextField',
            writingMode: 'horizontal-tb',
            restricted: false,
            locked: false,
            brandingType: 0,
            colorSchemeRef: -1,
            fontSchemeRef: -1,
            persist: false
        }
    },
    image: {
        doubleClick: events.doubleClick,
        initialSize: 0.75,
        maintainProportions: true,
        minHeight: 8,
        minWidth: 8,
        rotatable: true,
        defaults: {
            autoPlaced: false,
            crop: { top: 0, left: 0, bottom: 0, right: 0 },
            imageId: 0,
            height: 100,
            left: 0,
            locked: false,
            module: 'UploadedImage',
            restricted: false,
            rotation: 0,
            top: 0,
            width: 100
        }
    },
    shape: {
        doubleClick: events.doubleClick,
        initialSize: 0.75,
        rotatable: true,
        defaults: {
            Rectangle: {
                fillColor: '#579DEF',
                module: 'Rectangle',
                rotation: 0,
                strokeLineCap: 'square', // butt, round, square
                strokeLineJoin: 'mitre', // bevel, mitre, round
                strokeColor: '#000000',
                strokeWidth: 0
            },
            Ellipse: {
                fillColor: '#579DEF',
                module: 'Ellipse',
                rotation: 0,
                strokeColor: '#000000',
                strokeWidth: 0
            },
            Line: {
                module: 'Line',
                rotation: 0,
                strokeColor: '#579DEF',
                strokeLineCap: 'square', // butt, round, square
                strokeLineJoin: 'mitre', // bevel, mitre, round
                strokeWidth: 1
            }
        }
    }
};

module.exports = new ConfigurationBase(defaults);