const _merge = require('lodash-compat/object/merge');

/*
 * The base configuration class which provides a default merging capability.
 */
class ConfigurationBase {
    constructor(defaultValues) {
        _merge(this, defaultValues);
    }

    /*
     * Can be overridden by sub classes if custom merging is needed
     */
    merge(options) {
        _merge(this, options);
    }
}

module.exports = ConfigurationBase;