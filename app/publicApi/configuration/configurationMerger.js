const mergeRecursive = function mergeRecursive(parent, currentSection, sectionName, path) {
    if (typeof currentSection !== 'object') {
        return;
    }

    // first try to merge any leaves
    Object.keys(currentSection).forEach(subsection => mergeRecursive(currentSection, currentSection[subsection], subsection, `${path}/${subsection}`));

    // then merge this section if its module exists
    try {
        const configModule = require(`publicApi/configuration/${path}`);
        configModule.merge(currentSection);

        // override the property with the actual config module
        parent[sectionName] = configModule;
    } catch (e) {
        // module doesn't exist, carry on...
    }
};

/*
 * Iterates over the keys of the given options object, and recursively merges the options specified in those
 * keys with the configuration modules. This is done recursively, doing a depth first traversal of the options
 * object. If a module is found for the path at the current depth, it is loaded, and the options are merged into
 * it. After merging those options, its value is deleted from its parent, so that the parent's module won't
 * get the duplicated options merged into it.
 *
 * For example, if the options object is:
 * {
 *   ui: {
 *       anOption: aValue,
 *
 *       aComplexOption: {
 *         a: 1,
 *         b: 2
 *       },
 *
 *       widgets: {
 *         // a bunch of widget config
 *       }
 *   }
 * }
 *
 * The code will recursively get to aComplexOption, realize there is no module for it, move onto
 * widgets, load the widgets module, merge the widgets section into that module, delete the widgets
 * section from ui object, leaving the object in the following state:
 * {
 *   ui: {
 *       anOption: aValue,
 *
 *       aComplexOption: {
 *         a: 1,
 *         b: 2
 *       }
 *   }
 * }
 *
 * The code will then return back to the ui section, load the ui module, and merge anOption and
 * aComplexOption into the ui module.
 */
module.exports = function(options) {
    Object.keys(options).forEach(section => mergeRecursive(options, options[section], section, section));
};
