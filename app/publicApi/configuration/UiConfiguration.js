const _merge = require('lodash-compat/object/merge');
const ConfigurationBase = require('publicApi/configuration/ConfigurationBase');

/*
 * Overrides the base configuration class for ui.js
 */
class UiConfiguration extends ConfigurationBase {
    constructor(defaultValues) {
        super();
        ConfigurationBase.call(this, defaultValues);
    }

    //merge allows palette in index to override defaults, rather than merge with them
    merge(options) {
        if (options.colors && options.colors.palettes) {
            this.colors.palettes.print = [];
        }
        _merge(this, options);
    }
}

module.exports = UiConfiguration;