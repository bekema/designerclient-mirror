const docRepo = require('DocumentRepository');

module.exports = function getDocumentJson() {
    return docRepo.getActiveDocument().toJSON();
};