const clients = require('services/clientProvider');
const docRepo = require('DocumentRepository');

function updateDocumentReference(newDocJson) {
    const document = docRepo.getActiveDocument();

    if (document.get('documentReference') !== newDocJson.documentReference) {
        document.set('documentReference', newDocJson.documentReference);
    }

    return newDocJson;
}

module.exports = function saveDocument() {
    return clients.account.saveDocument(docRepo.getActiveDocument().toJSON()).then(updateDocumentReference);
};
