const clients = require('services/clientProvider');
const documentRepository = require('DocumentRepository');
const getDocumentJson = require('publicApi/documentJson');

module.exports = function generatePdfProof(documentJson) {
    const documentReference = documentRepository.getActiveDocument().get('documentReference');
    documentJson = documentJson || getDocumentJson();
    if (documentReference) {
        let prepressInstructionsUrl;
        return clients.uds.loadReference(documentReference).then(response => {
            prepressInstructionsUrl = response.prepressInstructionsUrl;
            return clients.uds.update(documentJson, response.cimDocUrl);
        }).then(() => clients.prepress.getProof(prepressInstructionsUrl));
    } else {
        return clients.uds
            .save(documentJson)
            .then(response => clients.prepress.getProof(response.instructionSourceUrl));
    }
};