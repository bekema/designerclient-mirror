// This loads all the SVG icons in the icons folder and puts them in a single sprite sheet, so that they
// can be used by their ID elsewhere. The SVGs are first piped through img-loader, which minifies them,
// and saves about 50% of the file size. It also allows us to not care about which tool produces the SVG,
// since different tools add different cruft and metadata.

const iconSpriteSheet = {};

const initialize = function() {
    delete iconSpriteSheet.initialize;

    // The !! at the start of the loader disables all other loaders so they don't interfere with this one.
    const files = require.context('!!svg-sprite?{"name": "dcl-icon-[name]"}!img!content/icons', true, /\.svg$/);
    files.keys().forEach(files);
};

iconSpriteSheet.initialize = initialize;

module.exports = iconSpriteSheet;
