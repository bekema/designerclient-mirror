require('../content/scss/main.scss');
require('backbone-extensions/backbone.allextensions');
require('es6-promise');
require('select2');

const configurationMerger = require('publicApi/configuration/configurationMerger');

const _ = require('underscore');
const Engine = require('Engine');
const clients = require('services/clientProvider');
const validations = require('validations/ValidationProvider');
const WidgetFactory = require('ui/WidgetFactory');
const lng = require('Localization');
const DesignerContext = require('DesignerContext');
const documentRepository = require('DocumentRepository');
const udsConverter = require('services/uds/converter/udsConverter');

const publicApi = require('publicApi/public');
const uiConfig = require('publicApi/configuration/ui');
const iconSpriteSheet = require('./iconSpriteSheet');

// TODO: What should happen if the skus don't match between mcpSku, surface specs sku, and document sku?
const getSku = ({ document, surfaceSpecifications, mcpSku }) => {
    let sku = mcpSku;

    if (surfaceSpecifications && surfaceSpecifications.mcpSku) {
        sku = surfaceSpecifications.mcpSku;
    }

    if (document && document.sku) {
        sku = document.sku;
    }

    return Promise.resolve(sku);
};

const getSurfaceSpecs = (surfaceSpecifications, sku) => {
    if (surfaceSpecifications) {
        return Promise.resolve(surfaceSpecifications);
    }

    if (!sku) {
        return Promise.reject('No MCP SKU found. DCL must be loaded with an MCP SKU or surface specification.');
    }

    return clients.surfaceSpecifications.getSurfaceSpecifications(sku);
};

const getDocument = (document, documentReference) => {
    if (!document && documentReference) {
        return clients.uds.loadReference(documentReference).then(response => clients.uds.loadDocument(response));
    }

    return Promise.resolve(document);
};

const getDocumentReference = (documentReference, mcpSku, developerMode) => {
    if (!documentReference && developerMode) {
        // test purposes
        return clients.account.getAllDocuments().then(response => {
            if (response.length > 0 && response[0].sku === mcpSku) {
                return response[0].documentReference;
            }
        });
    }

    return Promise.resolve(documentReference);
};

const getScenes = (docData, scenes, surfaceData, docRef) => {
    if (scenes || !uiConfig.canvas.useProductScenes) {
        return Promise.resolve({ documentResponse: docData, scenesResponse: scenes, surfaceSpecsResponse: surfaceData, docRef });
    }

    return clients.scene.getScenes(surfaceData.surfaces.map(surface => surface.surfaceId)).then(scenesResponse => {
        return { documentResponse: docData, scenesResponse, surfaceSpecsResponse: surfaceData, docRef };
    });
};

const loadFonts = () => clients.font.initialize();

// Retrieve all needed data, such as surface specs and scenes
const retrieveData = ({ document, mcpSku, surfaceSpecifications, scenes, documentReference, developerMode }) => {
    // When the documentReference is present, we can get the surface specifcations based on the SKU in the document
    let doc;
    return getDocumentReference(documentReference, mcpSku, developerMode).then(response => {
        documentReference = response;
    }).then(() => getDocument(document, documentReference)
        .then(response => {
            doc = response;
        }).then(() => getSku({ doc, surfaceSpecifications, mcpSku })
            .then(sku => getSurfaceSpecs(surfaceSpecifications, sku))
            .then(surfaceSpecs => getScenes(doc, scenes, surfaceSpecs, documentReference))
            .then(({ documentResponse, scenesResponse, surfaceSpecsResponse, docRef }) => {
                return DesignerContext.init({
                    clients,
                    technology: surfaceSpecsResponse.surfaces[0].decorationTechnologyCode
                })
                .then(loadFonts)
                .then(() => ({ documentResponse, scenesResponse, surfaceSpecsResponse, docRef }));
            })
    ));
};

const api = {
    start: ({ document, surfaceSpecifications, scenes, canvasObjects, configuration, mcpSku, documentReference, developerMode }) => {
        configurationMerger(configuration);

        // Initialize merged Localization files
        lng.init();
        lng.changeLanguage();

        //Initialize icon SVG sprite
        if (uiConfig.useDefaultIcons) {
            iconSpriteSheet.initialize();
        }

        return Promise.resolve()
            .then(clients.initialize)
            .then(validations.initialize)
            .then(() => retrieveData({ document, mcpSku, surfaceSpecifications, scenes, documentReference, developerMode }))
            .then(({ documentResponse, scenesResponse, surfaceSpecsResponse, docRef }) => {
                // Bad, I know...
                documentReference = docRef;
                document = documentReference ? udsConverter.convertToDocument(documentResponse, documentReference) : document;
                surfaceSpecifications = surfaceSpecsResponse;
                scenes = scenesResponse;
            })
            .then(WidgetFactory.buildWidgets)
            .then(() => new Engine().start({ document, surfaceSpecifications, scenes, canvasObjects }))
            // return important things that were loaded to the caller of start
            .then(() => {
                return {
                    surfaceSpecifications,
                    scenes,
                    documentId: documentRepository.getActiveDocument().get('id')
                };
            });
    }
};

const exports = _.extend(api, publicApi);

/* global ADD_TO_GLOBAL */
/*
 * Using the webpack hot middleware doesn't work correctly with UMD targets,
 * so we have to add to the global scope manually
 */
if (ADD_TO_GLOBAL) {
    window.dcl = exports;
}

module.exports = exports;
