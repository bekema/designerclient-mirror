const Version = require('../version');
const versionObject = new Version();

try {
    var mkdirp = require('mkdirp');
    var fs = require('fs');
    mkdirp.sync('./build');
    fs.writeFileSync('./build/version.txt', versionObject.toString());
} catch (e) {
    console.log('Failed to tag version to commit', e);

    process.exit(1);
}
