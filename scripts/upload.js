var AWS = require('aws-sdk');
var Version = require('../version');
var version = new Version();
var fs = require('fs');

var buildDir = 'build/';
var bucketName = 'mcp-designer-irl';
var region =  'eu-west-1';

AWS.config.region = region;
var s3bucket = new AWS.S3({ params: { Bucket: bucketName } });

var uploadToS3 = function(file, Key, maxAge) {
    var mime = require('mime');
    var params = {
        Key,
        Body: fs.createReadStream(buildDir + file),
        ContentType: mime.lookup(file),
        CacheControl: `max-age=${maxAge}`
    };

    s3bucket.upload(params, function(err, data) {
        if (err) {
            console.log(`Error uploading data: ${err}`);
            process.exit(1);
        } else {
            console.log(`${file} uploaded to ${Key}`);
        }
    });
};

var files = fs.readdirSync(buildDir);
var basePath = version.toString();
/* eslint-disable no-magic-numbers */
var fiveMinutes = 60 * 5;
var oneYear = 60 * 60 * 24 * 365;

files.forEach((file) => {
    var versionKey = `${basePath}/${file}`;
    var currentKey = `${version.majorVersion()}.current/${file}`;
    uploadToS3(file, versionKey, oneYear);
    uploadToS3(file, currentKey, fiveMinutes);
});
