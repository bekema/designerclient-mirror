/*
 * The designer library is currently supporting a major.minor.build versioning
 * scheme, where the build number is the jenkins build number
 *
 * Note: When updating the version, you must also reset the build
 * number in the jenkins job.
 */
'use strict';

const MAJOR_VERSION = 0;
const MINOR_VERSION = 5;
const execSync = require('child_process').execSync;

const tagList = function() {
    const versionList = execSync(`git tag`).toString().trim().split('\n');

    return versionList.map(item => {
        const data = item.split('.');
        return {
            major: parseInt(data[0].toString().trim()),
            minor: parseInt(data[1].toString().trim()),
            revision: parseInt(data[2].toString().trim())
        };
    });
};

class Version {

    constructor() {
        const latestVersionTag = this.latestVersionTag();
        const versionBumped = (latestVersionTag.major !== MAJOR_VERSION || latestVersionTag.minor !== MINOR_VERSION);

        this.revision =  versionBumped ? 0 : latestVersionTag.revision;
    }

    nextVersion() {
        return `${MAJOR_VERSION}.${MINOR_VERSION}.${this.revision + 1}`;
    }

    latestVersionTag() {
        const versionList = tagList();
        return versionList.sort((a, b) => {
            // sort the list based on major, minor, and revision
            if (a.major === b.major) {
                if (a.minor === b.minor) {
                    if (a.revision === b.revision) {
                        return 0;
                    } else {
                        return a.revision - b.revision;
                    }
                } else {
                    return a.minor - b.minor;
                }
            } else {
                return a.major - b.minor;
            }
        })[versionList.length - 1]; //and return the top one
    }

    toString() {
        return `${MAJOR_VERSION}.${MINOR_VERSION}.${this.revision}`;
    }

    majorVersion() {
        return MAJOR_VERSION;
    }
    minorVersion() {
        return MINOR_VERSION;
    }
}

module.exports = Version;