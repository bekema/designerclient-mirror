/* global __dirname */

var path = require('path');

var nodeModulesPath = path.resolve(__dirname, 'node_modules');
var srcPath = path.resolve(__dirname, 'app');
var contentPath = path.resolve(__dirname, 'content');
var corePath = path.resolve(srcPath, 'core');
var dclPath = path.resolve(srcPath, 'app.js');
var dclUIPath = path.resolve(__dirname, 'dcl-ui', 'dcl-ui.js');
var helpersPath = path.resolve(__dirname, 'app/ui/templateHelpers');
var outputPath = path.resolve(__dirname, 'build');
var sassPath = path.resolve(__dirname, 'content/scss');
var SassLintPlugin = require('sasslint-webpack-plugin');
var autoprefixer = require('autoprefixer');

var config = {
    entry: {
        dcl: [dclPath],
        dclUI: [dclUIPath]
    },

    devtool: 'source-map',

    resolve: {
        root: [srcPath, corePath, contentPath, __dirname],
        extensions: ['', '.js', '.tmp']
    },

    output: {
        publicPath: '/build/',
        path: outputPath,
        filename: '[name].js'
    },

    module: {
        preLoaders: [
            { test: /\.js$/, loaders: ['eslint'], include: srcPath }
        ],

        loaders: [
            {
                test: /\.js$/,
                exclude: [nodeModulesPath],
                loader: 'babel',
                query: {
                    presets: ['es2015'],
                    plugins: []
                }
            },
            { test: /\.json$/, loader: 'json-loader' },
            { test: /\.(jpe?g|gif|png)$/, loader: 'url?limit=1000000' },
            { test: /\.tmp$/, loader: `handlebars-loader?helperDirs[]=${helpersPath}` },
            { test: /\.svg$/, loader: 'file-loader' }
        ]
    },

    plugins: [
        new SassLintPlugin({
            configFile: '.sass-lint.yml',
            context: [sassPath],
            ignoreFiles: ['/node_modules/**/*']
        })
    ],

    eslint: {
        failOnError: false
    },

    sassLoader: {
        includePaths: [
            sassPath,
            'node_modules/support-for/sass',
            'node_modules/normalize-scss/sass',
            'node_modules/select2/dist/css'
        ]
    },

    postcss: [autoprefixer({ browsers: ['IE 10', '> 1%'] })]
};

module.exports = config;
