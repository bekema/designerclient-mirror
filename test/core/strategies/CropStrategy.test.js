/* global describe, expect, it */
/* eslint-disable no-magic-numbers */

var CropStrategy = require('strategies/CropStrategy');

describe('strategies/CropStrategy', function() {
    it('getAttributes returns the proper crop attributes', function() {

        var canvasSize = {
            height: 10,
            width: 10
        };

        var imageSize = {
            height: 20,
            width: 10
        };

        var currentAttributes = {};

        var result = CropStrategy.getAttributes(canvasSize, imageSize, currentAttributes);

        expect(result.height).toBe(10);
        expect(result.width).toBe(10);
    });
});
