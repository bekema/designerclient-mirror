/* global describe, expect, it */
/* eslint-disable no-magic-numbers */

var StretchStrategy = require('strategies/StretchStrategy');

describe('strategies/StretchStrategy/', function() {
    it('getAttributes returns the proper stretched attributes', function() {

        var canvasSize = {
            height: 100,
            width: 100
        };

        var imageSize = {
            height: 20,
            width: 10
        };

        var currentAttributes = {};

        var result = StretchStrategy.getAttributes(canvasSize, imageSize, currentAttributes);

        expect(result.height).toBe(100);
        expect(result.width).toBe(100);
    });
});
