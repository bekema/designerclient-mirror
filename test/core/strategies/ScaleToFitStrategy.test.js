/* global describe, expect, it */
/* eslint-disable no-magic-numbers */

var ScaleToFitStrategy = require('strategies/ScaleToFitStrategy');

describe('strategies/ScaleToFitStrategy', function() {
    it('getAttributes returns the proper scaled attributes', function() {

        var canvasSize = {
            height: 100,
            width: 100
        };

        var imageSize = {
            height: 20,
            width: 10
        };

        var currentAttributes = {};

        var result = ScaleToFitStrategy.getAttributes(canvasSize, imageSize, currentAttributes);

        expect(result.height).toBe(100);
        expect(result.width).toBe(50);
    });
});
