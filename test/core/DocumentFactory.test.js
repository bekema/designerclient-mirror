/* global describe, expect, it, afterEach */

const documentFactory = require('DocumentFactory');

const mockUtil = {
    generateUUID: () => 'a universally unique ID'
};

const surfaceSpecs = {
    surfaces: [
        {
            name: 'front',
            widthInMm: 1920,
            heightInMm: 1080
        },
        {
            name: 'back',
            widthInMm: 1024,
            heightInMm: 768
        }
    ]
};

describe('core/DocumentFactory', function() {

    beforeAll(function() {
        documentFactory.__Rewire__('util', mockUtil);
    });

    afterAll(function() {
        documentFactory.__ResetDependency__('util');
    });

    it('initializes a document with a UUID', function() {
        const initialDoc = {};
        const doc = documentFactory.createDocument(initialDoc, surfaceSpecs);
        expect(doc.id).toBe('a universally unique ID');
    });

    it("doesn't override a document's ID", function() {
        const initialDoc = {
            id: 'initial doc ID'
        };
        const doc = documentFactory.createDocument(initialDoc, surfaceSpecs);
        expect(doc.id).toBe('initial doc ID');
    });

    it('updates the document module', function() {
        const initialDoc = {};
        const doc = documentFactory.createDocument(initialDoc, surfaceSpecs);
        expect(doc.module).toBe('Document');
    });

    it('updates the canvas module', function() {
        const initialDoc = {
            canvases: [
                {},
                {}
            ]
        };
        const doc = documentFactory.createDocument(initialDoc, surfaceSpecs);
        expect(doc.canvases[0].module).toBe('Canvas');
        expect(doc.canvases[1].module).toBe('Canvas');
    });

    it('creates a new document from surface specifications', function() {
        const doc = documentFactory.createDocument(undefined, surfaceSpecs);
        expect(doc.canvases.length).toBe(surfaceSpecs.surfaces.length);
        expect(doc.canvases[0].name).toBe(surfaceSpecs.surfaces[0].name);
        expect(doc.canvases[0].width).toBe(surfaceSpecs.surfaces[0].widthInMm);
        expect(doc.canvases[0].height).toBe(surfaceSpecs.surfaces[0].heightInMm);
        expect(doc.canvases[1].name).toBe(surfaceSpecs.surfaces[1].name);
        expect(doc.canvases[1].width).toBe(surfaceSpecs.surfaces[1].widthInMm);
        expect(doc.canvases[1].height).toBe(surfaceSpecs.surfaces[1].heightInMm);
    });

    it('throws when document canvas count doesnt match surface specs surface count', () => {
        const initialDoc = {
            canvases: [
                {}
            ]
        };
        expect(() => documentFactory.validateDocument(initialDoc, surfaceSpecs)).toThrow();
    });

    it('throws when first canvas size does not match first surface size', () => {
        const initialDoc = {
            canvases: [
                {
                    name: 'front',
                    width: 1,
                    height: 2
                },
                {
                    name: 'back',
                    width: 1024,
                    height: 768
                }
            ]
        };
        expect(() => documentFactory.validateDocument(initialDoc, surfaceSpecs)).toThrow();
    });

    it('throws when second canvas size does not match second surface size', () => {
        const initialDoc = {
            canvases: [
                {
                    name: 'front',
                    width: 1920,
                    height: 1080
                },
                {
                    name: 'back',
                    width: 1,
                    height: 2
                }
            ]
        };
        expect(() => documentFactory.validateDocument(initialDoc, surfaceSpecs)).toThrow();
    });

    it('throws when first canvas name does not match first surface name', () => {
        const initialDoc = {
            canvases: [
                {
                    name: 'not front',
                    width: 1920,
                    height: 1080
                },
                {
                    name: 'back',
                    width: 1024,
                    height: 768
                }
            ]
        };
        expect(() => documentFactory.validateDocument(initialDoc, surfaceSpecs)).toThrow();
    });

    it('throws when second canvas name does not match second surface name', () => {
        const initialDoc = {
            canvases: [
                {
                    name: 'front',
                    width: 1920,
                    height: 1080
                },
                {
                    name: 'not back',
                    width: 1024,
                    height: 768
                }
            ]
        };
        expect(() => documentFactory.validateDocument(initialDoc, surfaceSpecs)).toThrow();
    });

    it('does not throw when names and sizes match', () => {
        const initialDoc = {
            canvases: [
                {
                    name: 'front',
                    width: 1920,
                    height: 1080
                },
                {
                    name: 'back',
                    width: 1024,
                    height: 768
                }
            ]
        };
        expect(() => documentFactory.validateDocument(initialDoc, surfaceSpecs)).not.toThrow();
    });
});
