/* global afterAll, beforeAll, beforeEach, describe, expect, it */

var _ = require('underscore');
var Backbone = require('backbone-extensions/backbone.allextensions');
var Canvas = require('models/Canvas');
var CanvasViewModel = require('viewModels/CanvasViewModel');
var selectionManager = require('SelectionManager');
var metaDataProvider = require('MetaDataProvider');

var docRepositoryMoc = (function() {
    var canvas, canvasViewModel;
    return _.extend({
        resetForUnitTests: function() {
            canvas = new Canvas({ id: 'c', items: [], module: 'Canvas' });
            canvasViewModel = new CanvasViewModel({
                id: 'c',
                model: canvas,
                itemViewModels: [],
                module: 'CanvasViewModel'
            });
        },
        getActiveCanvas: function() {
            return canvas;
        },
        getActiveCanvasViewModel: function() {
            return canvasViewModel;
        },
        getCanvasViewModel: function() {
            return canvasViewModel;
        }
    }, Backbone.Events);
})(); 

var AddCommand = require('commands/Add');

describe('core/commands/Add', function() {

    beforeAll(function() {
        AddCommand.__Rewire__('documentRepository', docRepositoryMoc);
        
        metaDataProvider.registerMetaData([
            { model: 'TextField', viewModel: 'TextFieldViewModel', previewAttributes: [] },
            { model: 'Canvas', viewModel: 'CanvasViewModel' }
        ]);
    });

    beforeEach(function() {
        docRepositoryMoc.resetForUnitTests();
    });
    
    afterAll(function() {
        AddCommand.__ResetDependency__('documentRepository');
    });

    it('adds all items to the canvas', function() {
        var canvasModel = new Backbone.Model();
        canvasModel.items = new Backbone.Collection();
        var command = new AddCommand();

        var items = [
            new Backbone.Model(),
            new Backbone.Model()
        ];

        command.execute({
            items: items,
            model: canvasModel
        });

        expect(canvasModel.items.length).toBe(2);
    });

    it('added items have unique ids', function() {
        var canvasModel = new Backbone.Model();
        canvasModel.items = new Backbone.Collection([{
            id: 1
        }]);
        var command = new AddCommand();

        var items = [
            new Backbone.Model({ id: 1 }),
            new Backbone.Model({ id: 2 })
        ];

        command.execute({
            items: items,
            model: canvasModel
        });

        expect(canvasModel.items.length).toBe(3);
        expect(canvasModel.items.at(0).get('id')).toBe(1);
        expect(canvasModel.items.at(1).get('id')).not.toBe(1);
        expect(canvasModel.items.at(2).get('id')).toBe(2);
    });

    it('added item gets sequentially increased tabIndex', function() {
        var canvasModel = new Backbone.Model();
        canvasModel.items = new Backbone.Collection([{
            id: 1,
            tabIndex: 1
        }]);
        var command = new AddCommand();

        var items = [
            new Backbone.Model({
                id: 1,
                tabIndex: 1
            })
        ];

        command.execute({
            items: items,
            model: canvasModel
        });

        expect(canvasModel.items.last().get('tabIndex')).toBe(2);
    });

    it('added item gets sequentially increased z-index even if nothing else has z-index', function() {
        var canvasModel = new Backbone.Model();
        canvasModel.items = new Backbone.Collection([{
            id: 1
        }]);
        var command = new AddCommand();

        var items = [
            new Backbone.Model({ id: 1 })
        ];

        command.execute({
            items: items,
            model: canvasModel
        });

        expect(canvasModel.items.last().get('zIndex')).toBe(1);
    });

    it('added item gets an id', function() {
        var canvasModel = new Backbone.Model();
        canvasModel.items = new Backbone.Collection([{
            id: 1
        }]);
        var command = new AddCommand();

        var items = [
            new Backbone.Model()
        ];

        command.execute({
            items: items,
            model: canvasModel
        });

        expect(canvasModel.items.last().has('id')).toBe(true);
    });

    it('option selectNewModels selects the added models', function() {
        var canvasModel = docRepositoryMoc.getActiveCanvas();
        var command = new AddCommand();

        var itemToAdd = new Backbone.Model({ id: 1, module: 'TextField' });
        var items = [itemToAdd];

        command.execute({
            items: items,
            model: canvasModel,
            selectNewModels: true
        });

        var newViewModel = docRepositoryMoc.getActiveCanvasViewModel().itemViewModels.first();
        expect(selectionManager.size()).toBe(1, 'one item is selected');
        expect(selectionManager.first()).toBe(newViewModel, 'the newly added item is selected');
    });
});
