/* global afterAll, beforeAll, beforeEach, describe, expect, it, spyOn */

const _ = require('underscore');
const Backbone = require('backbone-extensions/backbone.allextensions');
const Command = require('commands/Command');
const ChangeBackgroundColorCommand = require('commands/ChangeBackgroundColor');

const MockAddCommand = Command.extend({
    apply: () => {}
});

const MockDeleteCommand = Command.extend({
    apply: () => {}
});

const mockCanvasModel = _.extend({
    items: [],
    get: () => {}
});

const fakeBackgroundItem = {
    get: (value) => value === 'isBackground',
    set: () => {}
};

const mockCanvasModelWithBackground = _.extend({
    items: {
        models: [fakeBackgroundItem]
    },
    get: () => {}
});

const color = { hex: '#000000' };
const command = new ChangeBackgroundColorCommand();

describe('core/commands/ChangeBackgroundColor', function() {
    beforeAll(function() {
        Command.__Rewire__('Backbone', Backbone);
        ChangeBackgroundColorCommand.__Rewire__('Command', Command);
        ChangeBackgroundColorCommand.__Rewire__('AddCommand', MockAddCommand);
        ChangeBackgroundColorCommand.__Rewire__('DeleteCommand', MockDeleteCommand);
    });

    afterAll(function() {
        Command.__ResetDependency__('Backbone');
        ChangeBackgroundColorCommand.__ResetDependency__('Command');
        ChangeBackgroundColorCommand.__ResetDependency__('AddCommand');
        ChangeBackgroundColorCommand.__Rewire__('DeleteCommand');
    });

    beforeEach(function() {
        spyOn(MockAddCommand.prototype, 'apply');
        spyOn(MockDeleteCommand.prototype, 'apply');
        spyOn(fakeBackgroundItem, 'set');
    });

    it('adds a background shape when one does not exist ', function() {
        command.execute({
            model: mockCanvasModel,
            color
        });

        expect(MockAddCommand.prototype.apply).toHaveBeenCalled();
        expect(fakeBackgroundItem.set).not.toHaveBeenCalled();
        expect(MockDeleteCommand.prototype.apply).not.toHaveBeenCalled();
    });

    it('updates a background shape when one does exist ', function() {
        command.execute({
            model: mockCanvasModelWithBackground,
            color
        });

        expect(MockAddCommand.prototype.apply).not.toHaveBeenCalled();
        expect(fakeBackgroundItem.set).toHaveBeenCalled();
        expect(MockDeleteCommand.prototype.apply).not.toHaveBeenCalled();
    });

    it('removes background shape when no color is provided ', function() {
        command.execute({
            model: mockCanvasModelWithBackground
        });

        expect(MockAddCommand.prototype.apply).not.toHaveBeenCalled();
        expect(fakeBackgroundItem.set).not.toHaveBeenCalled();
        expect(MockDeleteCommand.prototype.apply).toHaveBeenCalled();
    });
});
