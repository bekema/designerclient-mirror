/* global describe, expect, it */

/* eslint-disable no-magic-numbers */

const ColorConverter = require('util/ColorConverter');

describe('core/ColorConverter', function() {

    // TODO fix something, it keeps getting "undefined is not a constructor" errors
    // it('can check for the RGB object', function() {
    //     const rgb1 = { r: 34, g: 76, b: 234 };
    //     const nonRgb1 = { r: 34, g: 76, b: NaN };
    //     const nonRgb2 = [34, 34, 34];
    //     const nonRgb3 = { r: 34, g: '34', b: 111 };
    //     const nonRgb4 = false;
    //     const nonRgb5 = null;
    //     const nonRgb6 = { r: 34, b: 111 };

    //     expect(ColorConverter.isRgbDefined(rgb1)).toBe(true);
    //     expect(ColorConverter.isRgbDefined(nonRgb1)).not.toBe(true);
    //     expect(ColorConverter.isRgbDefined(nonRgb2)).not.toBe(true);
    //     expect(ColorConverter.isRgbDefined(nonRgb3)).not.toBe(true);
    //     expect(ColorConverter.isRgbDefined(nonRgb4)).not.toBe(true);
    //     expect(ColorConverter.isRgbDefined(nonRgb5)).not.toBe(true);
    //     expect(ColorConverter.isRgbDefined(nonRgb6)).not.toBe(true);
    // });

    it('can convert RGB to HEX', function() {
        const rgb1 = { r: 0, g: 0, b: 0 };
        const rgb2 = { r: 34, g: 76, b: 234 };
        const rgb3 = { r: 255, g: 255, b: 255 };

        expect(ColorConverter.rgb2hex(rgb1)).toBe('#000000');
        expect(ColorConverter.rgb2hex(rgb2)).toBe('#224CEA');
        expect(ColorConverter.rgb2hex(rgb3)).toBe('#FFFFFF');
    });

    it('can convert HEX to RGB', function() {
        const hex1 = '#000000';
        const hex2 = '#224CEA';
        const hex3 = '#FFFFFF';

        expect(ColorConverter.hex2rgb(hex1)).toEqual({ r: 0, g: 0, b: 0 });
        expect(ColorConverter.hex2rgb(hex2)).toEqual({ r: 34, g: 76, b: 234 });
        expect(ColorConverter.hex2rgb(hex3)).toEqual({ r: 255, g: 255, b: 255 });
    });

    // TODO fix something, it keeps getting "undefined is not a constructor" errors
    // it('can check convert RGB to HSV', function() {
    //     const rgb1 = { r: 0, g: 0, b: 0 };
    //     const rgb2 = { r: 34, g: 76, b: 234 };
    //     const rgb3 = { r: 255, g: 255, b: 255 };

    //     expect(ColorConverter.hex2rgb(rgb1)).toEqual({ h: 0, s: 0, v: 0 });
    //     expect(ColorConverter.hex2rgb(rgb2)).toEqual({ h: 227, s: 85.5, v: 91.8 });
    //     expect(ColorConverter.hex2rgb(rgb3)).toEqual({ h: 0, s: 0, v: 100 });
    // });

});