/* global describe, expect, it, afterEach */

require('backbone-extensions/backbone.allextensions');

const ChromeModuleFactory = require('ChromeModuleFactory');
const _ = require('underscore');

const enabledItemsConfig = {
    canvas: {
        chromes: {
            ruler: {
                enabled: true
            },
            dimensions: {
                enabled: true
            },
            canvasTools: {
                enabled: true
            }
        }
    }
};

const disabledItemsConfig = {
    canvas: {
        chromes: {
            ruler: {
                enabled: false
            },
            dimensions: {
                enabled: false
            },
            canvasTools: {
                enabled: false
            }
        }
    }
};

const disabledCanvasToolsFeature = {
    canvasBackgroundColor: {
        enabled: false
    }
};

const enabledCanvasToolsFeature = {
    canvasBackgroundColor: {
        enabled: true
    }
};


describe('core/ChromeModuleFactory', function() {

    afterEach(function() {
        ChromeModuleFactory.__ResetDependency__('uiConfig');
        ChromeModuleFactory.__ResetDependency__('FeatureManager');
    });

    it('simply returns if no chromeData is provided', function() {
        expect(ChromeModuleFactory.buildChromes(null)).toBe(undefined);
    });

    it('returns the ruler chrome if its enabled in the configuration', function() {
        ChromeModuleFactory.__Rewire__('uiConfig', enabledItemsConfig);

        const chromes = ChromeModuleFactory.buildChromes({});
        const chrome = _.find(chromes, c => c.module === 'CanvasRulerViewModel');

        expect(chrome).not.toBe(undefined);
    });

    it('excludes the rule chrome if its disabled in the configuration', function() {
        ChromeModuleFactory.__Rewire__('uiConfig', disabledItemsConfig);

        const chromes = ChromeModuleFactory.buildChromes({});
        const chrome = _.find(chromes, c => c.module === 'CanvasRulerViewModel');

        expect(chrome).toBe(undefined);
    });

    it('returns the dimensions chrome if its enabled in the configuration', function() {
        ChromeModuleFactory.__Rewire__('uiConfig', enabledItemsConfig);

        const chromes = ChromeModuleFactory.buildChromes({});
        const chrome = _.find(chromes, c => c.module === 'CanvasDimensionsViewModel');

        expect(chrome).not.toBe(undefined);
    });

    it('excludes the dimensions chrome if its disabled in the configuration', function() {
        ChromeModuleFactory.__Rewire__('uiConfig', disabledItemsConfig);

        const chromes = ChromeModuleFactory.buildChromes({});
        const chrome = _.find(chromes, c => c.module === 'CanvasDimensionsViewModel');

        expect(chrome).toBe(undefined);
    });

    it('returns the canvas tools chrome if its enabled in the configuration and feature is enabled', function() {
        ChromeModuleFactory.__Rewire__('uiConfig', enabledItemsConfig);
        ChromeModuleFactory.__Rewire__('FeatureManager', enabledCanvasToolsFeature);

        const chromes = ChromeModuleFactory.buildChromes({});
        const chrome = _.find(chromes, c => c.module === 'CanvasToolsViewModel');

        expect(chrome).not.toBe(undefined);
    });

    it('excludes the canvas tools chrome if its enabled in the configuration but the feature is disabled', function() {
        ChromeModuleFactory.__Rewire__('uiConfig', enabledItemsConfig);
        ChromeModuleFactory.__Rewire__('FeatureManager', disabledCanvasToolsFeature);

        const chromes = ChromeModuleFactory.buildChromes({});
        const chrome = _.find(chromes, c => c.module === 'CanvasToolsViewModel');

        expect(chrome).toBe(undefined);
    });

    it('excludes the canvas tools chrome if its disabled in the configuration', function() {
        ChromeModuleFactory.__Rewire__('uiConfig', disabledItemsConfig);

        const chromes = ChromeModuleFactory.buildChromes({});
        const chrome = _.find(chromes, c => c.module === 'CanvasToolsViewModel');

        expect(chrome).toBe(undefined);
    });

    // TODO
    // - add tests covering chromeData.scene information
    // - add tests convering chromeData.canvasObjects
    // - add tests covering old surface spec margins
    // - add tests covering new surface spec margins
});
