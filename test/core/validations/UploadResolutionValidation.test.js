// /* global describe, expect, it */

// /* eslint-disable no-magic-numbers */

// const imageResolutionValidation = require('views/components/validations/ImageResolutionValidation');

// describe('core/views/components/validations/imageResolutionValidation', function() {

//     // TODO fix something, it keeps getting "undefined is not a constructor" errors
//     it('can validate', function() {
//         const zoom = 4;
//         const crop = { left: .1, top: .1, right: .1, bottom: .1 };
//         const naturalDimensions = { width: 2000, height: 1000 };
//         const dimensions1 = { width: 200, height: 100 };
//         const dimensions2 = { width: 1000, height: 500 };
//         const dimensions3 = { width: 2000, height: 1000 };
//         const dimensions4 = { width: 4000, height: 2000 };
//         const dimensions5 = { width: 8000, height: 4000 };

//         expect(imageResolutionValidation.validationLogic({ zoom, crop, naturalDimensions, dimensions: dimensions1 }).status).toBe('success');
//         expect(imageResolutionValidation.validationLogic({ zoom, crop, naturalDimensions, dimensions: dimensions2 }).status).toBe('success');
//         expect(imageResolutionValidation.validationLogic({ zoom, crop, naturalDimensions, dimensions: dimensions3 }).status).toBe('success');
//         expect(imageResolutionValidation.validationLogic({ zoom, crop, naturalDimensions, dimensions: dimensions4 }).status).toBe('success');
//         expect(imageResolutionValidation.validationLogic({ zoom, crop, naturalDimensions, dimensions: dimensions5 }).status).toBe('success');
//     });

// });