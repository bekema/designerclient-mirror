/* global describe, expect, it */

/* eslint-disable no-magic-numbers */

const Util = require('util/Util');

describe('core/util/Util', function() {

    it('can test for containsPoint correctly', function() {
        const rect = { left: 10, top: 10, right: 20, bottom: 20 };
        const containedPoint1 = { x: 11, y: 11 };
        const containedPoint2 = { x: 20, y: 20 };
        const outPoint1 = { x: 15, y: -15 };
        const outPoint2 = { x: 20, y: 21 };

        expect(Util.containsPoint(containedPoint1, rect)).toBe(true);
        expect(Util.containsPoint(containedPoint2, rect)).toBe(true);
        expect(Util.containsPoint(outPoint1, rect)).not.toBe(true);
        expect(Util.containsPoint(outPoint2, rect)).not.toBe(true);
    });

    // TODO fix something, it keeps getting "undefined is not a constructor" errors
    // it('can test for defined numbers correctly', function() {
    //     const number1 = -2345;
    //     // const number2 = 99999999;
    //     // const number3 = Infinity;
    //     // const nonNumber1 = { x: 19, y: 19 };
    //     // const nonNumber2 = [];
    //     // const nonNumber3 = null;
    //     // const nonNumber4 = 'a';
    //     // const nonNumber5 = false;
    //     // const nonNumber6 = NaN;

    //     expect(Util.isNumberDefined(number1)).toBe(true);
    //     // expect(Util.isNumberDefined(number2)).toBe(true);
    //     // expect(Util.isNumberDefined(number3)).toBe(true);
    //     // expect(Util.isNumberDefined(nonNumber1)).not.toBe(true);
    //     // expect(Util.isNumberDefined(nonNumber2)).not.toBe(true);
    //     // expect(Util.isNumberDefined(nonNumber3)).not.toBe(true);
    //     // expect(Util.isNumberDefined(nonNumber4)).not.toBe(true);
    //     // expect(Util.isNumberDefined(nonNumber5)).not.toBe(true);
    //     // expect(Util.isNumberDefined(nonNumber6)).not.toBe(true);
    // });

    it('can test for defined strings correctly', function() {
        const string1 = '';
        const string2 = 'cvjoxghjkgnbmcdsdfsfgdswtbsfgdfbdsrtgfyjdsuair';
        const nonString1 = { x: 19, y: 19 };
        const nonString2 = undefined;
        const nonString3 = null;
        const nonString4 = Infinity;
        const nonString5 = 837;
        const nonString6 = false;
        const nonString7 = NaN;

        expect(Util.isStringDefined(string1)).toBe(true);
        expect(Util.isStringDefined(string2)).toBe(true);
        expect(Util.isStringDefined(nonString1)).not.toBe(true);
        expect(Util.isStringDefined(nonString2)).not.toBe(true);
        expect(Util.isStringDefined(nonString3)).not.toBe(true);
        expect(Util.isStringDefined(nonString4)).not.toBe(true);
        expect(Util.isStringDefined(nonString5)).not.toBe(true);
        expect(Util.isStringDefined(nonString6)).not.toBe(true);
        expect(Util.isStringDefined(nonString7)).not.toBe(true);
    });

});