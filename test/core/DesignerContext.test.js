/* global describe, expect, it, afterEach */

const DesignerContext = require('DesignerContext');
const fakeEmbroideryAdapter = {
    threadForHex: (value) => `mcp-thread-for-${value}`,
    loadThreadData: () => {}
};

const fakeFontAdapter = {
    fontRepositoryUrl: 'fake'
};

describe('core/DesignerContext', function() {

    afterEach(function() {
        DesignerContext.print = false;
        DesignerContext.embroidery = false;
    });

    it('provides defaults to print technology if the technology doesnt match a defined option', function() {
        DesignerContext.init({
            clients: { embroidery: {} },
            technology: 'something'
        });

        expect(DesignerContext.print).toBe(true);
    });

    it('returns the proper color for a print context', function() {
        DesignerContext.init({
            clients: { embroidery: {} },
            technology: 'print'
        });

        expect(DesignerContext.itemColor('#333333')).toBe('rgb(#333333)');
    });

    it('returns the proper color for a embroidery context', function() {
        DesignerContext.init({
            clients: {
                embroidery: fakeEmbroideryAdapter,
                font: fakeFontAdapter
            },
            technology: 'embroidery'
        });

        expect(DesignerContext.itemColor('#B06A23')).toBe('thread(mcp-thread-for-#B06A23)');
    });

    it('returns a filtered list of colors for embroidery', function() {
        DesignerContext.init({
            clients: {
                embroidery: fakeEmbroideryAdapter,
                font: fakeFontAdapter
            },
            technology: 'embroidery'
        });

        const colors = [
            { hex: '#ffffff' },
            { hex: '#000000' }
        ];

        expect(DesignerContext.filterColors(colors).map(color => color.thread))
            .toEqual(colors.map(color => fakeEmbroideryAdapter.threadForHex(color.hex)));
    });

    it('returns the correct full list of values when filtering colors for print', function() {
        DesignerContext.init({
            clients: { embroidery: {} },
            technology: 'print'
        });

        const colors = [
            { hex: '#ffffff' },
            { hex: '#000000' }
        ];

        expect(DesignerContext.filterColors(colors)).toBe(colors);
    });
});
