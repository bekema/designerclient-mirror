/* global describe, expect, it */
const UploadsUrlBuilder = require('services/upload/uploadsUrlBuilder');

describe('services/upload/uploadsUrlBuilder', function() {
    it('returns the proper value for originalUrl', function() {
        const urlBuilder = new UploadsUrlBuilder({ originalUrlTemplate: 'original-{uploadId}'});

        expect(urlBuilder.getOriginalUrl('testing')).toBe('original-testing');
    });

    it('returns the proper value for previewUrl', function() {
        const urlBuilder = new UploadsUrlBuilder({
            originalUrlTemplate: 'original-{uploadId}',
            previewUrlTemplate: 'preview-{uploadId}'
        });

        expect(urlBuilder.getPreviewUrl('testing')).toBe('preview-testing');
    });

    it('returns the proper value for printUrl', function() {
        const urlBuilder = new UploadsUrlBuilder({
            printUrlTemplate: 'print-{uploadId}'
        });

        expect(urlBuilder.getPrintUrl('testing')).toBe('print-testing');
    });

    it('returns the proper value for thumbnailUrl', function() {
        const urlBuilder = new UploadsUrlBuilder({
            thumbnailUrlTemplate: 'thumb-{uploadId}'
        });

        expect(urlBuilder.getThumbnailUrl('testing')).toBe('thumb-testing');
    });
});
