/* global beforeEach, describe, expect, spyOn, it */
var _ = require('underscore');
var shapeConverter = require('services/uds/converter/shapeConverter');

describe('services/uds/converter/imageConverter', function() {
    it('properly translates the module to the type', function() {
        var shapes = [
            { module: 'Rectangle' },
            { module: 'Ellipse' },
            { module: 'Line' }
        ];

        var converted = shapeConverter.convertToUds(shapes);
        expect(_.map(converted, (item) => item.type)).toEqual(['rectangle', 'ellipse', 'line']);
    });

    it('properly translates the position and color attributes', function() {
        var shape = {
            module: '',
            top: 1,
            left: 2,
            width: 10,
            height: 10,
            fillColor: '#ff33ff',
            strokeWidth: 6,
            strokeColor: '#000000',
            strokeLineCap: 'square',
            strokeLineJoin: 'mitre'
        };
        var converted = shapeConverter.convertToUds([shape]);

        expect(converted[0].position.y).toBe(`${shape.top}mm`);
        expect(converted[0].position.x).toBe(`${shape.left}mm`);
        expect(converted[0].position.width).toBe(`${shape.width}mm`);
        expect(converted[0].position.height).toBe(`${shape.height}mm`);
        expect(converted[0].color).toBe(`rgb(${shape.fillColor})`);
        expect(converted[0].stroke.color).toBe(`rgb(${shape.strokeColor})`);
        expect(converted[0].stroke.thickness).toBe(`${shape.strokeWidth}mm`);
        expect(converted[0].stroke.lineCap).toBe(shape.strokeLineCap);
        expect(converted[0].stroke.lineJoin).toBe(shape.strokeLineJoin);
    });

    it('properly translates the start and end coordinates for line points', function() {
        var shape = {
            module: 'Line',
            width: 10,
            left: 0,
            top: 0
        };

        var converted = shapeConverter.convertToUds([shape]);

        expect(converted[0].start.x).toBe(`${shape.left}mm`);
        expect(converted[0].start.y).toBe(`${shape.top}mm`);
        expect(converted[0].end.x).toBe(`${shape.left + shape.width}mm`);
        expect(converted[0].end.y).toBe(`${shape.top}mm`);
    });

    it('properly gets the zIndex from the shape', function() {
        var shape = {
            module: 'Rectangle',
            zIndex: 10
        };
        var converted = shapeConverter.convertToUds([shape]);

        expect(converted[0].zIndex).toBe(shape.zIndex);
    });

    //TODO add test coverage for convertToDocument
});
