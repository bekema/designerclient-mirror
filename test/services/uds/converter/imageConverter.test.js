/* global beforeEach, describe, expect, spyOn, it */

var imageConverter = require('services/uds/converter/imageConverter');

describe('services/uds/converter/imageConverter', function() {
    it('uses url', function() {
        var image = {
            module: 'UploadedImage',
            left: 0,
            top: 0,
            width: 0,
            height: 0,
            zIndex: 0,
            url: 'http://example.com/image.png/preview',
            crop: {
                top: 0,
                left: 0,
                bottom: 0,
                right: 0
            }
        };

        var converted = imageConverter.convertToUds([image]);

        expect(converted[0].url).toBe(image.url);
    });

    it('uses print url when present', function() {
        var image = {
            module: 'UploadedImage',
            left: 0,
            top: 0,
            width: 0,
            height: 0,
            zIndex: 0,
            url: 'http://example.com/image.png/preview',
            printUrl: 'http://example.com/image.png/print',
            crop: {
                top: 0,
                left: 0,
                bottom: 0,
                right: 0
            }
        };

        var converted = imageConverter.convertToUds([image]);

        expect(converted[0].url).toBe(image.printUrl);
    });

    it('does not add comparison image when previewUrl is missing', function() {
        var image = {
            module: 'UploadedImage',
            left: 0,
            top: 0,
            width: 0,
            height: 0,
            zIndex: 0,
            url: 'http://example.com/image.png/preview',
            crop: {
                top: 0,
                left: 0,
                bottom: 0,
                right: 0
            }
        };

        var converted = imageConverter.convertToUds([image]);

        expect(converted[0].comparisonImages).toBeUndefined();
    });

    it('adds comparison image when previewUrl is present', function() {
        var image = {
            module: 'UploadedImage',
            left: 0,
            top: 0,
            width: 0,
            height: 0,
            zIndex: 0,
            url: 'http://example.com/image.png/preview',
            previewUrl: 'http://example.com/image.png/preview',
            crop: {
                top: 0,
                left: 0,
                bottom: 0,
                right: 0
            }
        };

        var converted = imageConverter.convertToUds([image]);

        expect(converted[0].comparisonImages.length).toBe(1);
        expect(converted[0].comparisonImages[0].url).toBe(image.previewUrl);
    });

    it('uses the crop converter for crops', function() {
        var cropConverter = {
            convertToUds: function(crop) {
                return crop;
            }
        };

        spyOn(cropConverter, 'convertToUds');
        imageConverter.__Rewire__('cropConverter', cropConverter);

        var image = {
            module: 'UploadedImage',
            left: 0,
            top: 0,
            width: 0,
            height: 0,
            zIndex: 0,
            url: 'http://example.com/image.png/preview',
            previewUrl: 'http://example.com/image.png/preview',
            crop: {
                top: 1,
                left: 2,
                bottom: 3,
                right: 4
            }
        };

        imageConverter.convertToUds([image]);

        expect(cropConverter.convertToUds.calls.count()).toBe(1);
        expect(cropConverter.convertToUds).toHaveBeenCalledWith(image.crop);

        imageConverter.__ResetDependency__('cropConverter');
    });

    it('attaches the vpThreadMapping for embroidery items', function() {
        const designerContext = {
            embroidery: true
        };

        imageConverter.__Rewire__('designerContext', designerContext);

        var image = {
            module: 'UploadedImage',
            left: 0,
            top: 0,
            width: 0,
            height: 0,
            zIndex: 0,
            url: 'http://example.com/image.png/preview',
            previewUrl: 'http://example.com/image.png/preview',
            crop: {
                top: 0,
                left: 0,
                bottom: 0,
                right: 0
            }
        };
        var converted = imageConverter.convertToUds([image]);

        expect(converted[0].vpThreadMapping).toBe('http://embroidery.documents.cimpress.io/vbu-mapping');
        imageConverter.__ResetDependency__('designerContext');
    });

    it('attaches color overrides if they are provided', function() {

        const designerContext = {
            embroidery: true
        };

        const fakeColorOverrides = [ {}, {}];

        imageConverter.__Rewire__('designerContext', designerContext);

        var image = {
            module: 'UploadedImage',
            left: 0,
            top: 0,
            width: 0,
            height: 0,
            zIndex: 0,
            url: 'http://example.com/image.png/preview',
            previewUrl: 'http://example.com/image.png/preview',
            crop: {
                top: 0,
                left: 0,
                bottom: 0,
                right: 0
            },
            colorOverrides: fakeColorOverrides
        };
        var converted = imageConverter.convertToUds([image]);

        expect(converted[0].colorOverrides).toBe(fakeColorOverrides);
        imageConverter.__ResetDependency__('designerContext');
    });
});