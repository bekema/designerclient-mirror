/* global describe, expect, it, beforeEach */

const canvasConverter = require('services/uds/converter/canvasConverter');
const DesignerContext = require('DesignerContext');

describe('services/uds/converter/canvasConverter', function() {
    beforeEach(function() {
        DesignerContext.init({
            clients: { },
            technology: 'print'
        });
    });

    it('creates a surface for a canvas', function() {
        const canvas = {
            name: 'canvas 1',
            id: '1',
            width: 800,
            height: 600,
            items: []
        };

        const surfaces = canvasConverter.convertToSurface([canvas]);

        expect(surfaces.length).toBe(1);
    });

    it('creates multiple surfaces for multiple canvases', function() {
        const canvases = [
            {
                name: 'canvas 1',
                width: 800,
                height: 600,
                items: []
            },
            {
                name: 'canvas 2',
                width: 800,
                height: 600,
                items: []
            }
        ];

        const surfaces = canvasConverter.convertToSurface(canvases);

        expect(surfaces.length).toBe(2);
        expect(surfaces[0].name).toBe(canvases[0].name);
        expect(surfaces[1].name).toBe(canvases[1].name);
    });

    it('converts width and height to millimeters', function() {
        const canvas = {
            name: 'canvas 1',
            id: '1',
            width: 800,
            height: 600,
            items: []
        };

        const surfaces = canvasConverter.convertToSurface([canvas]);

        expect(surfaces[0].width).toBe(`${canvas.width}mm`);
        expect(surfaces[0].height).toBe(`${canvas.height}mm`);
    });

    it('converts TextFields to simpleTextFields', function() {
        const canvas = {
            name: 'canvas 1',
            id: '1',
            width: 800,
            height: 600,
            items: [
                {
                    module: 'TextField',
                    left: 0,
                    top: 0,
                    fontFamily: 'Arial',
                    fontSize: 0,
                    innerHTML: 'Test text',
                    zIndex: 0
                }
            ]
        };

        const surfaces = canvasConverter.convertToSurface([canvas]);

        expect(surfaces[0].textAreas.length).toBe(1);
        expect(surfaces[0].images.length).toBe(0);
    });

    it('converts UploadedImages to images', function() {
        const canvas = {
            name: 'canvas 1',
            id: '1',
            width: 800,
            height: 600,
            items: [
                {
                    module: 'UploadedImage',
                    left: 0,
                    top: 0,
                    width: 0,
                    height: 0,
                    zIndex: 1,
                    url: 'http://example.com/image.png',
                    crop: {
                        top: 0,
                        left: 0,
                        bottom: 0,
                        right: 0
                    }
                }
            ]
        };

        const surfaces = canvasConverter.convertToSurface([canvas]);

        expect(surfaces[0].textAreas.length).toBe(0);
        expect(surfaces[0].images.length).toBe(1);
    });
});