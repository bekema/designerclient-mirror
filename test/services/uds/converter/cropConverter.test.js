/* global describe, expect, it, xit */

var cropConverter = require('services/uds/converter/cropConverter');

describe('services/uds/converter/cropConverter', function() {
        
    it('returns null for null input', function() {
        var converted = cropConverter.convertToUds(null);
        
        expect(converted).toBeNull();
    });
    
    it('returns null for undefined input', function() {
        var converted = cropConverter.convertToUds(void 0);
        
        expect(converted).toBeNull()
    });
    
    it('returns null when all crop values are zero', function() {
        var crop = {
            top: 0,
            left: 0,
            bottom: 0,
            right: 0
        };
        
        var converted = cropConverter.convertToUds(crop);
        
        expect(converted).toBeNull();
    });

    it('converts crops to strings', function() {
        var crop = {
            top: 0.1,
            left: 0.01,
            bottom: 0.015,
            right: 0
        };
        
        var converted = cropConverter.convertToUds(crop);
        
        expect(converted.top).toBe('0.1');
        expect(converted.left).toBe('0.01');
        expect(converted.bottom).toBe('0.015');
        expect(converted.right).toBe('0');
    });

    it('converts null and undef to 0', function() {
        var crop = {
            top: 0.1,
            left: 0.1,
            bottom: null
        };
        
        var converted = cropConverter.convertToUds(crop);
        
        expect(converted.bottom).toBe('0');
        expect(converted.right).toBe('0');
    });
});