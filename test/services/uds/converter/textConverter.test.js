/* global describe, expect, it, beforeEach */
/* eslint-disable no-magic-numbers */

const textConverter = require('services/uds/converter/textConverter');
const DesignerContext = require('DesignerContext');

describe('services/uds/converter/textConverter', function() {
    beforeEach(function() {
        DesignerContext.init({
            clients: { },
            technology: 'print'
        });
    });

    it('adds units to fontSize', function() {
        const textField = {
            module: 'TextField',
            left: 0,
            top: 0,
            width: 0,
            height: 0,
            fontFamily: 'Arial',
            fontSize: 12,
            innerHTML: 'Test text',
            zIndex: 0
        };

        const converted = textConverter.convertToUds([textField]);

        expect(converted[0].textFields[0].fontSize).toBe(`${textField.fontSize}pt`);
    });

    it('sets the correct styles for fonts', function() {
        const textField = {
            module: 'TextField',
            horizontalAlignment: 'center',
            left: 10,
            top: 20,
            width: 100,
            height: 100,
            fontFamily: 'Arial',
            fontSize: 0,
            innerHTML: 'Test text',
            zIndex: 0,
            fontBold: true,
            fontUnderline: true,
            fontItalic: true,
            fontStrikeout: true
        };

        const converted = textConverter.convertToUds([textField]);

        expect(converted[0].textFields[0].fontStyle).toMatch(/bold/);
        expect(converted[0].textFields[0].fontStyle).toMatch(/underline/);
        expect(converted[0].textFields[0].fontStyle).toMatch(/italic/);
        expect(converted[0].textFields[0].fontStyle).toMatch(/strikeout/);
        expect(converted[0].textFields[0].fontStyle).not.toMatch(/normal/);

        const textField2 = {
            module: 'TextField',
            horizontalAlignment: 'center',
            left: 10,
            top: 20,
            width: 100,
            height: 100,
            fontFamily: 'Arial',
            fontSize: 0,
            innerHTML: 'Test text',
            zIndex: 0,
            fontBold: false,
            fontUnderline: false,
            fontItalic: false,
            fontStrikeout: false
        };

        const converted2 = textConverter.convertToUds([textField2]);

        expect(converted2[0].textFields[0].fontStyle).toMatch(/normal/);
    });

    it('sets the correct rotation for uds', function() {
        const textField = {
            module: 'TextField',
            horizontalAlignment: 'right',
            left: 10,
            top: 20,
            width: 100,
            height: 100,
            fontFamily: 'Arial',
            fontSize: 0,
            innerHTML: 'Test text',
            zIndex: 0,
            rotation: 145
        };

        const converted = textConverter.convertToUds([textField]);

        expect(converted[0].rotationAngle).toBe(215);
    });
});