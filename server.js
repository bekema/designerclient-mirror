/* eslint-disable */

var http = require('http');
var express = require('express');
var path = require('path');
var webpack = require('webpack');
var webpackMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');
var config = require('./webpack.development.config.js');
var tokenRetriever = require('./server/tokenRetriever');
var app = express();

var port = 8080;
var publicPath = path.resolve(__dirname, 'public');

var compiler = webpack(config);
var middleware = webpackMiddleware(compiler, {
    publicPath: config.output.publicPath,
    stats: {
        colors: true,
        hash: false,
        timings: true,
        chunks: false,
        chunkModules: false,
        modules: false
    }
});

app.use(middleware);
app.use(webpackHotMiddleware(compiler));

// redirecting the old /public to root
app.get('/public', function(req, res) {
    res.redirect(301, '/');
});

// retrieves an authorization token from the auth0 delegation endpoint
app.get('/getAuthToken/:serviceName', function(req, res) {
    var refreshToken = 'T1PYInfE3VVpjktNGn3rJtlxjCti8KTh7FW8pSsXKkejd';
    res.set('Cache-Control', 'no-cache');

    tokenRetriever(refreshToken, req.params.serviceName, function(err, result) {
        if (err) {
            res.status(err.statusCode).json(err);
        }

        res.json(result);
    });
});

app.use(express.static(publicPath));

// And run the server
var server = http.createServer(app);
server.listen(port, function() {
    console.info('Server listening on port %s. Point your browser at http://localhost:%s', port, port);
});
