# Design Client Library (DCL)

A self-contained JavaScript client for designing documents. Out of the box, it will consume and publish to MCP services for all of its needs. Modules can also be replaced to interact with third party services.

See http://corewiki.cimpress.net/wiki/Design_Client_Library

### Building
1. Install node and npm. The 5.x release is preferred.
2. Clone the repository
3. npm install
3. npm run build

### Running locally using webpack dev server
1. Run npm start
2. Get your API refresh token from https://developer.cimpress.io
2. Go to http://localhost:8080/public?refreshToken=<your refresh token here>

### Visual Studio Code integration
Visual Studio Code can be used to edit/lint/build this project. It is also possible to debug node applications, and the .vscode directory has a target set up for debugging webpack in case there are issues with plugins that need to be debugged.

For linting, install the ESLint extension.

To build, press CTRL+Shift+B

### Ownership
* James Anderson
* Joshua Morse