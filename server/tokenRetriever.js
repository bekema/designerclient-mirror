var TokenCache = require('./tokenCache');

var tokenCache = new TokenCache('https://cimpress.auth0.com', 'QkxOvNz4fWRFT6vcq79ylcIuolFz2cwN');

var basicTokens = {
    scene: {
        TokenType: 'Basic',
        IdToken: 'aW50ZXJuYWwtdGVzdC1tZXJjaGFudDppYW1zdXBlcnRlc3Q='
    },
    surfaceSpecifications: {
        TokenType: 'Basic',
        IdToken: 'aW50ZXJuYWwtdGVzdC1tZXJjaGFudDppYW1zdXBlcnRlc3Q='
    }
};

var clientIds = {
    cleanimage: 'M1WTlwEiOHZ0xFIrmztdtlUM47qvqpBC',
    crispify: 'pl3q5J4khaLTaS7rkv73I87TrTlV7N4I',
    digitization: 'kt91dGF8n8t3LnTmhWAmTcUMccVtKrqr',
    preflight: 'UwGwHs2XtJ0v0ZZmVQlqv8aSndynRZNy',
    prepress: 'fNsKhNzIaueThLFPX8Luh3s6ru4yqe5R',
    uds: 'bKcEYMT42Eyg0ynXYqMwfM1jLIlBoHR4'
};

function isBasicToken(serviceName) {
    return !!basicTokens[serviceName];
}

function getAuthToken(refreshToken, serviceName, callback) {
    if (isBasicToken(serviceName)) {
        callback(null, basicTokens[serviceName]);
        return;
    }

    if (!clientIds[serviceName]) {
        callback('Client ID for service ' + serviceName + ' not found');
    }

    var wrapped = function(err, res) {
        if (res) {
            res = {
                ValidTo: res.expiration,
                TokenType: 'Bearer',
                IdToken: res.id_token
            };
        }
        callback(err, res);
    };

    tokenCache.getFromRefreshToken(refreshToken, clientIds[serviceName], wrapped);
}

module.exports = getAuthToken;