var JWT = require('jsonwebtoken');
var NodeCache = require('node-cache');
var URL = require('url');
var RestClient = require('node-rest-client').Client;

/* eslint-disable no-use-before-define */
/* eslint-disable no-magic-numbers */
/* eslint-disable camelcase */

function TokenCache(domain, sourceClientId) {
    this.domain = domain;
    this.sourceClientId = sourceClientId;
    this.tokenExpirationBuffer = 300;
    this.getFromIdToken = function(idToken, callback) {
        getOrLoadToken.call(this, idToken, function(cb) { loadFromIdToken.call(this, idToken, cb); }, callback);
    };
    this.getFromRefreshToken = function(refreshToken, targetClientId, callback) {
        getOrLoadToken.call(this, targetClientId, function(cb) { loadFromRefreshToken.call(this, refreshToken, targetClientId, cb); }, callback);
    };
    this.getFromUsernamePassword = function(username, password, connection, callback) {
        var key = connection + '|' + username;
        getOrLoadToken.call(this, key, function(cb) {
            loadFromUsernamePassword.call(this, username, password, connection, cb);
        }, callback);
    };

    var cache = new NodeCache;
    var restClient = new RestClient;

    function fromUnixTime(unixTime) {
        return new Date(unixTime * 1000);
    }

    function parseExpiration(jwtString, data) {
        if (data.hasOwnProperty('expires_in')) {
            var date = new Date(Date.now() + (parseInt(data.expires_in) * 1000));
            return date;
        }
        var tokenData = JWT.decode(jwtString);
        if (tokenData.hasOwnProperty('exp') && tokenData.exp !== null) {
            return fromUnixTime(parseInt(tokenData.exp.toString()));
        }
        return null;
    }

    function getOrLoadToken(key, loadToken, callback) {
        var value = cache.get(key);
        if (value) {
            callback(null, value);
        } else {
            loadToken.call(this, function(err, tokenObject) {
                if (err) {
                    callback(err);
                    return;
                }

                if (tokenObject.expiration !== null) {
                    cache.set(key, tokenObject, (tokenObject.expiration.getTime() / 1000) - (new Date().getTime() / 1000) - this.tokenExpirationBuffer);
                }
                callback(null, tokenObject);
            });
        }
    }
    function loadFromIdToken(idToken, callback) {
        var tokenData = JWT.decode(idToken);
        var args = {
            data: {
                grant_type: 'urn:ietf:params:oauth:grant-type:jwt-bearer',
                scope: 'openid name email',
                client_id: this.sourceClientId,
                id_token: idToken,
                target: tokenData['aud'],
                api_type: 'app'
            },
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json'
            }
        };
        executeRequest.call(this, 'delegation', args, 1, callback);
    }
    function loadFromRefreshToken(refreshToken, targetClientId, callback) {
        var args = {
            data: {
                grant_type: 'urn:ietf:params:oauth:grant-type:jwt-bearer',
                scope: 'openid name email',
                client_id: this.sourceClientId,
                refresh_token: refreshToken,
                target: targetClientId,
                api_type: 'app'
            },
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json'
            }
        };
        executeRequest.call(this, 'delegation', args, 1, callback);
    }
    function loadFromUsernamePassword(username, password, connection, callback) {
        var args = {
            data: {
                grant_type: 'password',
                scope: 'openid name email',
                client_id: this.sourceClientId,
                username: username,
                password: password,
                connection: connection,
                api_type: 'app'
            },
            headers: {
                'Content-Type': 'application/json',
                'accept': 'application/json'
            }
        };
        executeRequest.call(this, 'oauth/ro', args, 1, callback);
    }
    function executeRequest(route, args, requestCount, callback) {
        var url = URL.resolve(this.domain, route);
        restClient.post(url, args, function(data, response) {
            if (requestCount < 5 && response.statusCode === 429) {
                var delay = 2;
                if (response.headers.hasOwnProperty('X-RateLimit-Reset')) {
                    var resetEpoch = response.headers['X-RateLimit-Reset'];
                    if (resetEpoch) {
                        var newDelay = parseInt(resetEpoch).getTime() - new Date().getTime();
                        if (newDelay > 0) {
                            delay = newDelay / 1000;
                        }
                    }
                    setTimeout(executeRequest.bind(this, route, args, requestCount + 1, callback), delay * 1000);
                    return;
                }
            }
            if (response.statusCode >= 400) {
                callback({
                    message: 'Could not retrieve ID token/JWT for accessing the API',
                    statusCode: response.statusCode,
                    errorDescription: data && data.error_description || response.statusMessage
                });
                return;
            }
            var jwt = data.id_token;
            var expiration = parseExpiration(jwt, data);
            callback(null, {
                id_token: jwt,
                expiration: expiration
            });
        });
    }
}

module.exports = TokenCache;
