var webpack = require('webpack');
var config = require('./webpack.common.config.js');
var hotMiddlewareScript = 'webpack-hot-middleware/client?reload=true';

config.module.loaders.push({ test: /\.scss$/, loaders: ['style', 'css', 'postcss', 'sass'] });

config.entry.dcl.push(hotMiddlewareScript);
config.entry.dclUI.push(hotMiddlewareScript);

config.plugins.push(new webpack.optimize.OccurenceOrderPlugin());
config.plugins.push(new webpack.HotModuleReplacementPlugin());
config.plugins.push(new webpack.NoErrorsPlugin());
config.plugins.push(new webpack.DefinePlugin({
    ADD_TO_GLOBAL: true
}));

module.exports = config;